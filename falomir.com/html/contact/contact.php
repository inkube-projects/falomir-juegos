<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Contacto</title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section class="css-contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="css-oswaldextralight"><?=$this->trans['welcome']?></h2>
                    <p><?=$this->trans['contact_text']?></p><br><br>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div id="gmap"></div>
                    </div>

                    <form id="frm-contact" id="frm-contact" method="post" action="">
                        <div class="col-md-6">
                            <div class="js-alert"></div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="<?=$this->trans['name']?>">
                                        <div class="text-danger" id="name_validate"></div>
                                    </div>

                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="E-mail">
                                        <div class="text-danger" id="email_validate"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="<?=$this->trans['last_name']?>">
                                        <div class="text-danger" id="last_name_validate"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <textarea class="form-control" name="message" id="message" rows="20" placeholder="<?=$this->trans['type_here']?>"></textarea>
                                <div class="text-danger" id="message_validate"></div>
                            </div>
							   <div class="checkbox">
                        <label>
                           <input required="" type="checkbox" name="terms[]" id="terms-contact" value="1"> <?=$this->trans['terms']?>
                        </label>
                        <div class="text-danger" id="terms_validate"></div>
                     </div>
                     <p class="css-terms" style="font-size:11px; color:rgba(127,127,127,1.00); line-height:11px"><?=$this->trans['legal_text']?></p>

                        	 <img src="http://falomir.inkube.net/maqueta/images/google.png" width="260" height="66" alt=""/>
                            <button type="submit" class="btn btn-orange pull-right btn-submit-js"><?=$this->trans['send']?></button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <?php include('html/overall/categories.php'); ?>
        <?php include('html/overall/chat.php'); ?>
        <?php include('html/overall/footer.php'); ?>

        <div class="modal fade" tabindex="-1" role="dialog" id="mod-result">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Aviso</h4>
                    </div>
                    <div class="modal-body">
                        <p class="ajx-message"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <?php include('html/overall/js.php'); ?>
        <!-- Maps -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/gmaps.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-contact.js"></script>
        <script type="text/javascript">
            var scrollVal = 1480;
        </script>
    </body>
</html>
