<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Inicio</title>
        <?php include('html/overall/header.php'); ?>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section id="home-slider">
            <div id="carousel-home" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php foreach ($arr_carousel as $key => $val): $class = ($key == 0) ? "active" : ""; ?>
                        <li data-target="#carousel-home" data-slide-to="<?=$key?>" class="<?=$class?>"></li>
                    <?php endforeach; ?>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php foreach ($arr_carousel as $key => $val): $class = ($key == 0) ? "active" : ""; ?>
                        <div class="item <?=$class?>">
                            <div class="css-slide-image" style="background-color: <?=$val['background_color']?>">
                            <!--<div class="mascaraSlider"></div>---->
                            </div>
                            <div class="carousel-caption">

                                <h2 class="css-oswaldextralight"><?=$val['formed_title']?></h2>
                                <p class="txtSlider"><?=$val['formed_description']?></p>
                                <?php if ($val['url']): ?>
                                    <a href="<?=$val['url']?>" class="btn btn-big btn-yellow css-marginT20 css-uppercase"><?=$this->trans['novelty']?></a>
                                <?php endif; ?>
                                <img src="<?=APP_CAROUSEL.$val['image']?>" class="pull-right" alt="">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>
        <!--/#home-slider-->

        <section id="features">
            <div class="container">
                <div class="row css-novelty-container css-boderB2">
                    <?php foreach ($arr_novelty as $val): ?>
                        <a href="PRODUCTO-DETALLE/<?=$val['id']?>">
                            <div class="col-xs-6 col-sm-6 wow fadeInLeft css-noPadding" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="single-blog">
                                    <div class="single-blog-wrapper">
                                        <div class="post-thumb" style="background-image: url('<?=$val['image']?>')">
                                            <div class="post-overlay css-post-left">
                                                <span class="uppercase">
                                                    <small class="css-oswaldregular"><?=$this->trans['novelty']?></small>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <section class="posts">
            <div class="container">
                <div class="row css-novelty-container">
                    <?php foreach ($arr_news as $key => $val): ?>
                        <?php if ($key == 0): ?>
                            <div class="col-md-8 css-news-image" style="background-image: url('<?=$val['image']?>')">
                                <h3 class="css-fontSize25"><?=$val['title']?></h3>
                                <a href="POST/<?=$val['id']?>" class="css-fontSize16">+ LEER MÁS</a>
                            </div>
                            <div class="col-md-4 css-news-info">
                                <h3><?=$val['title']?></h3>
                                <p><?=$val['description']?></p>
                                <a href="POST/<?=$val['id']?>" class="css-fontSize16 css-text-white">+ LEER MÁS</a>
                            </div>
                        <?php elseif ($key == 1): ?>
                            <div class="col-md-4 css-news-image-thumb" style="background-image: url('<?=$val['image']?>')"></div>
                            <div class="col-md-4 css-news-info" style="height: 390px;">
                                <h3><?=$val['title']?></h3>
                                <p><?=$val['description']?></p>
                                <a href="POST/<?=$val['id']?>" class="css-fontSize16 css-text-white">+ LEER MÁS</a>
                            </div>
                        <?php elseif ($key == 2): ?>
                            <div class="col-md-4 css-news-image-thumb" style="background-image: url('<?=$val['image']?>')"></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>


        <?php include('html/overall/chat.php'); ?>
        <?php include('html/overall/footer.php'); ?>

        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            var scrollVal = 1480;
        </script>
    </body>
</html>
