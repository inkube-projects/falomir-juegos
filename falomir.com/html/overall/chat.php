<section class="css-section-chat">
    <div class="container" style="position: relative;">
    		<div class="row css-paddingR100 css-paddingL100">
                <div class="chatIco">
                    <img src="<?=APP_IMG?>icono_chat.png" width="90" height="86" alt=""/>
                </div>
                <!--<i class="chatIco fa fa-commenting-o css-fontSize56 pull-right css-text-white" aria-hidden="true"></i>-->
                <!--<span class="chatIco fa-stack css-fontSize56" style="text-rendering:geometricPrecision;">
                <i class="fa fa-circle fa-stack-1x" style="color:#4bbd34; text-align:center;"></i>
                <i class="fa fa-commenting-o fa-stack-1x fa-inverse"></i>
                </span>-->
              <!-- <h2 class="css-oswaldextralight css-text-white pull-right text-right">¿EN QUÉ <br> PODEMOS <br> AYUDARTE?</h2> -->
              <h2 class="css-help pull-right text-right"><?=$this->trans['how_can_we_help_you']?></h2>
            </div>
       </div>
</section>

<div class="col-md-3 css-chat-box css-hide" style="z-index:9999;">
    <div class="box box-success direct-chat direct-chat-success collapsed-box css-noMargin">
        <div class="box-header with-border">
            <h3 class="box-title">Contacto</h3>

            <div class="box-tools pull-right">
                <span data-toggle="tooltip" title="3 mesnajes nuevos" class="badge bg-green">3</span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" id="js-collapse"><i class="fa fa-plus"></i></button>

                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                    <i class="fa fa-comments"></i>
                </button>

                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
            </div>
        </div>

        <div class="box-body">
            <div class="direct-chat-messages" id="direct-chat-messages">
                <?php
                    // unset($_SESSION['FALOMIR_CHAT_EMAIL']);
                ?>
                <?php if (isset($_SESSION['FALOMIR_CHAT_EMAIL'])): ?>
                    <div class="direct-chat-msg">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">Falomir</span>
                            <span class="direct-chat-timestamp pull-right">23 Ene 2:00 pm</span>
                        </div>

                        <img class="direct-chat-img" src="<?=APP_NO_IMAGES?>profile.jpg" alt="Message User Image">

                        <div class="direct-chat-text">
                            Hola <?=$_SESSION['FALOMIR_CHAT_EMAIL']['username']?> escribe tu duda y te responderemos lo antes posible.
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-lg-12">
                        <form id="frm-chat" name="frm-chat" method="post" action="">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-submit-js" type="submit"><i class="fa fa-check"></i></button>
                                </span>
                                <input type="text" class="form-control" name="chat_user_name" id="chat_user_name" placeholder="Ingresa tu nombre">
                            </div>
                            <div class="text-danger" id="chat_user_name_validate"></div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>

            <div class="direct-chat-contacts">
                <ul class="contacts-list">
                    <li>
                        <a href="#">
                            <img class="contacts-list-img" src="<?=APP_IMG?>chat/user1.jpg" alt="User Image">

                            <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                    Servicio al cliente
                                    <small class="contacts-list-date pull-right">02/28/2015</small>
                                </span>
                                <span class="contacts-list-msg">Esto es un mensaje ...</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <?php if (isset($_SESSION['FALOMIR_CHAT_EMAIL'])): ?>
            <div class="box-footer">
                <form action="#" method="post">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Escribe tu mensaje" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-success btn-flat">Enviar</button>
                        </span>
                    </div>
                </form>
            </div>
        <?php endif; ?>
    </div>
</div>
