<script type="text/javascript" src="<?=APP_ASSETS?>js/jquery.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>js/lightbox.min.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>js/wow.min.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>plugins/slick-1.8.0/slick/slick.min.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>js/main.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>js/app.js"></script>
<!-- Paths JS -->
<script type="text/javascript" src="<?=APP_ASSETS?>js/__paths.js"></script>
<!-- jQuery validation -->
<script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
<script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
