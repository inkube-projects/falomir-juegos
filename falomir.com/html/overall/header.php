<link rel="shortcut icon" href="<?=APP_FAVICONS?>favicon.ico">
<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=APP_FAVICONS?>apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=APP_FAVICONS?>apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=APP_FAVICONS?>apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?=APP_FAVICONS?>apple-touch-icon-57-precomposed.png"> -->

<link href="<?=APP_ASSETS?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>plugins/slick-1.8.0/slick/slick.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>plugins/slick-1.8.0/slick/slick-theme.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>css/animate.min.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>css/lightbox.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>css/main.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>css/responsive.css" rel="stylesheet">
<link href="<?=APP_ASSETS?>css/css-style.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="<?=APP_ASSETS?>js/html5shiv.js"></script>
    <script src="<?=APP_ASSETS?>js/respond.min.js"></script>
<![endif]-->
