<div class="css-categories-nav css-show-desktop">
    <div class="container">
        <ul class="css-noPadding">
            <li class="css-uppercase"><a href="PRODUCTOS/C/12"><?=$this->trans['magic']?> <i class="fa fa-magic"></i></a></li>
            <li class="css-uppercase"><a href="PRODUCTOS/C/14"><?=$this->trans['artistic']?> <i class="fa fa-paint-brush"></i></a></li>
            <li class="css-uppercase"><a href="PRODUCTOS/C/7"><?=$this->trans['hability']?> <i class="fa fa-bullseye"></i></a></li>
            <li class="css-uppercase"><a href="PRODUCTOS/C/15"><?=$this->trans['table_games']?> <i class="fa fa-cube"></i></a></li>
            <li class="css-uppercase"><a href="PRODUCTOS/C/13"><?=$this->trans['classics']?> <i class="fa fa-puzzle-piece"></i></a></li>
        </ul>
    </div>
</div>
