<title>#</title>
<footer id="footer">
    <div class="container">
        <img src="<?=APP_IMG?>logo-gray.png" class="center-block" width="100" alt="">
        <h2 class="text-center css-text-gray css-fontSize17 css-uppercase"><a href=""><?=$this->trans['legal_note']?> </a></h2>
        <p class="text-center css-text-gray">Copyright 2018 - Juguetes Falomir, S.A - <a href=""> <?=mb_convert_case($this->trans['privacy_policy'], MB_CASE_TITLE, "UTF-8")?></a> <?=$this->trans['and']?> <a href=""> Cookies.</a></p>
    </div>

    <div class="css-nav-networks">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                    <div class="social-icons">
                        <ul class="nav nav-pills" style="margin-top: 7px; display: flex; justify-content: center;">
                            <li><a href="https://www.facebook.com/falomirjuegos" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/falomirjuegos"><i class="fa fa-twitter"  target="_blank"></i></a></li>
                             <li><a href="https://www.instagram.com/falomirjuegos/"><i class="fa fa-instagram"  target="_blank"></i></a></li>
                            <li><a href="https://www.pinterest.es/falomirjuegos/"><i class="fa fa-pinterest-p"  target="_blank"></i></a></li>
                            <li><a href="https://plus.google.com/+Falomirjuegosplus"><i class="fa fa-google-plus"  target="_blank"></i></a></li>
                            <li><a href="https://www.youtube.com/user/wwwfalomirjuegoscom"  target="_blank"><i class="fa fa-youtube-play"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
