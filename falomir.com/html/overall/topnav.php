<header id="header" class="css-paddingT0">
    <div class="css-nav-networks">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                    <div class="social-icons pull-left">
                        <ul class="nav nav-pills">
                            <li><a href="https://www.facebook.com/falomirjuegos" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/falomirjuegos"><i class="fa fa-twitter"  target="_blank"></i></a></li>
                             <li><a href="https://www.instagram.com/falomirjuegos/"><i class="fa fa-instagram"  target="_blank"></i></a></li>
                            <li><a href="https://www.pinterest.es/falomirjuegos/"><i class="fa fa-pinterest-p"  target="_blank"></i></a></li>
                            <li><a href="https://plus.google.com/+Falomirjuegosplus"><i class="fa fa-google-plus"  target="_blank"></i></a></li>
                            <li><a href="https://www.youtube.com/user/wwwfalomirjuegoscom"  target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-inverse" role="banner" id="main-navbar">
        <div class="container">
            <div class="css-language pull-right">
                <ul class="nav nav-pills">
                    <?php $class = ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") ? "active" : ""; ?>
                    <li class="<?=$class?>"><a href="#" class="lang" data-lang="ES">ES</a></li>
                    <?php $class = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "active" : ""; ?>
                    <li class="<?=$class?>"><a href="#" class="lang" data-lang="EN">EN</a></li>
                </ul>
            </div>

            <div class="navbar-header css-nav-responsive">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="HOME">
                    <h1><img src="<?=APP_IMG?>logo2.png" alt="logo" width="99"></h1>
                </a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right css-oswaldextralight">
                    <?php if ($this->user_view == "Index") { $class = "active"; } else { $class = ""; } ?>
                    <li class="<?=$class?>"><a href="HOME"><?=$this->trans['home']?></a></li>

                    <?php if ($this->user_view == "Products") { $class = "active"; } else { $class = ""; } ?>
                    <li class="<?=$class?>"><a href="NUESTROS-JUEGOS"><?=$this->trans['our_games']?></a></li>

                    <?php if ($this->user_view == "News") { $class = "active"; } else { $class = ""; } ?>
                    <li class="<?=$class?>"><a href="BLOG"><?=$this->trans['blog']?></a></li>

                    <?php if ($this->user_view == "Contact") { $class = "active"; } else { $class = ""; } ?>
                    <li class="<?=$class?>"><a href="CONTACTO"><?=$this->trans['contact']?></a></li>

                    <!-- <li><a href="#">ÁREA DE CLIENTES</a></li> -->
                </ul>
            </div>
            <!-- <div class="search">
                <form role="form">
                <i class="fa fa-search"></i>
                <div class="field-toggle">
                <input type="text" class="search-form" autocomplete="off" placeholder="Buscar">
                </div>
                </form>
            </div> -->
        </div>
    </div>
</header>
