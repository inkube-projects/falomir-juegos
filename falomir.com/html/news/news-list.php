<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Noticias</title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section class="css-section-new">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 css-new-left-panel">
                        <h2 class="css-uppercase"><?=$arr_p_news[0]['title'.$ext]?></h2>
                        <p><?=substr($arr_p_news[0]['description'.$ext],0, 300)?></p>
                        <a href="POST/<?=$arr_p_news[0]['id']?>" class="btn btn-yellow"><?=$this->trans['see_post']?></a>
                    </div>

                    <div class="col-md-8 css-new-image-panel" style="background-image: url('<?=$p_image?>')"></div>
                </div>
            </div>
        </section>

        <section class="css-section-new-body" style="background-color: #e8eef3;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 css-noPadding">
                        <?php foreach ($arr_news as $key => $val): ?>
                            <?php if ($key == 0): ?>
                                <div class="css-news-panel-1">
                                    <div class="css-image" style="background-image: url('<?=$val['image']?>')">
                                        <a href="POST/<?=$val['id']?>">
                                            <div class="css-new-description">
                                                <h3 class="css-uppercase"><?=$val['title']?></h3>

                                                <p class="css-justify"><?=substr($val['description'],0,150)?></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="css-news-panel-1-footer">
                                    <div class="social-icons">
                                        <ul class="nav nav-pills pull-right" style="margin-top: 0; display: flex; justify-content: center;">
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php else: ?>
                                <!-- ///////////////////////////////////////////////////////////////// -->

                                <div class="col-md-6 css-paddingL0 css-paddinR20 css-no-padding-mobile">
                                    <div class="css-news-panel-1 css-news-panel-2">
                                        <div class="css-image" style="background-image: url('<?=$val['image']?>')">
                                            <a href="POST/<?=$val['id']?>">
                                                <div class="css-new-description">
                                                    <h3 class="css-uppercase"><?=$val['title']?></h3>

                                                    <p class="css-justify"><?=substr($val['description'],0,100)?></p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <div class="clearfix css-marginB10"></div>

                        <?=$index_pages?>
                    </div>

                    <div class="col-md-4">
                        <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;"><?=$this->trans['search']?></a>
                        <input type="text" class="form-control css-input-search css-marginB80">

                        <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20 css-fontSize18" style="width: 100%;"><?=$this->trans['recent_entries']?></a>

                        <ul class="css-ul-recent-entries">
                            <?php foreach ($arr_recents as $val): ?>
                                <li><a href="POST/<?=$val['id']?>"><?=$val['title'.$ext]?></a></li>
                            <?php endforeach; ?>
                        </ul>

                        <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20 css-fontSize18" style="width: 100%;"><?=$this->trans['categories']?></a>

                        <ul class="css-ul-recent-entries">
                            <li><a href="BLOG"><?=$this->trans['all_posts']?></a></li>
                            <?php foreach ($arr_category as $val): ?>
                                <li><a href="BLOG/C/<?=$val['id']?>"><?=$val['name'.$ext]?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <?php include('html/overall/categories.php'); ?>
        <?php include('html/overall/chat.php'); ?>
        <?php include('html/overall/footer.php'); ?>

        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            var scrollVal = 1480;
        </script>
    </body>
</html>
