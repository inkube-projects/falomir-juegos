<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Listado de Juguetes</title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-post-image {
                background-position: center;
            }
            #home-slider .slide-text {
                margin-left: 100px;
            }
            #home-slider .slide-img {
                position: absolute;
                top: 0;
                right: 0;
                width: 340px;
                margin-right: 100px;
            }

            .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
                display: block;
                max-width: 100%;
                height: auto;
            }

			.css-ul-related {
                float: right;
                width: 100%;
            }

            .css-ul-related li {
                float: left;
                margin-left: 40px;
                height: 180px;
                width: 120px;
            }

            .css-ul-related p {
                color: #575756;
                font-size: 12px;
                bottom: 0 !important;
                position: absolute;
                width: 120px;
                line-height: 16px;
            }

            .imgMinG{
                border: solid 1px #DEDEDE;
                margin: 5px;
                float:right;
            }

            @media (max-width: 768px) {
                #home-slider .slide-text {
                    top: 37px;
                }
            }
        </style>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section id="home-slider" class="css-products-banner" style="background-color: <?=$background_color?>;">
            <div class="container">
                <div class="">
                    <div class="main-slider">
                        <div class="slide-text">
                            <h1 class="css-marginT30 css-oswaldextralight css-uppercase"><?=$arr_cat[0]['name'.$ext]?></h1>
                            <p class="css-justify hidden-xs"><?=$arr_cat[0]['description'.$ext]?></p>
                        </div>

                        <div class="slide-img hidden-xs">
                            <img src="<?=$banner?>" class="css-banner" alt="slider image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>

        <section class="css-products-list">
            <div class="container">
                <div class="col-md-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="100ms">
                    <a href="javascript:history.back()"><i class="fa fa-chevron-circle-left css-text-red css-fontSize21"></i></a>
                </div>

                <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="200ms">
                    <p class="css-text-gray css-fontSize12"><b><?=$arr_cat[0]['name'.$ext]?></b> / <?=$product['product_name']?></p>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <!-- <a href="FICHA-PRODUCTO/<?=$id?>"> -->
                    <a href="PRODUCTO-PDF/<?=$id?>" target="_blank">
                        <img src="<?=APP_IMG_PUBLIC?>ico/download-icon.png" class="pull-right" alt="">
                        <div class="pull-right css-fontSize12 text-right css-text-red css-marginR10 css-line-height-1 css-paddingT10">
                            <?=$this->trans['download_pdf']?>
                        </div>
                    </a>
                </div>

                <div class="clearfix css-marginB20"></div>
                <?php if ($arr_product[0]['video']): ?><div class="embed-responsive embed-responsive-16by9" style="margin-bottom:40PX;"> <?=$arr_product[0]['video']?></div> <?php endif; ?>
                <?php if ($product_banner): ?><div class="css-product-banner" style="background-image: url('<?=$product_banner?>')"></div> <?php endif; ?>

                <!--BLOQUE 1 DE PRECIO-->
                <div class="css-product-panel wow fadeInUp css-marginB0" data-wow-duration="1000ms" data-wow-delay="400ms">
                    <div class="col-md-5 css-paddingL0">
                        <div style="width:100%; float:left;">
                            <?php if ($product['product_logo']): ?>
                                <img src="<?=$logo_1?>" class="pull-left img-responsive" alt=""><br>
                            <?php endif; ?>
                        </div>
                        <p>
                            <b class="css-saira_semicondensedbold" style="text-transform: uppercase;"><?=$product['product_name']?></b> <br>
                            <b  class="css-saira_semicondensedbold"><?=$this->trans['age']?>:</b> <?=$product['age_range']?> <br>
                            <b  class="css-saira_semicondensedbold">Ref.:</b> <?=$product['reference']?>
                        </p>

                        <p class="css-justify"><?=$product['description_1'.$ext]?></p>

                        <p>
                            <b  class="css-saira_semicondensedbold"><?=$this->trans['measurements']?>:</b> <?=$product['measurement']?> <br>
                            <b  class="css-saira_semicondensedbold"><?=$this->trans['language']?>:</b>
                            <?php foreach ($arr_lang as $key => $val): ?>
                                <?php if ($key == 0): ?>
                                    <?=$val['name']?>
                                <?php else: ?>
                                    - <?=$val['name']?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <br>
                            <b class="css-saira_semicondensedbold"><?=$this->trans['content']?>:</b> <?=$product['content'.$ext]?>
                        </p>
                        <br>
                        <p><?=$product['description_2'.$ext]?></p>
                    </div>

                    <div class="col-md-7 css-paddingR0">
                        <?php if ($product['image_1']): ?>
                            <img src="<?=$image_1?>" class="pull-right css-product-image-detail img-responsive js-image" alt="">
                        <?php endif; ?>

                        <div class="clearfix"></div>

                        <div class="row ">
                            <?php $cnt = 0; foreach ($arr_gallery as $key => $val): ?>
                                <?php if ($val): $cnt++; ?>
                                    <div class="col-md-3 imgMinG" >
                                        <?php if ($cnt == 1) { $class = "css-active"; } else { $class = ""; }  ?>
                                        <div class="css-gallery-image <?=$class?>" style="background-image: url('<?=$val?>')" data-image="<?=$val?>"></div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5568a28f0e0b16ed"></script>

                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox" style="float:right !important; margin-top:30px;"></div>
                    </div>
                </div>
                <!--END. BLOQUE 1 PRODUCTO-->
                <div class="clearfix css-marginB30"></div>

                <hr class="css-hr css-marginB40">

                <div class="col-md-4">
                    <a href="#collapse-sellers" class="btn btn-warning btn-big css-oswaldextralight css-marginB20" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-sellers">
                        <?=$this->trans['our_games']?> <br>
                        <b class="css-saira_semicondensedbold"><?=$this->trans['best_seller']?></b>
                    </a>
                </div>

                <div class="col-md-8">
                    <div class="collapse" id="collapse-sellers">
                        <div class="css-close"><i class="fa fa-times"></i></div>

                        <div class="well css-scrollable">
                            <div class="row css-marginB20">
                                <?php foreach ($arr_best_seller as $val): ?>
                                    <div class="col-md-4 css-container-panel">
                                        <div class="css-seller-panel">
                                            <div class="css-seller-body">
                                                <!--<div class="css-seller-img" style="background-image: url('<//?=$val['image']?>')"></div>-->
                                                <div class="css-seller-img"><img src=<?=$val['image']?> alt="<?=$val['name']?> " class="img-responsive"></div>
                                            </div>

                                            <div class="css-seller-footer">
                                                <p class="pull-left css-marginT10 css-marginL10 css-uppercase" style="width:100%;"><?=$val['name']?></p><br>
                                                <a href="PRODUCTO-DETALLE/<?=$val['id']?>" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                    <ul class="css-ul-related wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <li>
                            <?=$this->trans['interested']?> <i class="fa fa-angle-right "></i>
                        </li>
                        <?php foreach ($arr_related as $val): ?>
                            <li>
                                <a href="PRODUCTO-DETALLE/<?=$val['id']?>">
                                    <img src="<?=$val['image']?>" class="css-noFloat center-block" alt="">
                                    <p class="css-uppercase"><?=$val['name']?></p>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="clearfix css-margin10"></div>
            </div>
        </section>

        <?php include('html/overall/categories.php'); ?>
        <?php include('html/overall/chat.php'); ?>
        <?php include('html/overall/footer.php'); ?>
        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            var scrollVal = 1480;

            $(function(e){
                $(".css-gallery-image").on('click', function(e){
                    var image = $(this).data('image');

                    $('.js-image').attr('src', image);
                    $(".css-gallery-image").removeClass("css-active");
                    $(this).addClass("css-active");
                });

                $('.css-close').on('click', function(e){
                    $('#collapse-sellers').collapse('hide')
                })
            });
        </script>
    </body>
</html>
