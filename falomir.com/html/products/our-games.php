<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Listado de Juguetes</title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section id="home-slider">
            <div id="carousel-home" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php if (count($arr_carousel) > 1): ?>
                        <?php foreach ($arr_carousel as $key => $val): $class = ($key == 0) ? "active" : ""; ?>
                            <li data-target="#carousel-home" data-slide-to="<?=$key?>" class="<?=$class?>"></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php foreach ($arr_carousel as $key => $val): $class = ($key == 0) ? "active" : ""; ?>
                        <div class="item <?=$class?>">
                            <div class="css-slide-image" style="background-color: <?=$val['background_color']?>"></div>
                            <div class="carousel-caption">
                                <h2 class="css-oswaldextralight css-uppercase"><?=$this->trans['classics']?></h2>
                                <p class="txtSlider"><?=$this->trans['classics_text_banner']?></p>
                                 <a href=" " class="btn btn-big btn-purple css-uppercase"><?=$this->trans['classics_games']?></a>
                                <img src="<?=APP_CAROUSEL.$val['image']?>" class="pull-right" alt="">
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php if (count($arr_carousel) > 1): ?>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                <?php endif; ?>
            </div>

            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>
        <!--/#home-slider-->

        <section class="posts">
            <div class="container">
                <div class="row css-novelty-container">
                    <?php foreach ($arr_cat as $key => $val): ?>

                        <?php if ($key == 0): ?>
                            <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                                <div class="folio-item" onclick="overlay()" data-wow-duration="1000ms" data-wow-delay="300ms" >
                                    <div class="folio-image css-post-image" style="background-image: url('<?=$val['image']?>');">

                                        <div class="css-cat-title-left-yellow css-uppercase">
                                            <?=$val['name']?>
                                        </div>
                                    </div>
                                    <div class="overlay css-overlay-yellow">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <div class="folio-info">
                                                    <div class="css-overlay-title-left css-uppercase"><?=$val['name']?></div>
                                                    <h3 class="css-uppercase"><?=$val['name']?></h3>
                                                    <p class="css-justify"><?=$val['description']?></p>
                                                    <div class="folio-overview">
                                                        <span class="folio-link css-left"><a class="folio-read-more" href="PRODUCTOS/C/<?=$val['id']?>" data-single_url="#">+ <?=$this->trans['see_more']?></a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php elseif($key == 1): ?>
                            <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                                <div class="folio-item" onclick="overlay()" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="folio-image css-post-image" style="background-image: url('<?=$val['image']?>');">
                                        <!-- <img class="img-responsive" src="images/posts/1.jpg" alt=""> -->
                                        <div class="css-cat-title-right-green css-uppercase">
                                            <?=$val['name']?>
                                        </div>
                                    </div>

                                    <div class="overlay css-overlay-green">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <div class="folio-info">
                                                    <div class="css-overlay-title-right css-uppercase"><?=$val['name']?></div>
                                                    <h3 class="css-uppercase"><?=$val['name']?></h3>
                                                    <p class="css-justify"><?=$val['description']?></p>
                                                    <div class="folio-overview">
                                                        <span class="folio-link css-left"><a class="folio-read-more" href="PRODUCTOS/C/<?=$val['id']?>" data-single_url="#">+ <?=$this->trans['see_more']?></a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php elseif($key == 2): ?>
                            <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                                <div class="folio-item" onclick="overlay()" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="folio-image css-post-image" style="background-image: url('<?=$val['image']?>');">
                                        <div class="css-cat-title-left-blue css-uppercase">
                                            <?=$val['name']?>
                                        </div>
                                    </div>
                                    <div class="overlay css-overlay-blue">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <div class="folio-info">
                                                    <div class="css-overlay-title-left css-uppercase"><?=$val['name']?></div>
                                                    <h3 class="css-uppercase"><?=$val['name']?></h3>
                                                    <p class="css-justify"><?=$val['description']?></p>
                                                    <div class="folio-overview">
                                                        <span class="folio-link css-left"><a class="folio-read-more" href="PRODUCTOS/C/<?=$val['id']?>" data-single_url="#">+ <?=$this->trans['see_more']?></a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php elseif($key == 3): ?>
                            <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                                <div class="folio-item" onclick="overlay()" data-wow-duration="1000ms" data-wow-delay="300ms">
                                    <div class="folio-image css-post-image" style="background-image: url('<?=$val['image']?>');">
                                        <div class="css-cat-title-right-red css-uppercase">
                                            <?=$val['name']?>
                                        </div>
                                    </div>
                                    <div class="overlay css-overlay-red">
                                        <div class="overlay-content">
                                            <div class="overlay-text">
                                                <div class="folio-info">
                                                    <div class="css-overlay-title-right css-uppercase"><?=$val['name']?></div>
                                                    <h3 class="css-uppercase"><?=$val['name']?></h3>
                                                    <p class="css-justify"><?=$val['description']?></p>
                                                    <div class="folio-overview">
                                                        <span class="folio-link css-left"><a class="folio-read-more" href="PRODUCTOS/C/<?=$val['id']?>" data-single_url="#">+ <?=$this->trans['see_more']?></a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <?php include('html/overall/categories.php'); ?>
        <div style="position: relative">
            <?php include('html/overall/chat.php'); ?>
            <?php include('html/overall/footer.php'); ?>
        </div>

        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            var scrollVal = 1480;
        </script>
        <script type="text/javascript">
            function mostrar(){ document.getElementById('overlay').style.opacity = '1';}
        </script>
    </body>
</html>
