<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-banner { margin-right: 97px; }
            .css-product-image-detail { height: 250px; }
            .css-product-panel { height: 260px; }
            #home-slider .banner-text {
                position: absolute;
                top: 50px;
                left: 0;
                width: 450px;
                margin-left: 100px;
            }
        </style>
    </head>

    <body>
        <div class="css-invoice-container">
            <img src="<?=APP_IMG?>logo.png" alt="" class="center-block css-marginB20">

            <div class="css-pdf-body">
                <div id="home-slider" class="css-products-banner" style="background-color: <?=$background_color?>;">
                    <div class="">
                        <div class="main-slider">
                            <div class="banner-text">
                                <h1 class="css-marginT30 css-oswaldextralight css-uppercase"><?=$arr_cat[0]['name'.$ext]?></h1>
                                <p class="css-justify hidden-xs"><?=$arr_cat[0]['description'.$ext]?></p>
                            </div>

                            <div class="slide-img hidden-xs">
                                <img src="<?=$banner?>" class="css-banner" alt="slider image">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="css-paddingL20 css-paddingR20">
                    <div class="col-md-8 text-center css-noFloat center-block">
                        <p class="css-text-gray css-fontSize12"><b><?=$arr_cat[0]['name'.$ext]?></b> / <?=$product['product_name']?></p>
                    </div>

                    <div class="clearfix css-marginB20"></div>
                    <?php if ($product_banner): ?><div class="css-product-banner" style="background-image: url('<?=$product_banner?>')"></div> <?php endif; ?>

                    <!--BLOQUE 1 DE PRECIO-->
                    <div class="css-product-panel css-marginB0">
                        <div class="col-md-5 css-paddingL0">
                            <div style="width:100%; float:left;">
                                <?php if ($product['product_logo']): ?>
                                    <img src="<?=$logo_1?>" class="pull-left img-responsive" alt=""><br>
                                <?php endif; ?>
                            </div>
                            <p>
                                <b class="css-saira_semicondensedbold" style="text-transform: uppercase;"><?=$product['product_name']?></b> <br>
                                <b  class="css-saira_semicondensedbold"><?=$this->trans['age']?>:</b> <?=$product['age_range']?> <br>
                                <b  class="css-saira_semicondensedbold">Ref.:</b> <?=$product['reference']?>
                            </p>

                            <p class="css-justify"><?=$product['description_1'.$ext]?></p>

                            <p>
                                <b  class="css-saira_semicondensedbold"><?=$this->trans['measurements']?>:</b> <?=$product['measurement']?> <br>
                                <b  class="css-saira_semicondensedbold"><?=$this->trans['language']?>:</b>
                                <?php foreach ($arr_lang as $key => $val): ?>
                                    <?php if ($key == 0): ?>
                                        <?=$val['name']?>
                                    <?php else: ?>
                                        - <?=$val['name']?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <br>
                                <b class="css-saira_semicondensedbold"><?=$this->trans['content']?>:</b> <?=$product['content'.$ext]?>
                            </p>
                            <br>
                            <p><?=$product['description_2'.$ext]?></p>
                        </div>

                        <div class="col-md-7 css-paddingR0">
                            <?php if ($product['image_1']): ?>
                                <img src="<?=$image_1?>" class="pull-right css-product-image-detail img-responsive js-image" alt="">
                            <?php endif; ?>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="row">
                        <?php $cnt = 0; foreach ($arr_gallery as $key => $val): ?>
                            <?php if ($val): $cnt++; ?>
                                <div class="col-md-3">
                                    <?php if ($cnt == 1) { $class = "css-active"; } else { $class = ""; }  ?>
                                    <div class="css-gallery-image <?=$class?>" style="background-image: url('<?=$val?>')" data-image="<?=$val?>"></div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="css-btn-container">
                <button type="button" class="btn btn-info btn-flat pull-right" onclick="genPDF()">Descargar como pdf <i class="fas fa-file-pdf"></i></button>
            </div>
        </div>
    </body>
    <!-- JS Global -->
    <?php include('html/overall/js.php'); ?>
    <!-- html2canvas -->
    <script src="<?=APP_ASSETS?>plugins/html2canvas/html2canvas.js"></script>
    <script src="<?=APP_ASSETS?>plugins/canvas2image/canvas2image.js"></script>
    <!-- jsPDF -->
    <script src="<?=APP_ASSETS?>plugins/jsPDF-master/dist/jspdf.min.js"></script>
    <!-- Custom JS -->
    <script src="<?=APP_ASSETS?>js/js-product-pdf.js" type="text/javascript"></script>
</html>
