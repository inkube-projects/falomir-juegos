<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Listado de Juguetes</title>
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            .css-post-image {
                background-position: center;
            }
            #home-slider .slide-text {
                margin-left: 100px;
            }
            #home-slider .slide-img {
                position: absolute;
                top: 0;
                right: 0;
                width: 340px;
                margin-right: 100px;
            }
            .floatNone{ float: inherit;}

			.css-categoris-M{ display:none;}

            @media (max-width: 768px) {

				.css-products  {
					width:50%;
					float:left;
                    text-align: center;
                }

                .css-products .col-xs-4 {
                    width: 100%;
                }

				.css-categoris-M{ display: block;    margin: 15px; }

				#home-slider .slide-text {
                    top: 0px;
                    margin-left: 0 !important;
                    text-align: left;
                }

                #home-slider .slide-img {
                    position: absolute;
                    top: 45px;
                    right: 0;
                    width: 300px;
                    margin-right: 10px;
                }

                .css-banner {
                    height: 250px;
                    width: auto;
                    float: right;
                    /* margin-right: 140px; */
                }

                .css-products-list {
                    padding: 10px 0px;
                }

                .floatNone{ float:none !important;}
            }

            @media (max-width: 400px) {
                .css-products  {
					width:100%;
					float:left;
                    text-align: center;
                }
            }

            .css-marginL100 {
                margin-left: 15px!important;
                margin: 17px 15px;
            }

            .css-noPadding {
                padding-right: 15px;
                padding-left: 15px;
            }
            .css-txt-Tit {
                margin-top: 12px;
                text-transform: uppercase;
                padding: 0 15px 30px 15px;
            }
            .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
                display: block;
                max-width: 280px;
                height: auto;
                float: none;
                margin: 0 auto;
            }
            .css-marginB20 {
                margin-bottom: 0px!important;
            }

            .css-search {
                margin-right: 13px;
            }
        }
        </style>
    </head><!--/head-->

    <body>
        <?php include('html/overall/topnav.php'); ?>
        <!--/#header-->

        <section id="home-slider" class="css-products-banner" style="background-color: <?=$arr_p_cat[0]['background_color']?>;">
            <div class="container">
                <div class="">
                    <div class="main-slider">
                        <?php if (isset($_GET['cat'])): ?>
                            <div class="slide-text">
                                <h1 class="css-marginT30 css-oswaldextralight css-uppercase"><?=$arr_p_cat[0]['name']?></h1>
                                <p class="css-justify hidden-xs css-r-hidden"><?=$arr_p_cat[0]['description']?></p>
                            </div>
                        <?php else: ?>
                            <div class="slide-text">
                                <h1 class="css-marginT30 css-oswaldextralight">NUESTROS JUEGOS</h1>
                                <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et ligula gravida, accumsan est sed, porttitor mi. In efficitur cursus ullamcorper. Curabitur libero est, varius in eleifend id, maximus in quam.</p>
                            </div>
                        <?php endif; ?>
                        <div class="slide-img hidden-xs">
                       		<img src="<?=APP_PRODUCTS_CATEGORY_IMAGE.$arr_p_cat[0]['banner']?>" class="css-banner" alt="slider image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>

        <section class="css-products-list">
            <div class="container css-paddingL100M">
                <div class="row">

                    <div class="col-xs-12 col-md-8 pull-right">
                        <div class="col-xs-6 col-md-6">
                            <?php if ($cat != 0 && $cat != ""): ?>
                                <p class="css-text-gray css-fontSize12"><?=mb_convert_case($arr_p_cat[0]['name'.$ext], MB_CASE_UPPER, "UTF-8")?></p>
                            <?php endif; ?>
                        </div>

                        <div class="col-md-6 pull-right floatNone">
                            <img src="<?=APP_IMG_PUBLIC?>ico/download-icon.png" class="pull-right" alt="">
                            <div class="pull-right css-fontSize12 text-right css-text-red css-marginR10 css-line-height-1 css-paddingT10">
                                <?=$this->trans['download_pdf']?>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix css-marginB20"></div>

                    <div class="row">
                        <div class="col-md-4">
                            <div id="menu_container">
                                <form id="frm-search" name="frm-search" method="post" action="BUSQUEDA-PRODUCTOS">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" name="search" id="search" class="form-control" value="<?=$search?>">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </form>

                                <nav class="navbar navbar-default sidebar" role="navigation" style="width: 100%;">
                                    <div class="container-fluid">
                                        <div class="navbar-header css-sub-menu">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sub-menu">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <span class="css-categoris-M"> NUESTROS JUEGOS ></span>
                                        </div>

                                        <div class="collapse navbar-collapse" id="sub-menu">
                                            <ul class="nav navbar-nav" style="width: 100%;">
                                                <?php foreach ($arr_category as $val): ?>
                                                    <?php if (isset($val['subcategory'][0])): $class = ($cat == $val['id']) ? "open" : ""; ?>
                                                        <li class="dropdown <?=$class?>">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=mb_convert_case($val['name'.$ext], MB_CASE_UPPER, "UTF-8")?><span class="caret"></span></a>
                                                            <ul class="dropdown-menu forAnimate" role="menu">
                                                                <?php foreach ($val['subcategory'] as $value): ?>
                                                                    <li><a href="#" class="js-scroll" data-scroll="sub_<?=$value['id']?>"><?=$value['name'.$ext]?></a></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </li>
                                                    <?php else: ?>
                                                        <li><a href="PRODUCTOS/C/<?=$val['id']?>"><?=mb_convert_case($val['name'.$ext], MB_CASE_UPPER, "UTF-8")?></a></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-8 css-list-products-a" data-t="<?=count($arr_products)?>">
                            <?php $cnt = 0; foreach ($arr_products as $key => $val): $cnt++; ?>
                                <div class="css-products product-<?=$cnt?>">
                                    <a href="PRODUCTO-DETALLE/<?=$val['id']?>" class="sub_<?=$val['sub_category']?>">
                                        <div class="col-xs-4 wow fadeInUp css-no-padding-mobile" data-wow-duration="1000ms" data-wow-delay="300ms">
                                            <img src="<?=$val['image']?>" class="css-noPadding css-product-list-image" alt="<?=$val['name']?>" height="170"> <br>

                                            <div class="pull-left css-center-responsive" style="max-width: 50%">
                                                <?=$val['name']?> <!--<i class="fa fa-share-alt-square"></i>-->
                                            </div>

                                            <?php if ($val['novelty'] == 1): ?>
                                                <div class="css-tag-yellow pull-right css-novelty">
                                                    NOVEDAD
                                                </div>
                                            <?php endif; ?>

                                            <div class="clearfix css-marginB10"></div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <?=$index_pages?>
                    </div>
                </div>
            </div>
        </section>

        <?php include('html/overall/categories.php'); ?>
        <?php include('html/overall/chat.php'); ?>
        <?php include('html/overall/footer.php'); ?>

        <?php include('html/overall/js.php'); ?>
        <script type="text/javascript">
            var scrollVal = 1480;
            $(document).ready(function (){
                var w_screen = screen.width;
                var c_products = $(".css-list-products-a").data('t');
                var m = "";

                for (var i = 1; i <= c_products; i++) {
                    if (w_screen <= 768) {
                        m = 2;
                    } else {
                        m = 3;
                    }

                    if (multiple(i, m)) {
                        $(".product-" + i).after('<div class="clearfix css-marginB30"></div>')
                    }
                }

                $(".js-scroll").on('click', function (e){
                    e.preventDefault();
                    var target = $(this).data('scroll');

                    $('html, body').animate({
                        scrollTop: $("." + target).offset().top
                    }, 500);
                });

                if ($('body').width() > 1000) {
                    $(window).on('scroll', function(){
                        var limit = $(".css-categories-nav").offset();
                        if($(window).scrollTop() >= 610){
                            $('#menu_container').addClass('css-element-fixed-top');
                            var elem_height = $('#menu_container').height();
                            var limit_hide = limit.top - elem_height;

                            if ($(window).scrollTop() >= limit_hide) {
                                $('#menu_container').removeClass('css-element-fixed-top');
                            }
                        } else {
                            $('#menu_container').removeClass('css-element-fixed-top');
                        }
                    });
                }

                function multiple(value, multiple) {
                    remainder = value % multiple;
                    if(remainder == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        </script>
    </body>
</html>
