<?php

require('core/core.php');

if (isset($_GET['m'])) {
    require('core/handler/access-ajax-handler.php');

    if ($_GET['m'] == "login") {  // Inicio de sesión
        require('core/bin/ajax/ajx-session.php');
    } elseif ($_GET['m'] == "contact") { // Contacto
        require('core/bin/ajax/ajx-contact.php');
    } elseif ($_GET['m'] == "chat") { // Chat
        require('core/bin/ajax/ajx-chat.php');
    } elseif ($_GET['m'] == "lang") { // Cambio de idioma
        require('core/bin/ajax/ajx-lang.php');
    } else {
        header('location: index.php');
    }
} else {
    include('location: index.php');
}

?>
