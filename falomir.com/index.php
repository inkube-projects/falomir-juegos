<?php
require('core/core.php');
require('core/methods/controllers.php');

if (isset($_GET['view']) && isset($_GET['meth'])) {
   if (file_exists("core/controller/".$_GET['view']."Controller.php") && in_array($_GET['meth'], $controllers[$_GET['view']])) {
      include("core/controller/".$_GET['view']."Controller.php");
      $class_name = $_GET['view']."Controller";
      $action_method = $_GET['meth']."Action";
      $controller = new $class_name;
      $controller->{$action_method}();
   } else {
      include("core/controller/ErrorController.php");
      $controller = new ErrorController;
      $controller->notFoundAction();
   }
} else {
   include("core/controller/IndexController.php");
   $controller = new IndexController;
   $controller->indexAction();
}

?>
