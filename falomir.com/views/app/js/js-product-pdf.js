function genPDF(){
    // html2canvas(document.querySelector(".css-pdf-body")).then(canvas => {
    //     document.body.appendChild(canvas);
    //     var img = canvas.toDataURL("image/png");
    //     var doc = new jsPDF();
    //     doc.addImage(img,'JPEG',20,20);
    //     doc.save('test.pdf');
    // });

    html2canvas(document.querySelector(".css-pdf-body"), {scale: 2}).then(canvas => {
        var imgData = canvas.toDataURL({
            format: 'jpeg',
            quality: 0.9
        });

        var width = $(".css-pdf-body").width();
        var height = $(".css-pdf-body").height();
        var pdf = new jsPDF('p', 'pt', [width,height]);
        pdf.addImage(imgData, 'JPEG', 0, 0, width, height);
        pdf.save("download.pdf");
    });
}

function createPdf() {
    var imgData = canvas.toDataURL({
        format: 'jpeg',
        quality: 0.2
    });

    var width = $(".css-pdf-body").width();
    var height = $(".css-pdf-body").height();
    var pdf = new jsPDF('p', 'pt', [width,height]);
    pdf.addImage(imgData, 'JPEG', 0, 0, width, height);
    pdf.save("download.pdf");
}
