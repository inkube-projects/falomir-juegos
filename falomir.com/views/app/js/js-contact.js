$(function(e){
    $("#frm-contact").validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            email:{
                required: true,
                email: true
            },
            last_name:{
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            message:{
                required: true,
            },
        },
        messages: {
            name:{
                required: "Debes ingresar tu nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            email:{
                required: "Debes ingresar tu email",
                email: "Debes ingresar un email válido"
            },
            last_name:{
                required: "Debes ingresar tu apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            message:{
                required: "Debes ingresar tu mensaje",
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            $(".btn-submit-js").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            persist();
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist() {
    var i = $("#frm-contact").serialize();
    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=contact',
        data: i,
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $("#mod-result").modal();
                $(".ajx-message").html('Tu mensaje ha sido enviado correctamente')
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }
            $(".btn-submit-js").attr("disabled", false).html('ENVIAR');
        }
    });
}
