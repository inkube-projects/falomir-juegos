jQuery(function($) {'use strict';

	// $(window).on('scroll', function(){
	//
	// 	if($(window).scrollTop() > 200){
	// 		$('#main-navbar').addClass('css-menu-fixed');
	// 	} else {
	// 		$('#main-navbar').removeClass('css-menu-fixed');
	// 	}
	// });

	//Responsive Nav
	$('li.dropdown').find('.fa-angle-down').each(function(){
		$(this).on('click', function(){
			if( $(window).width() < 768 ) {
				$(this).parent().next().slideToggle();
			}
			return false;
		});
	});

	//Fit Vids
	if( $('#video-container').length ) {
		$("#video-container").fitVids();
	}

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){

		$('.main-slider').addClass('animate-in');
		$('.preloader').remove();
		//End Preloader

		if( $('.masonery_area').length ) {
			$('.masonery_area').masonry();//Masonry
		}

		var $portfolio_selectors = $('.portfolio-filter >li>a');

		if($portfolio_selectors.length) {

			var $portfolio = $('.portfolio-items');
			$portfolio.isotope({
				itemSelector : '.portfolio-item',
				layoutMode : 'fitRows'
			});

			$portfolio_selectors.on('click', function(){
				$portfolio_selectors.removeClass('active');
				$(this).addClass('active');
				var selector = $(this).attr('data-filter');
				$portfolio.isotope({ filter: selector });
				return false;
			});
		}

	});

	//Scroll Menu
	$(window).on('scroll', function(){
		if( $(window).scrollTop() >= scrollVal ){
			$('#sct-animated').addClass('animate-in');
			$('.preloader').remove();
			$('.css-act-animated-hide').fadeIn(200);
		}

		// console.log($(window).scrollTop());
	});


	$('.timer').each(count);
	function count(options) {
		var $this = $(this);
		options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		$this.countTo(options);
	}

	// Search
	$('.fa-search').on('click', function() {
		$('.field-toggle').fadeToggle(200);
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),
			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
		});
	});

	// Progress Bar
	$.each($('div.progress-bar'),function(){
		$(this).css('width', $(this).attr('data-transition')+'%');
	});

	if( $('#gmap').length ) {
		var map;

		map = new GMaps({
			el: '#gmap',
			lat: 43.04446,
			lng: -76.130791,
			scrollwheel:false,
			zoom: 16,
			zoomControl : false,
			panControl : false,
			streetViewControl : false,
			mapTypeControl: false,
			overviewMapControl: false,
			clickable: false
		});

		map.addMarker({
			lat: 43.04446,
			lng: -76.130791,
			animation: google.maps.Animation.DROP,
			verticalAlign: 'bottom',
			horizontalAlign: 'center',
			backgroundColor: '#3e8bff',
		});
	}

	$("#frm-chat").validate({
        rules:{
            chat_user_name:{
                required: true,
                email: true
            },
        },
        messages: {
			chat_user_name:{
                required: "Debes ingresar tu email",
                email: "Debes ingresar un email válido"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            var i = $("#frm-chat").serialize();

		    $.ajax({
		        type: 'POST',
		        url: base_admin + 'ajax.php?m=chat&act=1',
		        data: i,
		        dataType: 'json',
				beforeSend: function(r){
					$(".btn-submit-js").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
				},
		        success: function(r){
		            if (r.status == 'OK') {
						$("#direct-chat-messages").html('<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">Falomir</span><span class="direct-chat-timestamp pull-right">23 Ene 2:00 pm</span></div><img class="direct-chat-img" src="' + r.data.profile_image + '" alt="Falomir Moderator"><div class="direct-chat-text">Hola ' + r.data.username + ' escribe tu duda y te responderemos lo antes posible.</div></div>')
					} else {
						alert('Se ha producido un error')
					}
		            $(".btn-submit-js").attr("disabled", false).html('<i class="fa fa-check"></i>');
		        }
		    });

            return false;
        }
    });

	$('.lang').on('click', function(e){
		e.preventDefault();

		$.ajax({
			type: 'GET',
			url: base_admin + 'ajax.php?m=lang&act=' + $(this).data('lang'),
			dataType: 'json',
			success: function(r){
				location.reload();
			}
		})
	})
});
