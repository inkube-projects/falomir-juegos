<?php

include('core/handler/session-handler.php');

/**
 * Controlador para los pdf de los productos
 */
class ProductPdfController extends FPDF
{
    protected $B = 0;
    protected $I = 0;
    protected $U = 0;
    protected $HREF = '';

    function __construct()
    {
        parent::__construct();
        $this->db = new Connection;
        $this->product = $this->getProduct();
    }

    /**
     * Disparador del PDF
     * @return void
     */
    public function launchAction()
    {
        $db = $this->db;

        $this->AliasNbPages();
        $this->AddPage();
        $this->ChapterBody();
        $this->Output();
    }

    /**
     * Se obtiene la información del producto
     * @return [type] [description]
     */
    public function getProduct()
    {
        $db = $this->db;
        $id = @number_format($_GET['id'],0,"","");

        $cnt_val = $db->getCount("view_products", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."PRODUCTOS"); exit;
        }

        // Producto
        $s = "SELECT * FROM view_products WHERE id='".$id."'";
        $arr_product = $db->fetchSQL($s);

        // Categoría
        $s = "SELECT * FROM products_category WHERE id='".$arr_product[0]['category_id']."'";
        $arr_cat = $db->fetchSQL($s);

        // Lenguajes
        $s = "SELECT pl.product_id, pl.language_id, l.name, l.index FROM product_language pl INNER JOIN language l ON pl.language_id = l.id WHERE pl.product_id = '".$id."'";
        $arr_lang = $db->fetchSQL($s);

        $arr_result = array(
            'product' => $arr_product[0],
            'category' => $arr_cat[0],
            'lang' => $arr_lang
        );

        return $arr_result;
    }

    /**
     * Cabecera
     * @return void
     */
    public function Header()
    {
        $this->SetFont('Arial','B',11);
        $product = $this->product;
        $this->SetTitle(mb_convert_case($product['product']['product_name'], MB_CASE_UPPER, "UTF-8"));

        // Logo
        $this->Image('views/images/logo2.png',10,8,20);
        $this->Ln(10);

        $ln_breadcrumb = 5;

        if ($product['product']['banner']) {
            $image = getimagesize(APP_PATH_PRODUCTS_IMAGE.'product_'.$product['product']['id'].'/gallery/'.$product['product']['image_1']);
            $banner = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['banner'];
            $this->Image($banner,10,25,190);
            $ln_breadcrumb = 138;
        }

        $this->ln($ln_breadcrumb);
        $this->SetFont('Arial','',6);
        $this->Cell(0,0,utf8_decode(mb_convert_case($product['category']['name']." / ".$product['product']['product_name'], MB_CASE_TITLE, "UTF-8")), 0, 0, 'C');
    }

    /**
     * Cuerpo del producto
     * @return void
     */
    public function ChapterBody()
    {
        $product = $this->product;
        $img_1_y = 60;
        $img_2_y = 140;

        if ($product['product']['banner']) {
            $img_1_y = 170;
            $img_2_y = 140;
        }

        if (isset($product['product']['image_1']) && isset($product['product']['image_2'])) {
            $image_1 = getimagesize(APP_PATH_PRODUCTS_IMAGE.'product_'.$product['product']['id'].'/gallery/'.$product['product']['image_1']);
            $image_2 = getimagesize(APP_PATH_PRODUCTS_IMAGE.'product_'.$product['product']['id'].'/gallery/'.$product['product']['image_2']);

            $width_1 = $image_1[0];
            $width_2 = $image_2[0];

            if ($width_1 > $width_2) {
                $p_img_1 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_2'];
                $p_img_2 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_1'];
            } else {
                $p_img_1 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_1'];
                $p_img_2 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_2'];
            }
        } else {
            if (isset($product['product']['image_1'])) {
                $p_img_1 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_1'];
            } elseif (isset($product['product']['image_2'])) {
                $p_img_1 = 'views/images/products/product_'.$product['product']['id'].'/gallery/'.$product['product']['image_2'];
            } else {
                $p_img_1 = NULL;
            }
        }

        // idiomas
        $lang = "";
        foreach ($product['lang'] as $key => $val) {
            if ($key == 0) {
                $lang .= $val['name'];
            } else {
                $lang .= ", ".$val['name'];
            }
        }

        $this->SetFont('Arial','B',9);
        $this->ln(10);
        $this->Cell(0,0,utf8_decode(mb_convert_case($product['product']['product_name'], MB_CASE_UPPER, "UTF-8")), 0, 0, 'L');

        $this->SetFont('Arial','',8);
        $this->writeHTML("<b>Edad:</b> ".$product['product']['age_range']);
        $this->ln(5);
        $this->writeHTML("<b>Ref.:</b> ".$product['product']['reference']);

        $this->SetFont('Arial','',7);
        $this->ln(5);
        $this->MultiCell(100,5,str_replace("\n", '', utf8_decode($product['product']['description_1'])),0,'J', false);

        $this->SetFont('Arial','',8);
        $this->ln(5);
        $this->writeHTML("<b>Medidas:</b> ".$product['product']['measurement']);
        $this->ln(5);
        $this->writeHTML("<b>Idiomas:</b> ".utf8_decode($lang));
        $this->ln(5);
        $this->WriteHtmlCell(100, '<b>Contenido:</b> '.$product['product']['content']);

        if ($p_img_1) {
            $this->Image($p_img_1,130,$img_1_y,60);
        }

        $this->ln(5);
        // $this->MultiCell(100,5,str_replace("\n", '', utf8_decode($product['product']['description_2'])),0,'J', false);
        // $this->Image($p_img_2,10,$img_2_y,60);
    }

    /**
     * Pie de página
     * @return void
     */
    public function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
    }

    //--------------------------------------------------------------------------------------

    function WriteHtmlCell($cellWidth, $html){
        $rm = $this->rMargin;
        $this->SetRightMargin($this->w - $this->GetX() - $cellWidth);
        $this->WriteHtml($html);
        $this->SetRightMargin($rm);
    }

    function WriteHTML($html)
    {
    	// HTML parser
    	$html = str_replace("\n",' ',$html);
    	$a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    	foreach($a as $i=>$e)
    	{
    		if($i%2==0)
    		{
    			// Text
    			if($this->HREF)
    				$this->PutLink($this->HREF,$e);
    			else
    				$this->Write(5,$e);
    		}
    		else
    		{
    			// Tag
    			if($e[0]=='/')
    				$this->CloseTag(strtoupper(substr($e,1)));
    			else
    			{
    				// Extract attributes
    				$a2 = explode(' ',$e);
    				$tag = strtoupper(array_shift($a2));
    				$attr = array();
    				foreach($a2 as $v)
    				{
    					if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
    						$attr[strtoupper($a3[1])] = $a3[2];
    				}
    				$this->OpenTag($tag,$attr);
    			}
    		}
    	}
    }

    function OpenTag($tag, $attr)
    {
    	// Opening tag
    	if($tag=='B' || $tag=='I' || $tag=='U')
    		$this->SetStyle($tag,true);
    	if($tag=='A')
    		$this->HREF = $attr['HREF'];
    	if($tag=='BR')
    		$this->Ln(5);
    }

    function CloseTag($tag)
    {
    	// Closing tag
    	if($tag=='B' || $tag=='I' || $tag=='U')
    		$this->SetStyle($tag,false);
    	if($tag=='A')
    		$this->HREF = '';
    }

    function SetStyle($tag, $enable)
    {
    	// Modify style and select corresponding font
    	$this->$tag += ($enable ? 1 : -1);
    	$style = '';
    	foreach(array('B', 'I', 'U') as $s)
    	{
    		if($this->$s>0)
    			$style .= $s;
    	}
    	$this->SetFont('',$style);
    }

    function PutLink($URL, $txt)
    {
    	// Put a hyperlink
    	$this->SetTextColor(0,0,255);
    	$this->SetStyle('U',true);
    	$this->Write(5,$txt,$URL);
    	$this->SetStyle('U',false);
    	$this->SetTextColor(0);
    }
}


?>
