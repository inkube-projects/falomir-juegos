<?php

include('core/handler/session-handler.php');
include('core/controller/controllerAware.php');

/**
* Controlador para el index y el Home
*/
class IndexController extends controllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
    * Home
    * @return void
    */
    public function indexAction()
    {
        $db = new Connection;

        // Novedades
        $s = "SELECT * FROM products WHERE novelty='1' ORDER BY id DESC LIMIT 0, 4";
        $arr_sql = $db->fetchSQL($s);
        $arr_novelty = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['novelty_image']) {
                $image = APP_PRODUCTS_IMAGE."product_".$val['id']."/gallery/".$val['novelty_image'];
            }

            $arr_novelty[] = array(
                'id' => $val['id'],
                'image' => $image,
            );
        }

        // Imagenes del carrusel
        $s = "SELECT * FROM carousel WHERE status='1' AND section='0'";
        $arr_carousel = $db->fetchSQL($s);

        foreach ($arr_carousel as $key => $val) {
            if ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") {
                $title = $val['title'];
                $desc = $val['description'];
            } elseif ($_SESSION['FALOMIR_LANGUAGE'] == "EN") {
                $title = $val['title_en'];
                $desc = $val['description_en'];
            }

            $arr_carousel[$key]['formed_title'] = $title;
            $arr_carousel[$key]['formed_description'] = $desc;
        }

        // Noticias
        $s = "SELECT * FROM news ORDER BY id DESC LIMIT 0,3";
        $arr_sql = $db->fetchSQL($s);
        $arr_news = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_NEWS."cover/".$val['cover_image'];
            }

            if ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") {
                $title = $val['title'];
                $desc = substr($val['description'], 0, 400);
            } elseif ($_SESSION['FALOMIR_LANGUAGE'] == "EN") {
                $title = $val['title_en'];
                $desc = substr($val['description_en'], 0, 400);
            }

            $arr_news[] = array(
                'id' => $val['id'],
                'title' => $title,
                'description' => $desc,
                'image' => $image
            );
        }

        require_once('html/index/home.php');
    }
}

?>
