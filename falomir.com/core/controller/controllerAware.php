<?php

class controllerAware
{
    function __construct()
    {
        $this->user_id = @$_SESSION['USER_SESSION_FALOMIR']['id'];
        $this->admin_pp_id = @$_SESSION['USER_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['USER_SESSION_FALOMIR']['username'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->trans = $this->getTranslate();
        $this->URI = $this->getURL();
    }

    /**
     * Obtiene la traducción
     * @return array
     */
    public function getTranslate()
    {
        $getLanguageES = file_get_contents("core/translations/translationES.json");
        $getLanguageEN = file_get_contents("core/translations/translationEN.json");

        // if(!isset($_GET["lan"])) {
        //     @$_SESSION['FALOMIR_LANGUAGE'] = "default";
        // } else {
        //     if ($_GET["lan"] == "ES" || $_GET["lan"] == "EN") {
        //         @$_SESSION['FALOMIR_LANGUAGE'] = $_GET["lan"];
        //     }
        // }

        if (!isset($_SESSION['FALOMIR_LANGUAGE'])) {
            @$_SESSION['FALOMIR_LANGUAGE'] = "default";
        }

        if ($_SESSION['FALOMIR_LANGUAGE'] != "ES" && $_SESSION['FALOMIR_LANGUAGE'] != "EN" && $_SESSION['FALOMIR_LANGUAGE'] != "default") {
            @$_SESSION['FALOMIR_LANGUAGE'] = "default";
        }

        if ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") {
            $trans = json_decode($getLanguageES, true);
        } elseif ($_SESSION['FALOMIR_LANGUAGE'] == "EN") {
            $trans = json_decode($getLanguageEN, true);
        }

        return $trans;
    }

    /**
     * Obtiene el url actual
     * @return void
     */
    public function getURL()
    {
        $currentURI = $_SERVER['REQUEST_URI'];
        $URI = str_replace(array('/falomir/', '/ES', '/EN'), array('', '', ''), $currentURI); // local

        return $URI;
    }
}


?>
