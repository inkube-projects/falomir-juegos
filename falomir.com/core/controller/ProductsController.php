<?php

include('core/handler/session-handler.php');
include('core/controller/controllerAware.php');

/**
 * Controlador para los productos
 */
class ProductsController extends controllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna el listado de productos con sus filtros
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $cat = @number_format($_GET['cat'],0,"","");
        $search = @sanitize($_POST['search']);
        $where = "";
        $url = BASE_URL."PRODUCTOS";
        $result_pages = 12;
        $ext = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "_en" : "";

        if ($cat != 0 && $cat != "") {
            // $where .= " AND category_id='".$cat."'";
            $where .= " AND id IN (SELECT product_id FROM products_categories WHERE category_id='".$cat."')";
            $url = BASE_URL."PRODUCTOS/C/".$cat;

            $s = "SELECT * FROM products_category WHERE id='".$cat."'";
            $arr_p_cat = $db->fetchSQL($s);

            if ($arr_p_cat[0]['principal'] == 0) {
                $arr_p_cat = $db->fetchSQL("SELECT * FROM products_category WHERE id='".$arr_p_cat[0]['father_id']."'");
            }
        } else {
            $s = "SELECT * FROM products_category WHERE principal='1' ORDER BY RAND() LIMIT 1";
            $arr_p_cat = $db->fetchSQL($s);
        }

        if ($search) {
            $where .= " AND (reference LIKE '%".$search."%' OR product_name LIKE '%".$search."%')";
            $result_pages = 1000;
        }

        if ($cat == "" || $cat == 0) {
            $arr_pag_products = array(
                'columns' => "*",
                'table' => "view_products",
                'where' => "status='1' ".$where,
                'results_pages' => $result_pages,
                'actually_page' => @number_format($_GET['page'],0,"",""),
                'url' => $url,
                'additional' => '' // Debe comenzar con /
            );
            $paginator = new Paginator($db, $arr_pag_products);
            $arr_sql = $paginator->getResults('ORDER BY relevance ASC');
            $index_pages = $paginator->getPaginator();
        } else {
            $s = "SELECT * FROM view_products WHERE status='1' ".$where." ORDER BY id DESC";
            $arr_sql = $db->fetchSQL($s);
            $index_pages = "";
        }


        $arr_products = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_PRODUCTS_IMAGE."product_".$val['id']."/cover/".$val['cover_image'];
            }

            $arr_products[] = array(
                'id' => $val['id'],
                'name' => $val['product_name'],
                'image' => $image,
                'novelty' => $val['novelty'],
                'sub_category' => $val['sub_category_id'],
            );
        }

        // Categorías
        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_category = $db->fetchSQL($s);

        foreach ($arr_category as $key => $val) {
            if ($val['id'] == $cat) {
                $arr_category[$key]['subcategory'] = $db->fetchSQL("SELECT * FROM products_category WHERE father_id='".$val['id']."'");
            } else {
                $arr_category[$key]['subcategory'] = "";
            }
        }

        require_once('html/products/product-list.php');
    }

    /**
     * Muestra el detalle del juego
     * @return void
     */
    public function showAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("view_products", "id='".$id."' AND status='1'");
        $ext = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "_en" : "";

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."PRODUCTOS"); exit;
        }

        // Producto
        $s = "SELECT * FROM view_products WHERE id='".$id."' AND status='1'";
        $arr_product = $db->fetchSQL($s);
        $product = $arr_product[0];

        $product_banner = NULL;
        if ($product['banner']) {
            $product_banner = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['banner'];
        }
        $image_1 = ($product['image_1']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['image_1'] : NULL;
        $image_2 = ($product['image_2']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['image_2'] : NULL;
		$logo_1 =  ($product['product_logo']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['product_logo'] : NULL;
        $cover =   ($product['cover_image']) ? APP_PRODUCTS_IMAGE."product_".$id."/cover/".$product['cover_image'] : NULL;

        $arr_gallery = [
            'image_1' => $image_1,
            'image_2' => $image_2,
            'image_3' => $logo_1,
            'image_4' => $cover
        ];

        // Categoría
        $s = "SELECT * FROM products_category WHERE id='".$arr_product[0]['category_id']."'";
        $arr_cat = $db->fetchSQL($s);
        $background_color = $arr_cat[0]['background_color'];
        $banner = APP_PRODUCTS_CATEGORY_IMAGE.$arr_cat[0]['banner'];

        if ($arr_cat[0]['principal'] == 0) {
            $s = "SELECT background_color, banner FROM products_category WHERE id='".$arr_cat[0]['father_id']."'";
            $arr_father = $db->fetchSQL($s);

            $background_color = $arr_father[0]['background_color'];
            $banner = APP_PRODUCTS_CATEGORY_IMAGE.$arr_father[0]['banner'];
        }

        // Lenguajes
        $s = "SELECT pl.product_id, pl.language_id, l.name, l.index FROM product_language pl INNER JOIN language l ON pl.language_id = l.id WHERE pl.product_id = '".$id."'";
        $arr_lang = $db->fetchSQL($s);

        // mas vendidos
        $s = "SELECT * FROM view_products WHERE best_seller='1' AND status='1'";
        $arr_sql = $db->fetchSQL($s);
        $arr_best_seller = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_PRODUCTS_IMAGE."product_".$val['id']."/cover/".$val['cover_image'];
            }

            $arr_best_seller[] = array(
                'id' => $val['id'],
                'name' => $val['product_name'],
                'image' => $image
            );
        }

        //Relacionados
        $s = "SELECT * FROM view_products WHERE category_id='".$product['category_id']."' AND status='1' ORDER BY relevance DESC LIMIT 0,3";
        $arr_sql = $db->fetchSQL($s);
        $arr_related = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_PRODUCTS_IMAGE."product_".$val['id']."/cover/".$val['cover_image'];
            }

            $arr_related[] = array(
                'id' => $val['id'],
                'name' => $val['product_name'],
                'image' => $image
            );
        }

        require_once('html/products/product-show.php');
    }

    /**
     * Muestra la info de nuestros juegos
     * @return void
     */
    public function ourGamesAction()
    {
        $db = new Connection;

        // Imagenes del carrusel
        $s = "SELECT * FROM carousel WHERE section='1'";
        $arr_carousel = $db->fetchSQL($s);

        $s = "SELECT * FROM products_category WHERE principal='1' AND NOT image IS NULL LIMIT 0,4";
        $arr_sql = $db->fetchSQL($s);
        $arr_cat = array();

        foreach ($arr_sql as $key => $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['image']) {
                $image = APP_PRODUCTS_CATEGORY_IMAGE.$val['image'];
            }

            if ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") {
                $name = $val['name'];
                $desc = $val['description'];
            } elseif ($_SESSION['FALOMIR_LANGUAGE'] == "EN") {
                $name = $val['name_en'];
                $desc = $val['description_en'];
            }

            $arr_cat[] = array(
                'id' => $val['id'],
                'name' => $name,
				'nameB' => $val['nameB'],
                'description' => $desc,
                'image' => $image,
            );
        }

        require_once('html/products/our-games.php');
    }

    /**
     * Muestra el pdf
     * @return void
     */
    public function pdfAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("view_products", "id='".$id."' AND status='1'");
        $ext = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "_en" : "";

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."PRODUCTOS"); exit;
        }

        // Producto
        $s = "SELECT * FROM view_products WHERE id='".$id."' AND status='1'";
        $arr_product = $db->fetchSQL($s);
        $product = $arr_product[0];

        $product_banner = NULL;
        if ($product['banner']) {
            $product_banner = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['banner'];
        }
        $image_1 = ($product['image_1']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['image_1'] : NULL;
        $image_2 = ($product['image_2']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['image_2'] : NULL;
		$logo_1 =  ($product['product_logo']) ? APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$product['product_logo'] : NULL;
        $cover =   ($product['cover_image']) ? APP_PRODUCTS_IMAGE."product_".$id."/cover/".$product['cover_image'] : NULL;

        $arr_gallery = [
            'image_1' => $image_1,
            'image_2' => $image_2,
            'image_3' => $logo_1,
            'image_4' => $cover
        ];

        // Categoría
        $s = "SELECT * FROM products_category WHERE id='".$arr_product[0]['category_id']."'";
        $arr_cat = $db->fetchSQL($s);
        $background_color = $arr_cat[0]['background_color'];
        $banner = APP_PRODUCTS_CATEGORY_IMAGE.$arr_cat[0]['banner'];

        if ($arr_cat[0]['principal'] == 0) {
            $s = "SELECT background_color, banner FROM products_category WHERE id='".$arr_cat[0]['father_id']."'";
            $arr_father = $db->fetchSQL($s);

            $background_color = $arr_father[0]['background_color'];
            $banner = APP_PRODUCTS_CATEGORY_IMAGE.$arr_father[0]['banner'];
        }

        // Lenguajes
        $s = "SELECT pl.product_id, pl.language_id, l.name, l.index FROM product_language pl INNER JOIN language l ON pl.language_id = l.id WHERE pl.product_id = '".$id."'";
        $arr_lang = $db->fetchSQL($s);

        require_once('html/products/product-pdf.php');
    }
}


?>
