<?php

include('core/handler/session-handler.php');
include('core/controller/controllerAware.php');

/**
 * Controlador para las noticias
 */
class NewsController extends controllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Listado de noticias
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $ext = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "_en" : "";
        $cat = @number_format($_GET['cat'],0,"","");
        $search = @sanitize($_POST['search']);
        $where = "";
        $url = BASE_URL."NOTICIAS";
        $result_pages = 10;

        // Primera noticia
        $s = "SELECT * FROM view_news WHERE status='1' ORDER BY id DESC LIMIT 0, 1";
        $arr_p_news = $db->fetchSQL($s);
        $p_image = APP_NO_IMAGES."no_image.jpg";
        if ($arr_p_news[0]['cover_image']) {
            $p_image = APP_NEWS."cover/".$arr_p_news[0]['cover_image'];
        }

        if ($cat != 0 && $cat != "") {
            $where .= " AND category_id='".$cat."'";
            $url = BASE_URL."BLOG/C/".$cat;
        }

        if ($search) {
            $where .= " AND (title LIKE '%".$search."%')";
            $result_pages = 1000;
        }

        $arr_pag_news = array(
            'columns' => "*",
            'table' => "view_news",
            'where' => "NOT id = '".$arr_p_news[0]['id']."' AND status='1' ".$where,
            'results_pages' => $result_pages,
            'actually_page' => @number_format($_GET['page'],0,"",""),
            'url' => $url,
            'additional' => '' // Debe comenzar con /
        );
        $paginator = new Paginator($db, $arr_pag_news);
        $arr_sql = $paginator->getResults('ORDER BY relevance ASC');
        $index_pages = $paginator->getPaginator();
        $arr_news = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_NEWS."cover/".$val['cover_image'];
            }

            if ($_SESSION['FALOMIR_LANGUAGE'] == "ES" || $_SESSION['FALOMIR_LANGUAGE'] == "default") {
                $title = $val['title'];
                $description = $val['description'];
            } elseif ($_SESSION['FALOMIR_LANGUAGE'] == "EN") {
                $title = $val['title_en'];
                $description = $val['description_en'];
            }

            $arr_news[] = array(
                'id' => $val['id'],
                'title' => $title,
                'description' => $description,
                'image' => $image,
            );
        }

        // Categorías
        $s = "SELECT * FROM news_category WHERE NOT id = ''";
        $arr_category = $db->fetchSQL($s);

        // Entradas recientes
        $s = "SELECT * FROM view_news WHERE status='1' ORDER BY RAND() LIMIT 0,5";
        $arr_recents = $db->fetchSQL($s);

        require_once('html/news/news-list.php');
    }

    /**
     * Detalle de la noticia
     * @return void
     */
    public function showAction()
    {
        $db = new Connection;
        $ext = ($_SESSION['FALOMIR_LANGUAGE'] == "EN") ? "_en" : "";
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("view_news", "id='".$id."' AND status='1'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."NOTICIAS"); exit;
        }

        $s = "SELECT * FROM view_news WHERE id='".$id."' AND status='1'";
        $arr_news = $db->fetchSQL($s);

        $banner = APP_NO_IMAGES."no_image.jpg";
        if ($arr_news[0]['image']) {
            $image = APP_NEWS."banner/".$arr_news[0]['image'];
        }

        // Categorías
        $s = "SELECT * FROM news_category WHERE NOT id = ''";
        $arr_category = $db->fetchSQL($s);

        // Entradas recientes
        $s = "SELECT * FROM view_news WHERE status='1' ORDER BY RAND() LIMIT 0,5";
        $arr_recents = $db->fetchSQL($s);

        // 6 siguientes noticias
        $s = "SELECT * FROM view_news WHERE NOT id='".$id."' AND status='1' ORDER BY relevance ASC LIMIT 0,6";
        $arr_sql = $db->fetchSQL($s);
        $arr_a_news = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_NEWS."cover/".$val['cover_image'];
            }

            $arr_a_news[] = array(
                'id' => $val['id'],
                'title' => $val['title'.$ext],
                'description' => $val['description'.$ext],
                'image' => $image,
            );
        }

        require_once('html/news/news-show.php');
    }
}


?>
