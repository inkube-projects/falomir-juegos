<?php

include('core/handler/session-handler.php');
include('core/controller/controllerAware.php');

/**
 * Controlador para contacto
 */
class ContactController extends controllerAware
{
    function __construct()
    {
        parent::__construct();
    }

    public function contactAction()
    {
        $sb = new Connection;

        require_once('html/contact/contact.php');
    }
}


?>
