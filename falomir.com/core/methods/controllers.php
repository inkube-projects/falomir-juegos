<?php

$controllers = array(
	'Index' => array('index'),
	'Products' => array('list', 'show', 'ourGames', 'pdf'),
	'News' => array('list', 'show'),
	'Contact' => array('contact'),
	'ProductPdf' => array('launch'),
);

?>
