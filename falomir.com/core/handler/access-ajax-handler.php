<?php
/**
 * Handler que controla el acceso a los archivos ajax
*/
$db = new Connection();
$current_ajax = $_GET['m'];

$role = "ROLE_ANONYMOUS";

$arr_ajax = array(
   'login' => array('ROLE_ANONYMOUS'),
   'contact' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
   'lang' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['USER_SESSION_FALOMIR']['id'])) {
   $role = $db->getValue("role", "role", "id='".$_SESSION['USER_SESSION_FALOMIR']['role']."'");
   $admin_pp_id = $_SESSION['USER_SESSION_FALOMIR']['id'];
}

try {
    foreach ($arr_ajax as $key => $val) {
       if ($key == $current_ajax) {
          if (!in_array($role, $val)) {
            throw new \Exception("No tienes acceso a esta acción", 1);
          }
       }
    }
} catch (Exception $e) {
    $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    header('Content-Type: application/json');
    echo json_encode($arr_response);
    exit;
}
?>
