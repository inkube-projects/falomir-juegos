<?php

/**
 * Helper para el contacto
 */
class ContactHelper
{
    /**
     * Guarda el mensaje en la base de datos
     * @return void
     */
    public function persist(){
        $db = new Connection;

        $arr_fields = array(
            'name',
            'last_name',
            'email',
            'message',
            'create_at',
            'update_at'
        );
        $arr_values = array(
            $_POST['name'],
            $_POST['last_name'],
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            sanitize($_POST['message']),
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $contact = $db->insertAction("contact_messages", $arr_fields, $arr_values);
    }
}


?>
