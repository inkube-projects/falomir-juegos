<?php

$db = new Connection();
$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_GET) {
    try {
        $lang = sanitize($_GET['act']);
        if ($lang != "ES" && $lang != "EN") { throw new \Exception("Idioma inválido", 1); }

        $_SESSION['FALOMIR_LANGUAGE'] = $lang;

        $arr_response = array('status' => 'OK', 'message' => 'Cambio de idioma correcto');
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
