<?php

// include('core/bin/helpers/ChatHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
    // $chatHelper = new ChatHelper($db);

    try {
        isRequiredValuesPost($_POST, array('chat_user_name'));
        isValidEmail($_POST['chat_user_name']);

        $chat_user_name = explode('@', $_POST['chat_user_name']);

        $_SESSION['FALOMIR_CHAT_EMAIL']['email'] = mb_convert_case($_POST['chat_user_name'], MB_CASE_LOWER, "UTF-8");
        $_SESSION['FALOMIR_CHAT_EMAIL']['username'] = mb_convert_case($chat_user_name[0], MB_CASE_TITLE, "UTF-8");

        $arr_response = array('status' => 'OK', 'message' => 'Se ha creado el nombre de usuario', 'username' => $_SESSION['FALOMIR_CHAT_EMAIL']['username']);
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
