<?php

include('core/bin/helpers/ContactHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

if ($_POST) {
    $contactHelper = new ContactHelper($db);
    // $db->beginTransaction();

    try {
        isRequiredValuesPost($_POST, array('name', 'email', 'last_name', 'message'));
        isValidString($_POST['name']);
        isValidString($_POST['last_name']);
        isValidEmail($_POST['email']);

        $contactHelper->persist();

        $arr_response = array('status' => 'OK', 'message' => 'Su mensaje ha sido enviado correctamente');
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
   }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
