<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// nucleo del sitio web
@session_start();

//------ Local
define('DB_USER', 'root');
define('DB_PASS', '123456');
define('DB_DSN', 'mysql:dbname=falomir_db_local;host=localhost;charset=utf8');

// ---- inkube
// define('DB_USER', 'falomirUserAppli');
// define('DB_PASS', 'falomir_1nkub3Appli');
// define('DB_DSN', 'mysql:dbname=falomir_WebAppli;host=falomir.inkube.net;charset=utf8');

define('APP_TITLE', 'Administrador de Contenido');
define('APP_COPY', '&copy; FALOMIR '.date('Y').', All rights reserved.');
define('HTML_DIR', 'html/');

// Local
define('BASE_URL', 'http://localhost/aaa/falomir-juegos/');

// Prod
// define('BASE_URL', 'http://falomir.inkube.net/');

define('PUBLIC_PATH', '../falomir.com/');
define('BASE_URL_FRONT', BASE_URL.'falomir.com/');
define('APP_ASSETS', BASE_URL_FRONT.'views/app/');
define('APP_PLUGINS', BASE_URL_FRONT.'views/app/plugins/');
define('APP_IMG_ADMIN', 'views/images/users/');
define('APP_IMG_ADMIN_USER', BASE_URL_FRONT.'views/images/users/');
define('APP_IMG', BASE_URL_FRONT.'views/images/');

define('APP_IMG_PUBLIC', BASE_URL.'falomir.com/views/images/');
define('APP_NO_IMAGES', BASE_URL.'falomir.com/views/images/no_images/');
define('APP_FAVICONS', BASE_URL.'falomir.com/views/images/ico/');

// Usuarios
define('APP_USERS', BASE_URL.'falomir.com/views/images/users/');
define('APP_PATH_USERS', PUBLIC_PATH.'views/images/users/');

// Categoría de productos
define('APP_PRODUCTS_CATEGORY_IMAGE', BASE_URL.'falomir.com/views/images/products/category/');
define('APP_PATH_PRODUCTS_CATEGORY_IMAGE', PUBLIC_PATH.'views/images/products/category/');

// Productos
define('APP_PRODUCTS_IMAGE', BASE_URL.'falomir.com/views/images/products/');
define('APP_PATH_PRODUCTS_IMAGE', PUBLIC_PATH.'views/images/products/');

// Noticias
define('APP_NEWS', BASE_URL.'falomir.com/views/images/news/');
define('APP_PATH_NEWS', PUBLIC_PATH.'views/images/news/');

// Carrusel
define('APP_CAROUSEL', BASE_URL.'falomir.com/views/images/slider/');

setlocale(LC_TIME, 'es_VE');
date_default_timezone_set('Europe/Madrid');

require('core/model/classConnection.php');
require('core/paginator/Paginator.php');
require('core/bin/functions/main-lib.php');
require('vendor/autoload.php');

?>
