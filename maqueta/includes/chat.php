<section class="css-section-chat">
    <div class="container" style="position: relative;">
    		<div class="row css-paddingR100 css-paddingL100">
                <div class="chatIco">
                    <img src="images/icono_chat.png" width="90" height="86" alt=""/>
                </div>
                <!--<i class="chatIco fa fa-commenting-o css-fontSize56 pull-right css-text-white" aria-hidden="true"></i>-->
                <!--<span class="chatIco fa-stack css-fontSize56" style="text-rendering:geometricPrecision;">
                                         <i class="fa fa-circle fa-stack-1x" style="color:#4bbd34; text-align:center;"></i>
                                         <i class="fa fa-commenting-o fa-stack-1x fa-inverse"></i>
                                         </span>-->
              <h2 class="css-oswaldextralight css-text-white pull-right text-right">¿EN QUE <br> PODEMOS <br> AYUDARTE?</h2>
            </div>
       </div>
</section>

								 

<div class="col-md-3 css-chat-box css-hide" style="z-index:9999;">
    <div class="box box-success direct-chat direct-chat-success collapsed-box css-noMargin">
        <div class="box-header with-border">
            <h3 class="box-title">Contacto</h3>

            <div class="box-tools pull-right">
                <span data-toggle="tooltip" title="3 mesnajes nuevos" class="badge bg-green">3</span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" id="js-collapse"><i class="fa fa-plus"></i></button>

                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                    <i class="fa fa-comments"></i>
                </button>

                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
            </div>
        </div>

        <div class="box-body">
            <div class="direct-chat-messages" id="direct-chat-messages">
                <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Ene 2:00 pm</span>
                    </div>

                    <img class="direct-chat-img" src="images/chat/user1.jpg" alt="Message User Image">
                    <div class="direct-chat-text">
                        Lorem ipsum dolor sit amet
                    </div>
                </div>

                <div class="direct-chat-msg right">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">Ali Guevara</span>
                        <span class="direct-chat-timestamp pull-left">23 Ene 2:05 pm</span>
                    </div>

                    <img class="direct-chat-img" src="images/chat/user2.jpg" alt="Message User Image">
                    <div class="direct-chat-text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </div>
                </div>
            </div>

            <div class="direct-chat-contacts">
                <ul class="contacts-list">
                    <li>
                        <a href="#">
                            <img class="contacts-list-img" src="images/chat/user1.jpg" alt="User Image">

                            <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                    Servicio al cliente
                                    <small class="contacts-list-date pull-right">02/28/2015</small>
                                </span>
                                <span class="contacts-list-msg">Esto es un mensaje ...</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="box-footer">
            <form action="#" method="post">
                <div class="input-group">
                    <input type="text" name="message" placeholder="Escribe tu mensaje" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-success btn-flat">Enviar</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
