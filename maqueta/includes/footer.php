<footer id="footer">
    <div class="container">
        <img src="images/logo-gray.png" class="center-block" width="100" alt="">
        <h2 class="text-center css-text-gray css-fontSize17">NOTA LEGAL</h2>
        <p class="text-center css-text-gray">Copyright 2018 - Juguetes Falomir, S.A Política de privacidad y Cookies. Aviso legal</p>
    </div>

    <div class="css-nav-networks">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                    <div class="social-icons">
                        <ul class="nav nav-pills" style="margin-top: 7px; display: flex; justify-content: center;">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
