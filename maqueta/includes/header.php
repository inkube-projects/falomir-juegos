<header id="header" class="css-paddingT0">
   <div class="css-nav-networks">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 overflow">
               <div class="social-icons pull-left">
                  <ul class="nav nav-pills">
                     <li><a href=""><i class="fa fa-facebook"></i></a></li>
                     <li><a href=""><i class="fa fa-twitter"></i></a></li>
                     <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                     <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                     <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                     <li><a href=""><i class="fa fa-instagram"></i></a></li>
                  </ul>
               </div>
            </div>
            
         </div>
      </div>
   </div>
   <div class="navbar navbar-inverse" role="banner">
      <div class="container">
      
                     <div class="css-language pull-right">
                  <ul class="nav nav-pills">
                     <li class="active"><a href="#">ES</a></li>
                     <li><a href="">EN</a></li>
                  </ul>
               </div>
      
      
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="http://falomir.inkube.net/maqueta/">
               <h1><img src="images/logo2.png" alt="logo" width="99"></h1>
            </a>
         </div>

         <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right css-oswaldextralight">
               <li class="active"><a href="index.php">Home</a></li>
               <li><a href="our-games.php">NUESTROS JUEGOS</a></li>
               <li><a href="contact.php">CONTACTO</a></li>
               <li><a href="news.php">BLOG</a></li>
            </ul>
         </div>
         <!-- <div class="search">
            <form role="form">
               <i class="fa fa-search"></i>
               <div class="field-toggle">
                  <input type="text" class="search-form" autocomplete="off" placeholder="Buscar">
               </div>
            </form>
         </div> -->
      </div>
   </div>
</header>
