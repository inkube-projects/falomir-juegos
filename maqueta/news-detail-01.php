<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Detalle de noticias</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body style="background-color: #e8eef3">
        <? include('includes/header.php'); ?>

        <section class="css-section-new">
            <div class="container">
                <div class="css-news-head-image" style="background-image: url('images/news/head.jpg')">
                    <div class="css-news-title">
                      ¡Haz magia en el cole!
                    </div>
                </div>

                <div class="css-news-description">
                    <p>Está claro que con el título de este post igual nos hemos venido arriba pero no queremos que se nos malinterprete. Tampoco queremos que hagas desaparecer en el cole al profe o la asignatura que no te guste o realizar un truco para que se alargue más el tiempo de recreo. No, seamos serios, lo que queremos es que pongas en práctica todo lo que has adelantado estos meses y sorprendas a tus compis. <br><br>
                    
Aprovecha que tras un verano en el que te has iniciado con el<strong> juego de magia</strong> que te regalaron y la ayuda del canal de YouTube que en Falomir Juegos hemos creado, ahora tienes la oportunidad de llegar al gran público; si, nos referimos a los compis de clase, incluso a tu profe de plástica, porque desde luego, la magia es una actividad que puede y debe ser representada con una gran belleza plástica.<br><br>

Ahora que las clases son tan participativas y en las que se estimulan habilidades y talento, seguro que en algún momento de este primer trimestre tienes la oportunidad de sorprender con esta nueva habilidad que estás perfeccionando. Preséntate ante tu auditorio y cuando llegue tu turno, simplemente les dices: ‘Yo voy a hacer magia’. Seguro que tu anuncio ya generará sorpresa. Escoge un par de trucos que hayas ensayado bien, prueba con algo fácil, por ejemplo, algún truco de cartas que, de repente, salen de la nada o recurre al truco de las bolas que cambian de color. Ya verás a cuantos dejas con la boca abierta, incluso puede que a tu profe.<br><br>

Desde luego, es una buena manera de arrancar el curso, seguro que haces amigos nuevos interesados en conocer tus trucos y quién sabe, igual hasta te sale un/a partenaire que se convertirá en tu pareja y guardará bien todos tus secretos.  
<span class="text-center">¡Pasen y vean!</span>
</p>
                </div>

                <div class="css-news-panel-1-footer">
                    <div class="social-icons">
                        <ul class="nav nav-pills pull-right" style="margin-top: 0; display: flex; justify-content: center;">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- //////////////////////////////////////////////////////////////////////// -->

                <div class="col-md-8 css-noPadding">
                    <div class="col-md-6 css-paddingL0 css-paddinR20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 1</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingR0 css-paddinL20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 2</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingL0 css-paddinR20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 3</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingR0 css-paddinL20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 4</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">BUSCAR</a>
                    <input type="text" class="form-control css-input-search css-marginB80">

                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">ENTRADAS RECIENTES</a>

                    <ul class="css-ul-recent-entries">
                        <li><a href="#">9 juegos de Lúdillo perfectos para este verano</a></li>
                        <li><a href="#">Los niños y niñas españoles juegancada vez menos</a></li>
                        <li><a href="#">Reciclando tus juegos también ayudas al medio ambiente</a></li>
                        <li><a href="#">¿Qué juegos elegir para un niño a partir de los 6 años?</a></li>
                        <li><a href="#">6 consejos para proteger a tus hijos en Internet</a></li>
                    </ul>

                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">CATEGORÍAS</a>

                    <ul class="css-ul-recent-entries">
                        <li><a href="#">JUEGOS Y JUGUETES</a></li>
                        <li><a href="#">LOS NIÑOS JUEGAN</a></li>
                        <li><a href="#">MANUALIDADES</a></li>
                        <li><a href="#">JUEGOS DE HABILIDAD</a></li>
                        <li><a href="#">TARDES DE JUEGOS</a></li>
                    </ul>
                </div>
            </div>
        </section>



        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
