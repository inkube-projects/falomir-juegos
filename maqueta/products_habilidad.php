<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Nuestros Juegos</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">
        <link href="css/menu.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->

        <section id="home-slider" class="css-products-banner" style=" height:300px; background-color: #e4b34d;">
            <div class="container">
                <div class="">
                    <div class="main-slider">
                        <div class="slide-text">
                            <h1 class="css-marginT30 css-oswaldextralight">JUEGOS DE HABILIDAD</h1>
                            <h3 class="css-marginT30 css-oswaldextralight css-text-white">Te encantará aprender así</h3>
                            <p class="css-justify">Potenciar la creatividad, la estrategia, aprender sobre estructuras, mejorar la precisión, la psicomotricidad y además ¡Pasarlo en grande! Si es que… los juegos de habilidad ¡Lo tienen todo!.</p>
                        </div>
                        <div class="slide-img">
                       		<img src="images/slider/categoria3.png" class="img-responsive pull-right" alt="slider image">
                        <!--<div class="css-banner" style="background-image: url('images/slider/slide-2.jpg');"></div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>

        <section class="css-products-list">
            <div class="container">
                <div class="col-md-8 pull-right">
                    <div class="col-md-6">
                        <p class="css-text-gray css-fontSize12">ÉCHALE MAGIA Y DIBI DIBADI DIBÚ</p>
                    </div>

                    <div class="col-md-6 pull-right">
                        <img src="images/download-icon.png" class="pull-right" alt="">
                        <div class="pull-right css-fontSize12 text-right css-text-red css-marginR10 css-line-height-1 css-paddingT10">
                            DESCARGA EL <br> CATÁLOGO EN PDF
                        </div>
                    </div>
                </div>

                <div class="clearfix css-marginB20"></div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group css-marginL100">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <? include('includes/menuCategory.php'); ?>
                        </div>
                    </div>

                    <? include('includes/listProducts.php'); ?>

                </div>
            </div>
        </section>

        <div class="css-categories-nav">
            <div class="container">
                <ul class="css-noPadding">
                    <li><a href="#">MAGIA <i class="fa fa-magic"></i></a></li>
                    <li><a href="#">ARTÍSTICOS <i class="fa fa-paint-brush"></i></a></li>
                    <li><a href="#">HABILIDAD <i class="fa fa-bullseye"></i></a></li>
                    <li><a href="#">JUEGOS DE MESA <i class="fa fa-cube"></i></a></li>
                    <li><a href="#">CLÁSICOS <i class="fa fa-puzzle-piece"></i></a></li>
                </ul>
            </div>
        </div>

        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
