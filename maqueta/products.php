<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Nuestros Juegos</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->

        <section id="home-slider" class="css-products-banner" style="background-color: #99aed9;">
            <div class="container">
                <div class="">
                    <div class="main-slider">
                        <div class="slide-text">
                            <h1 class="css-marginT30 css-oswaldextralight">JUEGOS CLÁSICOS</h1>
                            <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et ligula gravida, accumsan est sed, porttitor mi. In efficitur cursus ullamcorper. Curabitur libero est, varius in eleifend id, maximus in quam.</p>
                        </div>
                        <!-- <img src="images/slider/slide-2.jpg" class="img-responsive pull-right" alt="slider image"> -->
                        <div class="css-banner" style="background-image: url('images/slider/slide-2.jpg');"></div>
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>

        <section class="css-products-list">
            <div class="container">
                <div class="col-md-8 pull-right">
                    <div class="col-md-6">
                        <p class="css-text-gray css-fontSize12">JUEGOS CLÁSICOS Y MAGNÉTICOS</p>
                    </div>

                    <div class="col-md-6 pull-right">
                        <img src="images/download-icon.png" class="pull-right" alt="">
                        <div class="pull-right css-fontSize12 text-right css-text-red css-marginR10 css-line-height-1 css-paddingT10">
                            DESCARGA EL <br> CATÁLOGO EN PDF
                        </div>
                    </div>
                </div>

                <div class="clearfix css-marginB20"></div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="list-group">
                                <a href="#" class="list-group-item text-right">BINGOS</a>
                                <a href="#" class="list-group-item text-right">TABLEROS</a>
                                <a href="#" class="list-group-item text-right">MAGNÉTICOS</a>
                                <a href="#" class="list-group-item text-right">DOMINÓS</a>
                                <a href="#" class="list-group-item text-right">ACCESORIOS</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <a href="product-detail.php">
                            <div class="col-md-6 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <img src="images/products/1.jpg" class="img-responsive css-noFloat center-block" alt="">

                                <div class="pull-left">
                                    Parchis Magnético <i class="fa fa-share-alt-square"></i>
                                </div>

                                <div class="css-tag-yellow pull-right">
                                    NOVEDAD
                                </div>
                            </div>
                        </a>

                        <a href="product-detail.php">
                            <div class="col-md-6 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <img src="images/products/2.jpg" class="img-responsive css-noFloat center-block" alt="">

                                <div class="pull-left">
                                    Damas Magnético <i class="fa fa-share-alt-square"></i>
                                </div>

                                <div class="css-tag-yellow pull-right">
                                    NOVEDAD
                                </div>
                            </div>
                        </a>

                        <a href="product-detail.php">
                            <div class="col-md-6 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <img src="images/products/3.jpg" class="img-responsive css-noFloat center-block" alt="">

                                <div class="pull-left">
                                    Ajedrez Magnético <i class="fa fa-share-alt-square"></i>
                                </div>
                            </div>
                        </a>

                        <a href="product-detail.php">
                            <div class="col-md-6 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <img src="images/products/4.jpg" class="img-responsive css-noFloat center-block" alt="">

                                <div class="pull-left">
                                    Oca Magnético <i class="fa fa-share-alt-square"></i>
                                </div>
                            </div>
                        </a>

                        <div class="blog-pagination">
                            <ul class="pagination">
                                <li><a href="#">left</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li ><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li><a href="#">8</a></li>
                                <li><a href="#">9</a></li>
                                <li><a href="#">right</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="css-categories-nav">
            <div class="container">
                <ul class="css-noPadding">
                    <li><a href="#">MAGIA <i class="fa fa-magic"></i></a></li>
                    <li><a href="#">ARTÍSTICOS <i class="fa fa-paint-brush"></i></a></li>
                    <li><a href="#">HABILIDAD <i class="fa fa-bullseye"></i></a></li>
                    <li><a href="#">JUEGOS DE MESA <i class="fa fa-cube"></i></a></li>
                    <li><a href="#">CLÁSICOS <i class="fa fa-puzzle-piece"></i></a></li>
                </ul>
            </div>
        </div>

        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
