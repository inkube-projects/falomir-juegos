<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Inicio</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="plugins/slick-1.8.0/slick/slick.css" rel="stylesheet">
        <link href="plugins/slick-1.8.0/slick/slick-theme.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->

        <section id="home-slider">
            <div id="carousel-home" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-home" data-slide-to="1" class=""></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="css-slide-image" style="background-image: url('http://falomir.inkube.net/falomir.com/views/images/slider/slide-1.jpg')"></div>
                        <div class="carousel-caption">
                            <h2 class="css-oswaldextralight">APRENDE A CONSTRUIR LO IMPOSIBLE</h2>
                             <p class="txtSlider">¿Una silla para sentarse? ¡no! las sillas son para divertirse, apílalas, encájalas en cualquier dirección y sobre todo ¡No las dejes caer!</p>
                            <a href="#" class="btn btn-big btn-yellow css-marginT20">NOVEDAD</a>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="css-slide-image" style="background-image: url('http://falomir.inkube.net/falomir.com/views/images/slider/slide-2.jpg')"></div>
                        <div class="carousel-caption">
                            <h2 class="css-oswaldextralight">CLÁSICOS 3.0</h2>
                            <p class="txtSlider">El bingo es uno de esos juegos de “toda la vida” que adoramos y con nuestra nueva App SMART BINGO no se nos pasará ni un número.</p>
                             <a href="#" class="btn btn-big btn-yellow css-marginT20">NOVEDAD</a>
                        </div>
                    </div> -->
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-home" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-home" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>


        </section>

        <section id="features">
            <div class="container">
                <div class="row css-paddingR100 css-paddingL100">
                    <div class="col-sm-6 wow fadeInLeft css-noPadding" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb" style="background-image: url('images/novelty/1.jpg')">
                                    <div class="post-overlay css-post-left">
                                        <span class="uppercase">
                                            <a href="#">
                                                <small class="css-oswaldregular">Novedad</small>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 wow fadeInRight css-noPadding" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb" style="background-image: url('images/novelty/2.jpg')">
                                    <div class="post-overlay css-post-right">
                                        <span class="uppercase">
                                            <a href="#">
                                                <small class="css-oswaldregular">Novedad</small>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 wow fadeInRight css-noPadding" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb" style="background-image: url('images/novelty/3.jpg')">
                                    <div class="post-overlay css-post-left-bottom">
                                        <span class="uppercase">
                                            <a href="#">
                                                <small class="css-oswaldregular">Novedad</small>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 wow fadeInRight css-noPadding" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="single-blog">
                            <div class="single-blog-wrapper">
                                <div class="post-thumb" style="background-image: url('images/novelty/4.jpg')">
                                    <div class="post-overlay css-post-right-bottom">
                                        <span class="uppercase">
                                            <a href="#">
                                                <small class="css-oswaldregular">Novedad</small>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="posts">
            <div class="container">
                <!--<div class="content-head text-center">
                    <h3 class="css-marginB40 css-bold css-text-blue">DEJA QUE JUGAR TE ENSEÑE</h3>
                </div>-->
                <div class="row css-paddingR100 css-paddingL100">
                    <div class="col-md-8 css-news-image" style="background-image: url('images/posts/1.jpg')">
                        <h3 class="css-fontSize25">APRENDER JUGANDO</h3>
                        <a href="#" class="css-fontSize16">+ LEER MAS</a>
                    </div>
                    <div class="col-md-4 css-news-info">
                        <h3>TU HIJO Y EL JUEGO</h3>
                        <p>Aprender jugando es más que una filosofía de marca, es una filosofía de vida, por eso hemos creado este blog con artículos pedagógicos y los mejores consejos para que los más peques aprendan jugando.</p>
                        <a href="#" class="css-fontSize16 css-text-white">+ LEER MAS</a>
                    </div>

                    <div class="col-md-4 css-news-image-thumb" style="background-image: url('images/posts/2.jpg')"></div>
                    <div class="col-md-4 css-news-info" style="height: 390px;">
                        <h3>TU HIJO Y EL JUEGO</h3>
                        <p>Was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, becauwas</p>
                        <a href="#" class="css-fontSize16 css-text-white">+ LEER MAS</a>
                    </div>
                    <div class="col-md-4 css-news-image-thumb" style="background-image: url('images/posts/3.jpg')"></div>
                </div>
            </div>
        </section>

        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="plugins/slick-1.8.0/slick/slick.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1480;
        </script>
    </body>
</html>
