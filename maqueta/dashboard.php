<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>Falomir Juegos | Área de clientes</title>

      <link rel="shortcut icon" href="images/ico/favicon.ico">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

      <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/lightbox.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link href="css/css-style.css" rel="stylesheet">

      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->

      <style media="screen">
         .css-post-image {
            background-position: center;
         }
      </style>

   </head><!--/head-->

   <body>
      <? include('includes/header.php'); ?>
      <!--/#header-->

      <section class="css-dashboard">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="css-space"></div>

                  <div class="list-group">
                     <a href="#" class="list-group-item active">DASHBOARD</a>
                     <a href="#" class="list-group-item">USUARIOS</a>
                     <a href="#" class="list-group-item">CATÁLOGO</a>
                     <a href="#" class="list-group-item">PEDIDOS</a>
                     <a href="#" class="list-group-item">LOGÍSTICA</a>
                  </div>
               </div>

               <div class="col-md-8">
                  <ol class="breadcrumb">
                     <li><a href="#">Acceso clientes</a></li>
                     <li class="active">Dashboard</li>
                  </ol>

                  <div class="row css-marginB40">
                     <div class="col-md-4">
                        <div class="input-group">
                           <input type="text" class="form-control" placeholder="Búsqueda">

                           <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                 <i class="fa fa-search"></i>
                              </button>
                           </span>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="input-group">
                           <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                 <i class="fa fa-user"></i>
                              </button>
                           </span>

                           <select class="form-control" name="advanced_search" id="advanced_search">
                              <option value="">Búsqueda avanzada</option>
                              <option value="1">Opción 1</option>
                              <option value="2">Opción 2</option>
                              <option value="3">Opción 3</option>
                              <option value="4">Opción 4</option>
                              <option value="5">Opción 5</option>
                           </select>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="input-group">
                           <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                 <i class="fa fa-user"></i>
                              </button>
                           </span>

                           <select class="form-control" name="names" id="names">
                              <option value="">Nombres y apellidos</option>
                              <option value="1">Opción 1</option>
                              <option value="2">Opción 2</option>
                              <option value="3">Opción 3</option>
                              <option value="4">Opción 4</option>
                              <option value="5">Opción 5</option>
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="css-dash-panel">

                  </div>

               </div>
            </div>
         </div>
      </section>

      <? include('includes/footer.php'); ?>
      <!--/#footer-->

      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/lightbox.min.js"></script>
      <script type="text/javascript" src="js/wow.min.js"></script>
      <script type="text/javascript" src="js/main.js"></script>
      <script type="text/javascript">
         var scrollVal = 1350;
      </script>
   </body>
</html>
