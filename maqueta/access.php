<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>Falomir Juegos | Área de clientes</title>

      <link rel="shortcut icon" href="images/ico/favicon.ico">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

      <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
      <link href="css/animate.min.css" rel="stylesheet">
      <link href="css/lightbox.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link href="css/css-style.css" rel="stylesheet">

      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->

      <style media="screen">
         .css-post-image {
            background-position: center;
         }
      </style>

   </head><!--/head-->

   <body>
      <? include('includes/header.php'); ?>
      <!--/#header-->

      <section class="css-access">
         <div class="container">
            <div class="col-md-5 css-noFloat center-block text-center">
               <h2 class="css-text-gray">ACCESSO AL ÁREA DE CLIENTES</h2>
               <p class="css-text-gray">Falomir Juegos to you how all this mistaken idea of denouncing pleasure <br> and praising pain was born and iwill give you a complete account of the</p>

               <div class="form-group">
                  <form name="frm-login" id="frm-login" action="">
                     <div class="form-group css-marginT40">
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-user"></i></div>
                           <select class="form-control" name="user_type" id="user_type">
                              <option value="">Tipo de usuario</option>
                              <option value="1">Opcion 1</option>
                              <option value="2">Opcion 2</option>
                              <option value="3">Opcion 3</option>
                              <option value="4">Opcion 4</option>
                              <option value="5">Opcion 5</option>
                           </select>
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-user"></i></div>
                           <select class="form-control" name="user_level" id="user_level">
                              <option value="">Niveles de usuario</option>
                              <option value="1">Opcion 1</option>
                              <option value="2">Opcion 2</option>
                              <option value="3">Opcion 3</option>
                              <option value="4">Opcion 4</option>
                              <option value="5">Opcion 5</option>
                           </select>
                        </div>
                     </div>

                     <button type="submit" class="btn btn-orange pull-left">ENTRAR</button>

                     <div class="checkbox pull-right css-text-gray">
                        <label>
                           <input type="checkbox"> Recordarme
                        </label>
                     </div>
                     <div class="clearfix"></div>
                     <a href="#" class="pull-right css-text-gray">Recuperar contraseña</a>
                  </form>
               </div>
            </div>
         </div>
      </section>

      <? include('includes/footer.php'); ?>
      <!--/#footer-->

      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/lightbox.min.js"></script>
      <script type="text/javascript" src="js/wow.min.js"></script>
      <script type="text/javascript" src="js/main.js"></script>
      <script type="text/javascript">
         var scrollVal = 1350;
      </script>
   </body>
</html>
