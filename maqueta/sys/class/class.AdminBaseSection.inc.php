<?php 


  class AdminBaseSection{
	
	protected static $dbh;
	// leguaje de referencia en la que obligatoriamente ha de existir toda entrada, en los otros idiomas es opcional
    public static $mainLanguageId=1; // español
	
	public function __construct($db){}
	
	
	/* FUNCIONES PARA DATATABLES plugin de jquery para el tratamiento de tablas */
	/**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
	static function pluck ( $a, $prop )
	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
	}
	
	/**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL order by clause
	 */
	static function order ( $request, $columns )
	{
		$order = '';
		if ( isset($request['order']) && count($request['order']) ) {
			$orderBy = array();
			$dtColumns = self::pluck( $columns, 'dt' );
			for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
				// Convert the column index into the column data property
				$columnIdx = intval($request['order'][$i]['column']);
				$requestColumn = $request['columns'][$columnIdx];
				$columnIdx = array_search( $requestColumn['data'], $dtColumns );
				$column = $columns[ $columnIdx ];
				if ( $requestColumn['orderable'] == 'true' ) {
					$dir = $request['order'][$i]['dir'] === 'asc' ?
						'ASC' :
						'DESC';
					$orderBy[] = '`'.$column['db'].'` '.$dir;
				}
			}
			$order = ' ORDER BY '.implode(', ', $orderBy);
		}
		return $order;
	}
	   
	  
	 /**
	 * Create the data output array for the DataTables rows
	 *
	 *  @param  array $columns Column information array
	 *  @param  array $data    Data from the SQL get
	 *  @return array          Formatted data in a row based format
	 */
	static function data_output ( $columns, $data )
	{
		$out = array();
		
		
		for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
			$row = array();
			for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
				$column = $columns[$j];
				// Is there a formatter?
				if ( isset( $column['formatter'] ) ) {
					$row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
				}
				else {
					$row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
				}
			}
			$out[] = $row;
		}
		return $out;
	}
	
	
	
	// SECTIONS GENERAL FUNCTIONS 
	
	
	static function loadLanguages($db){
		
		if( !isset( $_SESSION['languages'] ) ) $_SESSION['languages'] = $db->query(" SELECT id, language FROM languages ORDER BY id ASC ");
		
		return $_SESSION['languages'];
		
		}
	
		
		
	static public function issetor(&$var, $default = '') { return isset($var) ? $var : $default;}
	
	// pasar fecha de formato español a inglés
	static public function traslateDate($d) { 
	  $d = preg_replace("([^0-9/-])", "", $d); // evitar inyección, sólo aceptamos dígitos y -/
	  return implode('-', array_reverse(explode('-', $d)));
	}
	
	
	// DELETE item 
	
  public function deleteItemLanguageImage($table,$hasLanguage=true){
		  $resArr=array();
		  if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){

			    self::$dbh->query("DELETE FROM ".$table."s WHERE id=%i ", $_POST['id']);
		        $counter =self::$dbh->affectedRows();
				if($counter){
					if($hasLanguage)self::$dbh->query("DELETE FROM ".$table."_language WHERE ".$table."Id=%i ", $_POST['id']);
					self::$dbh->query("DELETE FROM archivoAsociado WHERE seccion=%s AND idB=%i ",$this->sectionId, $_POST['id']);
					}
		       $resArr["resultado"]=$counter; 
				
			
	self::$dbh->disconnect();	
	echo json_encode($resArr);
			  
			  }
		 
		} 
		
	
	
	
	} // final clase



?>