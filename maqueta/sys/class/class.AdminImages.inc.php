<?php

class AdminImages {
	
	
  /**
   * @var \PDO Database handler
   */
  private static $dbh;
  
 
 /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
        self::$dbh=$db;
       
    }
	
	
	
	
	public function listImages(){
		
		$ruta=URL."/public/assets/images/".$_POST['sectionName'];
		$ds="/";
		
		if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['id']) ){
			
			$query="SELECT a.nombre, a.alt, a.id, aa.portada FROM `archivos` a LEFT JOIN archivoAsociado aa ON aa.idArchivo = a.id WHERE a.id IN ( SELECT idArchivo FROM archivoAsociado WHERE seccion=%s AND idB=%i )";
    
   		// CONSULTA
		$archivos = self::$dbh->query($query,'['.$_POST['sectionId'].']',intval($_POST['id']));
	
	    $result  = array();
	
		foreach ( $archivos as $row) {	
		// llamado desde redactor
		if( isset($_POST['origen']) && $_POST['origen']=='redactor' ){
			$obj = (object)array('thumb'=>$ruta."/thumbs/".$row['nombre'], 'image'=>$ruta."/".$row['nombre'], 'title'=>"", 'id'=>$row['id']); 
			}
			// llamado desde uploader
			else { 
			$obj=array();
		    $obj['name'] = $row['nombre'];
			$obj['id'] = $row['id'];
			$obj['portada'] = $row['portada'];
			$obj['alt'] = $row['alt'];
            $obj['size'] = $this->file_get_size('../../../../public/assets/images/'.$_POST['sectionName'].$ds.$row['nombre']);
			}
			
            $result[] = $obj;
		
		}
			
			
			return json_encode($result);
			
		}
		
		
	}
	
	
	public function updateImage(){
		
		if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
			
			
			$resp=0;
			$idImg=intval($_POST['idImg']);
		
			
			if( $idImg>0 ){
				
				if(isset($_POST['alt'])){
					
					$query="UPDATE archivos SET alt=%s WHERE id=%i";
					
					// CONSULTA
					$updateAlt = self::$dbh->query($query,$_POST['alt'],$idImg);
					self::$dbh->disconnect();	
					
					if($updateAlt) $resp=1;
						
				}
				
				if(isset($_POST['portada'])){
						 
						$query="UPDATE archivoAsociado SET portada=%i WHERE idB=%i AND idArchivo=%i";
						
						// CONSULTA
						$updateAlt =  self::$dbh->query($query,$_POST['portada'],$_POST['id'],$idImg);
						self::$dbh->disconnect();	
						
						if($updateAlt) $resp=1;
				}
				
				
				
				
				if(isset($_POST['nombre'])){
		
				$ruta_thumb_nuevo = "../../../..".$_POST['rutanombre']."/".$_POST['nombre'].".".$_POST['extnombre'];
				$ruta_thumb_previo = "../../../..".$_POST['rutanombre']."/".$_POST['nombreprevio'].".".$_POST['extnombre'];
				
				
				if (file_exists($ruta_thumb_nuevo)) {
						$resp=666; // ya existe un archivo con ese nombre
				} else {
						   
						  //renombramos thumbs
						 if(rename($ruta_thumb_previo, $ruta_thumb_nuevo)){
							 
							 $ruta_big_nuevo=str_replace("thumbs/", "", $ruta_thumb_nuevo);
							 $ruta_big_previo=str_replace("thumbs/", "", $ruta_thumb_previo);
							 
							  if(rename($ruta_big_previo, $ruta_big_nuevo)){
			 
									$query="UPDATE archivos SET nombre=%s WHERE id=%i";
			
									// CONSULTA
									$newName=$_POST['nombre'].".".$_POST['extnombre'];
									$updateAlt = self::$dbh->query($query,$newName,$idImg);
									
									 if($updateAlt) $resp=1;
							
									self::$dbh->disconnect();	
									
								  
							  } // renombrados grandes
							 
						 } // renombrado thumb
						 else {
							 $resp="no-rename";
							 }
				}	 // no existe el archivo	
		
		
		
		} // isset($_POST['nombre']
			
				
	} // $idImg>0
			
			
			return $resp;
			
		}
		
		
	}
		
 
 public function deleteImage(){
	 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token']  ){
		 
		$idImg=intval($_REQUEST['idImg']);
		$idA=intval($_REQUEST['id']);

		if( $idImg>0 ){
			   
			$query="DELETE from archivoAsociado WHERE idArchivo=%i && idB=%i";
			
			// CONSULTA
			$eliminado = self::$dbh->query($query,$idImg,$idA);
			self::$dbh->disconnect();	
			
			   if($eliminado) return "1";
				
				}
					
				 
		}
	}
	
	
	
private function file_get_size($file) {
    //open file
    $fh = fopen($file, "r");
    //declare some variables
    $size = "0";
    $char = "";
    //set file pointer to 0; I'm a little bit paranoid, you can remove this
    fseek($fh, 0, SEEK_SET);
    //set multiplicator to zero
    $count = 0;
    while (true) {
        //jump 1 MB forward in file
        fseek($fh, 1048576, SEEK_CUR);
        //check if we actually left the file
        if (($char = fgetc($fh)) !== false) {
            //if not, go on
            $count ++;
        } else {
            //else jump back where we were before leaving and exit loop
            fseek($fh, -1048576, SEEK_CUR);
            break;
        }
    }
    //we could make $count jumps, so the file is at least $count * 1.000001 MB large
    //1048577 because we jump 1 MB and fgetc goes 1 B forward too
    $size = bcmul("1048577", $count);
    //now count the last few bytes; they're always less than 1048576 so it's quite fast
    $fine = 0;
    while(false !== ($char = fgetc($fh))) {
        $fine ++;
    }
    //and add them
    $size = bcadd($size, $fine);
    fclose($fh);
    return $size;
}
	
	
	
} // final clase


?> 



