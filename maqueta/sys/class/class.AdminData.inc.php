<?php

class AdminData {
	
	/**
   * @var boolean Is company logged in
   */
  public static $loggedIn = false;

  /**
   * @var int|boolean Company ID
   */
  public static $companyID = "";

  /**
   * @var \PDO Database handler
   */
  private $dbh;
  /**
   * @var int|boolean Company ID
   */
  public static $filterCompany = false;

  
 
 /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db,$companyCode=false){
        $this->dbh=$db;
        if($companyCode!=false)$this->getInfoCompany($companyCode);
    
    }
	
	
	
	private function getInfoCompany($companyCode){
		  $companyDetail = $this->dbh->queryFirstRow("SELECT * FROM empresas WHERE codigo = %s",$companyCode);
		  if( is_null( $companyDetail) ) $_SESSION['company']['id']='error';
		  else{
			self::$companyID=$companyDetail['id'];
         	$_SESSION['company']['id']= $companyDetail['id'];
		  	$_SESSION['company']['name']= $companyDetail['nombre'];
			// para filtrar resultados búsquedas cuando el logueado sea el superadministrador puede modificar este valor
			$_SESSION['filters']['companyFilter']['id']=$companyDetail['id'];
	        $_SESSION['filters']['companyFilter']['code']=$companyDetail['codigo'];
			
		  }
		}
	
	
	/**
	* Super admin puede cambiar la web sobre la que ve los resultados, quedando reflejado en 
	* $_SESSION['filters']['company']
	*/
	public function changeCompanyFilter(){
		if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['code']) && isset($_POST['id']) ){
			
			$_SESSION['filters']['companyFilter']['id']=intval($_POST['id']);
	        $_SESSION['filters']['companyFilter']['code']=$_POST['code'];
			
			$msg = array("Success", "Filter company Changed Successfully");
			return json_encode($msg);
		}
		
	}
	
	
	
	
	} // final clase


?> 



