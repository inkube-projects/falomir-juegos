<?php

   class AdminProducts extends AdminBaseSection  {
	 
	
	/**
   * @var Meekrodb Database handler
   */   
  
   private static $idProduct;
   
   private $productsCampos;
   
   private  $columns;
   
   protected $sectionId='[1.1]';
   
	   
   /**
   * Intialize
   * @param array $db database
   */
   public function __construct($db){
        self::$dbh=$db;
		
		$this->columns = array(
			array( 'db' => 'id', 'dt' => 'DT_RowId' ),
			array( 'db' => 'codigo', 'dt' => 'web',
				   'formatter' => function( $d, $row ) {
					return '<div class="navbar-messages__avatar">
									<img  src="public/admin/img/empresa_'.$d.'.png">
							</div>';
				}
			 ),
			array( 'db' => 'ref',  'dt' => 'referencia' ),
			array( 'db' => 'model',  'dt' => 'modelo' ),
			array( 'db' => 'categoryName',  'dt' => 'categoria' ),
			array(
				'db'        => 'active',
				'dt'        => 'active',
				'formatter' => function( $d, $row ) {
					if($d==1){
					return '<div class="table__status online"><i class="fa fa-circle-o"></i>activa</div>';
					} else {
						return '<div class="table__status offline"><i class="fa fa-circle-o"></i>inactiva</div>';
						}
				}
			)
		);
		
     }
	
	 
	  
	public function listProducts(){
		
		if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['start'])  && isset($_POST['brandId'])  ){
			
		//self::$dbh->debugMode();
			
		$order = self::order( $_POST, $this->columns );
		$busqueda=$_POST['search']['value'];
		$totalTextFilter="";
		// WHERE
		$where = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
		
		// filtramos por marca
		$where->add('p.brandId=%i', $_POST['brandId']);
		$totalTextFilter=" AND p.brandId=".intval($_POST['brandId']);
			
		$where->add('pl.languageId=%i', self::$mainLanguageId);
		
		if(strlen($busqueda)>0){ 
		
		   $subclause = $where->addClause('or'); // add a sub-clause with ORs
		   $subclause->add('pl.model LIKE %ss', $busqueda); 
		   $subclause->add('cl.name LIKE %ss', $busqueda); 
		   $subclause->add('pl.ref LIKE %ss', $busqueda); 
		  
		   }
			
		$recordsTotal = self::$dbh->queryFirstField("SELECT COUNT(p.id) FROM products p INNER JOIN product_language pl ON pl.productId=p.id WHERE pl.languageId=%i".$totalTextFilter,self::$mainLanguageId);
		
		$listado = self::$dbh->query("Select p.brandId AS codigo, pl.ref, pl.model, p.active, p.id, cl.name AS categoryName FROM products p INNER JOIN product_language pl ON pl.productId=p.id  LEFT JOIN categoryLanguages cl ON cl.catId=p.categoryId AND cl.languageId=%i0 WHERE %l3 ".$order." LIMIT %i1,%i2",self::$mainLanguageId,$_POST['start'],$_POST['length'],$where);
		$recordsFiltered = (strlen($busqueda)<=0) ? $recordsTotal : self::$dbh->count();
		 
		 if(!isset($_POST['isJs'])) return $listado;
		 else {	 
			 
		 return json_encode(array(
			"draw"            => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) :	0,
			"recordsTotal"    => intval( $recordsTotal ),
			"recordsFiltered" => intval( $recordsFiltered ),
			"data"            => self::data_output( $this->columns, $listado )
	     	));
			 
			 
			 }
			 
			 
		} // existe token y es correcto
		
	} 
		
	/*
	* Leer detalle de etiqueta
	* @param  Meekrodb object  $sb    
	* @param  int  $id  
	* Se llama a esta función al cargar la página y leer por primera vez el detalle siempre en el idioma principal
	* 	 AdminProduct::productDetail($dbo,$_REQUEST['idL']);
	*/	
		
	public static function productDetail($db,$id){
		if(intval($id)>0){
		self::$dbh=$db;
		self::$idProduct = $id;
		$detail = self::$dbh->queryFirstRow("SELECT p.id, pl.id AS idLL, pl.ref, pl.title, pl.model, pl.description, pl.especifications, pl.metaTitle, pl.metaKeywords, pl.metaDescription, pl.video1, pl.video2,  pl.languageId, p.active, p.featured, p.madeInUsa, p.categoryId  FROM products p INNER JOIN product_language pl ON pl.productId=p.id WHERE p.id=%i0 AND pl.languageId=%i1",$id,self::$mainLanguageId);
		return $detail;
		}
		
	}
	  
	  
	 public function languageProduct(){
		 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['languageId']) ){
			 $resArr["resultado"]=array();
			 $detail = self::$dbh->queryFirstRow("SELECT id, title, ref, model, description, especifications, video1, video2, metaTitle, metaKeywords, metaDescription FROM product_language WHERE productId=%i0 AND languageId=%i1",$_POST['id'],$_POST['languageId']);
			 if(is_null($detail)) $resArr["resultado"]=0;
			 else {
				 $resArr["resultado"]=1;
				 $resArr["data"]=$detail;
				 }
			 
			 self::$dbh->disconnect();	
			 echo json_encode($resArr);
			 }
		 }
	   
	   
	 public function addProduct(){
		 if(isset($_POST['token']) && $_POST['token']==$_SESSION['token'] && isset($_POST['language']) ){
			 
			 if( isset($_POST['id']) && intval($_POST['id'])==0 && $_POST['language']==self::$mainLanguageId){
				  self::$dbh->insert('products', array(
				    'brandId' => $_POST['brandId'],
					'categoryId' => $_POST['category'],
					'active' => isset($_POST['activa']) ? 1 : 0,
					'featured' => isset($_POST['activa']) ? 1 : 0,
					'madeInUsa' => isset($_POST['madeInUsa']) ? 1 : 0
					));
					
					$_POST['id'] = self::$dbh->insertId(); 
				 
				 
				 }
			 
			 
			 if( isset($_POST['id']) && intval($_POST['id'])>0 ){ // ya existe entrada en español sólo añadir language
			 
				 self::$dbh->insert('product_language', array(
 							 'productId' => intval($_POST['id']),
  							 'languageId' => intval($_POST['language'])
					));
					
					$this->updateProduct($_POST['id']);
				 
				 }
			 
		 }
	 
		} 
	 
	 
	  public function updateProduct($addedNew=0){
	     if( isset($_POST['token']) && $_POST['token']==$_SESSION['token'] ){
			 
			  $this->productsCampos=array(
					'title' => $_POST['title'],
					'model' => $_POST['model'],
					'ref' => $_POST['ref'],
					'video1' => $_POST['video1'],
					'video2' => $_POST['video2'],
					'description' => $_POST['description'],
					'especifications' => $_POST['especifications'],
					'metaTitle' => $_POST['metaTitle'],
					'metaDescription' => $_POST['metaDescription'],
					'metaKeywords' => $_POST['metaKeywords']
					);
			 
			 $resArr=array();
			 
			 $where = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
			 $where->add('productId=%i', $_POST['id']);
			 $where->add('languageId=%i', $_POST['language']);
			 
			 self::$dbh->update('product_language', $this->productsCampos, '%l', $where);
				
			$counter = self::$dbh->affectedRows();
			
			// si fuera nueva ya he añadido antes en la función addProduct. 	
			if($addedNew==0){
				self::$dbh->update('products', array(
						'categoryId' => $_POST['category'],
						'active' => isset($_POST['activa']) ? 1 : 0,
						'featured' => isset($_POST['activa']) ? 1 : 0,
						'madeInUsa' => isset($_POST['madeInUsa']) ? 1 : 0
						), "id=%i ", $_POST['id']);
				
				$counter2 = self::$dbh->affectedRows();
			} else { $counter2=0; }
					
		 $resArr["resultado"] = ($counter || $counter2)  ? 1 : 0; 
		 // si he añadido alguna nueva paso id a detalle
		 if($addedNew>0) $resArr["id"] = $addedNew;
			
	self::$dbh->disconnect();	
	echo json_encode($resArr);
			 
			 }
		} 
	 
	   
	 
	 public function deleteProduct(){
		  self::deleteItemLanguageImage('product');
	
		  
		} 
		
	/*
	* Obtener id de idiomas en los que está dado de alta la etiqueta para mostrar un check en el selector de idioma
	* @param  int  $id  
	*/		
	static function languagesFilled ($id){
	   return self::$dbh->queryFirstColumn("SELECT DISTINCT(languageId) FROM product_language WHERE productId=%i",$id);
	 }
		
	static function loadCategories ($db,$brandId){
	   return $db->query("SELECT bc.categoryId AS id, cl.name as nombre FROM brand_categories bc LEFT JOIN categoryLanguages cl ON  cl.catId=bc.categoryId AND cl.languageId=%i1 WHERE brandId=%i0",$brandId,self::$mainLanguageId);
	 }
	   
	   
	} // class end



?> 