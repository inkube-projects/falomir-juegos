<?php

//declare(strict_types=1); php 7

/*
 * Create an empty array to store constants
 */
$C = array();
$C['URL'] = 'http://falomir.inkube.net/';
$C['URLADMIN'] = 'http://falomir.inkube.net/public/admin/';
$C['URLPUBLIC'] = 'http://falomir.inkube.net/public/';

$C['MAINLANGUAGE'] = '1'; // español

/*
 * The database host URL
 */
$C['DB_HOST'] = 'localhost';

/*
 * The database username
 */
$C['DB_USER'] = 'falomirUserAppli';

/*
 * The database password
 */
$C['DB_PASS'] = 'falomir_1nkub3Appli';

/*
 * The name of the database to work with
 */
$C['DB_NAME'] = 'falomir_WebAppli';

/*
 * The codification of the database to work with
 */
$C['DB_ENCODING'] = 'utf8';
/*
 * The port of the database to work with
 */
$C['DB_PORT'] = null;
/*
Tables for web users login system 
*/

$C['DB_USER_TABLE'] = 'users';
$C['DB_USER_TOKEN_TABLE'] = 'user_tokens';
$C['DB_USER_DEVICES_TABLE'] = 'user_devices';
/*
Tables for admin users login system 
*/
$C['DB_USER_ADMIN_TABLE'] = 'users_admin';
$C['DB_USER_ADMIN_TOKEN_TABLE'] = 'user_admin_tokens';
$C['DB_USER_ADMIN_DEVICES_TABLE'] = 'user_admin_devices';

?>