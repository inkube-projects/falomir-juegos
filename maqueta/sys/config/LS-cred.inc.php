<?php

$LS = new LS(array(
  "db" => array(
    "host" => DB_HOST,
    "port" => 3306,
    "username" => DB_USER,
    "password" => DB_PASS,
    "name" => DB_NAME,
    "table" => DB_USER_TABLE,
	"token_table" => DB_USER_TOKEN_TABLE,
	"connection" => $dbo,
	"columns" =>array(
   		 "username" => "email"
 	 )
  ),
  "features" => array(
    "auto_init" => true
  ),
  "pages" => array(
    "no_login" => array(
      "/public/",
      "/public/reset.php",
      "/public/register_user_web.php"
    ),
    "everyone" => array(
      "/public/status.php"
    ),
	 "two_step_login" => array(
      "devices_table" => DB_USER_DEVICES_TABLE
    ),
    "login_page" => "/public/login.php",
    "home_page" => "/public/home.php"
  )
));

?>