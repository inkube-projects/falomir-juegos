<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Contacto</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->
        <section class="css-contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="css-oswaldextralight">BIENVENIDOS</h2>
                    <p>Si necesitas contactar con nosotros por algún motivo relacionado con nuestros productos, por favor, hazlo cumplimentando el siguiente formulario.<br>
Deberás completar el código de seguridad situado en la zona inferior para poder realizar el envío.</p><br><br>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div id="gmap"></div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombre">
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Correo electrónico">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Apellido(s)">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="message" id="message" rows="20" placeholder="Escriba aquí"></textarea>
                        </div>
                        
                        <div class="checkbox">
                        <label>
                           <input required="" type="checkbox" name="terms[]" id="terms-contact" value="1"> Soy mayor de 14 años y ACEPTO las condiciones establecidas en <a href="N#">Aviso Legal y Política de Privacidad</a>
                        </label>
                        <div class="text-danger" id="terms_validate"></div>
                     </div>
                     <p class="css-terms" style="font-size:11px; color:rgba(127,127,127,1.00); line-height:11px">Los datos de carácter personal que se faciliten mediante este formulario quedarán registrados para su tratamiento, del que el responsable es Juguetes Falomir, S.A. y serán utilizados con la finalidad de contestar a las consultas planteadas. La legitimación para llevar a cabo este tratamiento tiene su base en su consentimiento otorgado. Vd. puede ejercitar los derechos de acceso, rectificación, supresión (“derecho al olvido”), limitación, portabilidad y oposición dirigiéndose al responsable del fichero, conforme a lo establecido en los Arts. 15 y ss del RGPD-EU mediante escrito dirigido a la Calle Tuéjar, 43, 46183, Valencia . Los datos serán conservados durante el plazo estrictamente necesario para cumplir con la normativa mencionada.  Visite nuestro <a href="#">Aviso legal y política de privacidad</a></p>
                        
                        <img src="images/google.png" width="260" height="66" alt=""/>
                      <button type="submit" class="btn btn-orange pull-right">ENVIAR</button>
                    </div>
                </div>
            </div>
        </section>

        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
