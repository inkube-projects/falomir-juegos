<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Nuestros Juegos</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->

        <section id="home-slider" style="background-color: #99aed9;">
            <div class="container">
                <div class="row">
                    <div class="main-slider">
                        <div class="slide-text" style="margin-left:140px;">
                            <h1 class="css-oswaldextralight">CLÁSICOS 3.0</h1>
                            <p>El bingo es uno de esos juegos de “toda la vida” que adoramos y con nuestra nueva App SMART BINGO no se nos pasará ni un número.</p>
                            <a href="http://falomir.inkube.net/maqueta/products_clasicos.php" class="btn btn-big btn-purple">JUEGOS CLÁSICOS</a>
                        </div>
                        <img src="images/slider/slide-2B.jpg"  alt="slider image">
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>
        <!--/#home-slider-->

        <section class="posts css-categories">
            <div class="container">
                <div class="row css-paddingR100 css-paddingL100">
                    <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                        <div class="folio-item" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="folio-image css-post-image" style="background-image: url('images/categories/hability.jpg');">
                                <!-- <img class="img-responsive" src="images/posts/1.jpg" alt=""> -->
                                <div class="css-cat-title-left-yellow">
                                    HABILIDAD
                                </div>
                            </div>
                            <div class="overlay css-overlay-yellow">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <div class="css-overlay-title-left">HABILIDAD</div>
                                            <h3>Juegos de habilidad.</h3>
                                            <p class="css-justify"><br>Te encantará aprender así Potenciar la creatividad, la estrategia, aprender sobre estructuras, mejorar la precisión, la psicomotricidad y además ¡Pasarlo en grande! Si es que… los juegos de habilidad ¡Lo tienen todo!.
                                            </p>
                                            <div class="folio-overview">
                                            <span class="folio-link css-left"><a class="folio-read-more" href="http://falomir.inkube.net/maqueta/products_habilidad.php" data-single_url="portfolio-single.html">+ LEER MÁS</a></span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                        <div class="folio-item" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="folio-image css-post-image" style="background-image: url('images/categories/magic.jpg');">
                                <!-- <img class="img-responsive" src="images/posts/1.jpg" alt=""> -->
                                <div class="css-cat-title-right-green">
                                    MAGIA
                                </div>
                            </div>

                            <div class="overlay css-overlay-green">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <div class="css-overlay-title-right">MAGIA</div>
                                            <h3>Echale magia y dibi dibadi dibú</h3>
                                            <p class="css-justify">Crecer, aprender, practicar y mejorar es el sueño de cualquier pequeño mago y con nuestro nuevo canal de magia lo tendrá muy fácil. 
Descubre todos los trucos de magia de la mano del mago Rafa Tamarit y los juegos de magia de Falomir. 
</p>
                                             <div class="folio-overview">
                                            <span class="folio-link css-left"><a class="folio-read-more" href="http://falomir.inkube.net/maqueta/products_magia.php" data-single_url="portfolio-single.html">+ LEER MÁS</a></span>
                                        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row css-paddingR100 css-paddingL100">
                    <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                        <div class="folio-item" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="folio-image css-post-image" style="background-image: url('images/categories/table-games.jpg');">
                                <!-- <img class="img-responsive" src="images/posts/1.jpg" alt=""> -->
                                <div class="css-cat-title-left-red">
                                    JUEGOS DE MESA
                                </div>
                            </div>
                            <div class="overlay css-overlay-red">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <div class="css-overlay-title-left">JUEGOS DE MESA</div>
                                            <h3>Aprender y divertirse es lo mismo con los juegos de mesa</h3>
                                            <p class="css-justify">Desde siempre, se aprende a compartir alrededor de una mesa y si es con nuestros juegos se aprende mucho mejor. Crea tus estrategias y machaca a la familia pero siempre con deportividad.</p>
                                            <div class="folio-overview">
                                            <span class="folio-link css-left"><a class="folio-read-more" href="http://falomir.inkube.net/maqueta/products_mesa.php" data-single_url="portfolio-single.html">+ LEER MÁS</a></span>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-relative css-noPadding js-panel-hover">
                        <div class="folio-item" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="folio-image css-post-image" style="background-image: url('images/categories/clasics.jpg');">
                                <!-- <img class="img-responsive" src="images/posts/1.jpg" alt=""> -->
                                <div class="css-cat-title-right-blue">
                                    CLÁSICOS
                                </div>
                            </div>
                            <div class="overlay css-overlay-blue">
                                <div class="overlay-content">
                                    <div class="overlay-text">
                                        <div class="folio-info">
                                            <div class="css-overlay-title-right">CLÁSICOS</div>
                                            <h3>Juegos clásicos. Que la nostalgia te acompañe</h3>
                                            <p class="css-justify">Aprender a ganar con deportividad se practica toda la vida, tengas 5 o 100 años... 
Entrena tu habilidad, tu picardía o tu agilidad mental con los juegos más nostálgicos de nuestra colección.</p>
                                                                                    <div class="folio-overview">
                                            <span class="folio-link css-left"><a class="folio-read-more" href="http://falomir.inkube.net/maqueta/products_clasicos.php" data-single_url="portfolio-single.html">+ LEER MÁS</a></span>
                                        </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="css-categories-nav">
            <div class="container">
                <ul class="css-noPadding">
                    <li><a href="#">MAGIA <i class="fa fa-magic"></i></a></li>
                    <li><a href="#">ARTÍSTICOS <i class="fa fa-paint-brush"></i></a></li>
                    <li><a href="#">HABILIDAD <i class="fa fa-bullseye"></i></a></li>
                    <li><a href="#">JUEGOS DE MESA <i class="fa fa-cube"></i></a></li>
                    <li><a href="#">CLÁSICOS <i class="fa fa-puzzle-piece"></i></a></li>
                </ul>
            </div>
        </div>

        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
