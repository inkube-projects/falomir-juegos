<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Detalle de noticias</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body style="background-color: #e8eef3">
        <? include('includes/header.php'); ?>

        <section class="css-section-new">
            <div class="container">
                <div class="css-news-head-image" style="background-image: url('images/news/head.jpg')">
                    <div class="css-news-title">
                        FALOMIR JUEGOS
                    </div>
                </div>

                <div class="css-news-description">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum dolor eros, ullamcorper eget vulputate at, sollicitudin quis quam. Proin commodo nibh eu imperdiet rhoncus. Nam vitae tincidunt felis. Proin sit amet enim non tellus pharetra aliquam in in justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et porta sapien. Praesent non metus pulvinar, rhoncus sem ut, vehicula dui. Etiam nec sem consectetur, dignissim velit a, tincidunt nisl. Aliquam imperdiet lorem ut odio efficitur, id vulputate lacus hendrerit. Phasellus sit amet arcu non erat sollicitudin venenatis at ac massa. Aliquam id purus non enim efficitur congue. </p>

                    <p>Nulla eleifend nisi nec condimentum viverra. Nulla sollicitudin blandit sem in placerat. Duis et commodo turpis. Donec tincidunt, lectus eu scelerisque sollicitudin, felis metus pulvinar dolor, volutpat lobortis dui libero sit amet magna. Cras tempus ac lectus quis vulputate. Nullam eget neque ut neque efficitur posuere non ut dolor. Nunc magna nisi, ornare quis sapien sit amet, mattis feugiat arcu.</p>

                    <p>Nulla eleifend nisi nec condimentum viverra. Nulla sollicitudin blandit sem in placerat. Duis et commodo turpis. Donec tincidunt, lectus eu scelerisque sollicitudin, felis metus pulvinar dolor, volutpat lobortis dui libero sit amet magna. Cras tempus ac lectus quis vulputate. Nullam eget neque ut neque efficitur posuere non ut dolor. Nunc magna nisi, ornare quis sapien sit amet, mattis feugiat arcu.</p>

                    <p>Nunc bibendum ipsum vel libero tempus, vel venenatis odio scelerisque. Suspendisse non cursus lacus. Vestibulum lobortis porttitor orci eu fringilla. Ut commodo velit vitae lorem consectetur, eget placerat ipsum eleifend. Sed tristique ut elit quis placerat. Praesent posuere auctor dui, et molestie enim. Nulla quis tincidunt mauris. Pellentesque ex arcu, scelerisque et vestibulum eu, venenatis aliquam arcu. Donec tempus nibh nec augue tempor, at vulputate diam vestibulum. Praesent rhoncus, quam suscipit semper mattis, orci justo eleifend enim, id iaculis lectus augue at elit. Proin blandit vel turpis sit amet feugiat. Curabitur a vestibulum augue. Nam nec ante a tortor faucibus venenatis vitae id justo. Vivamus leo turpis, tempor et enim a, faucibus ultrices libero. Donec cursus luctus nibh, posuere tincidunt sem varius id. Suspendisse potenti.</p>

                    <p>Nunc bibendum ipsum vel libero tempus, vel venenatis odio scelerisque. Suspendisse non cursus lacus. Vestibulum lobortis porttitor orci eu fringilla. Ut commodo velit vitae lorem consectetur, eget placerat ipsum eleifend. Sed tristique ut elit quis placerat. Praesent posuere auctor dui, et molestie enim. Nulla quis tincidunt mauris. Pellentesque ex arcu, scelerisque et vestibulum eu, venenatis aliquam arcu. Donec tempus nibh nec augue tempor, at vulputate diam vestibulum. Praesent rhoncus, quam suscipit semper mattis, orci justo eleifend enim, id iaculis lectus augue at elit. Proin blandit vel turpis sit amet feugiat. Curabitur a vestibulum augue. Nam nec ante a tortor faucibus venenatis vitae id justo. Vivamus leo turpis, tempor et enim a, faucibus ultrices libero. Donec cursus luctus nibh, posuere tincidunt sem varius id. Suspendisse potenti.</p>
                </div>

                <div class="css-news-panel-1-footer">
                    <div class="social-icons">
                        <ul class="nav nav-pills pull-right" style="margin-top: 0; display: flex; justify-content: center;">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- //////////////////////////////////////////////////////////////////////// -->

                <div class="col-md-8 css-noPadding">
                    <div class="col-md-6 css-paddingL0 css-paddinR20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 1</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingR0 css-paddinL20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 2</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingL0 css-paddinR20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 3</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 css-paddingR0 css-paddinL20">
                        <div class="css-news-panel-1 css-news-panel-2">
                            <div class="css-image" style="background-image: url('images/news/1.jpg')">
                                <div class="css-new-description">
                                    <h3>NOTICIA 4</h3>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">BUSCAR</a>
                    <input type="text" class="form-control css-input-search css-marginB80">

                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">ENTRADAS RECIENTES</a>

                    <ul class="css-ul-recent-entries">
                        <li><a href="#">9 juegos de Lúdillo perfectos para este verano</a></li>
                        <li><a href="#">Los niños y niñas españoles juegancada vez menos</a></li>
                        <li><a href="#">Reciclando tus juegos también ayudas al medio ambiente</a></li>
                        <li><a href="#">¿Qué juegos elegir para un niño a partir de los 6 años?</a></li>
                        <li><a href="#">6 consejos para proteger a tus hijos en Internet</a></li>
                    </ul>

                    <a href="#" class="btn btn-aqua css-oswaldextralight css-marginB20" style="width: 100%;">CATEGORÍAS</a>

                    <ul class="css-ul-recent-entries">
                        <li><a href="#">JUEGOS Y JUGUETES</a></li>
                        <li><a href="#">LOS NIÑOS JUEGAN</a></li>
                        <li><a href="#">MANUALIDADES</a></li>
                        <li><a href="#">JUEGOS DE HABILIDAD</a></li>
                        <li><a href="#">TARDES DE JUEGOS</a></li>
                    </ul>
                </div>
            </div>
        </section>



        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAfoFMSNU6Vr-JaVW4BN86B_VGkCwowO_w&sensor=true"></script>
        <script type="text/javascript" src="js/gmaps.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
