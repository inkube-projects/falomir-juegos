<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Falomir Juegos | Parchís Magnético</title>

        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/css-style.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->

        <style media="screen">
            .css-post-image {
                background-position: center;
            }
        </style>
    </head><!--/head-->

    <body>
        <? include('includes/header.php'); ?>
        <!--/#header-->

        <section id="home-slider" class="css-products-banner" style="background-color: #99aed9;">
            <div class="container">
                <div class="">
                    <div class="main-slider">
                        <div class="slide-text">
                            <h1 class="css-marginT30 css-oswaldextralight">JUEGOS CLÁSICOS</h1>
                            <p class="css-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et ligula gravida, accumsan est sed, porttitor mi. In efficitur cursus ullamcorper. Curabitur libero est, varius in eleifend id, maximus in quam.</p>
                        </div>
                        <!-- <img src="images/slider/slide-2.jpg" class="img-responsive pull-right" alt="slider image"> -->
                        <div class="css-banner" style="background-image: url('images/slider/slide-2.jpg');"></div>
                    </div>
                </div>
            </div>
            <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
        </section>

        <section class="css-products-list">
            <div class="container">

                <div class="col-md-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="100ms">
                    <a href="#"><i class="fa fa-chevron-circle-left css-text-red css-fontSize21"></i></a>
                </div>

                <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="200ms">
                    <p class="css-text-gray css-fontSize12"><b>JUEGOS CLÁSICOS Y MAGNÉTICOS</b> / PARCHÍS</p>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <img src="images/download-icon-2.png" class="pull-right" alt="">
                    <div class="pull-right css-fontSize12 text-right css-text-red css-marginR10 css-line-height-1 css-paddingT10">
                        DESCARGA LA <br> FICHA EN PDF
                    </div>
                </div>

                <div class="clearfix css-marginB20"></div>

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Nh0RBWS-cRs"></iframe>
                </div>

                <div class="css-product-panel wow fadeInUp css-marginB0" data-wow-duration="1000ms" data-wow-delay="400ms">
                    <div class="col-md-5">
                        <p>
                            <b>BEZZERWIZZER</b> <br>
                            Edad + 16 / +2 Jugadores <br>
                            Ref. 26554
                        </p>

                        <p class="css-justify">En cada ronda deberás contestar las preguntas de cuatro categorías que habrás sacado de la bolsa. Puedes usar tus comodines para contestar por otro jugador y ganar más puntos o simplemente róbale su categoría favorita, pero recuerda... ¡¡¡te pueden hacer lo mismo a ti también!!!</p>

                        <p>
                            <b>Medidas:</b> 20 x 16 x 2cm. <br>
                            <b>Idiomas:</b> Español, Portugués, Ingles, Frances. <br>
                            <b>Contenido:</b> 200 tarjetas, 20 fichas categorías, 4 fichas ZWAP (1 de cada color), 8 fichas BEZZERWIZZER (2 de cada color), 4 peones (1 de cada color), 1 caja porta-tarjetas, 1 bolsa de tela para las fichas de categorías, 1 tablero central, 4 tableros individuales, instrucciones.
                        </p>
                    </div>

                    <div class="col-md-7">
                        <img src="images/products/eljuego-1024x853B.jpg" class="pull-right" alt="" style="height:400px;">
                    </div>
                </div>

                <div class="css-product-panel wow fadeInUp css-marginT50 css-marginB0" data-wow-duration="1000ms" data-wow-delay="400ms">
                    <div class="col-md-7">
                        <img src="images/products/eljuego-1024x853.jpg" class="img-responsive" alt="">
                    </div>

                    <div class="col-md-5 text-right">
                        <h3>BEZZERWIZZER</h3>

                        <p class="css-bold">EL SABER ESTÁ EN JUEGO</p>
                        <p>Falomir lanza Bezzerwizzer, el juego de preguntas y respuestas de mayor éxito en el norte de Europa.</p>
                        <p>Con 20 categorías y más de 4000 preguntas</p>
                    </div>
                </div>

                <div class="clearfix css-marginB10"></div>

                <hr class="css-hr css-marginB40">

                <div class="col-md-4">
                    <a href="#collapseExample" class="btn btn-warning btn-big css-oswaldextralight" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">Nuestros juegos <br> <b>más vendidos</b></a>
                </div>

                <div class="col-md-8">
                    <!--TAMBIEN PUEDE <br> INTERESARTE <i class="fa fa-angle-right "></i> -->
                    <div class="collapse" id="collapseExample">
                        <div class="well css-scrollable">
                            <div class="row css-marginB20">
                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/last_word.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">LAST WORD</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/toco_tronco.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">TOCO TRONCO</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/no_digas.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">NO LO DIGAS</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row css-marginB20">
                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/last_word.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">LAST WORD</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/toco_tronco.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">TOCO TRONCO</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/no_digas.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">NO LO DIGAS</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row css-marginB20">
                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/last_word.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">LAST WORD</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/toco_tronco.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">TOCO TRONCO</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="css-seller-panel">
                                        <div class="css-seller-body">
                                            <div class="css-seller-img" style="background-image: url('images/products/no_digas.jpg')"></div>
                                        </div>

                                        <div class="css-seller-footer">
                                            <p class="pull-left css-marginT10 css-marginL10">NO LO DIGAS</p>

                                            <a href="#" class="btn btn-warning pull-right" style="min-height: 33px;">VER +</a>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="css-ul-related wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    	<li>
                         TAMBIEN PUEDE <br> INTERESARTE <i class="fa fa-angle-right "></i>
                        </li>
                        <li>
                            <a href="product-detail.php">
                                <img src="images/products/3.jpg" class="img-responsive css-noFloat center-block" width="100" alt="">
                                <p>AJEDREZ MAGNÉTICO</p>
                            </a>
                        </li>

                        <li>
                            <a href="product-detail.php">
                                <img src="images/products/4.jpg" class="img-responsive css-noFloat center-block" width="100" alt="">
                                <p>OCA MAGNÉTICO</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <div class="css-categories-nav">
            <div class="container">
                <ul class="css-noPadding">
                    <li><a href="#">MAGIA <i class="fa fa-magic"></i></a></li>
                    <li><a href="#">ARTÍSTICOS <i class="fa fa-paint-brush"></i></a></li>
                    <li><a href="#">HABILIDAD <i class="fa fa-bullseye"></i></a></li>
                    <li><a href="#">JUEGOS DE MESA <i class="fa fa-cube"></i></a></li>
                    <li><a href="#">CLÁSICOS <i class="fa fa-puzzle-piece"></i></a></li>
                </ul>
            </div>
        </div>

        <? include('includes/chat.php'); ?>
        <? include('includes/footer.php'); ?>
        <!--/#footer-->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/lightbox.min.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript">
            var scrollVal = 1350;
        </script>
    </body>
</html>
