/*
Navicat MySQL Data Transfer

Source Server         : FALOMIR-inkube
Source Server Version : 50556
Source Host           : falomir.inkube.net:3306
Source Database       : falomir_WebAppli

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2018-12-09 19:29:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for carousel
-- ----------------------------
DROP TABLE IF EXISTS `carousel`;
CREATE TABLE `carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(180) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(180) DEFAULT NULL,
  `background_color` varchar(50) NOT NULL,
  `status` int(2) NOT NULL COMMENT '0: deshabilitado, 1: Habilitado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carousel
-- ----------------------------
INSERT INTO `carousel` VALUES ('1', ' JUEGOS CLÁSICOS', 'Juegos clásicos. Que la nostalgia te acompañe\r\n\r\nAprender a ganar con deportividad se practica toda la vida, tengas 5 o 100 años... Entrena tu habilidad, tu picardía o tu agilidad mental con los juegos más nostálgicos de nuestra colección. ', null, 'slide_119265c0cb8976ea32.png', '#99aed9', '1');
INSERT INTO `carousel` VALUES ('2', 'APRENDE Y DIVIÉRTE', 'Aprender y divertirse es lo mismo con los juegos de mesa\r\n\r\nDesde siempre, se aprende a compartir alrededor de una mesa y si es con nuestros juegos se aprende mucho mejor. Crea tus estrategias y machaca a la familia pero siempre con deportividad.', null, 'slide_218775c0cb91fa8f83.png', '#cea5d0', '1');
INSERT INTO `carousel` VALUES ('3', ' JUEGOS DE HABILIDAD', 'Te encantará aprender así\r\n\r\nPotenciar la creatividad, la estrategia, aprender sobre estructuras, mejorar la precisión, la psicomotricidad y además ¡Pasarlo en grande! Si es que… los juegos de habilidad ¡Lo tienen todo!.', null, 'slide_970275c0cb9555fed6.png', '#e4b34d', '1');
INSERT INTO `carousel` VALUES ('4', ' ÉCHALE MAGIA Y DIBI DIBADI DIBÚ', 'Crecer, aprender, practicar y mejorar es el sueño de cualquier pequeño mago y con nuestro nuevo canal de magia lo tendrá muy fácil. Descubre todos los trucos de magia de la mano del mago Rafa Tamarit y los juegos de magia de Falomir. ', null, 'slide_434755c0cb98411cf2.png', '#6cd8a9', '1');
INSERT INTO `carousel` VALUES ('5', 'JUEGOS CON MUCHO ARTE', 'Hay tantas posibilidades como colores tiene el mundo, por eso, para desarrollar la creatividad, el pensamiento divergente y además pasar una tarde estupenda nada mejor que nuestros juegos de manualidades, un lienzo en blanco para que expresen todo lo que ', null, 'slide_567145c0cb9c0df689.png', '#546dc7', '1');

-- ----------------------------
-- Table structure for contact_messages
-- ----------------------------
DROP TABLE IF EXISTS `contact_messages`;
CREATE TABLE `contact_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `last_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `message` text COLLATE latin1_general_ci NOT NULL,
  `status` int(2) NOT NULL COMMENT '0: no leido, 1: Leido',
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of contact_messages
-- ----------------------------
INSERT INTO `contact_messages` VALUES ('1', 'Ali', 'Guevara', 'sulvior@hotmail.coms', 'mensaje prueba', '1', '2018-09-28 07:15:36', '2018-09-28 07:15:36');
INSERT INTO `contact_messages` VALUES ('2', 'RUFI', 'eeeee', 'rufi@inkube.net', 'Prueba de contacto', '1', '2018-10-02 10:08:38', '2018-10-02 10:08:38');

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `country_index` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'Afganistán', 'AF', '93');
INSERT INTO `country` VALUES ('2', 'Islas Gland', 'AX', null);
INSERT INTO `country` VALUES ('3', 'Albania', 'AL', '355');
INSERT INTO `country` VALUES ('4', 'Alemania', 'DE', '49');
INSERT INTO `country` VALUES ('5', 'Andorra', 'AD', '376');
INSERT INTO `country` VALUES ('6', 'Angola', 'AO', '244');
INSERT INTO `country` VALUES ('7', 'Anguilla', 'AI', null);
INSERT INTO `country` VALUES ('8', 'Antártida', 'AQ', null);
INSERT INTO `country` VALUES ('9', 'Antigua y Barbuda', 'AG', '1-268');
INSERT INTO `country` VALUES ('10', 'Antillas Holandesas', 'AN', null);
INSERT INTO `country` VALUES ('11', 'Arabia Saudí', 'SA', '966');
INSERT INTO `country` VALUES ('12', 'Argelia', 'DZ', '213');
INSERT INTO `country` VALUES ('13', 'Argentina', 'AR', '54');
INSERT INTO `country` VALUES ('14', 'Armenia', 'AM', '374');
INSERT INTO `country` VALUES ('15', 'Aruba', 'AW', '297');
INSERT INTO `country` VALUES ('16', 'Australia', 'AU', '61');
INSERT INTO `country` VALUES ('17', 'Austria', 'AT', '43');
INSERT INTO `country` VALUES ('18', 'Azerbaiyán', 'AZ', null);
INSERT INTO `country` VALUES ('19', 'Bahamas', 'BS', '1-242');
INSERT INTO `country` VALUES ('20', 'Bahréin', 'BH', '973');
INSERT INTO `country` VALUES ('21', 'Bangladesh', 'BD', '880');
INSERT INTO `country` VALUES ('22', 'Barbados', 'BB', '1-246');
INSERT INTO `country` VALUES ('23', 'Bielorrusia', 'BY', '375');
INSERT INTO `country` VALUES ('24', 'Bélgica', 'BE', '32');
INSERT INTO `country` VALUES ('25', 'Belice', 'BZ', '501');
INSERT INTO `country` VALUES ('26', 'Benin', 'BJ', '229');
INSERT INTO `country` VALUES ('27', 'Bermudas', 'BM', '1-441');
INSERT INTO `country` VALUES ('28', 'Bhután', 'BT', null);
INSERT INTO `country` VALUES ('29', 'Bolivia', 'BO', null);
INSERT INTO `country` VALUES ('30', 'Bosnia y Herzegovina', 'BA', null);
INSERT INTO `country` VALUES ('31', 'Botsuana', 'BW', null);
INSERT INTO `country` VALUES ('32', 'Isla Bouvet', 'BV', null);
INSERT INTO `country` VALUES ('33', 'Brasil', 'BR', null);
INSERT INTO `country` VALUES ('34', 'Brunéi', 'BN', null);
INSERT INTO `country` VALUES ('35', 'Bulgaria', 'BG', null);
INSERT INTO `country` VALUES ('36', 'Burkina Faso', 'BF', null);
INSERT INTO `country` VALUES ('37', 'Burundi', 'BI', null);
INSERT INTO `country` VALUES ('38', 'Cabo Verde', 'CV', null);
INSERT INTO `country` VALUES ('39', 'Islas Caimán', 'KY', null);
INSERT INTO `country` VALUES ('40', 'Camboya', 'KH', null);
INSERT INTO `country` VALUES ('41', 'Camerún', 'CM', null);
INSERT INTO `country` VALUES ('42', 'Canadá', 'CA', null);
INSERT INTO `country` VALUES ('43', 'República Centroafricana', 'CF', null);
INSERT INTO `country` VALUES ('44', 'Chad', 'TD', null);
INSERT INTO `country` VALUES ('45', 'República Checa', 'CZ', null);
INSERT INTO `country` VALUES ('46', 'Chile', 'CL', null);
INSERT INTO `country` VALUES ('47', 'China', 'CN', null);
INSERT INTO `country` VALUES ('48', 'Chipre', 'CY', null);
INSERT INTO `country` VALUES ('49', 'Isla de Navidad', 'CX', null);
INSERT INTO `country` VALUES ('50', 'Ciudad del Vaticano', 'VA', null);
INSERT INTO `country` VALUES ('51', 'Islas Cocos', 'CC', null);
INSERT INTO `country` VALUES ('52', 'Colombia', 'CO', null);
INSERT INTO `country` VALUES ('53', 'Comoras', 'KM', null);
INSERT INTO `country` VALUES ('54', 'República Democrática del Congo', 'CD', null);
INSERT INTO `country` VALUES ('55', 'Congo', 'CG', null);
INSERT INTO `country` VALUES ('56', 'Islas Cook', 'CK', null);
INSERT INTO `country` VALUES ('57', 'Corea del Norte', 'KP', null);
INSERT INTO `country` VALUES ('58', 'Corea del Sur', 'KR', null);
INSERT INTO `country` VALUES ('59', 'Costa de Marfil', 'CI', null);
INSERT INTO `country` VALUES ('60', 'Costa Rica', 'CR', null);
INSERT INTO `country` VALUES ('61', 'Croacia', 'HR', null);
INSERT INTO `country` VALUES ('62', 'Cuba', 'CU', null);
INSERT INTO `country` VALUES ('63', 'Dinamarca', 'DK', null);
INSERT INTO `country` VALUES ('64', 'Dominica', 'DM', null);
INSERT INTO `country` VALUES ('65', 'República Dominicana', 'DO', null);
INSERT INTO `country` VALUES ('66', 'Ecuador', 'EC', null);
INSERT INTO `country` VALUES ('67', 'Egipto', 'EG', null);
INSERT INTO `country` VALUES ('68', 'El Salvador', 'SV', null);
INSERT INTO `country` VALUES ('69', 'Emiratos Árabes Unidos', 'AE', null);
INSERT INTO `country` VALUES ('70', 'Eritrea', 'ER', null);
INSERT INTO `country` VALUES ('71', 'Eslovaquia', 'SK', null);
INSERT INTO `country` VALUES ('72', 'Eslovenia', 'SI', null);
INSERT INTO `country` VALUES ('73', 'España', 'ES', null);
INSERT INTO `country` VALUES ('74', 'Islas ultramarinas de Estados Unidos', 'UM', null);
INSERT INTO `country` VALUES ('75', 'Estados Unidos', 'US', null);
INSERT INTO `country` VALUES ('76', 'Estonia', 'EE', null);
INSERT INTO `country` VALUES ('77', 'Etiopía', 'ET', null);
INSERT INTO `country` VALUES ('78', 'Islas Feroe', 'FO', null);
INSERT INTO `country` VALUES ('79', 'Filipinas', 'PH', null);
INSERT INTO `country` VALUES ('80', 'Finlandia', 'FI', null);
INSERT INTO `country` VALUES ('81', 'Fiyi', 'FJ', null);
INSERT INTO `country` VALUES ('82', 'Francia', 'FR', null);
INSERT INTO `country` VALUES ('83', 'Gabón', 'GA', null);
INSERT INTO `country` VALUES ('84', 'Gambia', 'GM', null);
INSERT INTO `country` VALUES ('85', 'Georgia', 'GE', null);
INSERT INTO `country` VALUES ('86', 'Islas Georgias del Sur y Sandwich del Sur', 'GS', null);
INSERT INTO `country` VALUES ('87', 'Ghana', 'GH', null);
INSERT INTO `country` VALUES ('88', 'Gibraltar', 'GI', null);
INSERT INTO `country` VALUES ('89', 'Granada', 'GD', null);
INSERT INTO `country` VALUES ('90', 'Grecia', 'GR', null);
INSERT INTO `country` VALUES ('91', 'Groenlandia', 'GL', null);
INSERT INTO `country` VALUES ('92', 'Guadalupe', 'GP', null);
INSERT INTO `country` VALUES ('93', 'Guam', 'GU', null);
INSERT INTO `country` VALUES ('94', 'Guatemala', 'GT', null);
INSERT INTO `country` VALUES ('95', 'Guayana Francesa', 'GF', null);
INSERT INTO `country` VALUES ('96', 'Guinea', 'GN', null);
INSERT INTO `country` VALUES ('97', 'Guinea Ecuatorial', 'GQ', null);
INSERT INTO `country` VALUES ('98', 'Guinea-Bissau', 'GW', null);
INSERT INTO `country` VALUES ('99', 'Guyana', 'GY', null);
INSERT INTO `country` VALUES ('100', 'Haití', 'HT', null);
INSERT INTO `country` VALUES ('101', 'Islas Heard y McDonald', 'HM', null);
INSERT INTO `country` VALUES ('102', 'Honduras', 'HN', null);
INSERT INTO `country` VALUES ('103', 'Hong Kong', 'HK', null);
INSERT INTO `country` VALUES ('104', 'Hungría', 'HU', null);
INSERT INTO `country` VALUES ('105', 'India', 'IN', null);
INSERT INTO `country` VALUES ('106', 'Indonesia', 'ID', null);
INSERT INTO `country` VALUES ('107', 'Irán', 'IR', null);
INSERT INTO `country` VALUES ('108', 'Iraq', 'IQ', null);
INSERT INTO `country` VALUES ('109', 'Irlanda', 'IE', null);
INSERT INTO `country` VALUES ('110', 'Islandia', 'IS', null);
INSERT INTO `country` VALUES ('111', 'Israel', 'IL', null);
INSERT INTO `country` VALUES ('112', 'Italia', 'IT', null);
INSERT INTO `country` VALUES ('113', 'Jamaica', 'JM', null);
INSERT INTO `country` VALUES ('114', 'Japón', 'JP', null);
INSERT INTO `country` VALUES ('115', 'Jordania', 'JO', null);
INSERT INTO `country` VALUES ('116', 'Kazajstán', 'KZ', null);
INSERT INTO `country` VALUES ('117', 'Kenia', 'KE', null);
INSERT INTO `country` VALUES ('118', 'Kirguistán', 'KG', null);
INSERT INTO `country` VALUES ('119', 'Kiribati', 'KI', null);
INSERT INTO `country` VALUES ('120', 'Kuwait', 'KW', null);
INSERT INTO `country` VALUES ('121', 'Laos', 'LA', null);
INSERT INTO `country` VALUES ('122', 'Lesotho', 'LS', null);
INSERT INTO `country` VALUES ('123', 'Letonia', 'LV', null);
INSERT INTO `country` VALUES ('124', 'Líbano', 'LB', null);
INSERT INTO `country` VALUES ('125', 'Liberia', 'LR', null);
INSERT INTO `country` VALUES ('126', 'Libia', 'LY', null);
INSERT INTO `country` VALUES ('127', 'Liechtenstein', 'LI', null);
INSERT INTO `country` VALUES ('128', 'Lituania', 'LT', null);
INSERT INTO `country` VALUES ('129', 'Luxemburgo', 'LU', null);
INSERT INTO `country` VALUES ('130', 'Macao', 'MO', null);
INSERT INTO `country` VALUES ('131', 'ARY Macedonia', 'MK', null);
INSERT INTO `country` VALUES ('132', 'Madagascar', 'MG', null);
INSERT INTO `country` VALUES ('133', 'Malasia', 'MY', null);
INSERT INTO `country` VALUES ('134', 'Malawi', 'MW', null);
INSERT INTO `country` VALUES ('135', 'Maldivas', 'MV', null);
INSERT INTO `country` VALUES ('136', 'Malí', 'ML', null);
INSERT INTO `country` VALUES ('137', 'Malta', 'MT', null);
INSERT INTO `country` VALUES ('138', 'Islas Malvinas', 'FK', null);
INSERT INTO `country` VALUES ('139', 'Islas Marianas del Norte', 'MP', null);
INSERT INTO `country` VALUES ('140', 'Marruecos', 'MA', null);
INSERT INTO `country` VALUES ('141', 'Islas Marshall', 'MH', null);
INSERT INTO `country` VALUES ('142', 'Martinica', 'MQ', null);
INSERT INTO `country` VALUES ('143', 'Mauricio', 'MU', null);
INSERT INTO `country` VALUES ('144', 'Mauritania', 'MR', null);
INSERT INTO `country` VALUES ('145', 'Mayotte', 'YT', null);
INSERT INTO `country` VALUES ('146', 'México', 'MX', null);
INSERT INTO `country` VALUES ('147', 'Micronesia', 'FM', null);
INSERT INTO `country` VALUES ('148', 'Moldavia', 'MD', null);
INSERT INTO `country` VALUES ('149', 'Mónaco', 'MC', null);
INSERT INTO `country` VALUES ('150', 'Mongolia', 'MN', null);
INSERT INTO `country` VALUES ('151', 'Montserrat', 'MS', null);
INSERT INTO `country` VALUES ('152', 'Mozambique', 'MZ', null);
INSERT INTO `country` VALUES ('153', 'Myanmar', 'MM', null);
INSERT INTO `country` VALUES ('154', 'Namibia', 'NA', null);
INSERT INTO `country` VALUES ('155', 'Nauru', 'NR', null);
INSERT INTO `country` VALUES ('156', 'Nepal', 'NP', null);
INSERT INTO `country` VALUES ('157', 'Nicaragua', 'NI', null);
INSERT INTO `country` VALUES ('158', 'Níger', 'NE', null);
INSERT INTO `country` VALUES ('159', 'Nigeria', 'NG', null);
INSERT INTO `country` VALUES ('160', 'Niue', 'NU', null);
INSERT INTO `country` VALUES ('161', 'Isla Norfolk', 'NF', null);
INSERT INTO `country` VALUES ('162', 'Noruega', 'NO', null);
INSERT INTO `country` VALUES ('163', 'Nueva Caledonia', 'NC', null);
INSERT INTO `country` VALUES ('164', 'Nueva Zelanda', 'NZ', null);
INSERT INTO `country` VALUES ('165', 'Omán', 'OM', null);
INSERT INTO `country` VALUES ('166', 'Países Bajos', 'NL', null);
INSERT INTO `country` VALUES ('167', 'Pakistán', 'PK', null);
INSERT INTO `country` VALUES ('168', 'Palau', 'PW', null);
INSERT INTO `country` VALUES ('169', 'Palestina', 'PS', null);
INSERT INTO `country` VALUES ('170', 'Panamá', 'PA', null);
INSERT INTO `country` VALUES ('171', 'Papúa Nueva Guinea', 'PG', null);
INSERT INTO `country` VALUES ('172', 'Paraguay', 'PY', null);
INSERT INTO `country` VALUES ('173', 'Perú', 'PE', null);
INSERT INTO `country` VALUES ('174', 'Islas Pitcairn', 'PN', null);
INSERT INTO `country` VALUES ('175', 'Polinesia Francesa', 'PF', null);
INSERT INTO `country` VALUES ('176', 'Polonia', 'PL', null);
INSERT INTO `country` VALUES ('177', 'Portugal', 'PT', null);
INSERT INTO `country` VALUES ('178', 'Puerto Rico', 'PR', null);
INSERT INTO `country` VALUES ('179', 'Qatar', 'QA', null);
INSERT INTO `country` VALUES ('180', 'Reino Unido', 'GB', null);
INSERT INTO `country` VALUES ('181', 'Reunión', 'RE', null);
INSERT INTO `country` VALUES ('182', 'Ruanda', 'RW', null);
INSERT INTO `country` VALUES ('183', 'Rumania', 'RO', null);
INSERT INTO `country` VALUES ('184', 'Rusia', 'RU', null);
INSERT INTO `country` VALUES ('185', 'Sahara Occidental', 'EH', null);
INSERT INTO `country` VALUES ('186', 'Islas Salomón', 'SB', null);
INSERT INTO `country` VALUES ('187', 'Samoa', 'WS', null);
INSERT INTO `country` VALUES ('188', 'Samoa Americana', 'AS', null);
INSERT INTO `country` VALUES ('189', 'San Cristóbal y Nevis', 'KN', null);
INSERT INTO `country` VALUES ('190', 'San Marino', 'SM', null);
INSERT INTO `country` VALUES ('191', 'San Pedro y Miquelón', 'PM', null);
INSERT INTO `country` VALUES ('192', 'San Vicente y las Granadinas', 'VC', null);
INSERT INTO `country` VALUES ('193', 'Santa Helena', 'SH', null);
INSERT INTO `country` VALUES ('194', 'Santa Lucía', 'LC', null);
INSERT INTO `country` VALUES ('195', 'Santo Tomé y Príncipe', 'ST', null);
INSERT INTO `country` VALUES ('196', 'Senegal', 'SN', null);
INSERT INTO `country` VALUES ('197', 'Serbia y Montenegro', 'CS', null);
INSERT INTO `country` VALUES ('198', 'Seychelles', 'SC', null);
INSERT INTO `country` VALUES ('199', 'Sierra Leona', 'SL', null);
INSERT INTO `country` VALUES ('200', 'Singapur', 'SG', null);
INSERT INTO `country` VALUES ('201', 'Siria', 'SY', null);
INSERT INTO `country` VALUES ('202', 'Somalia', 'SO', null);
INSERT INTO `country` VALUES ('203', 'Sri Lanka', 'LK', null);
INSERT INTO `country` VALUES ('204', 'Suazilandia', 'SZ', null);
INSERT INTO `country` VALUES ('205', 'Sudáfrica', 'ZA', null);
INSERT INTO `country` VALUES ('206', 'Sudán', 'SD', null);
INSERT INTO `country` VALUES ('207', 'Suecia', 'SE', null);
INSERT INTO `country` VALUES ('208', 'Suiza', 'CH', null);
INSERT INTO `country` VALUES ('209', 'Surinam', 'SR', null);
INSERT INTO `country` VALUES ('210', 'Svalbard y Jan Mayen', 'SJ', null);
INSERT INTO `country` VALUES ('211', 'Tailandia', 'TH', null);
INSERT INTO `country` VALUES ('212', 'Taiwán', 'TW', null);
INSERT INTO `country` VALUES ('213', 'Tanzania', 'TZ', null);
INSERT INTO `country` VALUES ('214', 'Tayikistán', 'TJ', null);
INSERT INTO `country` VALUES ('215', 'Territorio Británico del Océano Índico', 'IO', null);
INSERT INTO `country` VALUES ('216', 'Territorios Australes Franceses', 'TF', null);
INSERT INTO `country` VALUES ('217', 'Timor Oriental', 'TL', null);
INSERT INTO `country` VALUES ('218', 'Togo', 'TG', null);
INSERT INTO `country` VALUES ('219', 'Tokelau', 'TK', null);
INSERT INTO `country` VALUES ('220', 'Tonga', 'TO', null);
INSERT INTO `country` VALUES ('221', 'Trinidad y Tobago', 'TT', null);
INSERT INTO `country` VALUES ('222', 'Túnez', 'TN', null);
INSERT INTO `country` VALUES ('223', 'Islas Turcas y Caicos', 'TC', null);
INSERT INTO `country` VALUES ('224', 'Turkmenistán', 'TM', null);
INSERT INTO `country` VALUES ('225', 'Turquía', 'TR', null);
INSERT INTO `country` VALUES ('226', 'Tuvalu', 'TV', null);
INSERT INTO `country` VALUES ('227', 'Ucrania', 'UA', null);
INSERT INTO `country` VALUES ('228', 'Uganda', 'UG', null);
INSERT INTO `country` VALUES ('229', 'Uruguay', 'UY', null);
INSERT INTO `country` VALUES ('230', 'Uzbekistán', 'UZ', null);
INSERT INTO `country` VALUES ('231', 'Vanuatu', 'VU', null);
INSERT INTO `country` VALUES ('232', 'Venezuela', 'VE', '58');
INSERT INTO `country` VALUES ('233', 'Vietnam', 'VN', null);
INSERT INTO `country` VALUES ('234', 'Islas Vírgenes Británicas', 'VG', null);
INSERT INTO `country` VALUES ('235', 'Islas Vírgenes de los Estados Unidos', 'VI', null);
INSERT INTO `country` VALUES ('236', 'Wallis y Futuna', 'WF', null);
INSERT INTO `country` VALUES ('237', 'Yemen', 'YE', null);
INSERT INTO `country` VALUES ('238', 'Yibuti', 'DJ', null);
INSERT INTO `country` VALUES ('239', 'Zambia', 'ZM', null);
INSERT INTO `country` VALUES ('240', 'Zimbabue', 'ZW', null);

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `index` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', 'Ingles', 'EN');
INSERT INTO `language` VALUES ('2', ' Español', 'ES');
INSERT INTO `language` VALUES ('3', 'Português', 'PR');
INSERT INTO `language` VALUES ('4', 'Français', 'FR');
INSERT INTO `language` VALUES ('5', 'Italiano', 'IT');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` longtext COLLATE latin1_general_ci NOT NULL,
  `cover_image` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `image` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `news_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('6', 'NOTICIA 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque lacus et volutpat aliquet. Donec id lorem id ipsum viverra varius quis in elit. Phasellus eget neque pulvinar arcu finibus pretium nec a lectus. Nunc eget quam elit. Donec ut urna orci. Phasellus maximus lacus non neque condimentum, a vulputate ipsum varius. Vestibulum quis ultrices lectus, finibus blandit ante. Ut dignissim ligula dui, quis venenatis mauris egestas placerat. Integer sed eleifend sem. Mauris ut semper magna. Ut leo dolor, luctus eu arcu eu, feugiat volutpat velit.</p>\r\n\r\n<p>Vestibulum vel sollicitudin diam. Pellentesque sed sem augue. Integer et diam libero. Nulla sit amet ante tristique, dapibus lectus nec, tempor nulla. Duis volutpat, nibh eget tincidunt dictum, est dui rutrum eros, sit amet feugiat arcu justo nec ex. Suspendisse eu felis leo. Praesent sodales rhoncus porttitor. Sed at pharetra ligula.</p>\r\n', 'ADM001_cover_91345bacba9e300ae.jpeg', 'banner_631265bacbacb11d4a.jpeg', '7', '2018-09-27 13:11:07', '2018-09-27 13:11:07');
INSERT INTO `news` VALUES ('7', 'NOTICIA 2', '<p>Sed non purus mi. Sed eget quam risus. Sed ornare leo eget congue elementum. Fusce non sollicitudin urna, bibendum ultricies sapien. Praesent ornare, leo in tempor bibendum, odio est finibus sapien, eget pellentesque tellus tellus in lacus. Curabitur vulputate mattis nisi, semper accumsan augue. Ut rutrum molestie justo. Suspendisse potenti. Pellentesque in molestie urna, eu dictum enim. Vestibulum finibus enim ut fermentum vehicula.</p>\r\n\r\n<p>Mauris a nisl et arcu pellentesque convallis quis eget neque. Proin nec pretium sapien. Vestibulum volutpat, nisi aliquet vulputate blandit, ante nunc rhoncus massa, vel finibus felis nibh nec nunc. Phasellus dignissim urna ut magna commodo finibus. Proin pulvinar ante et arcu malesuada, ac ornare dolor hendrerit. Ut eleifend, turpis non blandit venenatis, enim sem dapibus neque, vehicula dictum leo nunc ut nisl. Sed aliquam varius purus ac dignissim. Phasellus at aliquam lacus.</p>\r\n', 'ADM001_cover_263825bacbb46163f7.jpeg', 'banner_125245bacbb6faed4f.jpeg', '7', '2018-09-27 13:13:51', '2018-09-27 13:13:51');
INSERT INTO `news` VALUES ('8', 'TU HIJO Y EL JUEGO', '<p>Vivamus vestibulum vel tortor in sodales. Etiam in quam elementum, dapibus dui ac, ultrices libero. Ut dictum orci enim, eu congue augue sagittis at. In eget hendrerit enim, id fermentum ipsum. Fusce ac sollicitudin ante. Donec et arcu tellus. Integer faucibus libero a dictum pharetra. Pellentesque gravida sit amet dolor sed feugiat. Nam in elementum eros. Aliquam sit amet pulvinar mauris. Nulla quis blandit nisl. Aliquam vestibulum aliquet ultricies. Etiam id orci placerat, ornare est a, auctor sem. Curabitur suscipit lobortis metus, vel maximus massa rutrum ac. Vivamus lorem odio, dignissim ac ante ac, rutrum molestie nunc.</p>\r\n', 'ADM001_cover_134435bacbbe422b31.jpeg', 'banner_172725bacbc10a050d.jpeg', '6', '2018-09-27 13:16:32', '2018-09-27 13:16:32');

-- ----------------------------
-- Table structure for news_category
-- ----------------------------
DROP TABLE IF EXISTS `news_category`;
CREATE TABLE `news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of news_category
-- ----------------------------
INSERT INTO `news_category` VALUES ('6', 'JUEGOS Y JUGUETES');
INSERT INTO `news_category` VALUES ('7', 'LOS NIÑOS JUEGAN');
INSERT INTO `news_category` VALUES ('8', 'MANUALIDADES');
INSERT INTO `news_category` VALUES ('9', 'JUEGOS DE HABILIDAD');
INSERT INTO `news_category` VALUES ('10', 'TARDES DE JUEGOS');

-- ----------------------------
-- Table structure for product_language
-- ----------------------------
DROP TABLE IF EXISTS `product_language`;
CREATE TABLE `product_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `product_language_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_language_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of product_language
-- ----------------------------
INSERT INTO `product_language` VALUES ('201', '51', '2');
INSERT INTO `product_language` VALUES ('202', '52', '2');
INSERT INTO `product_language` VALUES ('204', '54', '2');
INSERT INTO `product_language` VALUES ('207', '56', '2');
INSERT INTO `product_language` VALUES ('208', '57', '2');
INSERT INTO `product_language` VALUES ('210', '59', '2');
INSERT INTO `product_language` VALUES ('211', '59', '3');
INSERT INTO `product_language` VALUES ('212', '60', '2');
INSERT INTO `product_language` VALUES ('213', '61', '2');
INSERT INTO `product_language` VALUES ('214', '61', '3');
INSERT INTO `product_language` VALUES ('264', '86', '2');
INSERT INTO `product_language` VALUES ('265', '87', '2');
INSERT INTO `product_language` VALUES ('266', '88', '2');
INSERT INTO `product_language` VALUES ('267', '89', '2');
INSERT INTO `product_language` VALUES ('269', '90', '2');
INSERT INTO `product_language` VALUES ('270', '91', '2');
INSERT INTO `product_language` VALUES ('271', '92', '2');
INSERT INTO `product_language` VALUES ('272', '93', '2');
INSERT INTO `product_language` VALUES ('273', '94', '2');
INSERT INTO `product_language` VALUES ('274', '95', '2');
INSERT INTO `product_language` VALUES ('275', '95', '4');
INSERT INTO `product_language` VALUES ('276', '95', '1');
INSERT INTO `product_language` VALUES ('277', '95', '5');
INSERT INTO `product_language` VALUES ('278', '95', '3');
INSERT INTO `product_language` VALUES ('279', '96', '2');
INSERT INTO `product_language` VALUES ('280', '96', '4');
INSERT INTO `product_language` VALUES ('281', '96', '1');
INSERT INTO `product_language` VALUES ('282', '96', '5');
INSERT INTO `product_language` VALUES ('283', '96', '3');
INSERT INTO `product_language` VALUES ('284', '97', '2');
INSERT INTO `product_language` VALUES ('285', '97', '4');
INSERT INTO `product_language` VALUES ('286', '97', '1');
INSERT INTO `product_language` VALUES ('287', '97', '5');
INSERT INTO `product_language` VALUES ('288', '97', '3');
INSERT INTO `product_language` VALUES ('289', '98', '2');
INSERT INTO `product_language` VALUES ('290', '98', '3');
INSERT INTO `product_language` VALUES ('291', '99', '2');
INSERT INTO `product_language` VALUES ('292', '99', '3');
INSERT INTO `product_language` VALUES ('293', '100', '2');
INSERT INTO `product_language` VALUES ('294', '100', '3');
INSERT INTO `product_language` VALUES ('295', '101', '2');
INSERT INTO `product_language` VALUES ('296', '101', '3');
INSERT INTO `product_language` VALUES ('297', '102', '2');
INSERT INTO `product_language` VALUES ('298', '102', '3');
INSERT INTO `product_language` VALUES ('299', '103', '2');
INSERT INTO `product_language` VALUES ('300', '103', '3');
INSERT INTO `product_language` VALUES ('301', '104', '2');
INSERT INTO `product_language` VALUES ('302', '104', '3');
INSERT INTO `product_language` VALUES ('303', '105', '2');
INSERT INTO `product_language` VALUES ('304', '105', '3');
INSERT INTO `product_language` VALUES ('305', '106', '2');
INSERT INTO `product_language` VALUES ('306', '106', '3');
INSERT INTO `product_language` VALUES ('307', '107', '2');
INSERT INTO `product_language` VALUES ('308', '107', '3');
INSERT INTO `product_language` VALUES ('309', '108', '2');
INSERT INTO `product_language` VALUES ('310', '108', '3');
INSERT INTO `product_language` VALUES ('311', '21', '2');
INSERT INTO `product_language` VALUES ('312', '21', '4');
INSERT INTO `product_language` VALUES ('313', '21', '1');
INSERT INTO `product_language` VALUES ('314', '21', '3');
INSERT INTO `product_language` VALUES ('315', '24', '2');
INSERT INTO `product_language` VALUES ('316', '24', '4');
INSERT INTO `product_language` VALUES ('317', '24', '1');
INSERT INTO `product_language` VALUES ('318', '24', '5');
INSERT INTO `product_language` VALUES ('319', '24', '3');
INSERT INTO `product_language` VALUES ('320', '23', '2');
INSERT INTO `product_language` VALUES ('321', '23', '4');
INSERT INTO `product_language` VALUES ('322', '23', '1');
INSERT INTO `product_language` VALUES ('323', '23', '5');
INSERT INTO `product_language` VALUES ('324', '23', '3');
INSERT INTO `product_language` VALUES ('325', '22', '2');
INSERT INTO `product_language` VALUES ('326', '22', '4');
INSERT INTO `product_language` VALUES ('327', '22', '1');
INSERT INTO `product_language` VALUES ('328', '22', '3');
INSERT INTO `product_language` VALUES ('329', '33', '2');
INSERT INTO `product_language` VALUES ('330', '33', '4');
INSERT INTO `product_language` VALUES ('331', '33', '1');
INSERT INTO `product_language` VALUES ('332', '33', '3');
INSERT INTO `product_language` VALUES ('333', '30', '2');
INSERT INTO `product_language` VALUES ('334', '30', '4');
INSERT INTO `product_language` VALUES ('335', '30', '1');
INSERT INTO `product_language` VALUES ('336', '30', '3');
INSERT INTO `product_language` VALUES ('337', '34', '2');
INSERT INTO `product_language` VALUES ('338', '34', '4');
INSERT INTO `product_language` VALUES ('339', '34', '1');
INSERT INTO `product_language` VALUES ('340', '34', '3');
INSERT INTO `product_language` VALUES ('341', '25', '2');
INSERT INTO `product_language` VALUES ('342', '25', '4');
INSERT INTO `product_language` VALUES ('343', '25', '1');
INSERT INTO `product_language` VALUES ('344', '25', '3');
INSERT INTO `product_language` VALUES ('345', '26', '2');
INSERT INTO `product_language` VALUES ('346', '26', '4');
INSERT INTO `product_language` VALUES ('347', '26', '1');
INSERT INTO `product_language` VALUES ('348', '26', '3');
INSERT INTO `product_language` VALUES ('349', '27', '2');
INSERT INTO `product_language` VALUES ('350', '27', '4');
INSERT INTO `product_language` VALUES ('351', '27', '1');
INSERT INTO `product_language` VALUES ('352', '27', '3');
INSERT INTO `product_language` VALUES ('353', '28', '2');
INSERT INTO `product_language` VALUES ('354', '28', '4');
INSERT INTO `product_language` VALUES ('355', '28', '1');
INSERT INTO `product_language` VALUES ('356', '28', '3');
INSERT INTO `product_language` VALUES ('357', '29', '2');
INSERT INTO `product_language` VALUES ('358', '29', '4');
INSERT INTO `product_language` VALUES ('359', '29', '1');
INSERT INTO `product_language` VALUES ('360', '29', '3');
INSERT INTO `product_language` VALUES ('361', '31', '2');
INSERT INTO `product_language` VALUES ('362', '31', '4');
INSERT INTO `product_language` VALUES ('363', '31', '1');
INSERT INTO `product_language` VALUES ('364', '31', '3');
INSERT INTO `product_language` VALUES ('365', '32', '2');
INSERT INTO `product_language` VALUES ('366', '32', '4');
INSERT INTO `product_language` VALUES ('367', '32', '1');
INSERT INTO `product_language` VALUES ('368', '32', '3');
INSERT INTO `product_language` VALUES ('369', '35', '2');
INSERT INTO `product_language` VALUES ('370', '35', '4');
INSERT INTO `product_language` VALUES ('371', '35', '1');
INSERT INTO `product_language` VALUES ('372', '35', '3');
INSERT INTO `product_language` VALUES ('373', '36', '2');
INSERT INTO `product_language` VALUES ('374', '36', '4');
INSERT INTO `product_language` VALUES ('375', '36', '1');
INSERT INTO `product_language` VALUES ('376', '36', '3');
INSERT INTO `product_language` VALUES ('377', '38', '2');
INSERT INTO `product_language` VALUES ('378', '38', '4');
INSERT INTO `product_language` VALUES ('379', '38', '1');
INSERT INTO `product_language` VALUES ('380', '38', '3');
INSERT INTO `product_language` VALUES ('381', '39', '2');
INSERT INTO `product_language` VALUES ('382', '39', '4');
INSERT INTO `product_language` VALUES ('383', '39', '1');
INSERT INTO `product_language` VALUES ('384', '39', '3');
INSERT INTO `product_language` VALUES ('385', '40', '2');
INSERT INTO `product_language` VALUES ('386', '40', '4');
INSERT INTO `product_language` VALUES ('387', '40', '1');
INSERT INTO `product_language` VALUES ('388', '40', '3');
INSERT INTO `product_language` VALUES ('389', '41', '2');
INSERT INTO `product_language` VALUES ('390', '41', '4');
INSERT INTO `product_language` VALUES ('391', '41', '1');
INSERT INTO `product_language` VALUES ('392', '41', '3');
INSERT INTO `product_language` VALUES ('393', '37', '2');
INSERT INTO `product_language` VALUES ('394', '37', '4');
INSERT INTO `product_language` VALUES ('395', '37', '1');
INSERT INTO `product_language` VALUES ('396', '37', '3');
INSERT INTO `product_language` VALUES ('397', '42', '2');
INSERT INTO `product_language` VALUES ('398', '42', '4');
INSERT INTO `product_language` VALUES ('399', '42', '1');
INSERT INTO `product_language` VALUES ('400', '42', '3');
INSERT INTO `product_language` VALUES ('401', '43', '2');
INSERT INTO `product_language` VALUES ('402', '43', '4');
INSERT INTO `product_language` VALUES ('403', '43', '1');
INSERT INTO `product_language` VALUES ('404', '43', '3');
INSERT INTO `product_language` VALUES ('405', '44', '2');
INSERT INTO `product_language` VALUES ('406', '44', '4');
INSERT INTO `product_language` VALUES ('407', '44', '1');
INSERT INTO `product_language` VALUES ('408', '44', '3');
INSERT INTO `product_language` VALUES ('409', '45', '2');
INSERT INTO `product_language` VALUES ('410', '45', '4');
INSERT INTO `product_language` VALUES ('411', '45', '1');
INSERT INTO `product_language` VALUES ('412', '45', '3');
INSERT INTO `product_language` VALUES ('413', '46', '2');
INSERT INTO `product_language` VALUES ('414', '46', '4');
INSERT INTO `product_language` VALUES ('415', '46', '1');
INSERT INTO `product_language` VALUES ('416', '46', '3');
INSERT INTO `product_language` VALUES ('417', '47', '2');
INSERT INTO `product_language` VALUES ('418', '47', '4');
INSERT INTO `product_language` VALUES ('419', '47', '1');
INSERT INTO `product_language` VALUES ('420', '47', '3');
INSERT INTO `product_language` VALUES ('421', '48', '2');
INSERT INTO `product_language` VALUES ('422', '48', '4');
INSERT INTO `product_language` VALUES ('423', '48', '1');
INSERT INTO `product_language` VALUES ('424', '48', '3');
INSERT INTO `product_language` VALUES ('425', '49', '2');
INSERT INTO `product_language` VALUES ('426', '49', '4');
INSERT INTO `product_language` VALUES ('427', '49', '1');
INSERT INTO `product_language` VALUES ('428', '49', '3');
INSERT INTO `product_language` VALUES ('429', '50', '2');
INSERT INTO `product_language` VALUES ('430', '50', '4');
INSERT INTO `product_language` VALUES ('431', '50', '1');
INSERT INTO `product_language` VALUES ('432', '50', '3');
INSERT INTO `product_language` VALUES ('434', '64', '2');
INSERT INTO `product_language` VALUES ('435', '65', '2');
INSERT INTO `product_language` VALUES ('438', '77', '2');
INSERT INTO `product_language` VALUES ('439', '77', '4');
INSERT INTO `product_language` VALUES ('440', '77', '1');
INSERT INTO `product_language` VALUES ('441', '77', '5');
INSERT INTO `product_language` VALUES ('442', '77', '3');
INSERT INTO `product_language` VALUES ('443', '73', '2');
INSERT INTO `product_language` VALUES ('445', '75', '2');
INSERT INTO `product_language` VALUES ('446', '72', '2');
INSERT INTO `product_language` VALUES ('447', '71', '2');
INSERT INTO `product_language` VALUES ('448', '66', '2');
INSERT INTO `product_language` VALUES ('449', '82', '2');
INSERT INTO `product_language` VALUES ('450', '67', '2');
INSERT INTO `product_language` VALUES ('451', '78', '2');
INSERT INTO `product_language` VALUES ('452', '78', '4');
INSERT INTO `product_language` VALUES ('453', '78', '1');
INSERT INTO `product_language` VALUES ('454', '78', '5');
INSERT INTO `product_language` VALUES ('455', '78', '3');
INSERT INTO `product_language` VALUES ('457', '69', '2');
INSERT INTO `product_language` VALUES ('458', '80', '2');
INSERT INTO `product_language` VALUES ('459', '68', '2');
INSERT INTO `product_language` VALUES ('460', '81', '2');
INSERT INTO `product_language` VALUES ('461', '83', '2');
INSERT INTO `product_language` VALUES ('462', '85', '2');
INSERT INTO `product_language` VALUES ('463', '84', '2');
INSERT INTO `product_language` VALUES ('464', '79', '2');
INSERT INTO `product_language` VALUES ('465', '76', '2');
INSERT INTO `product_language` VALUES ('466', '70', '2');
INSERT INTO `product_language` VALUES ('467', '74', '2');
INSERT INTO `product_language` VALUES ('468', '62', '2');
INSERT INTO `product_language` VALUES ('469', '58', '2');
INSERT INTO `product_language` VALUES ('470', '55', '2');
INSERT INTO `product_language` VALUES ('471', '53', '2');
INSERT INTO `product_language` VALUES ('472', '63', '2');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `age_range` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `measurement` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `content` text COLLATE latin1_general_ci NOT NULL,
  `description_1` longtext COLLATE latin1_general_ci NOT NULL,
  `description_2` longtext COLLATE latin1_general_ci NOT NULL,
  `cover_image` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `image_1` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `image_2` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `novelty_image` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `product_logo` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `video` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `novelty` int(2) NOT NULL,
  `best_seller` int(2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `products_category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('21', '-', 'BINGO BASIC AUTOMÁTICO', '6', '28 x 29 x 11', '- 1 Bingo con extracción automática.\r\n- 48 cartones.\r\n- 90 bolas.\r\n- Fichas para tapar las casillas. ', 'Versión básica de nuestro Bingo automático con 48 cartones y 90 bolas. Bingo compacto y con la base de plástico rígido.', 'Ahora podrás comprobar los cartones ganadores con tu móvil o tablet. Sólo has de descargar nuestra app gratuita para Android o iOS. Es totalmente opcional, no es necesaria para jugar a tu bingo de Falomir Juegos.', 'ADM001_cover_2925ba9828cf183d.jpeg', 'gallery_954035ba9849e50ebd.jpeg', 'gallery_354435ba9849e51765.jpeg', null, null, null, null, '0', '0', '13', '2018-10-11 01:56:52', '2018-10-11 01:56:52');
INSERT INTO `products` VALUES ('22', ' 27922', 'BINGO ELÉCTRICO', '12', '27 x 24 x 17', '1 Bingo con extracción automática.\r\n1 base para colocar las bolas.\r\n48 cartones.\r\n90 bolas.\r\nFichas para tapar las casillas.', 'Versión electrónica del clásico Bingo. El bombo giratorio se pone en marcha cuando presionas el botón y está girando hasta que lo vuelves a presionar, mientras tanto van saliendo las bolas de una en una por la rampa.', 'Ahora podrás comprobar los cartones ganadores con tu móvil o tablet. Sólo has de descargar nuestra app gratuita para Android o iOS. Es totalmente opcional, no es necesaria para jugar a tu bingo de Falomir Juegos.', 'ADM001_cover_245735ba99e69b93d0.jpeg', 'gallery_195615ba99e983516f.jpeg', 'gallery_760165ba99e98357bc.jpeg', null, null, null, null, '0', '0', '13', '2018-10-11 02:01:57', '2018-10-11 02:01:57');
INSERT INTO `products` VALUES ('23', ' 23030', 'BINGO XXL Premium', '12', '34 x 41 x 17,5', '48 cartones, 90 bolas, 1 base bombo, 1 bolsas fichas', 'El juego de siempre no ha perdido su atractivo. Completa tus cartones y canta LÍNEA o BINGO. En este modelo XXL Premium sale una bola automáticamente en cada vuelta del bombo. Su tamaño y sus bolas imborrables lo hacen único en su género.', 'Ahora podrás comprobar los cartones ganadores con tu móvil o tablet. Sólo has de descargar nuestra app gratuita para Android o iOS. Es totalmente opcional, no es necesaria para jugar a tu bingo de Falomir Juegos.', 'ADM001_cover_251005ba9a151b6d3f.png', 'gallery_154395ba9a18431d23.jpeg', 'gallery_194625ba9a18432410.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:01:28', '2018-10-11 02:01:28');
INSERT INTO `products` VALUES ('24', ' 11519', 'Bingo - Lotto ', '7', '27 x 38,8 x 5,1', '48 cartones, 90 fichas de madera, Tablero, Fichas plástico.', 'Clásico BINGO LOTTO con fichas de madera, ¡¡coge tus cartones y a jugar!!', 'Ahora podrás comprobar los cartones ganadores con tu móvil o tablet. Sólo has de descargar nuestra app gratuita para Android o iOS. Es totalmente opcional, no es necesaria para jugar a tu bingo de Falomir Juegos.', 'ADM001_cover_93595ba9a2c797f10.png', 'gallery_776965ba9a301950ef.jpeg', 'gallery_764505ba9a30195a97.png', null, null, null, null, '0', '0', '13', '2018-10-11 01:59:13', '2018-10-11 01:59:13');
INSERT INTO `products` VALUES ('25', ' 27906', 'PARCHÍS OCA 33 cm. ', '4', '33 x 33 x 1', '1 Tablero de 33 x 33 cm ', 'Juego de Parchís y Oca. La medida del tablero es de 33 x 33cm.', '-', 'ADM001_cover_863005ba9a54ebbfdc.png', 'gallery_43455ba9a58172a3a.png', 'gallery_928815ba9a58172fc3.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:33', '2018-10-11 02:06:33');
INSERT INTO `products` VALUES ('26', ' 27907', 'PARCHÍS AJEDREZ DAMAS 33 cm.', '4', '33 x 33 x 1', '1 Tablero de 33 x 33 cm ', 'Juego de Parchís y Ajedrez - Damas. La medida del tablero es de 33 x 33cm.', '-', 'ADM001_cover_330495ba9a6104d6a2.png', 'gallery_14505ba9a62601c91.png', 'gallery_958135ba9a62602260.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:40', '2018-10-11 02:06:40');
INSERT INTO `products` VALUES ('27', ' 27908', 'PARCHÍS OCA 40 cm. ', '4', '40 x 40 x 1', '1 Tablero de 40 x 40 cm ', '-', '-', null, 'gallery_237695ba9a69b01d1b.png', 'gallery_151575ba9a69b02290.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:48', '2018-10-11 02:06:48');
INSERT INTO `products` VALUES ('28', '27909', 'AJEDREZ DAMAS 40 cm. ', '4', '40 x 40 x 1', '1 Tablero de 40 x 40 cm ', '-', '-', 'ADM001_cover_445725ba9a6f52a320.png', 'gallery_11405ba9a6fe3a05b.png', 'gallery_639705ba9a6fe3a9a2.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:54', '2018-10-11 02:06:54');
INSERT INTO `products` VALUES ('29', ' 27910', 'PARCHÍS 6 jugadores OCA 40 cm. ', '4', '40 x 40 x 1', '1 Tablero de 40 x 40 cm ', 'Juego de Parchís de 6 jugadores y Oca. La medida del tablero es de 40 x 40 cm.', '', 'ADM001_cover_421945ba9a74b4104a.png', 'gallery_769085ba9a759e8fe2.png', 'gallery_849515ba9a759e9505.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:07:01', '2018-10-11 02:07:01');
INSERT INTO `products` VALUES ('30', ' 27915', 'PARCHÍS OCA 40 cm. más Accesorios', '4', '33 x 33 x 4', '1 Tablero de 40 x 40 cm\r\n1 cubilete rojo.\r\n1 cubilete verde.\r\n1 cubilete amarillo.\r\n1 cubilete azul.\r\n4 fichas rojas.\r\n4 fichas verdes.\r\n4 fichas amarillas.\r\n4 fichas azules.\r\n4 dados.\r\nCaja para guardar los accesorios. ', 'Juego de Parchís y Oca con cubiletes, fichas y dados. La medida del tablero es de 40 x 40 cm. En el cartón que lo envuelve hay troquelada una caja que se monta para guardar los accesorios.', '-', 'ADM001_cover_939135ba9a7b131ccf.png', 'gallery_342585ba9a7df7a945.jpeg', 'gallery_497145ba9a7df7b0be.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:16', '2018-10-11 02:06:16');
INSERT INTO `products` VALUES ('31', ' 27911', 'PARCHÍS 4 y 6 jugadores 40 cm. ', '4', '40 x 40 x 1', '1 Tablero de 40 x 40 cm ', 'Juego de Parchís y Oca. La medida del tablero es de 40 x 40 cm.', '-', 'ADM001_cover_896085ba9a8d6b0892.png', 'gallery_128725ba9a8ecd527c.png', 'gallery_184445ba9a8ecd5841.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:07:10', '2018-10-11 02:07:10');
INSERT INTO `products` VALUES ('32', ' 27912', 'PARCHÍS CRISTAL - OCA 40 cm. ', '2', '44 x 44 x 2', '1 Tablero de 44 x 44 cm', 'Juego de Parchís con marco de madera y cristal protector. En la otra cara está el juego de la Oca, sin cristal. La medida del tablero es de 44 x 44 cm.\r\n', 'Este artículo no es un juguete.', null, 'gallery_894375ba9a976534f4.jpeg', 'gallery_553165ba9a97653d2d.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:07:16', '2018-10-11 02:07:16');
INSERT INTO `products` VALUES ('33', ' 27914', 'PARCHÍS Ajedrez Damas 33 cm. más Accesorios ', '4', '33 x 33 x 4', '1 Tablero de 33 x 33 cm\r\n1 cubilete rojo.\r\n1 cubilete verde.\r\n1 cubilete amarillo.\r\n1 cubilete azul.\r\n4 fichas rojas.\r\n4 fichas verdes.\r\n4 fichas amarillas.\r\n4 fichas azules.\r\n4 dados.\r\n8 piezas ajedrez blancas.\r\n8 piezas ajedrez negras.\r\n12 fichas negras de damas.\r\n12 fichas blancas de damas.\r\nCaja para guardar los accesorios. ', 'Juego de Parchís Ajedrez y Damas con cubiletes, fichas de parchís, dados, piezas de ajedrez y fichas de damas. La medida del tablero es de 33 x 33cm. En el cartón que lo envuelve hay troquelada una caja que se monta para guardar los accesorios.', '-', null, 'gallery_224025ba9aa149e4ce.jpeg', 'gallery_76305ba9aa149eaa0.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:10', '2018-10-11 02:06:10');
INSERT INTO `products` VALUES ('34', ' 27916', 'PARCHÍS 6 jugadores OCA 40 cm. más Accesorios ', '4', '33 x 33 x 4', '1 Tablero de 40 x 40 cm\r\n1 cubilete rojo.\r\n1 cubilete verde.\r\n1 cubilete amarillo.\r\n1 cubilete azul.\r\n1 cubilete naranja.\r\n1 cubilete morado.\r\n4 fichas rojas.\r\n4 fichas verdes.\r\n4 fichas amarillas.\r\n4 fichas azules.\r\n4 fichas naranjas.\r\n4 fichas moradas.\r\n6 dados.\r\nCaja para guardar los accesorios. ', 'Juego de Parchís para 6 jugadores y Oca con cubiletes, fichas y dados. La medida del tablero es de 40 x 40 cm. En el cartón que lo envuelve hay troquelada una caja que se monta para guardar los accesorios.', '-', 'ADM001_cover_702475ba9ab2a9dd43.png', 'gallery_578225ba9ab84dbf97.jpeg', 'gallery_749425ba9ab84dca0d.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:06:24', '2018-10-11 02:06:24');
INSERT INTO `products` VALUES ('35', '27917', 'Ajedrez Damas 40 cm. más Accesorios ', '4', '40 x 40 x 4', '1 Tablero de 40 x 40 cm\r\n8 piezas ajedrez blancas.\r\n8 piezas ajedrez negras.\r\n12 fichas negras de damas.\r\n12 fichas blancas de damas.\r\nCaja para guardar los accesorios. ', 'Juego de Ajedrez y Damas con piezas de ajedrez y fichas de damas. La medida del tablero es de 40 x 40 cm. En el cartón que lo envuelve hay troquelada una caja que se monta para guardar los accesorios.', '', null, 'gallery_710565ba9ac5f3c054.jpeg', 'gallery_653195ba9ac5f3c7b8.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:07:55', '2018-10-11 02:07:55');
INSERT INTO `products` VALUES ('36', ' 27923', ' PARCHÍS AUTOMÁTICO OCA ', '2', '37 x 37 x 6', '1 Tablero con parchís y 4 dados incrustados.\r\n1 dado para jugar a la oca.\r\n16 fichas (4 por color). ', 'Con este Parchís automático de 40 x 40cm en el que cada jugador dispone de su tirador automático y su cajón para guardar las fichas, ¡nunca más perderás un dado! También puedes jugar a La Oca dándole la vuelta al tablero.', '-', 'ADM001_cover_385445ba9acc37ae2f.png', 'gallery_567035ba9ace8f24e5.jpeg', 'gallery_76845ba9ace8f2c0b.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:07:59', '2018-10-11 02:07:59');
INSERT INTO `products` VALUES ('37', ' 27901', 'AJEDREZ MAGNÉTICO ', '8', '20 x 16 x 2', '1 Tablero de 16 x 16 cm con tapa.\r\n16 piezas blancas.\r\n16 piezas negras.', 'Juego de Ajedrez magnético ideal para viajes, gracias a su caja de plástico con tapa transparente se puede transportar con comodidad. La medida del tablero es de 16 x 16cm y las fichas son adecuadas para el tamaño de la cuadrícula del mismo.', '', 'ADM001_cover_157335ba9ad91e11d6.png', 'gallery_444805ba9adba17b61.jpeg', 'gallery_268895ba9adba183e1.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:09:41', '2018-10-11 02:09:41');
INSERT INTO `products` VALUES ('38', ' 27902', 'PARCHÍS MAGNÉTICO ', '4', '20 x 16 x 2', '1 Tablero de 16 x 16 cm con tapa.\r\n4 fichas rojas.\r\n4 fichas verdes.\r\n4 fichas amarillas.\r\n4 fichas azules.', 'Juego de Parchís magnético ideal para viajes, gracias a su caja de plástico con tapa transparente se puede transportar con comodidad. La medida del tablero es de 16 x 16cm y las fichas son adecuadas para el tamaño de las casillas del mismo.', '-', 'ADM001_cover_205615ba9ae0c1b727.png', 'gallery_91915ba9ae2d50112.jpeg', 'gallery_581115ba9ae2d5067f.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:09:09', '2018-10-11 02:09:09');
INSERT INTO `products` VALUES ('39', '27903', 'OCA MAGNÉTICO ', '4', '20 x 16 x 2', '1 Tablero de 16 x 16 cm con tapa.\r\n1 ficha roja.\r\n1 ficha verde.\r\n1 ficha amarilla.\r\n1 ficha azul.\r\n1 dado.', 'Juego de La Oca magnético ideal para viajes, gracias a su caja de plástico con tapa transparente se puede transportar con comodidad. La medida del tablero es de 16 x 16cm y las fichas son adecuadas para el tamaño de las casillas del mismo.', '-', null, 'gallery_948145ba9aeaa94946.jpeg', 'gallery_282705ba9aeaa96e0d.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:09:12', '2018-10-11 02:09:12');
INSERT INTO `products` VALUES ('40', ' 27904', 'DAMAS MAGNÉTICO ', '4', '20 x 16 x 2', '1 Tablero de 16 x 16 cm con tapa.\r\n12 fichas blancas.\r\n12 fichas negras. ', 'Juego de Las Damas magnético ideal para viajes, gracias a su caja de plástico con tapa transparente se puede transportar con comodidad. La medida del tablero es de 16 x 16cm y las fichas son adecuadas para el tamaño de las casillas del mismo.', '-', 'ADM001_cover_725725ba9aef456abf.png', 'gallery_395125ba9af167f89c.jpeg', 'gallery_432455ba9af168008e.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:09:19', '2018-10-11 02:09:19');
INSERT INTO `products` VALUES ('41', ' 27905', 'Ajedrez Damas Magnético 25 cm.', '8', '25 x 12 x 4', '1 Tablero de 23 x 23 cm plegable.\r\n16 piezas blancas.\r\n16 piezas negras.\r\n16 fichas rojas.\r\n16 fichas amarillas. ', 'Juego de Ajedrez y Damas magnético ideal para viajes, gracias a su caja de plástico plegable se puede disfrutar de estos tres juegos en uno en cualquier lugar. La medida del tablero es de 25 x 25cm y las fichas son adecuadas para el tamaño de las casillas del mismo.', '-', 'ADM001_cover_249835ba9af6ab3130.png', 'gallery_232575ba9af8160008.png', 'gallery_121465ba9af8160465.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:09:25', '2018-10-11 02:09:25');
INSERT INTO `products` VALUES ('42', ' 27918', 'DOMINÓ MARFIL ', '6', '15 x 4 x 5', '1 Estuche con tapa transparente.\r\n28 fichas Dominó.', 'Juego de Dominó en caja de plástico con tapa transparente. ', 'Medidas de las fichas: 2,1 x 4,2 x 0,7cm', 'ADM001_cover_88395baa4bc6ea1c0.png', 'gallery_96485baa4bec76015.png', 'gallery_343235baa4bec764be.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:11:44', '2018-10-11 02:11:44');
INSERT INTO `products` VALUES ('43', ' 27919', 'DOMINÓ CHAMELO JUNIOR', '6', '15 x 4 x 5', '1 Estuche con tapa transparente.\r\n28 fichas Dominó. ', 'Juego de Dominó en caja de plástico con tapa transparente.', 'Medidas de las fichas: 2,1 x 4,2 x 1,1cm', 'ADM001_cover_677205baa4cde0e445.png', 'gallery_753745baa4cecba5d2.png', 'gallery_241035baa4cecbaaa2.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:11:48', '2018-10-11 02:11:48');
INSERT INTO `products` VALUES ('44', ' 27920', 'DOMINÓ COMPETICIÓN', '6', '17 x 7 x 7', '1 Caja de madera.\r\n28 fichas Dominó. ', 'Juego de Dominó en caja de madera. transparente.', 'Medidas de las fichas: 2,2 x 4,4 x 1,1cm', 'ADM001_cover_250385baa4d49e994f.png', 'gallery_560365baa4d5fc0c81.png', 'gallery_835835baa4d5fc111f.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:11:53', '2018-10-11 02:11:53');
INSERT INTO `products` VALUES ('45', ' 27932', 'ACCESORIOS PARCHÍS 4 JUGADORES', '4', '14 x 6 x 4', '4 cubiletes de plástico\r\n4 fichas de cada color (rojo, verde, amarillo y azul).\r\n4 dados.\r\nCaja de plástico transparente para su almacenaje. ', 'Set completo de 4 cubiletes de plástico. Azul, Rojo, Verde y Amarillo. En caja de plástico transparente.', '-', 'ADM001_cover_86515baa6a5567a0d.png', 'gallery_788265baa6a7080de6.png', 'gallery_700295baa6a70812da.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:04', '2018-10-11 02:14:04');
INSERT INTO `products` VALUES ('46', ' 27934', 'ACCESORIOS PARCHÍS 6 JUGADORES', '4', '18 x 6 x 4', '6 cubiletes de plástico\r\n4 fichas de cada color (rojo, verde, amarillo, azul, naranja y morado).\r\n6 dados.\r\nCaja de plástico transparente para su almacenaje. ', 'Set completo de 6 cubiletes de plástico. Azul, Rojo, Verde, Amarillo, Naranja y Morado. En caja de plástico transparente.', '-', 'ADM001_cover_531535baa6ac5d379c.png', 'gallery_116395baa6ad45d86a.png', 'gallery_22235baa6ad45dcdb.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:12', '2018-10-11 02:14:12');
INSERT INTO `products` VALUES ('47', ' 27935', 'ACCESORIOS DAMAS', '4', '14 x 6 x 4', '12 fichas blancas.\r\n12 fichas negras.\r\nCaja de plástico transparente para su almacenaje.', 'Set completo de 24 fichas de plástico en caja de plástico transparente.', '-', 'ADM001_cover_322785baa6b8cef90e.png', 'gallery_211075baa6b9c2099b.png', 'gallery_18705baa6b9c24a65.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:18', '2018-10-11 02:14:18');
INSERT INTO `products` VALUES ('48', ' 27936', 'PIEZAS AJEDREZ STAUNTON Nº 3', '4', '14 x 7 x 5', '16 piezas blancas.\r\n16 piezas negras.\r\nCaja de plástico para su almacenaje. ', 'Set completo de figuras de Ajedrez Staunton Nº 3 en caja de plástico con tapa transparente.', '', 'ADM001_cover_649185baa6bfb1a73d.png', 'gallery_654315baa6c082faa1.png', 'gallery_210105baa6c082ff5c.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:24', '2018-10-11 02:14:24');
INSERT INTO `products` VALUES ('49', ' 27937', 'PIEZAS AJEDREZ STAUNTON Nº 4', '4', '19 x 12 x 7', '16 piezas blancas.\r\n16 piezas negras.\r\nCaja de plástico para su almacenaje. ', 'Set completo de figuras de Ajedrez Staunton Nº 4 en caja de plástico con tapa transparente.', '-', 'ADM001_cover_945495baa6c557d122.png', 'gallery_870355baa6c6439e82.png', 'gallery_487715baa6c643a336.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:32', '2018-10-11 02:14:32');
INSERT INTO `products` VALUES ('50', ' 27938', 'TAPETE 50 x 50 CM ', '-', '50 x 10 x 4', '1 tapete de fieltro. ', 'Tapete de fieltro ribeteado, dimensiones 50 x 50cm.', '-', 'ADM001_cover_374715baa6cbd59afd.png', 'gallery_148755baa6cce1fadd.png', 'gallery_758025baa6cce20034.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:14:37', '2018-10-11 02:14:37');
INSERT INTO `products` VALUES ('51', ' 28022', 'SILLAS COLORS ', '5', '26 x 26 x 8', '33 sillas en 3 colores diferentes.', 'Da un toque de color a tu mobiliario con Sillas Colors. 33 sillas para que os retéis a ver quién las equilibra mejor.', '-', 'ADM001_cover_310045baa6daa26390.png', 'gallery_136165baa6da1d9810.png', 'gallery_629475baa6da1d9e89.png', null, null, null, null, '0', '0', '7', '2018-09-25 19:17:39', '2018-09-25 19:17:39');
INSERT INTO `products` VALUES ('52', ' 28401', 'TOCO TRONCO ', '5', '18 x 27 x 10', '8 bloques interiores.\r\n32 bloques de corteza en 3 colores.\r\n1 bloque base de árbol\r\n1 hacha (inofensiva)\r\nInstrucciones.', '\r\nSaca las cortezas sin tirar el tronco. Toco Tronco es un juego de habilidad en el que dispones de 2 golpes en cada turno para sacar todas las cortezas que puedas, pero recuerda que no debes tirar las piezas interiores.\r\n', '-', 'ADM001_cover_715495baa6dff34b34.png', 'gallery_555965baa6e2482a5e.jpeg', 'gallery_618725baa6e248498c.png', null, null, null, '', '0', '0', '7', '2018-09-25 19:19:32', '2018-09-25 19:19:32');
INSERT INTO `products` VALUES ('53', ' 28402', 'THE NOODLE GAME ', '6', '14 x 17,5 x 14', '1 caja de noodles.\r\n29 noodles de 4 tamaños.\r\n4 pares de palillos chinos de colores.\r\n4 platos de colores.\r\nInstrucciones.', 'Hazte con todos lo noodles que puedas usando tus palillos pero, ¡cuidado!, los demás también querrán coger el que llevas. Cuanto más largo sea el noodle, más puntos valdrá, pero más fácil es de quitar.\r\n', '-', 'ADM001_cover_230825baa6eb3d5b3e.png', 'gallery_496255baa6ed6cceef.png', null, 'gallery_429375c0cbc80e36c5.jpeg', null, null, null, '1', '0', '7', '2018-12-09 07:56:00', '2018-12-09 07:56:00');
INSERT INTO `products` VALUES ('54', ' 28403', 'PARTY HEADZ', '8', '27 x 27 x 7', '10 Globos pequeños.\r\n3 Aros.\r\n1 Pelota de ping-pong.\r\n1 Ruleta.\r\n1 Botella de agua vacía.\r\n1 SOMBRERO DE BOMBÍN con una flor tornillo.\r\n1 SOMBRERO DE COPA con una pelota de ping-pong atada.\r\n4 SOMBREROS CONO.\r\n2 DIADEMAS con dos manos de velcro.\r\nInstrucciones.', 'Con 8 sombreros superdivertidos para intercambiar entre los amigos, hay un desafío diferente en cada turno. Intenta jugar al ping-pong con tu cabeza… o con tu SOMBRERO DE COPA lleno de agua, juega a las anillas con el SOMBRERO CONO de otro o pierde la cabeza en una batalla de DIADEMAS con manos. ', 'No querrás llevar el SOMBRERO BOMBÍN cuando el GLOBO explote….Ups!', 'ADM001_cover_838665baae5988d18f.png', 'gallery_448015baae5bb3f733.jpeg', 'gallery_83595baae5bc1365b.png', null, null, null, '', '0', '0', '7', '2018-09-26 03:49:47', '2018-09-26 03:49:47');
INSERT INTO `products` VALUES ('55', ' 27269', 'TOSTADORA LOCA ', '4', '27 x 27 x 12', '1 tostadora (segura) 40 tarjetas tostada 4 cazos 4 manteles Instrucciones', 'Un juego de reflejos y de habilidad para partirse de risa. Activa la tostadora... Atrapa las tostadas al vuelo, 1, 2, 3 ó 4 tostadas ¡no sabes cuántas van a saltar! Llena tu plato con las tostadas correctas, pero cuidado: no te vayas a llevar el pescado podrido.', '-', 'ADM001_cover_101905baae6148d06a.png', 'gallery_943545baae638017e5.png', 'gallery_114035baae63801d74.png', 'gallery_179595c0cbc63bca8b.jpeg', null, null, null, '1', '0', '7', '2018-12-09 07:55:31', '2018-12-09 07:55:31');
INSERT INTO `products` VALUES ('56', ' 27270', 'CAZA ZOMBIS ', '4', '27 x 27 x 14', '1 casa. 1 base electrónica. 6 personajes. Una pistola (segura). 4 dardos de espuma. Instrucciones', 'Qué vecindario más terrorífico... ¿has visto esa casa al final de la calle?... es una casa encantada... dicen que aparecen criaturas extrañas por la noche y se oyen escalofriantes sonidos... ', 'Ayúdanos a eliminar a todos los zombis... cuando aparezcan... dispárales... oirás horribles aullidos... ¿Podrás eliminarlos a todos? A por ellos... ¿Podrás tú solo con ellos? ¿O llamamos a los amigos?', null, 'gallery_312165baae6e7082f6.png', 'gallery_73525baae6e708992.png', null, null, null, null, '0', '0', '7', '2018-09-26 03:54:47', '2018-09-26 03:54:47');
INSERT INTO `products` VALUES ('57', ' 26600', 'FRUTTI FRUTTI ', '4', '27 x 27 x 14', '1 batidora (segura) 25 Frutti tarjetas Instrucciones', '\r\nReparte las Frutti tarjetas con pares de frutas. Cada jugador deberá meter en la batidora una de sus cartas que contenga una fruta que coincida con la última que se ha echado, pero ¡cuidado!: la batidora te las puede devolver en cualquier momento. Ganará quien primero se quede sin cartas. ¡¡Superdivertido!!', '-', null, 'gallery_206355baae76df0e9e.jpeg', 'gallery_362065baae76df1905.png', null, null, null, '', '0', '0', '7', '2018-09-26 03:57:01', '2018-09-26 03:57:01');
INSERT INTO `products` VALUES ('58', ' 25020', 'Cerditos Glotones ', '5', '26,7 x 26,7 x 8', '4 Hocicos con correas ajustables\r\n1 comedero con sonido (no necesita pilas)\r\n5 limones\r\n5 naranjas\r\n5 patatas\r\n5 manzanas\r\n2 dados.', 'No podrás parar de reír con este juego. Coge todas las frutas y verduras que puedas del comedero antes de que se te pase el turno, pero hazlo con tu morro de cerdito glotón y escucha cómo suena el comedero cuando lo intentas. ¡Diversión asegurada!', '-', 'ADM001_cover_50015baae7df787c6.png', 'gallery_718485baae8072ae4b.jpeg', 'gallery_115825baae8072b743.png', 'gallery_738365c0cbc43d40b4.jpeg', null, null, null, '1', '0', '7', '2018-12-09 07:54:59', '2018-12-09 07:54:59');
INSERT INTO `products` VALUES ('59', '24015', 'Torre de ratones', '4', '27 x 27 x 6 cm.', '1 base de queso, 7 trozos circulares de queso, 14 ratones, 20 fichas', '¿Conocéis a algún ratón al que no le guste el queso? A esos traviesos ratones lo que más les gusta es jugar a hacer torres. Ayúdales, pero con cuidado: cuanto más alta sea la torre más difícil será hacerla. Pon 2 ratones y una base de queso para cada altura.\r\n', '-', 'ADM001_cover_61095baae895da89b.png', 'gallery_768015baae8d79f877.jpeg', 'gallery_335375baae8d79feb5.png', null, null, null, '', '0', '0', '7', '2018-09-26 04:03:03', '2018-09-26 04:03:03');
INSERT INTO `products` VALUES ('60', ' 21011', 'SILLAS ', '5', '26,6 x 10,5 x 10,5', '8 sillas rojas, 8 sillas blancas y 8 sillas negras', '¿Eres de los que juega a hacer equilibrios con la cucharilla del café? Pues te va a encantar este juego. En sillas, se juega un número de rondas prefijado y el ganador es quien derriba menos sillas. Puedes ponerlas en cualquier dirección.\r\n', '-', 'ADM001_cover_439715baae93b7fbff.png', 'gallery_941265baae962baeab.jpeg', 'gallery_283085baae962bb461.png', null, null, null, '', '0', '0', '7', '2018-09-26 04:05:22', '2018-09-26 04:05:22');
INSERT INTO `products` VALUES ('61', ' 3570', 'Pincha El Pirata', '3', '27 x 20,7 x 15,1', '1 base, 1 barril, 1 pirata, 24 espadas, Adhesivos.', 'Consigue que salte el pirata con tu espada. Cada jugador coloca una espada en las ranuras libres y quien consiga que salte… ¡¡gana!!\r\n', '-', null, 'gallery_106455baaeb62a0364.png', 'gallery_327195baaeb62a0a70.png', null, null, null, '', '0', '0', '7', '2018-09-26 04:13:54', '2018-09-26 04:13:54');
INSERT INTO `products` VALUES ('62', ' 7777', 'Pincha Pirata más Torre Risa', '3', '26,8 x 40,2 x 14,5', 'Pincha el Pirata: 1 base, 1 barril, 1 pirata, 24 espadas, adhesivos. Torre de la risa: Torre de la risa (5 pisos) , Ruleta + Fichas payasos', 'Pincha El Pirata:Consigue que salte el pirata con tu espada. Cada jugador coloca una espada en las ranuras libres y quien consiga que salte… ¡¡gana!!Torre de la Risa: ¡Vaya torre!, ¡Vaya risa! Haz girar la ruleta y ve colocando tus payasetes..... Pero con cuidado, no vayas a derribar la torre y pierdas la partida.', '-', 'ADM001_cover_449915baaebe0a1c41.png', 'gallery_947965baaebecc8325.png', 'gallery_495385baaebecc8950.png', null, null, null, null, '0', '1', '7', '2018-12-09 07:53:45', '2018-12-09 07:53:45');
INSERT INTO `products` VALUES ('63', '28411', 'Sapiens ', '6', '13 x 13 x 8', 'Lata para guardar las cartas\r\n56 cartas\r\nInstrucciones.', 'Busca entre tus cartas una con el dibujo que ha dicho en idioma cavernícola el anterior jugador. Aquí no hay turnos, el más rápido se lleva el gato al agua.', '-', 'ADM001_cover_855545baaec7c82ab1.png', 'gallery_227805baaeca5099ed.jpeg', 'gallery_453445baaeca50a359.png', 'gallery_97945c0cbc9bb56f8.jpeg', null, null, null, '1', '0', '13', '2018-12-09 07:56:27', '2018-12-09 07:56:27');
INSERT INTO `products` VALUES ('64', ' 28412', 'Safari', '6', '13 x 13 x 8', 'Lata para guardar las cartas\r\n56 cartas\r\nInstrucciones.', '¿Qué animal falta en la carta? Sé quien lo descubre primero y llévate la carta. Con dos niveles de dificultad.\r\n', '-', 'ADM001_cover_363045baaecfda6946.png', 'gallery_332205baaed28a7fd3.jpeg', 'gallery_883085baaed28a8ad5.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:15:29', '2018-10-11 02:15:29');
INSERT INTO `products` VALUES ('65', ' 28413', 'Astrionauta ', '6', '13 x 13 x 8', 'Lata para guardar las cartas\r\n54 cartas\r\nInstrucciones.', 'En Astrionauta ganará quien antes sea capaz de formar tríos con sus cartas y las que hay en la mesa. Aquí no hay turnos…¡se juega a la velocidad de la luz!\r\n', '-', 'ADM001_cover_703075baaed81a311e.png', 'gallery_292525baaeda8c85d9.jpeg', 'gallery_247055baaeda8c91ab.png', null, null, null, null, '0', '0', '13', '2018-10-11 02:15:32', '2018-10-11 02:15:32');
INSERT INTO `products` VALUES ('66', ' 24009', 'Pizarra Letras y Números', '4', '27 x 38,8 x 5,1', '1 pizarra de plástico, 120 letras de plástico, 70 números y signos.', 'El juego que iniciará a los más peques en la formación de palabras y cifras: aprenderán letras y números de una forma divertida.\r\n', '-', 'ADM001_cover_721765baaee20cc41e.png', 'gallery_406915baaee3286c74.png', 'gallery_497885baaee32871eb.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:18:37', '2018-10-11 02:18:37');
INSERT INTO `products` VALUES ('67', ' 26541', 'Adivina Pintando ', '5', '25,6 x 25,6 x 6,3', 'Temporizador electrónico de cuenta atrás con sonido de cacareo, 50 Tarjetas con Palabras, 1 base para sujetar la tarjeta, 1 Dado especial de colores, 3 Libretas de dibujo pre-impresas con círculos. ', 'Dibuja, adivina y aprende ¿Podrán adivinar los otros jugadores lo que estás dibujando? ¡Súper fácil. Utiliza los círculo dibujados en la libreta para crear tu dibujo a partir de ellos! Cada tarjeta viene con 3 conceptos en español e inglés. El primero de ellos ya está dibujado en la tarjeta para que los más pequeños lo puedan copiar. El cronómetro cacareará cuando se acabe el tiempo y cuenta los puntos en inglés. ', '-', 'ADM001_cover_196735baaee7f124a0.png', 'gallery_875865baaee951b83e.png', 'gallery_555255baaee951bf1d.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:18:47', '2018-10-11 02:18:47');
INSERT INTO `products` VALUES ('68', ' 26542', 'Mimikids ', '4', '25,6 x 25,6 x 6,3', '150 tarjetas, dado, reloj de arena. ', '¿Te los imaginas haciendo mímica para adivinar conceptos como “pisar un chicle” o “limpiaparabrisas”? Con Mimikids, niñas y niños de diferentes edades podrán jugar juntos. Los más pequeños se divertirán haciendo gestos de palabras como: León, Perro, Gato… que además incluyen dibujos. Los más mayores actuarán para que los otros adivinen conceptos como “un perro buscándose la cola”, “freír bacon” o “un caleidoscopio”. ', 'Quien acierta se lleva un punto y el que interpreta también. Quien más puntos tenga al final habrá ganado la partida. Es un juego muy divertido donde toda la familia tratará de descubrir lo que pone en la tarjeta antes de que el reloj se acabe el tiempo.', 'ADM001_cover_547575baaef897b05e.png', 'gallery_803315baaefa3798ac.png', 'gallery_70005baaefa379e77.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:20:22', '2018-10-11 02:20:22');
INSERT INTO `products` VALUES ('69', ' 26543', 'Tic tac cuentos ', '6', '25,6 x 25,6 x 6,3', '150 tarjetas, cronómetro (no necesita pilas), instrucciones. ', '¡Contad un cuento…pero dentro del tiempo! Cuenta un fragmento de cuento con una de las palabras de tus tarjetas y pasa el turno al siguiente, que lo deberá continuar. Si suena el cronómetro en tu turno te quedarás con todas las tarjetas que han usado los demás. El primer jugador que se quede sin tarjetas gana el juego. Hay tarjetas con palabras más fáciles para los más pequeños.\r\n', '-', 'ADM001_cover_891545baaf181a0840.png', 'gallery_639105baaf1a1d5a2f.png', 'gallery_923175baaf1a1d6240.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:19:55', '2018-10-11 02:19:55');
INSERT INTO `products` VALUES ('70', ' 1408', 'Aprobado ', '6', '27 x 38,8 x 5,1', '1 Tablero, 6 Peones, 1 Dado, 2000 Preguntas.', 'El juego que les ayudará a estudiar jugando. Podrán aprender parte de los contenidos de enseñanza primaria y secundaria mientras pasan un rato divertido.\r\n', '-', 'ADM001_cover_840985baaf20a91113.png', 'gallery_932715baaf21e123bf.png', 'gallery_856325baaf21e12920.png', null, null, null, null, '0', '1', '13', '2018-12-09 07:53:22', '2018-12-09 07:53:22');
INSERT INTO `products` VALUES ('71', ' 24002', 'Sopa con letras ', '4', '37 x 27 x 9 cm', '1 mantel\r\n1 olla\r\n4 platos hondos\r\n4 cucharas de colores\r\n44 bolas de colores con letras\r\n21 fichas con palabras', 'En \"Sopa con letras\" has de intentar rellenar con las letras de la olla los huecos en las fichas que te toquen. Las tendrás que coger con tu cuchara lo antes posible para ser el ganador, pero ... si además la olla no deja de moverse y saltar, ¡todo es aún más divertido!\r\n', '-', null, 'gallery_17345baaf29c28fc0.jpeg', 'gallery_586955baaf29c29720.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:18:28', '2018-10-11 02:18:28');
INSERT INTO `products` VALUES ('72', ' 21001', 'No lo Digas ', '6', '26,5 x 26,5 x 5,4', '100 tarjetas, soporte tarjetas, temporizador, bloc de puntuación, lápiz y hoja de instrucciones.', 'Intenta ganar la mayor cantidad de puntos posibles, eso sí, no digas ninguna de las palabras prohibidas.\r\n', '-', 'ADM001_cover_181015baaf33784414.png', 'gallery_923995baaf36d291c4.png', 'gallery_655715baaf36d29a2d.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:18:25', '2018-10-11 02:18:25');
INSERT INTO `products` VALUES ('73', '1710', 'El Primero de la Clase 1.000', '6', '25,6 x 25,6 x 3,7', 'Tablero , 6 peones , 1000 preguntas, 1 dado', 'Avanza por el tablero de juego de pupitre en pupitre, contestando a las diferentes preguntas para llegar a la prueba final y conseguir el graduado escolar. Con 1000 preguntas de primaria.', '-', null, 'gallery_923195baaf574c93b4.png', 'gallery_996005baaf574c9a1b.png', null, null, null, null, '0', '1', '13', '2018-10-11 02:16:44', '2018-10-11 02:16:44');
INSERT INTO `products` VALUES ('74', ' 1720', 'El Primero de la Clase 2.000', '8', '25,6 x 25,6 x 3,7', 'Tablero , 6 peones , 2000 preguntas, 1 dado', 'Avanza por el tablero de juego de pupitre en pupitre, contestando a las diferentes preguntas para llegar a la prueba final y conseguir el graduado escolar. Con 2000 preguntas de primaria.', '-', 'ADM001_cover_183875baaf62e0e790.png', 'gallery_201585baaf64feaffb.png', 'gallery_611985baaf64feb603.png', null, null, null, null, '0', '0', '13', '2018-12-09 07:53:36', '2018-12-09 07:53:36');
INSERT INTO `products` VALUES ('75', ' 1730', 'El Primero de la Clase 3.000', '10', '25,6 x 25,6 x 6,3', 'Tablero , 6 peones , 3000 preguntas, 1 dado', 'Avanza por el tablero de juego de pupitre en pupitre, contestando a las diferentes preguntas para llegar a la prueba final y conseguir el graduado escolar. Con 3000 preguntas de primaria.', '-', 'ADM001_cover_867885baaf700821df.png', 'gallery_743635baaf721c41c1.png', 'gallery_18545baaf721c4d4e.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:17:54', '2018-10-11 02:17:54');
INSERT INTO `products` VALUES ('76', '10', 'El Primero de la Clase 5.000', '1750', '25,6 x 25,6 x 6,3', 'Tablero , 6 peones , 5000 preguntas, 1 dado', 'Avanza por el tablero de juego de pupitre en pupitre, contestando a las diferentes preguntas para llegar a la prueba final y conseguir el graduado escolar. Con 5000 preguntas de primaria.', '-', 'ADM001_cover_916315baafada481a1.png', 'gallery_624225baafaf55331c.png', 'gallery_803645baafaf553d43.png', null, null, null, null, '0', '1', '13', '2018-12-09 07:53:09', '2018-12-09 07:53:09');
INSERT INTO `products` VALUES ('77', ' 1453', 'Dominó Madera Colors', '3', '16,8 x 35,8 x 4', '28 Fichas de Madera maciza de 8x4x1 Cm', 'Este Dominó de gran tamaño hecho de madera maciza es el más indicado para los más pequeños. El tamaño de las fichas permite que éstas se mantengan en pie. Este juego desarrolla la atención y estimula el pensamiento lógico.\r\n', '-', null, 'gallery_424615bab055ed3be3.png', 'gallery_373935bab055ed42a8.png', null, null, null, null, '0', '1', '13', '2018-10-11 02:16:38', '2018-10-11 02:16:38');
INSERT INTO `products` VALUES ('78', ' 11605', 'Tangram Madera', '8', '25 x 17,5 x 3,5', '7 Figuras de madera + Libro de reglamento', 'En este interesante y legendario rompecabezas chino deben utilizarse las 7 piezas que lo componen para ir formando las diferentes figuras, cada cual más complicada y entretenida.\r\n', '-', 'ADM001_cover_16235bab06163a4f7.png', 'gallery_693395bab062cab9a4.png', 'gallery_223095bab062cac22a.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:19:42', '2018-10-11 02:19:42');
INSERT INTO `products` VALUES ('79', ' 28415', '¿Qué soy yo? Máscaras ', '7', '27 x 27 x 6', '30 máscaras a doble cara (30 animales y 30 personas)\r\n1 tablero a doble cara (para jugar a ser animales o personas)\r\n4 gafas.\r\n4 fichas de juego.\r\n48 fichas SÍ/NO\r\n1 Ruleta\r\nHoja de respuestas.\r\nInstrucciones', 'El divertido juego de las adivinanzas con personajes humanos y animales.\r\nHaz girar la flecha y realiza la pregunta que te haya tocado en el tablero. Podrás guardar la contestación poniendo una ficha SÍ/NO.\r\nHay 30 máscaras con un personaje en cada cara, lo que hace un total de 60 personajes.', '-', 'ADM001_cover_512575bad89ab5747c.png', 'gallery_416875bad89dcf0c6b.jpeg', 'gallery_24475bad89dcf17ec.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:22:54', '2018-10-11 02:22:54');
INSERT INTO `products` VALUES ('80', ' 27572', ' Gran Jaleo ', '6', '25,6 x 25,6 x 7,1', '6 animales de espuma (oveja, cabra, cerdo, caballo, vaca y gallina).\r\n24 tarjetas.\r\n1 dado negro.\r\n1 dado blanco.\r\nInstrucciones.', 'Madre mía menudo lío tenemos en la granja…\r\n¡A Leo el granjero se le han escapado los animales!\r\nUn juego de rapidez mental y visual en el que tienes que ser el más ágil y rápido capturando los animales de la granja.\r\nPara jugar con amigos… colocad los 6 animales en círculo… sacad una tarjeta… tirad los dados… Cada tarjeta que coincida con alguno de los dados se activa y tenéis que intentar capturar el animal que muestre la tarjeta activada.\r\nDebéis traerlos de vuelta antes de que se metan en algún jaleo… y antes que vuestros oponentes.\r\nUn juego de habilidad y ¡manos rápidas!', '-', null, 'gallery_566815bad8a56aa162.jpeg', 'gallery_327335bad8a56aa772.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:20:00', '2018-10-11 02:20:00');
INSERT INTO `products` VALUES ('81', ' 25014', ' Caperucita Roja ', '4', '27 x 27 x 7', '1 tablero, 2 casas 3D, 4 peones caperucita con 4 peanas, 3 árboles 3D, 1 lobo, 1 abuelita, 24 accesorios (4 x cazador, 4 x miel, 4 x tarta, 4 x caperuza, 4 x cesta, 4 x tortitas), 1 dado, 1 librito.', '¿Quién llevará primero a Caperucita a casa de la abuelita con todo lo necesario para sacar al lobo de allí?. Sal de la casa de la mamá, recorre el tablero con tu Caperucita y recoge tu capa, el cazador, el pastel… ', '¡Con figuras 3D para montar y un librito con el cuento!', 'ADM001_cover_365455bad8aa487131.png', 'gallery_117765bad8acb718b3.png', 'gallery_34785bad8acb71f49.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:22:34', '2018-10-11 02:22:34');
INSERT INTO `products` VALUES ('82', ' 25015', 'Pinocho', '4', '27 x 27 x 7', '1 tablero, 28 narices de 4 colores y 4 gomas elásticas, 4 peones pinocho con 4 peanas, 1 dado, 1 librito.', 'Para llegar a ser un niño de verdad, Pinocho deberá recorrer el tablero. En ese camino, te irás colocando y quitando narices una encima de otra, según contestes una mentira o una verdad. Sé el primero en quitártelas todas al llegar a la casilla final.', '¡Con 28 narices para ponerse y un librito con el cuento!', 'ADM001_cover_972615bad8b2aab695.png', 'gallery_622845bad8b416f8d3.png', 'gallery_913325bad8b41701b6.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:18:41', '2018-10-11 02:18:41');
INSERT INTO `products` VALUES ('83', ' 25015', ' Blancanieves ', '4', '27 x 27 x 7', '1 tablero, 4 peones Príncipe con 4 peanas, 28 enanitos (7 por jugador), 1 Blancanieves, 1 Reina Malévola, 1 casa 3D, 2 árboles 3D, 1 dado, 1 librito.', 'La reina Malévola no puede soportar que Blancanieves sea la más guapa del reino. Ayuda con tu Príncipe a Blancanieves a recuperar a todos los enanitos y enviar fuera del bosque a la reina Malévola.', '¡Con figuras 3D para montar y un librito con el cuento!', null, 'gallery_302505bad8bd5d24ae.png', 'gallery_182785bad8bd5d2b53.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:22:41', '2018-10-11 02:22:41');
INSERT INTO `products` VALUES ('84', ' 25017', 'Los Tres Cerditos ', '4', '27 x 27 x 7', '1 tablero, 4 peones cerdos con 4 peanas, 12 casas 3D, 1 caldero 3D, 1 lobo, 1 caballo, 1 oveja, 1 gallina, 1 dado, 1 librito.', 'Un lobo hambriento merodea el bosque. Los tres cerditos parten de casa de su mamá para comenzar una nueva vida, pero no saben lo que les espera. Ayuda a los cerditos a construir las tres casas del cuento. Sopla fuerte y diviértete tirando al lobo al caldero.', '¡Con figuras 3D para montar y un librito con el cuento!', 'ADM001_cover_570105bad8c44b48c3.png', 'gallery_940715bad8c5b43802.png', 'gallery_397995bad8c5b43e88.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:22:52', '2018-10-11 02:22:52');
INSERT INTO `products` VALUES ('85', ' 9506', 'Qué Soy Yo ', '9', '31 x 31 x 5,3', '4 tableros de juego, 1 cartón ruleta, 1 saeta ruleta, 20 tarjetas oficios, 4 cintas elásticas.', 'Todos saben quién eres menos tú. Tienes que descubrir quién eres haciendo preguntas a los otros jugadores. Utiliza tu cabeza para descubrir qué tienes en ella. El primer jugador que consiga tres tarjetas ¡¡gana!!\r\n', '-', 'ADM001_cover_440235bad8cbb9fe11.png', 'gallery_301905bad8ce12a92c.png', 'gallery_46365bad8ce12afbc.png', null, null, null, null, '0', '0', '15', '2018-10-11 02:22:45', '2018-10-11 02:22:45');
INSERT INTO `products` VALUES ('86', ' 28403', 'PARTY HEADZ', '8', '27 x 27 x 7', '10 Globos pequeños.\r\n3 Aros.\r\n1 Pelota de ping-pong.\r\n1 Ruleta.\r\n1 Botella de agua vacía.\r\n1 SOMBRERO DE BOMBÍN con una flor tornillo.\r\n1 SOMBRERO DE COPA con una pelota de ping-pong atada.\r\n4 SOMBREROS CONO.\r\n2 DIADEMAS con dos manos de velcro.\r\nInstrucciones.', 'Con 8 sombreros superdivertidos para intercambiar entre los amigos, hay un desafío diferente en cada turno. Intenta jugar al ping-pong con tu cabeza… o con tu SOMBRERO DE COPA lleno de agua, juega a las anillas con el SOMBRERO CONO de otro o pierde la cabeza en una batalla de DIADEMAS con manos. No querrás llevar el SOMBRERO BOMBÍN cuando el GLOBO explote….Ups!\r\n', '-', 'ADM001_cover_628105bad8da4d1f73.png', 'gallery_281375bad8dcface6d.jpeg', 'gallery_3445bad8dcfad49b.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:11:27', '2018-09-28 04:11:27');
INSERT INTO `products` VALUES ('87', ' 28422', 'De uvas a peras ', '8', '27 x 27 x 7', '528 tarjetas a dos caras.\r\nInstrucciones.', '¿Qué tiene que ver el tocino con la velocidad? Pues seguro que tienen algo en común. En De Uvas a Peras, enlaza conceptos rápidamente hasta que te quedes sin tarjetas. Con 528 tarjetas y más de 1000 conceptos.', '-', 'ADM001_cover_165285bad8e462d24f.png', 'gallery_436725bad8e7c41c52.jpeg', 'gallery_116035bad8e7c4221e.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:14:20', '2018-09-28 04:14:20');
INSERT INTO `products` VALUES ('88', ' 26554', ' BEZZERWIZZER ', '16', '25,6 x 25,6 x 6,3', '200 tarjetas, 20 fichas categorías, 4 fichas ZWAP (1 de cada color), 8 fichas BEZZERWIZZER (2 de cada color), 4 peones (1 de cada color), 1 caja porta-tarjetas, 1 bolsa de tela para las fichas de categorías, 1 tablero central, 4 tableros individuales, instrucciones.', 'BEZZERWIZZER by Jesper Bülow El juego que pondrá a prueba tu conocimiento y tu estrategia. En cada ronda deberás contestar las preguntas de cuatro categorías que habrás sacado de la bolsa. Puedes usar tus comodines para contestar por otro jugador y ganar más puntos o simplemente róbale su categoría favorita, pero recuerda... ¡¡¡te pueden hacer lo mismo a ti también!!! ¿Te atreves con el reto Bezzerwizzer?', 'Más información: www.bezzerwizzer.es.', 'ADM001_cover_477205bad8ee42dbd4.png', 'gallery_63555bad8f1339694.jpeg', 'gallery_600145bad8f1339b9c.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:16:51', '2018-09-28 04:16:51');
INSERT INTO `products` VALUES ('89', ' 26549', 'Talk & win BEST SELLER PLUS ', '12', '25,6 x 25,6 x 6,3', '252 tarjetas, 1 reloj de arena, tablero, 2 peones, instrucciones.', '\r\nCuantas más tarjetas contestéis más avanzaréis. Un juego de habilidad, colaboración y diversión. Dividíos en dos equipos… describid, haced payasadas, sonidos y cualquier otra cosa que se os ocurra para acertar el máximo de palabras. ¡Ok, la acertamos! Rápido, ¡siguiente tarjeta! 3 categorías de tarjetas 1.- Descripción Si caéis en una casilla Persona, Mundo, Objeto, Acción, Naturaleza, Varios o Reto es momento de describir las palabras con ayuda de frases, gestos, sonidos y todo lo que se os pase por la cabeza. 2.-La lista ¿Queréis pasar a la siguiente tarjeta?, pues decid 5 palabras relacionadas con el concepto que aparece en ella. 3.-Ordenar Correctamente ¿Cuál es más antiguo? ¿Cuál es más pequeño? ¿Cuál es más veloz? Averiguad cuál es el orden correcto de los 5 conceptos que aparecen en la tarjeta. ', '-', 'ADM001_cover_835955bad8feeaf4c7.png', 'gallery_440965bad9018acf84.png', 'gallery_506945bad9018ad4c5.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:21:12', '2018-09-28 04:21:12');
INSERT INTO `products` VALUES ('90', ' 26548', 'Sketchy BEST SELLER PLUS ', '8', '25,6 x 25,6 x 6,3', '100 Tarjetas, dado, cronómetro electrónico, 8 lápices, bloc de papel “Sketchy”. ', '¿Preparados? ¿Listos ?..¡Ya! Dibujad rápidamente cosas relacionadas con la categoría que ha salido en el dado, como por ejemplo…”Cosas que se pueden enchufar”. Vais a perder los estribos intentando coincidir con las respuestas de vuestro compañero. Cuando el tiempo se acabe comprobaréis cuántos dibujos coinciden. Veréis que no hay nada mejor ni más divertido que ¡jugar a SKETCHY! ', '-', 'ADM001_cover_203335bad909646eb5.png', 'gallery_649075bad9527c69b3.jpeg', 'gallery_626935bad90d4d8eda.png', null, null, null, null, '0', '0', '11', '2018-09-28 04:42:47', '2018-09-28 04:42:47');
INSERT INTO `products` VALUES ('91', ' 25008', 'Cámara y Acción BEST SELLER PLUS', '12', '27 x 27 x 7', '250 tarjetas, 2 libretas anotación, 2 lápices, 1 reloj de arena, instrucciones.', '\r\nConviértete en un director, un actor, un mimo, un poeta. ¡Libera al artista que hay dentro de ti! Formad dos equipos y ganad tantos puntos como sea posible cumpliendo con éxito todos los desafíos de las tarjetas en el tiempo asignado por el reloj. Hay 700 retos de 5 categorías (mímica, improvisación, preguntas, interpretación y poesía.) El equipo que obtenga más puntos en las rondas que estipuléis será el ganador.', '-', 'ADM001_cover_657775bad962769a81.png', 'gallery_316115bad963c0ba91.png', 'gallery_83325bad963c0c0cb.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:47:24', '2018-09-28 04:47:24');
INSERT INTO `products` VALUES ('92', '24012  ', 'Last Word ', '14', '20,4 x 27 x 6,4 cm', '2 tableros de juego, 230 tarjetas de tema, 56 tarjetas de letras, 8 peones de color, 1 temporizador electrónico, instrucciones.', 'El juego que decide quién tiene la última palabra. Con más de 1.000.000 de unidades vendidas en EE.UU. llega a España LAST WORD. Todos saben la pregunta ... todos saben las repuestas ... pero lo que nadie sabe es cuándo acaba el tiempo para contestar. Puedes contestar todas las veces que quieras, pero no puedes repetir lo que han contestado tus compañeros. ¡CONTESTA JUSTO ANTES DE QUE SE ACABE EL TIEMPO!', '', 'ADM001_cover_732935bad96db94663.png', 'gallery_912545bad96f05b37c.png', 'gallery_704615bad96f05b98c.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:50:24', '2018-09-28 04:50:24');
INSERT INTO `products` VALUES ('93', ' 27525', 'Cifras y Letras ', '10', '25,6 x 25,6 x 6,3', 'Tablero cartón con ruleta incluida, 2 blocs de notas, 4 rotuladores, 360 tarjetas, 1 reloj de arena, 4 peones', '\r\n\r\nCifras y Letras es un juego para combinar letras y calcular. Intenta hacer la palabra más larga con la combinación de letras que salga, o bien intenta llegar al número exacto con las cifras que aparezcan.\r\n', '-', 'ADM001_cover_447015bad97483ec02.png', 'gallery_452845bad97728c873.jpeg', 'gallery_96375bad97728d416.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:52:34', '2018-09-28 04:52:34');
INSERT INTO `products` VALUES ('94', ' 4200', 'Sinónimos y Antónimos ', '9', '25 x 35,5 x 6', '1 tablero\r\n6 fichas + 6 peones\r\n432 tarjetas preguntas\r\n1 reloj de arena', '\r\nEl juego de los sinónimos, contrarios e ideas afines. Sé el primero que completa las vueltas estipuladas al tablero contestando con sinónimos o antónimos dependiendo de la casilla en la que caigas.\r\n', '-', 'ADM001_cover_782695bad97c405acc.png', 'gallery_673365bad97eaba32f.jpeg', 'gallery_308315bad97eabaa7b.png', null, null, null, '', '0', '0', '11', '2018-09-28 04:54:34', '2018-09-28 04:54:34');
INSERT INTO `products` VALUES ('95', ' 1040', 'Caja Magia 50 Trucos ', '7', '21 x 29,6 x 4,8', 'Contiene manual y accesorios para realizar 50 Trucos diferentes.', 'Con la caja de 50 Trucos de Magia Falomir podrás realizar divertidos trucos e iniciarte en el mundo de la magia.\r\nPuedes descubrir trucos de este juego en nuestro canal YouTube', '-', 'ADM001_cover_273735bad98a69fd3e.png', 'gallery_904455bad98d1c1565.jpeg', 'gallery_16635bad98d1c1c6f.png', null, null, null, '', '0', '0', '12', '2018-09-28 04:58:25', '2018-09-28 04:58:25');
INSERT INTO `products` VALUES ('96', ' 1060', 'Caja Magia 100 Trucos ', '7', '24 x 35,6 x 4,7', 'Contiene manual y accesorios para realizar 100 Trucos diferentes.', 'Con la caja de 100 Trucos de Magia Falomir podrás realizar divertidos trucos e iniciarte en el mundo de la magia.\r\nPuedes descubrir trucos de este juego en nuestro canal YouTube', '-', 'ADM001_cover_854175bad994cc45cb.png', 'gallery_681115bad99754918c.jpeg', 'gallery_972995bad997549b5b.png', null, null, null, '', '0', '0', '12', '2018-09-28 05:01:09', '2018-09-28 05:01:09');
INSERT INTO `products` VALUES ('97', ' 1160', 'Taller de Magia 200 Trucos', '7', '30 x 43 x 5', 'Contiene manual y accesorios para realizar 200 Trucos diferentes', 'Con la caja de 200 Trucos de Magia Falomir podrás realizar divertidos trucos e iniciarte en el mundo de la magia.\r\nPuedes descubrir trucos de este juego en nuestro canal YouTube', '-', 'ADM001_cover_123485bad9a5181aea.png', 'gallery_27375bad9a64bf773.jpeg', 'gallery_567085bad9a64bffa3.png', null, null, null, '', '0', '0', '12', '2018-09-28 05:05:08', '2018-09-28 05:05:08');
INSERT INTO `products` VALUES ('98', ' 28435', 'Alfarería POP ART ', '8', '27 x 39 x 6', '    2 bandejas de plástico con 18 moldes.\r\n    Juego de 6 acuarelas.\r\n    Pincel.\r\n    Bolsa con escayola.\r\n    Cazoleta y espátula.\r\n    Instrucciones.', 'El mundo del POP ART en tus manos hecho de escayola. 18 moldes que te llevarán a la parte más creativa de esa tendencia. Para decorar con acuarelas a tu gusto.\r\n', '-', 'ADM001_cover_397755bb00cd37e3be.png', 'gallery_683885bb00ce87540c.png', 'gallery_918675bb00ce875e03.png', null, null, null, '', '0', '0', '14', '2018-09-30 01:38:16', '2018-09-30 01:38:16');
INSERT INTO `products` VALUES ('99', ' 28436', 'Alfarería Flower Power', '8', '27 x 39 x 6', '2 bandejas de plástico con 15 moldes.\r\nJuego de 6 acuarelas.\r\nPincel.\r\nBolsa con escayola.\r\nCazoleta y espátula.\r\nInstrucciones. ', 'Alfarería Flower Power. 15 moldes para escayola llenos de paz y amor para decorarlos a tu gusto.', '-', 'ADM001_cover_244975bb00d4aee1d2.png', 'gallery_312855bb00d57d45d0.png', 'gallery_580295bb00d57d4fc8.png', null, null, null, '', '0', '0', '14', '2018-09-30 01:40:07', '2018-09-30 01:40:07');
INSERT INTO `products` VALUES ('100', ' 28437', 'Alfarería Princess', '8', '27 x 39 x 6', '2 bandejas de plástico con 10 moldes.\r\nJuego de 6 acuarelas.\r\nPincel.\r\nBolsa con escayola.\r\nCazoleta y espátula.\r\nInstrucciones. ', 'Crea y decora tus modelos de escayola de temática princesas. Con 10 moldes diferentes.', '-', 'ADM001_cover_745145bb00ddbddf6e.png', 'gallery_591585bb00df272915.png', 'gallery_405315bb00df272ef1.png', null, null, null, '', '0', '0', '14', '2018-09-30 01:42:42', '2018-09-30 01:42:42');
INSERT INTO `products` VALUES ('101', '28438', 'Alfarería Piratas', '8', '27 x 39 x 6', '2 bandejas de plástico con 14 moldes.\r\nJuego de 6 acuarelas.\r\nPincel.\r\nBolsa con escayola.\r\nCazoleta y espátula.\r\nInstrucciones. ', 'Moldea tus diseños con figuras piratas de escayola. Luego podrás decorarlas pintándolas a tu gusto. Con 14 moldes distintos.', '-', 'ADM001_cover_919865bb00f47bcda2.png', 'gallery_834405bb00f578d4c9.png', 'gallery_563845bb00f578da51.png', null, null, null, '', '0', '0', '14', '2018-09-30 01:48:39', '2018-09-30 01:48:39');
INSERT INTO `products` VALUES ('102', ' 28439', 'Alfarería Dinosaurios ', '8', '27 x 39 x 6', '2 bandejas de plástico con 8 moldes.\r\nJuego de 6 acuarelas.\r\nPincel.\r\nBolsa con escayola.\r\nCazoleta y espátula.\r\nInstrucciones.', '\r\n¿Vas buscando huesos de dinosaurio cuando sales al campo? Con Alfarería Dinosaurios podrás moldear tus propios esqueletos del Jurásico y decorarlos a tu gusto. Con 8 moldes diferentes.\r\n', '-', 'ADM001_cover_917395bb00faf81d44.png', 'gallery_469255bb00fc1f0700.png', 'gallery_702845bb00fc1f0fe6.png', null, null, null, '', '0', '0', '14', '2018-09-30 01:50:25', '2018-09-30 01:50:25');
INSERT INTO `products` VALUES ('103', ' 26539', 'ALFARERÍA', '8', '26,8 x 39 x 5,8', '1 Bolsa de escayola\r\n1 Molde jarra\r\n1 Molde vasija\r\n1 Cánula de plástico\r\n4 Clips cierre moldes jarra-vasija\r\n2 Soportes moldes jarra-vasija\r\n1 Espátula\r\n1 Cazoleta\r\n1 Pincel\r\n1 Juego de acuarelas', 'Deja volar tu imaginación y conviértete en un artista. Con ALFARERÍA, podrás decorar como quieras las piezas que hayas creado antes con escayola. En este juego dispones de moldes para realizar jarras y vasijas, así como de acuarelas para decorarlas y de la escayola necesaria.\r\n', '-', 'ADM001_cover_572355bb012371d18c.png', 'gallery_246245bb01250d2a16.png', 'gallery_842135bb01250d3000.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:01:20', '2018-09-30 02:01:20');
INSERT INTO `products` VALUES ('104', ' 26540', 'ALFARERÍA PLUS ', '8', '34 x 49,5 x 6', '1 Bolsa de escayola\r\n1 Molde jarra\r\n1 Molde vasija\r\n2 Moldes maceta\r\n2 Cánulas de plástico\r\n2 Vacíos maceta\r\n4 Clips cierre moldes jarra-vasija\r\n8 Clips cierre moldes maceta\r\n2 Soportes moldes jarra-vasija\r\n2 Bandejas con 30 moldes de ?guras planas\r\n1 Espátula\r\n1 Cazoleta\r\n1 Pincel\r\n1 Juego de acuarelas', 'Deja volar tu imaginación y conviértete en un artista. Con ALFARERÍA PLUS, podrás decorar como quieras las piezas que hayas creado antes con escayola. En este juego dispones de moldes para realizar jarras, vasijas, macetas y figuras en relieve, así como de acuarelas para decorarlas y de la escayola necesaria.\r\n', '-', 'ADM001_cover_870505bb0130ed3d8c.png', 'gallery_405785bb0131e55fa6.png', 'gallery_624415bb0131e5650b.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:04:46', '2018-09-30 02:04:46');
INSERT INTO `products` VALUES ('105', ' 11531', 'Tatuajes Mágicos Azul ', '6', ' 27 x 38,8 x 5,1', '1 pupitre, 4 rotuladores, Tarjetas con diseños, 25 hojas de papel cebolla, 1 esponjita.', '¡¡Crea y decora tus propios tatuajes mágicos!!', '-', 'ADM001_cover_769405bb013700a9c8.png', 'gallery_53075bb0138d035f0.png', 'gallery_637335bb0138d03b7a.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:06:37', '2018-09-30 02:06:37');
INSERT INTO `products` VALUES ('106', '11532', 'Tatuajes Mágicos Rosa ', '6', '27 x 38,8 x 5,1', '1 pupitre, 4 rotuladores, Tarjetas con diseños, 25 hojas de papel cebolla, 1 esponjita.', '¡¡Crea y decora tus propios tatuajes mágicos!!', '-', 'ADM001_cover_138365bb013e69ac29.png', 'gallery_544875bb013fa754ec.png', 'gallery_58795bb013fa75a66.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:08:26', '2018-09-30 02:08:26');
INSERT INTO `products` VALUES ('107', ' 2484', 'Mis Tatuajes y Pegatinas', '6', '33,8 x 49,4 x 6', 'Papel cebolla, Máquina de tatuajes, 4 rotuladores, Hojas con diseños, Tatuajes, Esponja, pegatinas', 'Pásatelo en grande creando tus propios tatuajes y pegatinas, crea diseños únicos, fáciles de poner y quitar.', '-', 'ADM001_cover_26515bb01552b9880.png', 'gallery_242035bb0157b891ee.png', 'gallery_499125bb0157b899a9.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:14:51', '2018-09-30 02:14:51');
INSERT INTO `products` VALUES ('108', ' 11539', 'Diseños Mágicos', '4', '27 x 38,8 x 5,1', '1 pantalla de diseño, 5 Placas reversibles, 9 rotuladores, 8 hojas, 1 rascador.', 'Este juego ofrece la posibilidad a los niños de pasar un rato divertido, imaginando y creando, gracias a la cantidad de combinaciones de sus plantillas, podrán crear diferentes movimientos y vestir de muchas maneras diferentes a sus personajes. Con este juego podrán desarrollar la creatividad e imaginación.\r\n', '', 'ADM001_cover_498535bb015eaa9be2.png', 'gallery_520265bb0160048556.png', 'gallery_876945bb0160048d89.png', null, null, null, '', '0', '0', '14', '2018-09-30 02:17:04', '2018-09-30 02:17:04');

-- ----------------------------
-- Table structure for products_category
-- ----------------------------
DROP TABLE IF EXISTS `products_category`;
CREATE TABLE `products_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `image` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `banner` varchar(180) COLLATE latin1_general_ci DEFAULT NULL,
  `background_color` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `principal` int(2) NOT NULL,
  `father_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `father_id` (`father_id`),
  CONSTRAINT `products_category_ibfk_1` FOREIGN KEY (`father_id`) REFERENCES `products_category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of products_category
-- ----------------------------
INSERT INTO `products_category` VALUES ('2', 'Bingos', null, null, null, null, '0', '13');
INSERT INTO `products_category` VALUES ('3', 'Tableros', 'ADM001_cat_393245bacbd507073e.jpeg', null, null, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum urna ac libero viverra, sed bibendum lacus sagittis. Maecenas ullamcorper tellus placerat lorem rhoncus, eget volutpat dolor placerat.', '0', '13');
INSERT INTO `products_category` VALUES ('4', 'Magnéticos', null, null, null, null, '0', '13');
INSERT INTO `products_category` VALUES ('5', 'Dominós', null, null, null, null, '0', '13');
INSERT INTO `products_category` VALUES ('6', 'Accesorios', null, null, null, null, '0', '13');
INSERT INTO `products_category` VALUES ('7', 'Habilidad', 'ADM001_cat_330885bad725048fff.jpeg', null, null, null, '1', null);
INSERT INTO `products_category` VALUES ('8', 'Cartas', null, null, null, null, '0', '13');
INSERT INTO `products_category` VALUES ('9', 'Educativos', null, null, null, null, '0', '15');
INSERT INTO `products_category` VALUES ('10', 'Infantiles', null, null, null, null, '0', '15');
INSERT INTO `products_category` VALUES ('11', 'Family & Friends', null, null, null, null, '0', '15');
INSERT INTO `products_category` VALUES ('12', 'Magia', 'ADM001_cat_330385bad726c1e4ea.jpeg', null, null, '', '1', null);
INSERT INTO `products_category` VALUES ('13', 'Juegos Clásicos', 'ADM001_cat_164965bad728907d13.jpeg', null, null, '', '1', null);
INSERT INTO `products_category` VALUES ('14', 'Artísticos', null, null, null, '-', '0', '7');
INSERT INTO `products_category` VALUES ('15', 'Juegos de Mesa', 'ADM001_cat_411145c0da32045e1e.jpeg', 'banner_521745c0da1b1d5208.png', null, null, '1', null);

-- ----------------------------
-- Table structure for products_subcategory
-- ----------------------------
DROP TABLE IF EXISTS `products_subcategory`;
CREATE TABLE `products_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_subcategory_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `products_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products_subcategory
-- ----------------------------
INSERT INTO `products_subcategory` VALUES ('2', 'Bingo', '13');
INSERT INTO `products_subcategory` VALUES ('3', 'Tableros', '13');
INSERT INTO `products_subcategory` VALUES ('4', 'Magnéticos', '13');
INSERT INTO `products_subcategory` VALUES ('5', 'Dominó', '13');
INSERT INTO `products_subcategory` VALUES ('6', 'Accesorios', '13');
INSERT INTO `products_subcategory` VALUES ('7', 'Cartas', '13');
INSERT INTO `products_subcategory` VALUES ('8', 'Educativos', '15');
INSERT INTO `products_subcategory` VALUES ('9', 'Infantiles', '15');
INSERT INTO `products_subcategory` VALUES ('10', 'Family and Friends', '15');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(80) NOT NULL,
  `role_name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'ROLE_ADMIN', 'ADMINISTRADOR');
INSERT INTO `role` VALUES ('2', 'ROLE_MODERATOR', 'MODERADOR');
INSERT INTO `role` VALUES ('3', 'ROLE_USER', 'USUARIO');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) NOT NULL,
  `email` varchar(180) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `status_id` int(2) NOT NULL,
  `username_canonical` varchar(180) NOT NULL,
  `email_canonical` varchar(180) NOT NULL,
  `profile_image` varchar(180) DEFAULT NULL,
  `code` varchar(180) DEFAULT NULL,
  `online` int(2) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_users` (`username`,`email`),
  KEY `role` (`role`),
  KEY `status` (`status_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'MACROALI', 'ali.jose118@gmail.com', '$2y$12$sfYKFZlzfp53s5VQyKhPW.R5W/RjFBP8PSDqycq8.6nclmvUCtkpe', '1', '2', 'MACROALI', 'ali.jose118@gmail.com', '1_banner_767375b6b0e0435258.jpeg', 'ADM001', '2', '2018-12-10 00:03:37', null, '2018-08-08 17:37:31', '2018-08-08 17:37:31');
INSERT INTO `user` VALUES ('5', 'INKUBE', 'rufi@inkube.net', '$2y$12$z/Gb2gXyWDH0NKRbQTfDVe.H4Ylv8B96Ona3EkQMyK0W4ksvtFxSe', '1', '2', 'INKUBE', 'rufi@inkube.net', null, '5mmlm', '2', null, null, '2018-09-25 02:14:53', '2018-09-25 02:14:53');
INSERT INTO `user` VALUES ('6', 'TEST', 'rufi@inkube.es', '$2y$12$TWmLxL0SgctX/UGnD48GYu.G1t/TIyJ7UW0nMHACL5BdMfsHoWFzq', '1', '2', 'TEST', 'rufi@inkube.es', null, '6xsJmj', '2', null, null, '2018-10-02 10:10:36', '2018-10-02 10:10:36');

-- ----------------------------
-- Table structure for user_information
-- ----------------------------
DROP TABLE IF EXISTS `user_information`;
CREATE TABLE `user_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `last_name` varchar(180) COLLATE latin1_general_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `direction` text COLLATE latin1_general_ci,
  `phone_1` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `phone_2` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `note` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_information_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `user_information_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of user_information
-- ----------------------------
INSERT INTO `user_information` VALUES ('1', 'ALI', 'GUEVARA', '1', '3', null, '123456', '132456', null);
INSERT INTO `user_information` VALUES ('5', 'RUFI', '-', '5', null, null, '123456', '', 'Administrador');
INSERT INTO `user_information` VALUES ('6', 'RUFI', 'ASENSIO MARTINEZ', '6', null, null, '669081554', '', 'Prueba de usuario');

-- ----------------------------
-- Table structure for user_logs
-- ----------------------------
DROP TABLE IF EXISTS `user_logs`;
CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(180) NOT NULL,
  `action` text NOT NULL,
  `ip_access` varchar(20) DEFAULT NULL,
  `ip_proxy` varchar(20) DEFAULT NULL,
  `ip_company` varchar(20) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_logs
-- ----------------------------

-- ----------------------------
-- Table structure for user_status
-- ----------------------------
DROP TABLE IF EXISTS `user_status`;
CREATE TABLE `user_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_status
-- ----------------------------
INSERT INTO `user_status` VALUES ('1', 'No verificada');
INSERT INTO `user_status` VALUES ('2', 'Verificada');
INSERT INTO `user_status` VALUES ('3', 'Suspendida');
INSERT INTO `user_status` VALUES ('4', 'De baja');
