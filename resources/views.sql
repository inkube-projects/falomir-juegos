-- ----------------------------
-- View structure for view_user
-- ----------------------------
DROP VIEW IF EXISTS view_user;
CREATE VIEW view_user AS SELECT
   u.id,
   u.username,
   u.email,
   u.`password`,
   u.role,
   u.status_id,
   u.username_canonical,
   u.email_canonical,
   u.profile_image,
   u.last_login,
   u.password_requested_at,
   u.code,
   u.online,
   u.create_at,
   u.update_at,
   ui.name,
   ui.last_name,
   ui.user_id,
   ui.country_id,
   ui.direction,
   ui.phone_1,
   ui.phone_2,
   ui.note
FROM
   user u
INNER JOIN user_information ui ON ui.user_id = u.id ;

-- ----------------------------
-- View structure for view_products
-- ----------------------------
DROP VIEW IF EXISTS view_products;
CREATE VIEW view_products AS SELECT
    pc.`name` AS category_name,
    pc.image AS category_image,
    p.id,
    p.reference,
    p.`name` AS product_name,
    p.age_range,
    p.measurement,
    p.content,
    p.content_en,
    p.description_1,
    p.description_2,
    p.description_1_en,
    p.description_2_en,
    p.cover_image,
    p.image_1,
    p.image_2,
    p.banner,
    p.product_logo,
    p.video,
    p.novelty,
    p.best_seller,
    p.category_id,
    p.sub_category_id,
    p.relevance,
    p.status,
    p.create_at,
    p.update_at
FROM
    products p
LEFT JOIN products_category pc ON p.category_id = pc.id;

-- ----------------------------
-- View structure for view_products
-- ----------------------------
DROP VIEW IF EXISTS view_news;
CREATE VIEW view_news AS SELECT
	n.id,
	n.title,
    n.title_en,
	n.description,
    n.description_en,
	n.cover_image,
	n.image,
	n.category_id,
    n.status,
    n.relevance,
	n.create_at,
	n.update_at,
	nc.`name` AS category_name
FROM
	news n
INNER JOIN news_category nc ON n.category_id = nc.id;
