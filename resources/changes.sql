SET FOREIGN_KEY_CHECKS=0;

SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE TABLE products_categories ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `product_id` INT(11) NOT NULL ,  `category_id` INT(11) NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
ALTER TABLE `products_categories` ADD INDEX(`product_id`);
ALTER TABLE `products_categories` ADD INDEX(`category_id`);
ALTER TABLE `products_categories` ADD FOREIGN KEY (`product_id`) REFERENCES `products`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `products_categories` ADD FOREIGN KEY (`category_id`) REFERENCES `products_category`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `products` CHANGE `description_2` `description_2` LONGTEXT CHARACTER SET latin1 COLLATE latin1_general_ci NULL;
CREATE TABLE `products_subcategories` ( `id` INT(11) NOT NULL AUTO_INCREMENT ,  `product_id` INT(11) NOT NULL ,  `sub_category_id` INT(11) NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
ALTER TABLE `products_subcategories` ADD INDEX(`product_id`);
ALTER TABLE `products_subcategories` ADD INDEX(`sub_category_id`);
ALTER TABLE `products_subcategories` ADD FOREIGN KEY (`product_id`) REFERENCES `products`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `products_subcategories` ADD FOREIGN KEY (`sub_category_id`) REFERENCES `products_subcategory`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `products_subcategories` DROP FOREIGN KEY `products_subcategories_ibfk_2`; ALTER TABLE `products_subcategories` ADD CONSTRAINT `products_subcategories_ibfk_2` FOREIGN KEY (`sub_category_id`) REFERENCES `products_category`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;
SET FOREIGN_KEY_CHECKS=1;
