
# Falomir Juegos
============================================================

Proyecto de la empresa falomir juegos, en este archivo se explica como instalar dicho proyecto. Este proyecto cuenta con un archivo *.gitignore* que contiene las rutas ignaradas como son las rutas que generan archivos dinamicamente, pero estas rutas deben de crearse para el correcto funcionamiento del proyecto, a medida que se creen nuevas rutas se iran actualizando en este proyecto.

## Rutas ##

```bash
admin.falomir.com/views/images/users/user_ADM001/
admin.falomir.com/views/images/users/user_ADM001/temp_files/news/
admin.falomir.com/views/images/users/user_ADM001/temp_files/news/cover/
admin.falomir.com/views/images/users/user_ADM001/temp_files/products/
admin.falomir.com/views/images/users/user_ADM001/temp_files/products/banner/
admin.falomir.com/views/images/users/user_ADM001/temp_files/products/category/
admin.falomir.com/views/images/users/user_ADM001/temp_files/products/cover/
admin.falomir.com/views/images/users/user_ADM001/temp_files/products/gallery/
admin.falomir.com/views/images/users/user_ADM001/temp_files/users/
admin.falomir.com/views/images/users/user_ADM001/temp_files/users/profile

falomir.com/views/images/users/
falomir.com/views/images/products/
falomir.com/views/images/products/category/
falomir.com/views/images/news/
falomir.com/views/images/news/banner/
falomir.com/views/images/news/cover/

```

## Programas necesarios ##

* Git: Repositorio - Instalación [Sitio web oficial de Git][1]
* Composer: Gestor de dependencias - Instalación [Sitio web oficial de composer][2]

## Instalación ##

Este proyecto utiliza varias librerías adicionales, a continuación se procede a explicar su instalación:

* PHPMailer: Librería php que se encarga del envío de emails [PHPMailer GitHub][3], para instalar se recomienda utilizar composer y utilizar el siguiente comando: **composer require phpmailer/phpmailer**


## Resources ##

EL directorio resources contiene todo lo relacionado con SQL:

* changes.sql: Cambios de la base de datos entre versiones.
* db.sql: Base de datos, con tablas maestras.
* views.sql: Vistas SQL.

Para la instalación del proyecto se requiere ejecutar los querys de los archivos *db.sql* y *views.sql* (Segun se amerite), para actualizar el proyecto ejecutar *views.sql* y *changes.sql*

<!-- Enlaces -->
[1]:  https://git-scm.com/book/es/v1/Empezando-Instalando-Git
[2]: https://getcomposer.org/
[3]: https://github.com/PHPMailer/PHPMailer
