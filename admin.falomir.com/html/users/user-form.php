<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datepicker/datepicker3.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Usuarios
                            <?php if ($acc == 1): ?>
                                <small>Agregar usuario</small>
                            <?php else: ?>
                                <small>Editar usuario</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="row">
                        <!--INFORMACIÓN-->
                        <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                            <input type="hidden" name="hid-name" id="hid-name" value="">
                            <div class="col-xs-12 col-sm-9">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php if ($acc == 1): ?>
                                            <h4 class="panel-title">Agregar usuario</h4>
                                        <?php else: ?>
                                            <h4 class="panel-title">Editar usuario</h4>
                                        <?php endif; ?>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <?php if ($acc == 2): ?>
                                                <label class="col-sm-2 control-label">ID</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="client_code" id="client_code" value="<?=$client_code?>" disabled>
                                                    <div class="text-danger" id="client_code_validate"></div>
                                                </div>
                                                <label class="col-sm-2 control-label">Fecha de Registro</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" name="discharge_date" id="discharge_date" value="<?=$create_at['date']?>" disabled>
                                                    <div class="text-danger" id="discharge_date_validate"></div>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nombre*</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                                <div class="text-danger" id="name_validate"></div>
                                            </div>
                                            <label class="col-sm-2 control-label">Apellidos*</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="<?=$last_name?>">
                                                <div class="text-danger" id="last_name_validate"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Teléfono*</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="phone_1" id="phone_1" value="<?=$phone_1?>">
                                                <div class="text-danger" id="phone_1_validate"></div>
                                            </div>

                                            <label class="col-sm-2 control-label">Email*</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="email" id="email" value="<?=$email?>">
                                                <div class="text-danger" id="email_validate"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Teléfono 2</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="phone_2" id="phone_2" value="<?=$phone_2?>">
                                                <div class="text-danger" id="phone_2_validate"></div>
                                            </div>

                                            <label class="col-sm-2 control-label">Nombre de usuario*</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="username" id="username" value="<?=$username?>">
                                                <div class="text-danger" id="username_validate"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Contraseña*</label>
                                            <div class="col-sm-3">
                                                <input type="password" class="form-control" name="pass_1" id="pass_1">
                                                <div class="text-danger" id="pass_1_validate"></div>
                                            </div>

                                            <label class="col-sm-2 control-label">Repita la contraseña*</label>
                                            <div class="col-sm-5">
                                                <input type="password" class="form-control" name="pass_2" id="pass_2">
                                                <div class="text-danger" id="pass_2_validate"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Estado de la cuenta*</label>
                                            <div class="col-sm-3">
                                                <select class="select2 form-control" name="status" id="status">
                                                    <option value="">Seleccione un Estado</option>
                                                    <?php foreach ($arr_status as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $status)?>><?=$val['description']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="status_validate"></div>
                                            </div>

                                            <label class="col-sm-2 control-label">Rol*</label>
                                            <div class="col-sm-5">
                                                <select class="select2 form-control" name="role" id="role">
                                                    <option value="">Seleccione un Rol</option>
                                                    <?php foreach ($arr_role as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $role)?>><?=$val['role_name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="text-danger" id="role_validate"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Comentarios</label>
                                            <div class="col-sm-10">
                                                <textarea rows="4" class="form-control" name="note" id="note"><?=$note?></textarea>
                                                <div class="text-danger" id="note_validate"></div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-success pull-right marginleft10 btn-submit">Guardar</button>
                                        <button type="button" class="btn btn-warning pull-right">Deshacer Cambios</button>
                                    </div>
                                </div>
                            </div>

                            <!--FOTO PERFIL-->
                            <div class="col-xs-12 col-sm-3">
                                <form class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Foto usuario</h4>
                                        </div>

                                        <div class="panel-body">
                                            <div class="col-xs-12 thumb">
                                                <a class="thumbnail" href="#">
                                                    <img id="img-cover" class="img-responsive" src="<?=$image?>" alt="" width="200" height="200">
                                                </a>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="col-md-12 text-center">
                                               <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled style="width: 100%">Debes una imagen</button>
                                               <div class="clearfix css-espacio10"></div>

                                               <div class="btn btn-default btn-file btn-flat" style="width: 100%">
                                                  <i class="fa fa-camera"></i> Subir foto
                                                  <input type="file" name="cover_image" id="cover_image" class="cover_image">
                                               </div>
                                               <div class="text-danger" id="cover_image_validate"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--END.FOTOPERFIL-->
                        </form>
                        <!--END.INFORMACION-->
                    </div>

                </section>
                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- Modal para la imagen de Perfil -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
           <div class="modal-dialog modal-lg css-width1100">
              <div class="modal-content">
                 <div class="modal-header">
                    Imagen de perfil
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 </div>
                 <div class="modal-body">
                    <div class="col-md-12" id="ajx-img-cover"></div>

                    <div class="clearfix css-marginB10"></div>
                 </div>
              </div>
           </div>
        </div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?=APP_ASSETS?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-users.js" type="text/javascript"></script>
    </body>
</html>
