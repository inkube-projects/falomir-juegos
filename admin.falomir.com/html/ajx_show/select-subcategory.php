<label for="age">sub-categoría:</label>
<select class="form-control select2" name="subcategory[]" id="subcategory" multiple>
    <?php foreach ($arr_subcategory as $val): ?>
        <option value="<?=$val['id']?>" <?=isSelectedMultiple($arr_s_subcat, $val['id'])?>><?=$val['name']?></option>
    <?php endforeach; ?>
</select>
<div class="text-danger" id="subcategory_validate"></div>
