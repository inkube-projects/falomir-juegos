<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>

        <div class="wrapper">
            <!-- SIDEBAR
            ================================================== -->
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header">
                            Escritorio <small>Actividad</small>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <h4>Resumen</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-teal">
                            <i class="fa fa-comment"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_products?></span>
                                <small>Productos</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-pink">
                            <i class="fa fa-user"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_users?></span>
                                <small>Usuarios</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-accent">
                            <i class="fa fas fa-file-alt"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_messages?></span>
                                <small>Mensajes</small>
                            </h3>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-3">

                        <div class="dashboard-stats__item bg-orange">
                            <i class="fa fa-exclamation-circle"></i>
                            <h3 class="dashboard-stats__title">
                                <span class="count-to"><?=$cnt_news?></span>
                                <small>Noticias</small>
                            </h3>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>

        <script type="text/javascript">
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    placement : 'top'
                });
            });
        </script>
    </body>
</html>
