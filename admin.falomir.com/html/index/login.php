<!DOCTYPE html>
   <html lang="es">
      <head>
         <base href="<?=BASE_URL?>">
         <meta charset="utf-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="description" content="admin">
         <meta name="author" content="Inkube">
         <link rel="shortcut icon" type="image/png" href="views/images/favicon.png" />
         <title>ADMIN </title>
         <!-- CSS Plugins -->
         <link href="<?=APP_ASSETS?>css/perfect-scrollbar.min.css" rel="stylesheet">
         <!-- Bootstrap -->
         <link href="<?=APP_ASSETS?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
         <!-- CSS Global -->
         <link href="<?=APP_ASSETS?>css/styles.css" rel="stylesheet">

         <!-- Google Fonts -->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
         <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300,500' rel='stylesheet' type='text/css'>
         <!--CSS Custom-->
         <link href="<?=APP_ASSETS?>css/BFT.css" rel="stylesheet">
      </head>

      <body class="fondoAcceso">

         <div class="container">
            <div class="row">
               <div class="acceso_avatar center-block">
                  <img src="<?=APP_IMG?>logos/logo.png" alt="" class="img-responsive">
               </div>
               <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

                  <div class="sign__container">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              Acceso al gestor de contenidos
                           </h4>
                        </div>

                        <div class="panel-body">
                           <form id="frm-login" name="frm-login" method="post" action="">
                              <div class="alert css-hide" role="alert" id="alert-message"></div>
                              <div class="form-group">
                                 <label for="user-email">Usuario o Email</label>
                                 <input type="text" class="form-control" id="user_email" name="user_email">
                                 <div class="text-danger" id="user_email_validate"></div>
                              </div>
                              <div class="form-group">
                                 <label for="password">Contraseña</label>
                                 <input type="password" class="form-control" id="password" name="password">
                                 <div class="text-danger" id="password_validate"></div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6">
                                    <div class="checkbox">
                                       <input type="checkbox" id="remember-me">
                                       <label for="remember-me">
                                          Recordar
                                       </label>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 text-right">
                                    <button type="submit" class="btn btn-success btn-lg btn-flat" id="js-buttom">
                                       Acceder
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>

                        <div class="panel-footer">
                           <a href="#" data-toggle="collapse" data-target="#sign__resend-password">¿Has olvidado tu contraseña?</a>
                           <div class="collapse" id="sign__resend-password">
                              <form class="form-inline">
                                 <div class="form-group">
                                    <label class="sr-only">Introduce tu email</label>
                                    <input type="text" class="form-control" name="pass_recovery" id="pass_recovery" placeholder="Introduce tu email">
                                 </div>
                                 <button type="submit" class="btn btn-primary btn-flat">
                                    Reenviar
                                 </button>
                              </form>
                           </div>
                        </div>
                     </div>

                     <p class="sign__extra text-muted text-center"><?=APP_COPY?></p>
                  </div>
               </div>
            </div>
         </div>

      <!-- JS -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="<?=APP_ASSETS?>plugins/bootstrap/js/bootstrap.min.js"></script>
      <!-- JS Plugins -->
      <script src="<?=APP_ASSETS?>js/perfect-scrollbar.jquery.min.js"></script>
      <!-- jQuery validation -->
      <script src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js" type="text/javascript"></script>
      <script src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js" type="text/javascript"></script>
      <!-- JS Custom -->
      <script src="<?=APP_ASSETS?>js/custom.js"></script>
      <!-- Variables de entorno -->
      <script src="<?=APP_ASSETS?>js/__paths.js" type="text/javascript"></script>
      <!-- Main JS -->
      <script src="<?=APP_ASSETS?>js/js-login.js" type="text/javascript"></script>
      <script>
         $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip({
               placement : 'top'
            });
         });
      </script>
   </body>
</html>
