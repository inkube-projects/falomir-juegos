<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Carrusel
                            <small>Listado de imagenes</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin-agregar-imagen" class="btn btn-info btn-flat pull-right">Nueva imagen</a>
                        </div>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de noticias</h2>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
                                        <th>titulo</th>
                                        <th>Estado</th>
                                        <th>imagen</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_carousel as $val): ?>
                                        <tr>
                                            <td><?=$val['id']?></td>
                                            <td><?=$val['title']?></td>
                                            <td><?=$val['status_name']?></td>
                                            <td> <img src="<?=$val['slider_image']?>" alt="" width="100"> </td>
                                            <td>
                                                <a href="admin-editar-imagen/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                                <?php if ($this->user_role_name === "ROLE_ADMIN"): ?>
                                                    <a href="javascript: deleteBoostrapAction('remove&id=<?=$val['id']?>&acc=4', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar Producto</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar esta <b>Producto</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?=APP_ASSETS?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- InputMask -->
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript">
            $(function(e){
                // Select 2
                $(".select2").select2();

                // Mascara para fechas
                $("[data-mask]").inputmask();

                // Configuración de la paginación con DataTable
                $('.table').DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ entradas por página",
                        "zeroRecords": "No se han encontrado resultados",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No se han encontrado resultados",
                        "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                        "search": "Buscar: ",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });

                // Limpiar el formulario de búsqueda
                $('.btn-reset').on('click', function(e){
                    $('#frm-search select').val('').trigger('change');
                    $('#frm-search input[type=text]').val('');
                });
            });
        </script>
    </body>
</html>
