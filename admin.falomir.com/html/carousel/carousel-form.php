<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Carrusel
                            <?php if ($acc == 1): ?>
                                <small>Agregar imagen</small>
                            <?php else: ?>
                                <small>Editar imagen</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div>

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" enctype="multipart/form-data" method="post" action="">
                        <!--INFORMACIÓN-->
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <?php if ($acc == 1): ?>
                                            <h2 class="box-title">Agregar imagen</h2>
                                        <?php else: ?>
                                            <h2 class="box-title">Editar imagen</h2>
                                        <?php endif; ?>

                                        <div class="pull-right">
                                            <span class="button-checkbox">
                                                <button type="button" class="btn btn-flat css-marginR10 btn-primary" data-color="primary" data-input="status">
                                                    <i class="state-icon glyphicon glyphicon-check"></i> Habilitado
                                                </button>
                                                <input class="hidden" type="checkbox" id="status" name="status" value="1" <?=isChecked(array(1), $status)?>>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Título:</label>
                                            <input type="text" class="form-control" name="title" id="title" value="<?=$title?>">
                                            <div class="text-danger" id="title_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Título (EN):</label>
                                            <input type="text" class="form-control" name="title_en" id="title_en" value="<?=$title_en?>">
                                            <div class="text-danger" id="title_en_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Link:</label>
                                            <input type="text" class="form-control" name="url" id="url" value="<?=$url?>">
                                            <div class="text-danger" id="url_validate"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="css-bold css-text-black">Color de fondo:</label>
                                                    <div class="input-group my-colorpicker">
                                                        <input type="text" class="form-control" name="background_color" id="background_color" value="<?=$background_color?>">

                                                        <div class="input-group-addon">
                                                            <i></i>
                                                        </div>
                                                    </div>
                                                    <div class="text-danger" id="background_color_validate"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción (EN):</label>
                                            <textarea name="description_en" id="description_en" class="form-control" rows="8"><?=$description_en?></textarea>
                                            <div class="text-danger" id="description_en_validate"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Imagen de banner</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-xs-12 text-center">
                                            <img class="img-responsive css-marginT20 css-marginB20 css-noFloat center-block" src="<?=$image?>" alt="">
                                        </div>

                                        <div class="btn btn-default btn-file css-width100 btn-files-adj" id="img-image">
                                            <i class="far fa-image"></i> <span>Seleccione una imagen</span>
                                            <input type="file" name="image" id="image" class="img-section">
                                        </div>
                                        <div class="text-danger" id="image_validate"></div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            </div>
                        </div>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-carousel.js" type="text/javascript"></script>
    </body>
</html>
