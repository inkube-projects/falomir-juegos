<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Seo
                            <small>Agregar SEO</small>
                        </h3>
                    </div>
                </div>

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                        <!--INFORMACIÓN-->
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Agregar SEO</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Palabras clave <small class="text-teal">(Seben estar separadas por una coma ",")</small>:</label>
                                            <textarea name="keywords" id="keywords" class="form-control" rows="8"><?=$keywords?></textarea>
                                            <div class="text-danger" id="keywords_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="css-bold css-text-black">Descripción:</label>
                                            <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            </div>
                        </div>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-seo.js" type="text/javascript"></script>
    </body>
</html>
