<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Noticias
                            <?php if ($acc == 1): ?>
                                <small>Agregar noticia</small>
                            <?php else: ?>
                                <small>Editar noticia</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div>

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" enctype="multipart/form-data" method="post" action="">
                        <!--INFORMACIÓN-->
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <?php if ($acc == 1): ?>
                                            <h2 class="box-title">Agregar noticia</h2>
                                        <?php else: ?>
                                            <h2 class="box-title">Editar noticia</h2>
                                        <?php endif; ?>

                                        <div class="pull-right">
                                            <span class="button-checkbox">
                                                <button type="button" class="btn btn-flat css-marginR10 btn-primary active" data-color="primary" data-input="best_seller">
                                                    <i class="state-icon glyphicon glyphicon-check"></i> Activo
                                                </button>
                                                <input class="hidden" type="checkbox" id="status" name="status" value="1" <?=isChecked(array(1), $status)?>>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">Título:</label>
                                            <input type="text" class="form-control" name="title" id="title" value="<?=$title?>">
                                            <div class="text-danger" id="title_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Título (EN):</label>
                                            <input type="text" class="form-control" name="title_en" id="title_en" value="<?=$title_en?>">
                                            <div class="text-danger" id="title_en_validate"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Relevancia:</label>
                                                    <input type="text" class="form-control only-number" name="relevance" id="relevance" value="<?=$relevance?>">
                                                    <div class="text-danger" id="relevance_validate"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="age">Descripción:</label>
                                            <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                            <div class="text-danger" id="description_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="age">Descripción (EN):</label>
                                            <textarea name="description_en" id="description_en" class="form-control" rows="8"><?=$description_en?></textarea>
                                            <div class="text-danger" id="description_en_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="measurement">Categoría:</label>
                                            <select class="select2 form-control" name="category" id="category">
                                                <option value="">Seleccione una categoría</option>
                                                <?php foreach ($arr_category as $val): ?>
                                                    <option value="<?=$val['id']?>" <?=isSelected($val['id'], $category)?>><?=$val['name']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <div class="text-danger" id="category_validate"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Banner</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-xs-12 text-center">
                                            <img id="img-banner" class="img-responsive css-marginT20 css-marginB20 css-noFloat center-block" src="<?=$image?>" alt="">
                                        </div>

                                        <div class="btn btn-default btn-file css-width100 btn-files-adj" id="img-banner">
                                            <i class="far fa-image"></i> <span>Seleccione una imagen</span>
                                            <input type="file" name="banner" id="banner" class="img-section">
                                        </div>
                                        <div class="text-danger" id="banner_validate"></div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            </div>

                            <div class="col-md-4">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="panel-title">Imagen de portada</h2>
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-xs-12 text-center">
                                            <img id="img-cover" class="img-responsive css-marginT20 css-marginB20" src="<?=$cover_image?>" alt="">
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="col-md-12 text-center">
                                            <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled style="width: 100%">Debes agregar una imagen</button>
                                            <div class="clearfix css-espacio10"></div>

                                            <div class="btn btn-default btn-file btn-flat" style="width: 100%">
                                                <i class="fa fa-camera"></i> Subir foto
                                                <input type="file" name="cover_image" id="cover_image" class="cover_image">
                                            </div>
                                            <div class="text-danger" id="cover_image_validate"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">SEO</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Título:</label>
                                            <input type="text" name="seo_title" id="seo_title" class="form-control" value="<?=$seo_title?>">
                                            <div class="text-danger" id="seo_title_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label>Palabras clave (Separadas por coma ","):</label>
                                            <input type="text" name="seo_keywords" id="seo_keywords" class="form-control" value="<?=$seo_keywords?>">
                                            <div class="text-danger" id="seo_keywords_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label>Descripción:</label>
                                            <textarea class="form-control" name="seo_description" id="seo_description" rows="8"><?=$seo_description?></textarea>
                                            <div class="text-danger" id="seo_description_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- Modal para la imagen de Perfil -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
            <div class="modal-dialog modal-lg css-width1100">
                <div class="modal-content">
                    <div class="modal-header">
                        Imagen
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12" id="ajx-img-cover"></div>
                        <div class="clearfix css-marginB10"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- CKeditor -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/ckeditor/ckeditor.js"></script>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?=APP_ASSETS?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-news.js" type="text/javascript"></script>
    </body>
</html>
