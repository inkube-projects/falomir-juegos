<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Date picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datepicker/datepicker3.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Blog
                            <small>Listado de post</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin-agregar-noticia" class="btn btn-info btn-flat pull-right">Nuevo post</a>
                        </div>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <form name="frm-search" id="frm-search" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Búsqueda avanzada</h2>
                            </div>

                            <div class="box-body">
                                <div class="row css-marginB20">
                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Categoría</label>
                                        <select class="form-control select2" name="src_category" id="src_category">
                                            <option value="">Todas las categorías</option>
                                            <?php foreach ($arr_category as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_category)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Fecha de creación</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_start_date" id="src_start_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Desde" value="<?=$src_start_date?>">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_end_date" id="src_end_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Hasta" value="<?=$src_end_date?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                                <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                            </div>
                        </div>
                    </form>

                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado post Blog</h2>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Orden</th>
                                        <th>titulo</th>
                                        <th>categoría</th>
                                        <th>Relevancia</th>
                                        <th>imagen</th>
                                        <th>Creado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_news as $val): ?>
                                        <tr>
                                            <td><?=$val['relevance']?></td>
                                            <td><?=$val['title']?></td>
                                            <td><?=$val['category']?></td>
                                            <td>
                                                <input type="text" id="relevance_<?=$val['id']?>" class="form-control relevance" value="<?=$val['relevance']?>" style="width: 60px;" onfocusout="setRelevance(<?=$val['id']?>)">
                                            </td>
                                            <td> <img src="<?=$val['image']?>" alt="" width="100"> </td>
                                            <td><?=$val['create']?></td>
                                            <td>
                                                <a href="admin-editar-noticia/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                                <?php if ($this->user_role_name === "ROLE_ADMIN"): ?>
                                                    <a href="javascript: deleteBoostrapAction('remove&id=<?=$val['id']?>&acc=5', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar Publicación</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar esta <b>Publicación</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?=APP_ASSETS?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- InputMask -->
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript">
            $(function(e){
                // Select 2
                $(".select2").select2();

                // Mascara para fechas
                $("[data-mask]").inputmask();

                // Configuración de la paginación con DataTable
                $('.table').DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ entradas por página",
                        "zeroRecords": "No se han encontrado resultados",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "No se han encontrado resultados",
                        "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                        "search": "Buscar: ",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });

                // Limpiar el formulario de búsqueda
                $('.btn-reset').on('click', function(e){
                    $('#frm-search select').val('').trigger('change');
                    $('#frm-search input[type=text]').val('');
                });
            });

            function setRelevance(postID)
            {
                $.ajax({
                    type: 'POST',
                    url: base_admin + 'ajax.php?m=news&acc=5&id=' + postID,
                    data: {
                        relevance: $('#relevance_' + postID).val()
                    },
                    dataType: 'json',
                    success: function(r){

                    }
                });
            }
        </script>
    </body>
</html>
