<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <?php if ($acc == 1): ?>
                                <small>Agregar categoría</small>
                            <?php else: ?>
                                <small>Editar Categoría</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="row">
                        <!--INFORMACIÓN-->
                        <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                            <input type="hidden" name="hid-name" id="hid-name" value="">
                            <div class="col-md-7">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <?php if ($acc == 1): ?>
                                            <h2 class="box-title">Agregar categoría</h2>
                                        <?php else: ?>
                                            <h2 class="box-title">Editar categoría</h2>
                                        <?php endif; ?>

                                        <div class="pull-right">
                                            <span class="button-checkbox">
                                                <button type="button" class="btn btn-flat css-marginR10 btn-primary" data-color="primary" data-input="principal">
                                                    <i class="state-icon glyphicon glyphicon-check"></i> Principal
                                                </button>
                                                <input class="hidden" type="checkbox" id="principal" name="principal" value="1" <?=isChecked(array(1), $principal)?>>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Nombre de la categoría</label>
                                                <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                                                <div class="text-danger" id="name_validate"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Nombre de la categoría (EN)</label>
                                                <input type="text" class="form-control" name="name_en" id="name_en" value="<?=$name_en?>">
                                                <div class="text-danger" id="name_en_validate"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Color de fondo:</label>
                                                <div class="input-group my-colorpicker">
                                                    <input type="text" class="form-control" name="background_color" id="background_color" value="<?=$background_color?>">

                                                    <div class="input-group-addon">
                                                        <i></i>
                                                    </div>
                                                </div>
                                                <div class="text-danger" id="background_color_validate"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Padre:</label>
                                                <select class="select2 form-control" name="father" id="father">
                                                    <option value="">Seleccione un padre</option>
                                                    <?php foreach ($arr_p_categories as $val): ?>
                                                        <option value="<?=$val['id']?>" <?=isSelected($val['id'], $father)?>><?=$val['name']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Descripción:</label>
                                                <textarea name="description" id="description" class="form-control" rows="8"><?=$description?></textarea>
                                                <div class="text-danger" id="description_validate"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="css-bold css-text-black">Descripción (EN):</label>
                                                <textarea name="description_en" id="description_en" class="form-control" rows="8"><?=$description_en?></textarea>
                                                <div class="text-danger" id="description_en_validate"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($acc == 2): ?>
                                    <div class="box box-solid">
                                        <div class="box-header with-border">
                                            <h2 class="box-title">Productos</h2>
                                        </div>

                                        <div class="box-body">
                                            <?php foreach ($this->formatedArray($arr_products, 3) as $val): ?>
                                                <div class="row css-marginT20">
                                                    <?php for ($i = 0; $i < count($val); $i++): ?>
                                                        <div class="col-md-4">
                                                            <span class="button-checkbox">
                                                                <button type="button" class="btn btn-flat css-marginR10 btn-primary" data-color="primary" data-input="prod-<?=$val[$i]['id']?>" style="overflow-wrap: break-word; white-space:inherit; text-align:left">
                                                                    <i class="state-icon glyphicon glyphicon-check"></i> <?=$val[$i]['product_name']?> <br> <small><?=$val[$i]['formed_category']?></small>
                                                                </button>
                                                                <input class="hidden" type="checkbox" id="prod-<?=$val[$i]['id']?>" name="prod[]" value="<?=$val[$i]['id']?>" <?=isChecked($arr_check, $val[$i]['id'])?>>
                                                            </span>
                                                        </div>
                                                    <?php endfor; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-5">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="panel-title">Imagen de categoría</h2>
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-xs-12 text-center">
                                            <img id="img-cover" class="img-responsive css-marginT20 css-marginB20" src="<?=$image?>" alt="">
                                        </div>
                                        <br>
                                        <hr>
                                        <div class="col-md-12 text-center">
                                           <button type="button" class="btn btn-success btn-flat btn-submit-cover-image" disabled style="width: 100%">Debes agregar una imagen</button>
                                           <div class="clearfix css-espacio10"></div>

                                           <div class="btn btn-default btn-file btn-flat" style="width: 100%">
                                              <i class="fa fa-camera"></i> Subir foto
                                              <input type="file" name="cover_image" id="cover_image" class="cover_image">
                                           </div>
                                           <div class="text-danger" id="cover_image_validate"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Imagen de banner</h2>
                                    </div>

                                    <div class="box-body">
                                        <div class="col-xs-12 text-center">
                                            <img class="img-responsive css-marginT20 css-marginB20 css-noFloat center-block" src="<?=$banner?>" alt="">
                                        </div>

                                        <div class="btn btn-default btn-file css-width100 btn-files-adj" id="img-banner">
                                            <i class="far fa-image"></i> <span>Seleccione una imagen</span>
                                            <input type="file" name="banner" id="banner" class="img-section">
                                        </div>
                                        <div class="text-danger" id="banner_validate"></div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            </div>
                        </form>
                        <!--END.INFORMACION-->
                    </div>

                </section>
                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- Modal para la imagen de Perfil -->
        <div class="modal fade" tabindex="-1" role="dialog" id="mod-img-cover">
           <div class="modal-dialog modal-lg css-width1100">
              <div class="modal-content">
                 <div class="modal-header">
                    Imagen
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 </div>
                 <div class="modal-body">
                    <div class="col-md-12" id="ajx-img-cover"></div>

                    <div class="clearfix css-marginB10"></div>
                 </div>
              </div>
           </div>
        </div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-products-category.js" type="text/javascript"></script>
    </body>
</html>
