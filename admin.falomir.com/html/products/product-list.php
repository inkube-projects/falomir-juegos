<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.css">
        <!-- Select 2 -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/select2/select2.min.css">
        <!-- Date picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/datepicker/datepicker3.css">
        <?php include('html/overall/header.php'); ?>
        <style media="screen">
            form input[type="radio"] {
                display: block;
            }
        </style>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Productos
                            <small>Listado de productos</small>
                        </h3>

                        <div class="col-md-2 pull-right css-marginT35">
                            <a href="admin-agregar-producto" class="btn btn-info btn-flat pull-right">Nuevo producto</a>
                        </div>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <form name="frm-search" id="frm-search" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Búsqueda avanzada</h2>
                            </div>

                            <div class="box-body">
                                <div class="row css-marginB20">
                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Categoría</label>
                                        <select class="form-control select2" name="src_category" id="src_category">
                                            <option value="">Todas las categorías</option>
                                            <?php foreach ($arr_category as $val): ?>
                                                <option value="<?=$val['id']?>" <?=isSelected($val['id'], $src_category)?>><?=$val['name']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Fecha de creación</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_start_date" id="src_start_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Desde" value="<?=$src_start_date?>">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control js-date" name="src_end_date" id="src_end_date" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask placeholder="Hasta" value="<?=$src_end_date?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="css-bold css-text-black">Novedad</label>
                                        <select class="form-control select2" name="src_novelty" id="src_novelty">
                                            <option value="">Todos</option>
                                            <option value="1" <?=isSelected(1, $src_novelty)?>>Novedad</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="button" class="btn btn-warning btn-flat btn-reset" onclick="formReset('frm-search')"><i class="fa fa-brush"></i> Limpiar formulario</button>
                                <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fas fa-search"></i> Buscar</button>
                            </div>
                        </div>
                    </form>

                    <div class="js-alert"><?=$flash_message?></div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <h2 class="box-title">Listado de productos</h2>

                            <div class="pull-right col-md-5">
                                <div class="form-group">
                                    <label class="css-bold css-text-black">Categorías</label>
                                    <select class="form-control select2" name="categories[]" id="categories" multiple>
                                        <?php foreach ($arr_categories as $val): ?>
                                            <option value="<?=$val['id']?>"><?=$val['name']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Orden</th>
                                        <th>Ref</th>
                                        <th>Nombre</th>
                                        <th>Relevancia</th>
                                        <th>Categoría</th>
                                        <th>Creado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($arr_porducts as $val): ?>
                                        <tr>
                                            <td><?=$val['relevance']?></td>
                                            <td><?=$val['ref']?></td>
                                            <td><?=$val['product_name']?></td>
                                            <td>
                                                <input type="text" id="relevance_<?=$val['id']?>" class="form-control relevance" value="<?=$val['relevance']?>" style="width: 60px;" onfocusout="setRelevance(<?=$val['id']?>)">
                                            </td>
                                            <td id="ajx-category-labels-<?=$val['id']?>"><?=$val['category_name']?></td>
                                            <td><?=$val['create']?></td>
                                            <td>
                                                <button type="button" class="btn btn-warning btn-flat" onclick="addCategory(<?=$val['id']?>)" data-toggle="tooltip" data-placement="top" title="Añádir a categorías" data-original-title="Añádir a categorías">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                                <a href="admin-editar-producto/<?=$val['id']?>" class="btn btn-info btn-flat">Editar</a>
                                                <?php if ($role_name === "ROLE_ADMIN"): ?>
                                                    <a href="javascript: deleteBoostrapAction('remove&id=<?=$val['id']?>&acc=2', 1)" class="btn btn-danger btn-flat">Eliminar</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>

                <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Eliminar Producto</h4>
                            </div>
                            <div class="modal-body">
                                <p>Estas seguro de eliminar este <b>Producto</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                                <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- Select2 -->
        <script src="<?=APP_ASSETS?>plugins/select2/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?=APP_ASSETS?>plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- InputMask -->
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?=APP_ASSETS?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-products-list.js"></script>
    </body>
</html>
