<div>
   <?
   if($pager_act == ""){
      $pager_act = 1;
   }

   if ($pager_act > 2) {
      $pager_it_lo = $pager_act-2;

      if (($pager_act + 2) > $pager_tot) {
         $pager_it_hi = $pager_tot;
      } else {
         $pager_it_hi = $pager_act + 2;
      }
   } else {
      $pager_it_lo = 1;

      if ($pager_tot > 5) {
         $pager_it_hi = 5;
      } else {
         $pager_it_hi = $pager_tot;
      }
   }
   ?>
   <!--------------------------------------------------------------------------------------------------->
   <ul class="pagination pagination-sm no-margin pull-right">
   <?
      if ($pager_act > 1) {
         ?>
         <li>
            <a href="<?=$path_url.$adicionals?>&pag=1">Inicio&nbsp;&nbsp;</a>
         </li>
         <?
      }

      if ($pager_act != 1) {
         ?>
         <li>
            <a href="<?=$path_url.$adicionals?>&pag=<?=($pager_act-1)?>" title="Anterior">&laquo;</a>
         </li>
         <?
      }
      for($j = $pager_it_lo; $j <= $pager_act; $j++) {
         $adc = ''; $lnk = '';

         if ($j == $pager_act){
            $adc = 'class="active"'; $lnk = "#";
         }

         if ($pager_res!=0) {
            ?>
            &nbsp;
            <li <?=$adc?>>
               <a href="<?=$path_url.$adicionals?>&pag=<?=$j?>"<?=$adc?>><?=$j?></a>
            </li>
            &nbsp;
            <?
         }
      }

      for($j = $pager_act + 1; $j <= $pager_it_hi; $j++) {
      ?>
         <li>
            <a class="css-no-active" href="<?=$path_url.$adicionals?>&pag=<?=$j?>"><?=$j?></a>
         </li>
         <?
      }
      if (($pager_act != $pager_tot)&&($pager_res!=0)){
         ?>
         <li>
            <a href="<?=$path_url.$adicionals?>&pag=<?=($pager_act+1)?>" title="Siguiente">&raquo;</a>
         </li>
         <?
         if (($j - 1) <= $pager_tot){
            ?>
            <li>
               <a href="<?=$path_url.$adicionals?>&pag=<?=$pager_tot?>">Final</a>
            </li>
            <?
         }
      }
   ?>
   <!--------------------------------------------------------------------------------------------------->
   </ul>
</div>
