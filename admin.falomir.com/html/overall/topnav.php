<div class="sidebar">
    <!-- Close button (mobile devices) -->
    <div class="sidebar__close">
        <img src="<?=APP_IMG?>close.svg" alt="Close sidebar">
    </div>

    <!-- Sidebar user -->
    <div class="sidebar__user center-block text-center">
        <div class="sidebar-user__avatar center-block">
            <img src="<?=APP_IMG?>logos/logo.png" alt="" class="img-responsive">
        </div>
        <a class="sidebar-user__info">
            <h4 class="txtMenu"><?= mb_convert_case($this->admin_username, MB_CASE_UPPER, "UTF-8") ?></h4>
            <p>Opciones <i class="fa fa-caret-down"></i></p>
        </a>
    </div>

    <!-- Sidebar user nav -->
    <nav class="sidebar-user__nav">
        <ul class="sidebar__nav">
            <li><a href="#"><i class="fa fa-user icoG"></i><span class="txtMenu">Mi Perfil</span></a></li>
            <li><a href="#"><i class="fa fa-users icoG"></i> <span class="txtMenu">Perfil Usuairos</span></a></li>
            <li><a href="#"><i class="fa fa-briefcase icoG"></i><span class="txtMenu">Perfil Empresas</span></a></li>
        </ul>
    </nav>

    <!-- Sidebar nav -->
    <nav>
        <ul class="sidebar__nav">
            <!-- Inicio -->
            <?php if ($this->user_view == "Index") { $class = "active"; } else { $class = ""; } ?>
            <li class="<?=$class?>">
                <a href="<?=BASE_URL?>admin-dashboard"><i class="fa fa-th-large icoG"></i> <span class="txtMenu">DASHBOARD</span></a>
            </li>

            <!-- SEO -->
            <?php if ($this->user_view == "Seo") { $class = "active"; } else { $class = ""; } ?>
            <!-- <li class="<?=$class?>">
                <a href="admin-seo"><i class="fab fa-sellsy icoG"></i> <span class="txtMenu">SEO</span></a>
            </li> -->

            <!-- Usuarios -->
            <?php if ($this->user_view == "Users") { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
            <li class="sidebar-nav__dropdown <?=$class?>">
                <a href="#"><i class="fas fa-users icoG"></i><span class="txtMenu">Usuarios<i class="fa fa-angle-down"></i></span></a>
                <ul class="sidebar-nav__submenu" <?=$style?>>
                    <?php if ($this->user_view == "Users" && $this->user_method == "list") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-usuarios">Listado de usuarios</a></li>

                    <?php if ($this->user_view == "Users" && $this->user_method == "new") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-usuario">Agregar usuario</a></li>
                </ul>
            </li>

            <!-- Productos -->
            <?php if ($this->user_view == "Products") { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
            <li class="sidebar-nav__dropdown <?=$class?>">
                <a href="#"><i class="fas fa-box-open icoG"></i><span class="txtMenu">Productos<i class="fa fa-angle-down"></i></span></a>
                <ul class="sidebar-nav__submenu" <?=$style?>>
                    <?php if ($this->user_view == "Products" && $this->user_method == "categoryList") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-categoria-productos">Listado de categorías</a></li>

                    <?php if ($this->user_view == "Products" && $this->user_method == "categoryAdd") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-categoria-producto">Agregar categoría</a></li>

                    <?php if ($this->user_view == "Products" && $this->user_method == "list") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-productos">Listado de productos</a></li>

                    <?php if ($this->user_view == "Products" && $this->user_method == "add") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-producto">Agregar producto</a></li>
                </ul>
            </li>

            <!-- Noticias -->
            <?php if ($this->user_view == "News") { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
            <li class="sidebar-nav__dropdown <?=$class?>">
                <a href="#"><i class="fas fa-bullhorn icoG"></i><span class="txtMenu">Blog<i class="fa fa-angle-down"></i></span></a>
                <ul class="sidebar-nav__submenu" <?=$style?>>
                    <?php if ($this->user_view == "News" && $this->user_method == "categoryList") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-categoria-noticias">Listado de categorías</a></li>

                    <?php if ($this->user_view == "News" && $this->user_method == "categoryAdd") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-categoria-noticia">Agregar categoría</a></li>

                    <?php if ($this->user_view == "News" && $this->user_method == "list") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-noticias">Listado de post</a></li>

                    <?php if ($this->user_view == "News" && $this->user_method == "add") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-noticia">Agregar post</a></li>
                </ul>
            </li>

            <!-- Carrusel -->
            <?php if ($this->user_view == "Carousel") { $class = "active open"; $style = 'style="display:block"'; } else { $class = ""; $style = ''; } ?>
            <li class="sidebar-nav__dropdown <?=$class?>">
                <a href="#"><i class="far fa-images icoG"></i><span class="txtMenu">Carrusel<i class="fa fa-angle-down"></i></span></a>
                <ul class="sidebar-nav__submenu" <?=$style?>>
                    <?php if ($this->user_view == "Carousel" && $this->user_method == "list") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-carrusel">Listado de imagenes</a></li>

                    <?php if ($this->user_view == "Carousel" && $this->user_method == "add") { $class = "active open"; } else { $class = "";} ?>
                    <li class="<?=$class?>"><a href="admin-agregar-imagen">Agregar imagen</a></li>
                </ul>
            </li>

            <!-- Mensajes -->
            <?php if ($this->user_view == "Contact") { $class = "active"; } else { $class = ""; } ?>
            <li class="<?=$class?>">
                <a href="<?=BASE_URL?>admin-contacto"><i class="fas fa-envelope icoG"></i> <span class="txtMenu">Mensajes</span></a>
            </li>

            <!-- Configuraciones -->
            <?php if ($this->user_view == "Config") { $class = "active"; } else { $class = ""; } ?>
            <li class="<?=$class?>">
                <a href="<?=BASE_URL?>admin-configuraciones"><i class="fas fa-cogs icoG"></i> <span class="txtMenu">Configuraciones</span></a>
            </li>
        </ul>
    </nav>

    <div class="sidebar-footer hidden-small">
        <a href="<?=BASE_URL?>admin-logout" data-toggle="tooltip" data-placement="top" title="" data-original-title="Salir">
            <span class="glyphicon glyphicon-off text-red" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
    </div>
</div>


    <!-- MAIN CONTENT
    ================================================== -->
    <!-- Navbar -->
    <div class="row">
        <div class="col-xs-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="navbar_main">
                        <a href="index.html#" class="btn btn-default navbar-btn navbar-left" id="sidebar__toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="hidden-xs">Accesos</span> <i class="fa fa-briefcase fa-2x visible-xs-inline-block"></i>
                                </a>
                                <div class="dropdown-menu navbar-messages">
                                <a href="http://niu.inkube.net/HOME-NIU" class="navbar-messages__item active" target="_blank">
                                <div class="navbar-messages__avatar">
                                <img src="<?=APP_IMG?>access/niu.jpg" alt="...">
                                </div>
                                <div class="navbar-messages__body">
                                <h5 class="navbar-messages__sender">
                                NIU
                                </h5>
                                </div>
                                </a>
                                </div> -->
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div> <!-- / .row -->
