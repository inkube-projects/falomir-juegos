<script src="<?=APP_ASSETS?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?=APP_ASSETS?>plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Plugins -->
<script src="<?=APP_ASSETS?>js/perfect-scrollbar.jquery.min.js"></script>
<!-- JS Custom -->
<script src="<?=APP_ASSETS?>js/custom.js"></script>
<!-- General methods -->
<script src="<?=APP_ASSETS?>js/generals-functions.js" type="text/javascript"></script>
<!-- Variables de entorno -->
<script src="<?=APP_ASSETS?>js/__paths.js" type="text/javascript"></script>
