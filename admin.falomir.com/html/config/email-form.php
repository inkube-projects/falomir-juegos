<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Configuraciones
                            <?php if ($acc == 1): ?>
                                <small>Agregar email</small>
                            <?php else: ?>
                                <small>Editar email</small>
                            <?php endif; ?>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <?php if ($acc == 1): ?>
                                    <h2 class="box-title">Agregar E-mail</h2>
                                <?php else: ?>
                                    <h2 class="box-title">Editar E-mail</h2>
                                <?php endif; ?>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Email</label>
                                            <input type="text" class="form-control" name="email" id="email" value="<?=$email?>">
                                            <div class="text-danger" id="email_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Contraseña</label>
                                            <input type="password" class="form-control" name="pass" id="pass" value="">
                                            <div class="text-danger" id="pass_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Host</label>
                                            <input type="text" class="form-control" name="host" id="host" value="<?=$host?>">
                                            <div class="text-danger" id="host_validate"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                    <!--END.INFORMACION-->
                </section>
                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-config-email.js" type="text/javascript"></script>
    </body>
</html>
