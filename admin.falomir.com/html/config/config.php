<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?=APP_ASSETS?>plugins/colorpicker/bootstrap-colorpicker.min.css">
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Configuraciones
                            <small>Configuraciones generales</small>
                        </h3>
                    </div>
                </div>

                <section class="content">
                    <div class="js-alert"><?=$flash_message?></div>

                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                        <!--INFORMACIÓN-->
                        <input type="hidden" name="hid-name" id="hid-name" value="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <h2 class="box-title">Emails (Contacto)</h2>
                                        <a href="admin-configuracion-agregar-email" class="btn btn-warning btn-flat pull-right">Agregar email</a>
                                    </div>

                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>Creado</th>
                                                    <th>Editado</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($arr_emails as $val): ?>
                                                    <tr>
                                                        <td><?=$val['email']?></td>
                                                        <td><?=$val['formed_create']?></td>
                                                        <td><?=$val['formed_create']?></td>
                                                        <td>
                                                            <a href="admin-configuracion-editar-email/<?=$val['id']?>" class="btn btn-info btn-flat"><i class="fas fa-search"></i></a>
                                                            <?php if ($this->user_role_name === "ROLE_ADMIN"): ?>
                                                                <a href="javascript: deleteBoostrapAction('remove&id=<?=$val['id']?>&acc=6', 1)" class="btn btn-danger btn-flat"><i class="fas fa-trash-alt"></i></a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                            </div>
                        </div>
                    </form>
                </section>

                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <div class="modal fade modal-danger" tabindex="-1" role="dialog" id="mod-remove">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar Email</h4>
                    </div>
                    <div class="modal-body">
                        <p>Estas seguro de eliminar este <b>Email</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No eliminar</button>
                        <button type="button" class="btn btn-danger btn-flat" id="mod-remove-btn" data-id="" onclick="deleteBoostrapAction('',2)"><i class="fa fa-trash"></i> Eliminar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- DataTables -->
        <script src="<?=APP_ASSETS?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?=APP_ASSETS?>plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-config.js" type="text/javascript"></script>
    </body>
</html>
