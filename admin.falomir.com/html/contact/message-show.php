<!DOCTYPE html>
<html lang="es">
    <head>
        <base href="<?=BASE_URL?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="admin">
        <meta name="author" content="Inkube">
        <link rel="shortcut icon" type="image/png" href="admin/img/favicon.png" />
        <title><?=APP_TITLE?></title>
        <?php include('html/overall/header.php'); ?>
    </head>

    <body>
        <div class="wrapper">
            <div class="container-fluid">
                <?php include('html/overall/topnav.php'); ?>

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="page-header pull-left">
                            Mensajes
                            <small>Detalle del mensaje</small>
                        </h3>
                    </div>
                </div> <!-- / .row -->

                <section class="content">
                    <!--INFORMACIÓN-->
                    <form class="form-horizontal" name="<?=$frm?>" id="<?=$frm?>" method="post" action="">
                        <div class="box box-solid">
                            <div class="box-header">
                                <h2 class="box-title">Detalle del mensaje</h2>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <input type="text" class="form-control" name="email" id="email" value="<?=$arr_message[0]['email']?>">
                                            <div class="text-danger" id="email_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label>Apellidos:</label>
                                            <input type="text" class="form-control" name="last_name" id="last_name" value="<?=$arr_message[0]['last_name']?>">
                                            <div class="text-danger" id="last_name_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nombres:</label>
                                            <input type="text" class="form-control" name="name" id="name" value="<?=$arr_message[0]['name']?>">
                                            <div class="text-danger" id="name_validate"></div>
                                        </div>

                                        <div class="form-group">
                                            <label>Mensaje:</label>
                                            <textarea class="form-control" name="message" id="message" rows="8"><?=$arr_message[0]['message']?></textarea>
                                            <div class="text-danger" id="message_validate"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label>Nota:</label>
                                        <textarea class="form-control" name="note" id="note" rows="8"><?=$arr_message[0]['note']?></textarea>
                                        <div class="text-danger" id="note_validate"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat btn-submit pull-right">Guardar</button>
                    </form>
                    <!--END.INFORMACION-->
                </section>
                <!-- Footer -->
                <?php include('html/overall/footer.php') ?>
            </div> <!-- / .container-fluid -->
        </div> <!-- / .wrapper -->
        <div id="key" data-form="<?=$frm?>" data-acc="<?=$acc?>" data-id="<?=$id?>"></div>

        <!-- JavaScript
        ================================================== -->
        <!-- JS Global -->
        <?php include('html/overall/js.php'); ?>
        <!-- jQuery validation -->
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/jquery.validate.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>plugins/jquery-validation/additional-methods.js"></script>
        <script type="text/javascript" src="<?=APP_ASSETS?>js/js-additional-method.js"></script>
        <!-- Custom JS -->
        <script src="<?=APP_ASSETS?>js/js-contact.js" type="text/javascript"></script>
    </body>
</html>
