$(function(e){
    // Select 2
    $(".select2").select2();

    // Mascara para fechas
    $("[data-mask]").inputmask();

    // Configuración de la paginación con DataTable
    $('.table').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas por página",
            "zeroRecords": "No se han encontrado resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No se han encontrado resultados",
            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
            "search": "Buscar: ",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });

    // Limpiar el formulario de búsqueda
    $('.btn-reset').on('click', function(e){
        $('#frm-search select').val('').trigger('change');
        $('#frm-search input[type=text]').val('');
    });
});

/**
 * Edita la relevancia de un producto
 * @param {integer} productID ID del producto
 */
function setRelevance(productID)
{
    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=products&acc=5&id=' + productID,
        data: {
            relevance: $('#relevance_' + productID).val()
        },
        dataType: 'json',
        success: function(r){

        }
    });
}

function addCategory(productID) {
    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=products&acc=6&id=' + productID,
        data: {
            categories: $("#categories").val()
        },
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $('#ajx-category-labels-' + productID).html(r.data);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(document).scrollTop(0);
            }
        }
    });
}
