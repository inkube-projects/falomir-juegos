$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    CKEDITOR.replace('description', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    CKEDITOR.replace('description_en', {
       customConfig : '../../js/js-ckeditor-config.js'
    });

    // select2
    $(".select2").select2();

    /**
     * Se verifica que la imagen de portada sea una imagen válida
     * @param  {event} e Evento
     * @return {string}
     */
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|png|gif)$/i).test(img)) {
            $(".btn-submit-cover-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
        } else {
            $(".btn-submit-cover-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
        }
    });

    $(".btn-submit-cover-image").on("click", function(e){
        $("#mod-img-cover").modal('show');
        $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        $("#ajx-img").html("");
        saveImgAjax(form_name, base_admin + 'ajax.php?m=news&acc=3', 'ajx-img-cover');
        return false;
    });

    // checkbox
    $('.button-checkbox').each(function () {
        // Settings
        var $widget = $(this),
        $button = $widget.find('button'),
        $checkbox = $widget.find('input:checkbox'),
        color = $button.data('color'),
        settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            var inp_id;

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                .removeClass('btn-default')
                .addClass('btn-' + color + ' active');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', true);
            } else {
                $button
                .removeClass('btn-' + color + ' active')
                .addClass('btn-default');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', false);
            }
        }

        // Initialization
        function init() {
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });

    $("#" + form_name).validate({
        rules:{
            title:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
            },
            title_en:{
                noSpecialCharacters: /([*+^${}><|\[\]\/\\])/g
            },
            category: {
                required: true,
            },
            seo_title: {
                noSpecialCharacters: /([*^${}><|\[\]\\])/g
            },
            seo_keywords: {
                noSpecialCharacters: /([*^${}><|\[\]\\])/g
            },
            seo_description: {
                noSpecialCharacters: /([*^${}><|\[\]\\])/g
            }
        },
        messages: {
            title:{
                required: "Debes ingresar el título",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            title_en:{
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            category: {
                required: "Debes seleccionar una categoría",
            },
            seo_title: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_keywords: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            seo_description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            var description = CKEDITOR.instances.description.getData();

            if (description == "") {
                $("#description_validate").html('<label id="title-error" class="error" for="title">Debes ingresar la descripción de la noticia</label>'); return false
            } else {
                $('#description').val(CKEDITOR.instances.description.getData());
            }

            $('#description_en').val(CKEDITOR.instances.description_en.getData());

            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
            persist(acc, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(acc, form_name) {
    var i = new FormData(document.getElementById(form_name));;
    var ext_url = "";

    if (acc == 2) {
        ext_url = "&n=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=news&acc=' + acc + ext_url,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin-editar-noticia/" + r.id + "/OK" + acc);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
