$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    $("#" + form_name).validate({
        rules:{
            email: {
                required: true,
                email: true
            },
            host: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            pass: {
                required: (acc == 1) ? true : false,
            }
        },
        messages: {
            email:{
                required: "Debes agregar un email",
                email: "Debes agregar un email válido"
            },
            host: {
                required: "Debes gregar el host",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            pass: {
                required: "Debes agregar una contraseña",
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");

            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            persist(acc, form_name); return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(acc, form_name) {
    var i = $("#" + form_name).serialize();

    if (acc == 2) {
        ext_url = "&id=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=config&acc=' + acc + ext_url,
        data: i,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin-configuracion-editar-email/" + r.id + "/OK" + acc);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
