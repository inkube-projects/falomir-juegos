$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    $("#image").on('change', function(e){
        $("#img-image").addClass('btn-success').removeClass('btn-default');
    });

    //color picker with addon
    $(".my-colorpicker").colorpicker();

    // checkbox
    $('.button-checkbox').each(function () {
        // Settings
        var $widget = $(this),
        $button = $widget.find('button'),
        $checkbox = $widget.find('input:checkbox'),
        color = $button.data('color'),
        settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            var inp_id;

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                .removeClass('btn-default')
                .addClass('btn-' + color + ' active');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', true);
            } else {
                $button
                .removeClass('btn-' + color + ' active')
                .addClass('btn-default');

                inp_id = $button.data('input');
                $('#' + inp_id).attr('checked', false);
            }
        }

        // Initialization
        function init() {
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });

    $("#" + form_name).validate({
        rules:{
            title:{
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            title_en:{
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            url: {
                url: true
            },
            description: {
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            description_en: {
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            },
            image: {
                // required: true,
                extension: "jpg|jpeg|png|gif"
            },
            background_color: {
                required: true,
                noSpecialCharacters: /([*+?^${}><|\[\]\/\\])/g
            }
        },
        messages: {
            title:{
                required: "Debes ingresar un título",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            title_en:{
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            url: {
                url: "Debes ingresar un URL válido"
            },
            description: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            description_en: {
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            image: {
                // required: "Debes agregar una imagen",
                extension: "Debes ingresar una imagen válida (jpg, jpeg, png, gif)"
            },
            background_color: {
                required: "Debes ingresar un código de color de fondo",
                noSpecialCharacters: "Debes ingresar un código válido"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            persist(acc, form_name);
            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(acc, form_name) {
    var i = new FormData(document.getElementById(form_name));
    var ext_url = "";

    if (acc == 2) {
        ext_url = "&i=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=carousel&acc=' + acc + ext_url,
        data: i,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(){
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        },
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin-editar-imagen/" + r.id + "/OK" + acc);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
