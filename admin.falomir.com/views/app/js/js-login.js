$(function(e){
   // Metodo adicional de validación
   $.validator.addMethod("noSpecialCharacters", function(value, element, arg){
      var response = true;
      if (value.match(arg)) {
         response = false;
      }
      return response;
   }, "No special characters allowed");

   // Validación del formulario de inicio de sesión
   $("#frm-login").validate({
      rules:{
         user_email: {
           required: true,
           rangelength: [2, 20],
           noSpecialCharacters: /([*+?^${}()|\[\]\/\\])/g
         },
         password: {
           required: true,
           rangelength: [3, 20],
         },
      },
      messages: {
         user_email: {
           required: "Debes ingresar tu Nombre de Usuario",
           rangelength: "El Usuario debe tener entre 2 y 20 caracteres",
           noSpecialCharacters: "No se permiten caracteres especiales"
         },
         password: {
           required: "Debes ingresar tu contraseña",
           rangelength: "La contraseña debe tener entre 2 y 20 caracteres",
         },
      },
      errorPlacement: function (error, element) {
         var name = $(element).attr("id");
         error.appendTo($("#" + name + "_validate"));
      },
      submitHandler: function(form) {
         $("#js-buttom").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando');

         loginAction();
         return false;
      }
   });
});

function loginAction() {
   var i = $("#frm-login").serialize();
   $.ajax({
      type: 'POST',
      url: base_admin + "ajax.php?m=login",
      dataType: 'json',
      data: i,
      success: function(r){
         if (r.status == "OK") {
            $("#alert-message").removeClass('alert-danger').addClass('alert-success').fadeIn(200).html('Inicio de sesión corrceto. Redireccionando <i class="fa fa-circle-o-notch fa-spin"></i>');
            $(location).attr('href', base_url + 'admin-dashboard');
         } else if (r.status == "Error") {
            $("#alert-message").removeClass('alert-success').addClass('alert-danger').fadeIn(200).html(r.message);
         }
         $("#js-buttom").attr("disabled", false).html('Iniciar Sesión');
      }
   });
}
