$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    $("#" + form_name).validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            name_en:{
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            }
        },
        messages: {
            name:{
                required: "Debes ingresar el nombre de la categoría",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            name_en:{
                noSpecialCharacters: "No se permiten caracteres especiales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");

            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

            persist(acc, form_name)

            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(acc, form_name) {
    var i = $("#" + form_name).serialize();
    var ext_url = "";

    if (acc == 2) {
        ext_url = "&c=" + $('#key').data('id');
    }

    $.ajax({
        type: 'POST',
        url: base_admin + 'ajax.php?m=news-category&acc=' + acc + ext_url,
        data: i,
        dataType: 'json',
        success: function(r){
            if (r.status == 'OK') {
                $(location).attr("href", "admin-editar-categoria-noticia/" + r.id + "/OK" + acc);
            } else {
                $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
                $(".btn-submit").attr("disabled", false).html('Guardar');
                $(document).scrollTop(0);
            }
        }
    });
}
