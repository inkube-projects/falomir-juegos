$(function(e){
    var form_name = $("#key").data('form');
    var acc = $("#key").data('acc');

    // Inicializar select2
    $('.select2').select2();

    /**
    * Se verifica que la imagen de portada sea una imagen válida
    * @param  {event} e Evento
    * @return {string}
    */
    $("#cover_image").on("change", function(e){
        var img = $(this).val();

        if (!(/\.(jpg|jpeg|png|gif)$/i).test(img)) {
            $(".btn-submit-cover-image").addClass('btn-warning').html("Debes ingresar una imagen válida (jpg, jpeg, png, gif)").attr('disabled', true);
        } else {
            $(".btn-submit-cover-image").removeClass('btn-warning').addClass('btn-success').html("Guardar imagen de portada").attr('disabled', false);
        }
    });

    /**
    * Se envía la imagen de portada
    * @param  {event} e Evento por defecto
    * @return {boolean}
    */
    $(".btn-submit-cover-image").on("click", function(e){
        $("#mod-img-cover").modal('show');
        $("#ajx-img-cover").html('<i class="fa fa-spinner fa-spin"></i> Cargando...');
        $("#ajx-img").html("");
        saveImgAjax(form_name, base_admin + 'ajax.php?m=user&acc=3', 'ajx-img-cover');
        return false;
    });

    $("#frm-add").validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            last_name:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            phone_1:{
                required: true,
                number: true,
                minlength: 5,
                maxlength: 11,
            },
            email:{
                required: true,
                email: true
            },
            phone_2:{
                number: true,
                minlength: 5,
                maxlength: 11,
            },
            username:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            status:{
                required: true,
            },
            role:{
                required: true,
            },
            pass_1:{
                required: true,
            },
            pass_2:{
                required: true,
                equalTo: "#pass_1"
            },
        },
        messages: {
            name:{
                required: "Debes ingresar un nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name:{
                required: "Debes ingresar un apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            phone_1:{
                required: "Debes ingresar un número telfónico",
                number: "Debes ingresar un número válido",
                minlength: "Debes ingresar un número válido",
                maxlength: "Debes ingresar un número válido",
            },
            email:{
                required: "Debes ingresar un email",
                email: "Debes ingresar un email válido"
            },
            phone_2:{
                number: "Debes ingresar un número válido",
                range: "Debes ingresar un número válido",
                minlength: "Debes ingresar un número válido",
                maxlength: "Debes ingresar un número válido"
            },
            username:{
                required: "Debes ingresar un nombre de usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            status:{
                required: "Debes ingresar el estado de la cuenta",
            },
            role:{
                required: "Debes ingresar un rol",
            },
            pass_1:{
                required: "Debes ingresar una contraseña",
            },
            pass_2:{
                required: "Debes repetir la contraseña",
                equalTo: "Las contraseñas deben de ser iguales"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");

            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

            persist(acc, form_name)

            return false;
        }
    });

    $("#frm-edit").validate({
        rules:{
            name:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            last_name:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            phone_1:{
                required: true,
                number: true,
                minlength: 5,
                maxlength: 11,
            },
            email:{
                required: true,
                email: true
            },
            phone_2:{
                number: true,
                minlength: 5,
                maxlength: 11,
            },
            username:{
                required: true,
                noSpecialCharacters: /([*+^${}><|\[\]\\])/g
            },
            status:{
                required: true,
            },
            role:{
                required: true,
            },
            pass_2:{
                equalTo: "#pass_1"
            },
        },
        messages: {
            name:{
                required: "Debes ingresar un nombre",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            last_name:{
                required: "Debes ingresar un apellido",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            phone_1:{
                required: "Debes ingresar un número telfónico",
                number: "Debes ingresar un número válido",
                minlength: "Debes ingresar un número válido",
                maxlength: "Debes ingresar un número válido",
            },
            email:{
                required: "Debes ingresar un email",
                email: "Debes ingresar un email válido"
            },
            phone_2:{
                number: "Debes ingresar un número válido",
                range: "Debes ingresar un número válido",
                minlength: "Debes ingresar un número válido",
                maxlength: "Debes ingresar un número válido"
            },
            username:{
                required: "Debes ingresar un nombre de usuario",
                noSpecialCharacters: "No se permiten caracteres especiales"
            },
            status:{
                required: "Debes ingresar el estado de la cuenta",
            },
            role:{
                required: "Debes ingresar un rol",
            },
            pass_2:{
                equalTo: "Las contraseñas deben de ser iguales"
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");

            error.appendTo($("#" + name + "_validate"));
        },
        submitHandler: function(form) {
            $(".btn-submit").attr("disabled", true).html('<i class="fa fa-spinner fa-spin"></i> Cargando...');

            persist(acc, form_name)

            return false;
        }
    });
});

/**
 * Persiste la información al ajax
 * @param  {ineteger} acc     Acción a realizar
 * @param  {string} form_name Nombre del formulario
 * @return {void}
 */
function persist(acc, form_name) {
    // var i = new FormData(document.getElementById(form_name));
   var i = $("#" + form_name).serialize();
   var ext_url = "";

   if (acc == 2) {
      ext_url = "&u=" + $('#key').data('id');
   }

   $.ajax({
      type: 'POST',
      url: base_admin + 'ajax.php?m=user&acc=' + acc + ext_url,
      data: i,
      // cache: false,
      // contentType: false,
      // processData: false,
      dataType: 'json',
      success: function(r){
         if (r.status == 'OK') {
            $(location).attr("href", "admin-editar-usuario/" + r.id + "/OK" + acc);
         } else {
            $('.js-alert').html('<div class="alert alt-alert alert-danger" role="alert">' + r.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + '</div>');
            $(".btn-submit").attr("disabled", false).html('Guardar');
            $(document).scrollTop(0);
         }
      }

   });
}
