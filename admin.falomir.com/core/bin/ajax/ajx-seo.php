<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/SeoHelper.php');
$db = new Connection();

if ($_POST) {
    $seoHelper = new SeoHelper($db);

    try {
        isValidString($_POST['keywords']);
        isValidString($_POST['description']);

        $seoHelper->persist();

        $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el SEO");
    } catch (\Exception $e) {
        $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
    }
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
