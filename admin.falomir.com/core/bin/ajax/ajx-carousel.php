<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/CarouselHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $carouselHelper = new CarouselHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('title', 'background_color'));
                isValidString($_POST['title']);
                isValidString($_POST['title_en']);
                isValidString($_POST['description']);
                isValidString($_POST['description_en']);
                isValidString($_POST['background_color'], "$%^*\|/");
                isValidURL($_POST['url']);

                if (isset($_FILES['image']['tmp_name'])) {
                    isValidImageFull('image', 300, 300);
                } else {
                    throw new \Exception("Debes ingresar una imagen", 1);
                }

                if (!isset($_POST['status'])) {
                    $_POST['status'] = "0";
                }

                $result = $carouselHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la imagen", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se edita una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['i'],0,"","");

            try {
                $db->existRecord("id='".$id."'", 'carousel', 'La imagen no existe');
                isRequiredValuesPost($_POST, array('title', 'background_color'));
                isValidString($_POST['title']);
                isValidString($_POST['title_en']);
                isValidString($_POST['description']);
                isValidString($_POST['description_en']);
                isValidString($_POST['background_color'], "$%^*\|/");
                isValidURL($_POST['url']);

                if (isset($_FILES['image']['tmp_name'])) {
                    isValidImageFull('image', 300, 300);
                } else {
                    throw new \Exception("Debes ingresar una imagen", 1);
                }

                if (!isset($_POST['status'])) {
                    $_POST['status'] = "0";
                }

                $result = $carouselHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la noticia", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
