<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/NewsHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $newsHelper = new NewsHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            try {
                isRequiredValuesPost($_POST, array('title', 'description', 'category', 'relevance'));
                isValidString($_POST['title']);
                isValidString($_POST['title_en']);
                isValidNumber($_POST['relevance']);
                isValidString($_POST['seo_title']);
                isValidString($_POST['seo_keywords']);
                isValidString($_POST['seo_description']);

                $db->existRecord("id='".$_POST['category']."'", 'news_category', 'La categoría no es válida');

                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImage('banner');
                }
                if (isset($_POST['status'])) {
                    isValidOptionGlobal(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                $result = $newsHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la noticia", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['n'],0,"","");

            try {
                $db->existRecord("id='".$id."'", 'news', 'La noticia no existe');
                isRequiredValuesPost($_POST, array('title', 'description', 'category', 'relevance'));
                isValidString($_POST['title']);
                isValidString($_POST['title_en']);
                isValidNumber($_POST['relevance']);
                isValidString($_POST['seo_title']);
                isValidString($_POST['seo_keywords']);
                isValidString($_POST['seo_description']);

                if (isset($_POST['status'])) {
                    isValidOptionGlobal(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                $db->existRecord("id='".$_POST['category']."'", 'news_category', 'La categoría no es válida');

                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImage('banner');
                }

                $result = $newsHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la noticia", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen de portada
            try {
                isValidPostImage(1, 'cover_image');
                $result = $newsHelper->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $newsHelper->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Editar relevancia
            $post_id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$post_id."'", "news", "El post no es válido");
                isValidNumber($_POST['relevance']);

                $newsHelper->setRelevance($post_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente");
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Verifica la imagen de portada
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPostImage($acc, $parameter)
{
    if ($acc == 1) { // Imagen de portada
        if (!empty($_FILES[$parameter]['tmp_name'])) {
            $img_type = $_FILES[$parameter]['type'];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                list($width, $height) = getimagesize($_FILES[$parameter]['tmp_name']);

                if (($width < 400) || ($height < 400)) {
                    throw new Exception("La imagen debe tener por lo menos 400 x 400px", 1);
                }
            } else {
                throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
        } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
        }
    }
    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
