<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ProductsHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $productsHelper = new ProductsHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            $arr_required = array(
                'ref',
                'name',
                'age',
                'measurement',
                'content',
                'description_1',
                'relevance'
            );

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['ref']);
                isValidString($_POST['name']);
                isValidString($_POST['age']);
                isValidNumber($_POST['relevance']);
                isValidString($_POST['seo_title']);
                isValidString($_POST['seo_keywords']);
                isValidString($_POST['seo_description']);

                // Se validan las categorías
                isValidSelectMultiple($_POST, 'cat', 'categoría');
                foreach ($_POST['cat'] as $key => $val) {
                    $db->existRecord("id='".@number_format($val,0,"","")."' AND principal='1'", 'products_category', 'La categoría no es válida');
                }

                // Se validan las subcategorías
                if (isset($_POST['subcategory'])) {
                    foreach ($_POST['subcategory'] as $key => $val) {
                        $db->existRecord("id='".@number_format($val,0,"","")."' AND principal='0'", 'products_category', 'La subcategoría no es válida');
                    }
                }

                if (isset($_POST['lang'])) {
                    isValidLang($_POST['lang']);
                } else {
                    throw new \Exception("Debes seleccionar al menos un idioma", 1);
                }

                if (isset($_POST['novelty'])) {
                    isValidNovelty($_POST['novelty']);
                } else {
                    $_POST['novelty'] = "0";
                }

                if (isset($_POST['best_seller'])) {
                    isValidNovelty($_POST['best_seller']);
                } else {
                    $_POST['best_seller'] = "0";
                }


                if (isset($_FILES['img_1']['tmp_name'])) {
                    isValidImage('img_1');
                }
                if (isset($_FILES['img_2']['tmp_name'])) {
                    isValidImage('img_2');
                }
                if (isset($_FILES['logo']['tmp_name'])) {
                    isValidImage('logo');
                }
                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImage('banner');
                }
                if (isset($_FILES['novelty_image']['tmp_name'])) {
                    isValidImage('novelty_image');
                }

                if (isset($_POST['status'])) {
                    isValidOptionGlobal(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                $result = $productsHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el producto", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Se guarda una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['p'],0,"","");

            $arr_required = array(
                'ref',
                'name',
                'age',
                'measurement',
                'content',
                'description_1',
                'relevance'
            );

            try {
                $db->existRecord("id='".$id."'", 'products', 'El producto no existe');
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['ref']);
                isValidString($_POST['name']);
                isValidString($_POST['age']);
                isValidNumber($_POST['relevance']);
                isValidString($_POST['seo_title']);
                isValidString($_POST['seo_keywords']);
                isValidString($_POST['seo_description']);

                // Se validan las categorías
                isValidSelectMultiple($_POST, 'cat', 'categoría');
                foreach ($_POST['cat'] as $key => $val) {
                    $db->existRecord("id='".@number_format($val,0,"","")."' AND principal='1'", 'products_category', 'La categoría no es válida');
                }

                // Se validan las subcategorías
                if (isset($_POST['subcategory'])) {
                    foreach ($_POST['subcategory'] as $key => $val) {
                        $db->existRecord("id='".@number_format($val,0,"","")."' AND principal='0'", 'products_category', 'La subcategoría no es válida');
                    }
                }

                if (isset($_POST['lang'])) {
                    isValidLang($_POST['lang']);
                } else {
                    throw new \Exception("Debes seleccionar al menos un idioma", 1);
                }

                if (isset($_POST['novelty'])) {
                    isValidNovelty($_POST['novelty']);
                } else {
                    $_POST['novelty'] = "0";
                }

                if (isset($_POST['best_seller'])) {
                    isValidNovelty($_POST['best_seller']);
                } else {
                    $_POST['best_seller'] = "0";
                }

                if (isset($_FILES['img_1']['tmp_name'])) {
                    isValidImage('img_1');
                }
                if (isset($_FILES['img_2']['tmp_name'])) {
                    isValidImage('img_2');
                }
                if (isset($_FILES['logo']['tmp_name'])) {
                    isValidImage('logo');
                }
                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImage('banner');
                }
                if (isset($_FILES['novelty_image']['tmp_name'])) {
                    isValidImage('novelty_image');
                }

                if (isset($_POST['status'])) {
                    isValidOptionGlobal(0, 1, $_POST['status'], "El estado no es válido");
                } else {
                    $_POST['status'] = "0";
                }

                $result = $productsHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el producto", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen de portada
            try {
                isValidPostImage(1, 'cover_image');
                $result = $productsHelper->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $productsHelper->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Editar relevancia
            $product_id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El producto no es válido");
                isValidNumber($_POST['relevance']);

                $productsHelper->setRelevance($product_id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente");
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6: // Se agrega productos a categorías desde el listado
            $db->beginTransaction();
            $product_id = @number_format($_GET['id'],0,"","");

            try {
                $db->existRecord("id='".$product_id."'", "products", "El produto no es válido");

                // Se validan las categorías
                isValidSelectMultiple($_POST, 'categories', 'categoría');
                foreach ($_POST['categories'] as $key => $val) {
                    $db->existRecord("id='".@number_format($val,0,"","")."' AND principal='1'", 'products_category', 'La categoría no es válida');
                }

                $result = $productsHelper->setCategories($product_id, $_POST['categories']);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente el producto", 'data' => $result);
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Valida si el idioma es válido
 * @param  array  $arr Arreglo con los ID de idiomas
 * @return boolean|exception
 */
function isValidLang($arr)
{
    $db = new Connection;
    foreach ($arr as $val) {
        $id = number_format($val,0,"","");
        $db->existRecord("id='".$id."'", 'language', 'El lenguaje no es válido');
    }

    return true;
}

/**
 * Valida si el valor de novedad es válido
 * @param  integer  $val valor de la novedad
 * @return boolean|Exception
 */
function isValidNovelty($val)
{
    if ($val != 1 && $val != 0) {
        throw new \Exception("El valor Novedad es inválido", 1);
    }

    return true;
}

/**
 * Verifica la imagen de portada
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPostImage($acc, $parameter)
{
    if ($acc == 1) { // Imagen de portada
        if (!empty($_FILES[$parameter]['tmp_name'])) {
            $img_type = $_FILES[$parameter]['type'];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                list($width, $height) = getimagesize($_FILES[$parameter]['tmp_name']);

                if (($width < 200) || ($height < 100)) {
                    throw new Exception("La imagen debe tener por lo menos 200 x 100px", 1);
                }
            } else {
                throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
        } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
        }
    }
    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
