<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ConfigHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $configHelper = new ConfigHelper($db);

    switch ($acc) {
        case 1: // Se agrega un email
            $db->beginTransaction();
            $arr_required = array('email', 'host', 'pass');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidEmail($_POST['email']);
                isValidString($_POST['host'], "#$%^*\|");

                $result = $configHelper->persistEmail();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2:
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");

            $arr_required = array('email', 'host');

            try {
                $db->existRecord("id='".$id."'", "config_email", "La entrada no es válida");
                isRequiredValuesPost($_POST, $arr_required);
                isValidEmail($_POST['email']);
                isValidString($_POST['host'], "#$%^*\|");

                $result = $configHelper->updateEmail($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
