<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ContactHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $contactHelper = new ContactHelper($db);

    switch ($acc) {
        case 1: // Se edita el mensaje de contacto
            $db->beginTransaction();
            $id = @number_format($_GET['id'],0,"","");
            $arr_required = array('email', 'name', 'last_name', 'message');

            try {
                $db->existRecord("id='".$id."'", "contact_messages", "El mensaje no es válido");
                isRequiredValuesPost($_POST, $arr_required);
                isValidEmail($_POST['email']);
                isValidString($_POST['name'], "#$%^*\|");
                isValidString($_POST['last_name'], "#$%^*\|");
                isValidString($_POST['message'], "#$%^*\|");
                isValidString($_POST['note'], "#$%^*\|");

                $result = $contactHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
