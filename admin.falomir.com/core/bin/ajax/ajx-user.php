<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/UserHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $userHelper = new UserHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            $arr_required = array(
                'name',
                'last_name',
                'phone_1',
                'email',
                'username',
                'status',
                'role',
                'pass_1'
            );

            try {

                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['last_name']);
                isValidPhoneNumber($_POST['phone_1']);
                isValidPhoneNumber($_POST['phone_2']);
                isValidEmail($_POST['email']);
                isValidString($_POST['username']);
                isValidString($_POST['note']);

                $db->existRecord("id='".$_POST['status']."'", "user_status", "El estado de la cuenta no es válido");
                $db->existRecord("id='".$_POST['role']."'", "role", "El rol no es válido");
                $db->duplicatedRecord("email='".$_POST['email']."'", "user", "El email ya se encuentra en uso");
                $db->duplicatedRecord("username='".$_POST['username']."'", "user", "El Nombre de usuario ya se encuentra en uso");

                $result = $userHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el usuario", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Editar una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['u'],0,"","");

            $arr_required = array(
                'name',
                'last_name',
                'email',
                'username',
                'status',
                'role',
            );

            try {

                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['last_name']);
                isValidPhoneNumber($_POST['phone_1']);
                isValidPhoneNumber($_POST['phone_2']);
                isValidEmail($_POST['email']);
                isValidString($_POST['username']);
                isValidString($_POST['note']);

                $db->existRecord("id='".$id."'", "user", "El usuario no existe");
                $db->existRecord("id='".$_POST['status']."'", "user_status", "El estado de la cuenta no es válido");
                $db->existRecord("id='".$_POST['role']."'", "role", "El rol no es válido");
                $db->duplicatedRecord("email='".$_POST['email']."' AND NOT id='".$id."'", "user", "El email ya se encuentra en uso");
                $db->duplicatedRecord("username='".$_POST['username']."' AND NOT id='".$id."'", "user", "El Nombre de usuario ya se encuentra en uso");

                $result = $userHelper->edit($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente el usuario", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen en la carpeta temporal
            try {
                isValidPostImage(1);
                $result = $userHelper->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $userHelper->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------
function idValidRole()
{
    $db = new Connection();
    $cnt_val = $db->getCount("portfolio", "id = '".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La Obra no existe", 1);
    }

    return true;
}

/**
 * Verifica si la promocion existe
 * @param  integer  $id ID de la promoción
 * @return boolean
 */
function isValidPortfolio($id)
{
    $db = new Connection();
    $cnt_val = $db->getCount("portfolio", "id = '".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La Obra no existe", 1);
    }

    return true;
}

/**
 * Verifica la imagen de portada
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPostImage($acc)
{
    if ($acc == 1) { // Imagen de portada
        if (!empty($_FILES['cover_image']['tmp_name'])) {
            $img_type = $_FILES['cover_image']['type'];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                list($width, $height) = getimagesize($_FILES['cover_image']['tmp_name']);

                if (($width < 400) || ($height < 400)) {
                    throw new Exception("La imagen debe tener por lo menos 400 x 400px", 1);
                }
            } else {
                throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
        } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
        }
    }
    return true;
}

/**
 * Valida si la imagen es válida
 * @return boolean
 */
function isValidLogo()
{
    if (!empty($_FILES['logo']['tmp_name'])) {
        $img_type = $_FILES['logo']['type'];

        if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['logo']['tmp_name']);

            if (($width < 100) || ($height < 50)) {
                throw new Exception("La imagen debe tener por lo menos 100 x 50px", 1);
            }
        } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
        }
    }

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
