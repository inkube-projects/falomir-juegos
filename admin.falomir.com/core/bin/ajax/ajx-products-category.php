<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/ProductsCategoryHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $productsCategoryHelper = new ProductsCategoryHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['name_en']);
                isValidString($_POST['background_color'], "$%^*\|/");

                if (isset($_POST['principal'])) {
                    isValidOption($_POST['principal']);
                    $_POST['father'] = "";
                } else {
                    $db->existRecord("id='".@number_format($_POST['father'],0,"","")."' AND principal='1'", "products_category", "El padre no es válido");
                    $_POST['principal'] = "0";
                }

                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImageFull('banner', 300, 300);
                } else {
                    throw new \Exception("Debes ingresar una imagen de banner", 1);
                }

                $result = $productsCategoryHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Editar una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['c'],0,"","");
            $arr_required = array('name');

            try {
                $db->existRecord("id='".$id."'", "products_category", "La categoría no existe");
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['name_en']);
                isValidString($_POST['background_color'], "$%^*\|/");

                if (isset($_POST['principal'])) {
                    isValidOption($_POST['principal']);
                    $_POST['father'] = "";
                } else {
                    $db->existRecord("id='".@number_format($_POST['father'],0,"","")."' AND principal='1'", "products_category", "El padre no es válido");
                    $_POST['principal'] = "0";
                }

                if (isset($_FILES['banner']['tmp_name'])) {
                    isValidImageFull('banner', 300, 300);
                } else {
                    throw new \Exception("Debes ingresar una imagen de banner", 1);
                }

                if (isset($_POST['prod'])) {
                    foreach ($_POST['prod'] as $val) {
                        $db->existRecord("id='".$val."'", "products", "El producto no es válido");
                    }
                }

                $result = $productsCategoryHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Se guarda la imagen en la carpeta temporal
            try {
                isValidPostImage(1);
                $result = $productsCategoryHelper->prepareCoverImage(1);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // se recorta la imagen
            try {
                isRequiredValuesPost($_POST, array('x','y','w','h'));
                $result = $productsCategoryHelper->prepareCoverImage(2);

                $arr_response = array('status' => 'OK', 'data' => $result);
            } catch (Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5: // Muestra la subcategoría
            // $id = @number_format($_GET['id'],0,"","");
            $product_id = @number_format($_POST['product_id'],0,"","");
            try {
                if (is_array($_POST['category'])) {
                    foreach ($_POST['category'] as $key => $cat) {
                        $db->existRecord("id='".@number_format($cat,0,"","")."' AND principal='1'", "products_category", "La categoría no existe");
                    }
                }

                $result = $productsCategoryHelper->showSubCategory($_POST['category'], $product_id);

                $arr_response = array('status' => 'OK', 'response' => $result);
            } catch (\Exception $e) {
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }

        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------


/**
 * Verifica la imagen de portada
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPostImage($acc)
{
    if ($acc == 1) { // Imagen de portada
        if (!empty($_FILES['cover_image']['tmp_name'])) {
            $img_type = $_FILES['cover_image']['type'];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                list($width, $height) = getimagesize($_FILES['cover_image']['tmp_name']);

                if (($width < 400) || ($height < 400)) {
                    throw new Exception("La imagen debe tener por lo menos 400 x 400px", 1);
                }
            } else {
                throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
        } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
        }
    }
    return true;
}

/**
 * Valida si la opcion principal es válida
 * @param  integer  $value Opción
 * @return boolean|exception
 */
function isValidOption($value)
{
    if ($value != 1 && $value != 0) {
        throw new \Exception("La opción no es válida", 1);
    }

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
