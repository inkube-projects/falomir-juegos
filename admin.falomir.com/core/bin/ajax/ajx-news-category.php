<?php

include('core/model/GeneralMethods.php');
include('core/bin/helpers/NewsCategoryHelper.php');
$db = new Connection();

$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');
$acc = @number_format($_GET['acc'],0,"","");

if ($_GET) {
    $newsCategoryHelper = new NewsCategoryHelper($db);

    switch ($acc) {
        case 1: // Se guarda una entrada
            $db->beginTransaction();

            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['name_en']);

                $result = $newsCategoryHelper->persist();

                $arr_response = array('status' => 'OK', 'message' => "Se ha guardado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Editar una entrada
            $db->beginTransaction();
            $id = @number_format($_GET['c'],0,"","");
            $arr_required = array('name');

            try {
                isRequiredValuesPost($_POST, $arr_required);
                isValidString($_POST['name']);
                isValidString($_POST['name_en']);
                $db->existRecord("id='".$id."'", "news_category", "La categoría no existe");

                $result = $newsCategoryHelper->update($id);

                $arr_response = array('status' => 'OK', 'message' => "Se ha editado correctamente la categoría", 'id' => $result['id']);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------
function idValidRole()
{
    $db = new Connection();
    $cnt_val = $db->getCount("portfolio", "id = '".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La Obra no existe", 1);
    }

    return true;
}

/**
 * Verifica si la promocion existe
 * @param  integer  $id ID de la promoción
 * @return boolean
 */
function isValidPortfolio($id)
{
    $db = new Connection();
    $cnt_val = $db->getCount("portfolio", "id = '".$id."'");

    if ($cnt_val == 0) {
        throw new \Exception("La Obra no existe", 1);
    }

    return true;
}

/**
 * Verifica la imagen de portada
 * @param  integer  $acc Acción a ejecutar [1: validar imagen nueva de protada]
 * @return boolean
 */
function isValidPostImage($acc)
{
    if ($acc == 1) { // Imagen de portada
        if (!empty($_FILES['cover_image']['tmp_name'])) {
            $img_type = $_FILES['cover_image']['type'];

            if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                list($width, $height) = getimagesize($_FILES['cover_image']['tmp_name']);

                if (($width < 400) || ($height < 400)) {
                    throw new Exception("La imagen debe tener por lo menos 400 x 400px", 1);
                }
            } else {
                throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
            }
        } else {
            throw new Exception("La imagen de portada no puede estar vacia", 1);
        }
    }
    return true;
}

/**
 * Valida si la imagen es válida
 * @return boolean
 */
function isValidLogo()
{
    if (!empty($_FILES['logo']['tmp_name'])) {
        $img_type = $_FILES['logo']['type'];

        if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
            list($width, $height) = getimagesize($_FILES['logo']['tmp_name']);

            if (($width < 100) || ($height < 50)) {
                throw new Exception("La imagen debe tener por lo menos 100 x 50px", 1);
            }
        } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
        }
    }

    return true;
}

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null
?>
