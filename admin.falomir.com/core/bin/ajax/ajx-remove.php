<?php

$db = new Connection();
$arr_response = array('status' => 'Error', 'message' => 'Se ha producido un error');

include('core/model/GeneralMethods.php');
include('core/bin/helpers/RemoveHelper.php');

if ($_GET) {
    $acc = @number_format($_GET['acc'],0,"","");
    $removeHelper = new RemoveHelper($db);

    switch ($acc) {
        case 1: // categorías de productos
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "products_category", "La categoría no existe");
                $removeHelper->removeProductCategory($id);

                $arr_response = array('status' => 'OK', 'message' => 'La categoría ha sido eliminada');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 2: // Productos
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "products", "El producto no es válido");
                $removeHelper->removeProduct($id);

                $arr_response = array('status' => 'OK', 'message' => 'El producto ha sido eliminado');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 3: // Categoria de noticias
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "news_category", "La categoría no es válida");
                $removeHelper->removeNewsCategory($id);

                $arr_response = array('status' => 'OK', 'message' => 'La categoría ha sido eliminada');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 4: // borrar imagenes de carrusel
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "carousel", "La imagen de carrusel no es válida");
                $removeHelper->removeCarousel($id);

                $arr_response = array('status' => 'OK', 'message' => 'La imagen ha sido eliminada');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 5:
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "news", "La publicación no existe");
                $removeHelper->removeNews($id);

                $arr_response = array('status' => 'OK', 'message' => 'La publicación ha sido eliminada');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;

        case 6:
            $id = number_format($_GET['id'],0,"","");

            $db->beginTransaction();

            try {
                $db->existRecord("id='".$id."'", "config_email", "El email no existe");
                $removeHelper->removeConfigEmail($id);

                $arr_response = array('status' => 'OK', 'message' => 'El email ha sido eliminada');
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
                $arr_response = array('status' => 'Error', 'message' => $e->getMessage());
            }
        break;
    }
}

//------------------------------------------------------------------------------

header('Content-Type: application/json');
echo json_encode($arr_response);
$db = null;
?>
