<?php
/**
 * Encriptado basico de contraseña
 * @param  String $input Contraseña a encriptar
 * @return String
 */
function encriptar($input)
{
    $basura = "cla_tu";
    $basura_2 = "ve_ner";
    $clave = sha1($basura.$input.$basura_2);
    return $clave;
}

/**
 * Encriptado con bcrypt
 * @param  String $value Entrada a encriptar
 * @param  Integer $salt  Salt de la contraseña (1 al 12)
 * @return Sttring
 */
function hashPass($value, $salt)
{
    $opt = array('cost' => $salt);
    $encoded = password_hash($value, PASSWORD_BCRYPT, $opt);
    return $encoded;
}

/**
 * Retorna el atributo html selected si los valores son iguales
 * @param  String  $cmp1 Valor 1
 * @param  String  $cmp2 Valor 2
 * @return String
 */
function isSelected($cmp1, $cmp2)
{
    if ($cmp1==$cmp2) {
        return ' selected="selected"';
    } else {
        return '';
    }
}

/**
 * Retorna el atributo html selected si los valores son iguales en select multiple
 * @param  array   $arr Arreglo de valores
 * @param  integer  $val Valor actual
 * @return boolean
 */
function isSelectedMultiple(array $arr, $val)
{
	$r = "";
	foreach ($arr as $v) {
		if ($v == $val) { return ' selected="selected"'; }
	}

	return $r;
}


/**
 * Retorna el atributo html checked si los valores son iguales
 * @param  String  $cmp1 Valor 1
 * @param  String  $cmp2 Valor 2
 * @return String
 */
function isOption($cmp1, $cmp2)
{
    if ($cmp1==$cmp2) {
        return ' checked="checked"';
    } else {
        return '';
    }
}

/**
 * Retorna el atributo html checked si el valor se encuentra dentro del arreglo
 * @param  Array  $arreglo      Arreglo a verificar
 * @param  String $valor_actual Valor a Buecar en el arreglo
 * @return String
 */
function isChecked($arreglo, $valor_actual)
{
    if (in_array($valor_actual, $arreglo)) {
        return ' checked="checked"';
    } else {
        return '';
    }
}

/**
 * Valida contra caracteres especiales
 * @param  String  $value Valor a verificar
 * @return String
 */
function isValidString($value, $not_allowed = "#$%^*\|/")
{
    for ($i  = 0; $i<strlen($value); $i++) {
        if (!strpos($not_allowed, substr($value, $i, 1))===false) {
            throw new Exception("No se permiten caracteres especiales", 1);
        }
    }
    return $value;
}

/**
 * Se valida que sea un numero entero válido
 * @param  Integer  $value Valor a validar
 * @return Integer
 */
function isValidNumber($value)
{
    $allowed = "1234567890";
    for ($i = 0; $i < strlen($value); $i++) {
        if (strpos($allowed, substr($value, $i, 1)) === false) {
            throw new Exception("Solo se permiten números", 1);
        }
    }
    return $value;
}

/**
 * Se valida si es número telefónico válido
 * @param  String  $value Valor a validar
 * @return String
 */
function isValidPhoneNumber($value)
{
    $allowed = "1234567890 ()-";
    for ($i = 0; $i < strlen($value); $i++) {
        if (strpos($allowed, substr($value, $i, 1)) === false) {
            throw new Exception("Número telefónico inválido", 1);
        }
    }
    return $value;
}

/**
 * Se valida si es un decimal válido
 * @param  Decimal  $value Valor a validar
 * @return Decimal
 */
function isValidDecimal($value)
{
    $regex = '/^\s*[+\-]?(?:\d+(?:\.\d*)?|\.\d+)\s*$/';
    if (!preg_match($regex, $value)) {
        throw new Exception("El decimal es inválido", 1);
    }
    return $value;
}

/**
 * Se valida que sea una URL válida
 * @param  String  $url Valor a verificar
 * @return String
 */
function isValidURL($url)
{
    if (!empty($url)) {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new Exception("Debes ingresar un URL válido", 1);
        }
    }
    return $url;
}

/**
 * Se verifica si es un Email válido
 * @param  String  $email Valor a validar
 * @return String
 */
function isValidEmail($email)
{
    if (!empty($email)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new Exception("Debes ingresar un email válido", 1);
        }
    }
    return $email;
}

/**
 * Se verifica que sea una fecha válida
 * @param  String  $date   Valor a verificar
 * @param  String  $format Formato de la fecha
 * @param  String  $sep    Separador
 * @return String
 */
function isValidDate($date, $format = "es", $sep = "-")
{
    if (!empty($date)) {
        $date = explode($sep, $date);
        if ($format == "es") {
            $day = $date[0];
            $month = $date[1];
            $year = $date[2];
        } elseif (condition) {
            $day = $date[1];
            $month = $date[0];
            $year = $date[2];
        }

        if (!checkdate($month, $day, $year)) {
            throw new Exception("Debes ingresar una fecha válida", 1);
        }
    }

    return $date;
}

/**
 * Valida la extension de una imagen
 * @param  string  $parameter Nombre del paramnetro
 * @return boolean|exception
 */
function isValidImage($parameter)
{
    if (!empty($_FILES[$parameter]['tmp_name'])) {
        $img_type = $_FILES[$parameter]['type'];

        if ((!strpos($img_type, "gif")) && (!strpos($img_type, "jpeg")) && (!strpos($img_type, "jpg")) && (!strpos($img_type, "png"))) {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
        }
    }

    return false;
}

/**
 * Valida si una una opción entre un rango de dos valores
 * @param  integer  $range_init Rango mínimo
 * @param  integer  $range_end  Rango máximo
 * @param  integer  $value      Valor que se esta ingresando
 * @param  string  $message    Mensaje de excepción
 * @return boolean
 */
function isValidOptionGlobal($range_init, $range_end, $value, $message)
{
    $value = @number_format($value, 0, "", "");
    if ($value < $range_init && $value > $range_end) {
        throw new exception($message, 400);
    }

    return true;
}

/**
 * Valida los parametros obligatorios en POST
 * @param  object  $post         Objeto Post
 * @param  array  $arr_required  Arreglo con los parametros a verificar
 * @return boolean
 */
function isRequiredValuesPost($post, $arr_required)
{
    for ($i = 0; $i < sizeof($arr_required); $i++) {
        if (isset($post[$arr_required[$i]])) {
            if (trim($post[$arr_required[$i]]) == "") {
                throw new Exception("El campo ".$arr_required[$i]." no puede estar vacío", 1);
            }
        } else {
            throw new Exception("El campo ".$arr_required[$i]." no puede estar vacío", 1);
        }
    }

    return true;
}

/**
 * Valida si un select multiple se esta enviando
 * @param  array   $post       array
 * @param  string  $parameter  [description]
 * @param  string  $name_param [description]
 * @return boolean             [description]
 */
function isValidSelectMultiple($post, $parameter, $name_param)
{
    if (!isset($post[$parameter])) {
        throw new Exception("Debes seleccionar al menos una opción: ".$name_param, 1);
    }
    if ($post[$parameter] == 0) {
        throw new Exception("Debes seleccionar al menos una opción: ".$name_param, 1);
    }

    return true;
}

/**
 * Muentra un arreglo o obejto
 * @param  object $arr Objeto a mostrar
 * @return object
 */
function showArr($arr)
{
    echo "<pre>";
    var_dump($arr);
    echo "</pre>";
}

/**
 * Limpia las entradas de etiquetas html
 * @param string $input Entrada a limpiar
 * @return [type]        [description]
 */
function sanitize($input)
{
    if (is_array($input)) {
        foreach ($input as $var=>$val) {
            $output[$var] = cleanInput($val);
        }
    } else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = $input;//$output = mysql_real_escape_string($input);
    }
    return addslashes($output);
}
//----------------------------------------------------------------------------------------------------------------
function cleanInput($input)
{
    $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    //'CREATE', 'DELETE','INSERT', 'UPDATE', 'DROP'
  );
    $output = preg_replace($search, '', $input);
    return $output;
}

/**
 * Invierte el formato de las fechas
 * @param  string $date_en fecha
 * @return string
 */
function to_date($date_en)
{
    $f = explode("-", $date_en);
    $f = array_reverse($f);
    return implode("-", $f);
}

/**
 * Cambia el formato de fechas
 * @param  string $datetime Fecha en formato datetime
 * @return array
 */
function datetime_format($datetime)
{
    if ($datetime != "") {
        $datetime = explode(" ", $datetime);
        $arr_result = array('date' => to_date($datetime[0]), 'time' => $datetime[1]);
    } else {
        $arr_result = array('date' => '', 'time' => '');
    }

    return $arr_result;
}

/**
 * Calcula los dias entre dos fechas
 * @param  string $fecha_i fecha inicial
 * @param  string $fecha_f Fecha final
 * @return string
 */
function dias_transcurridos($fecha_i, $fecha_f)
{
    $dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
    $dias = abs($dias);
    $dias = floor($dias);
    return $dias;
}

/**
 * Genera un token o contraseña
 * @param  string $pre Variable basura
 * @param  integer $num Tamaño del token
 * @return string
 */
function genPass($pre, $num)
{
    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $cad = "";
    for ($i=0;$i<$num;$i++) {
        $cad.= substr($str, rand(0, 62), 1);
    }
    return $pre.$cad;
}

/**
 * Retorna la fecha con el nombre del mes
 * @param  string $date La fecha que se le cambiara el formato
 * @return string
 */
function dateName($date)
{
    $arrMes = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre');
    $mes = explode("-", $date);
    $mesPos = $mes[1];
    $mesNB = $arrMes[$mesPos];
    $year = $mes[2];
    return $mes[0]." ".$mesNB.". ".$year;
}

/**
 * Elimina un directorio
 * @param  string $carpeta ruta a elimnar
 * @return boolean
 */
function removeDir($folder)
{
    foreach (glob($folder . "/*") as $file) {
        if (is_dir($file)) {
            eliminarDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($folder);

    return true;
}

/**
 * Crea un token unico
 * @return string
 */
function UniqueToken()
{
    $s = strtoupper(md5(uniqid(rand(), true)));
    $guidText =
        substr($s, 0, 8).
        substr($s, 8, 4).
        substr($s, 12, 4).
        substr($s, 16, 4).
        substr($s, 20);
    return $guidText;
}

/**
 * Limpia un directorio
 * @param  string $path Directorio a limpiar
 * @return void
 */
function clearDirectory($path)
{
    $handle = opendir($path);

    while ($file = readdir($handle)) {
        if (is_file($path.$file)) {
            @unlink($path.$file);
        }
    }
}

/**
 * Obtiene el dominio y lo retorna con el formato deseado
 * @param  string $url RL a modificar
 * @return string
 */
function saca_dominio($url)
{
    $protocolos = array('www.');
    $url = explode('/', str_replace($protocolos, '', $url));
    return "http://".$url[0]."/cristobal";
}

/**
 * Verifica si el archivo existe
 * @param  string $filename Nombre y ruta del archivo
 * @return boolean
 */
function fileExist($filename)
{
    if ((file_exists($filename)) && (basename($filename) != "")) {
        return true;
    } else {
        return false;
    }
}

/**
 * Verifica si una imagen es válida
 * @param  string  $param Nombre del parametro que contiene la imagen
 * @param  integer  $min_w Ancho mínima
 * @param  integer  $min_h Altura mínima
 * @return boolean|exception
 */
function isValidImageFull($param, $min_w, $min_h)
{
    if (!empty($_FILES[$param]['tmp_name'])) {
        $img_type = $_FILES[$param]['type'];

        if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type, "jpg"))||(strpos($img_type, "png"))) {
            list($width, $height) = getimagesize($_FILES[$param]['tmp_name']);

            if (($width < $min_w) || ($height < $min_h)) {
                throw new Exception(sprintf("La imagen debe tener por lo menos %d x %d px", $min_w, $min_h), 1);
            }
        } else {
            throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
        }
    }

    return true;
}

//----------------------------------------------------------------------------------------------------------------
function UpPicture($input, $thumbWid, $adcName, $ruta)
{
    $imgOK = false;
    $img_type = $_FILES[$input]['type'];
    if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type, "jpg"))||(strpos($img_type, "png"))) {
        $extens = str_replace("image/", ".", $_FILES[$input]['type']);
        $imagen = $adcName;
        if (move_uploaded_file($_FILES[$input]['tmp_name'], $ruta.$imagen)) {
            if (($extens==".jpeg")||($extens==".jpg")) {
                $frame = imagecreatefromjpeg($ruta.$imagen);
            }
            if ($extens==".png") {
                $frame = imagecreatefrompng($ruta.$imagen);
            }
            if ($extens==".gif") {
                $frame = imagecreatefromgif($ruta.$imagen);
            }
            $width = imagesx($frame);
            $height = imagesy($frame);
            $new_width = $thumbWid;
            $new_height = floor($height * ($thumbWid / $width));
            $img=imagecreatetruecolor($new_width, $new_height);
            imagealphablending($img, true);
            $transparent = imagecolorallocatealpha($img, 0, 0, 0, 127);
            imagefill($img, 0, 0, $transparent);
            imagecopyresampled($img, $frame, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagealphablending($img, false);
            imagesavealpha($img, true);
            imagepng($img, $ruta."min_".$imagen);
            if (($extens==".jpeg")||($extens==".jpg")) {
                if (imagejpeg($img, $ruta."min_".$imagen, 90)) {
                    $imgOK = true;
                }
            }
            if ($extens==".png") {
                if (imagepng($img, $ruta."min_".$imagen)) {
                    $imgOK = true;
                }
            }
            if ($extens==".gif") {
                if (imagegif($img, $ruta."min_".$imagen)) {
                    $imgOK = true;
                }
            }
            imagedestroy($img);
        }
    }
    return $imgOK;
}
//----------------------------------------------------------------------------------------------------------------
function UpPictureArray($input, $thumbWid, $adcName, $ruta, $i)
{
    $imgOK = false;
    $img_type = $_FILES[$input]['type'][$i];
    if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type, "jpg"))||(strpos($img_type, "png"))) {
        $extens = str_replace("image/", ".", $_FILES[$input]['type'][$i]);
        $imagen = $adcName;
        if (move_uploaded_file($_FILES[$input]['tmp_name'][$i], $ruta.$imagen)) {
            if (($extens==".jpeg")||($extens==".jpg")) {
                $frame = imagecreatefromjpeg($ruta.$imagen);
            }
            if ($extens==".png") {
                $frame = imagecreatefrompng($ruta.$imagen);
            }
            if ($extens==".gif") {
                $frame = imagecreatefromgif($ruta.$imagen);
            }
            $width = imagesx($frame);
            $height = imagesy($frame);
            $new_width = $thumbWid;
            $new_height = floor($height * ($thumbWid / $width));
            $img=imagecreatetruecolor($new_width, $new_height);
            imagealphablending($img, true);
            $transparent = imagecolorallocatealpha($img, 0, 0, 0, 127);
            imagefill($img, 0, 0, $transparent);
            imagecopyresampled($img, $frame, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagealphablending($img, false);
            imagesavealpha($img, true);
            imagepng($img, $ruta."min_".$imagen);
            if (($extens==".jpeg")||($extens==".jpg")) {
                if (imagejpeg($img, $ruta."min_".$imagen, 90)) {
                    $imgOK = true;
                }
            }
            if ($extens==".png") {
                if (imagepng($img, $ruta."min_".$imagen)) {
                    $imgOK = true;
                }
            }
            if ($extens==".gif") {
                if (imagegif($img, $ruta."min_".$imagen)) {
                    $imgOK = true;
                }
            }
            imagedestroy($img);
        }
    }
    return $imgOK;
}
//----------------------------------------------------------------------------------------------------------------
function ImagenTemp($input, $newWidth, $adcName, $ruta) // sube una imagen con una redimension
{
    $imgOK = false;
    $img_type = $_FILES[$input]['type'];
    if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type, "jpg"))||(strpos($img_type, "png"))) {
        $extens = str_replace("image/", ".", $_FILES[$input]['type']);
        $imagen = $adcName;
        if (move_uploaded_file($_FILES[$input]['tmp_name'], $ruta.$imagen)) {
            if (($extens==".jpeg")||($extens==".jpg")) {
                $frame = imagecreatefromjpeg($ruta.$imagen);
            }
            if ($extens==".png") {
                $frame = imagecreatefrompng($ruta.$imagen);
            }
            if ($extens==".gif") {
                $frame = imagecreatefromgif($ruta.$imagen);
            }
            $width = imagesx($frame);
            $height = imagesy($frame);
            if ($newWidth=='') {
                $new_width = $width;
            } else {
                $new_width = $newWidth;
            }
            $new_height = floor($height * ($newWidth / $width));
            $img=imagecreatetruecolor($new_width, $new_height);
            imagealphablending($img, true);
            $transparent = imagecolorallocatealpha($img, 255, 255, 255, 1);
            imagefill($img, 0, 0, $transparent);
            imagecopyresampled($img, $frame, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagealphablending($img, false);
            imagesavealpha($img, true);
            imagepng($img, $ruta.$imagen);
            if (($extens==".jpeg")||($extens==".jpg")) {
                if (imagejpeg($img, $ruta.$imagen, 90)) {
                    $imgOK = true;
                }
            }
            if ($extens==".png") {
                if (imagepng($img, $ruta.$imagen)) {
                    $imgOK = true;
                }
            }
            if ($extens==".gif") {
                if (imagegif($img, $ruta.$imagen)) {
                    $imgOK = true;
                }
            }
            imagedestroy($img);
        }
    }
    return $imgOK;
}

/**
 * Limpia la entrada de caracteres de riesgo para SQL
 * @param  string $string Caracteres a limpiar
 * @return string
 */
function secure_mysql($string)
{
    $arr_not_allowed = array('/*', '*/', '--', '//');
    return str_replace($arr_not_allowed, " ", $string);
}

function openCypher($string = false, $action = 'encrypt')
{
    $action = trim($action);
    $output = false;

    $myKey = 'oW%c76+jb2';
    $myIV = 'A)2!u467a^';
    $encrypt_method = 'AES-256-CBC';

    $secret_key = hash('sha256',$myKey);
    $secret_iv = substr(hash('sha256',$myIV),0,16);

    if ( $action && ($action == 'encrypt' || $action == 'decrypt') && $string )
    {
        $string = trim(strval($string));

        if ( $action == 'encrypt' )
        {
            $output = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
        };

        if ( $action == 'decrypt' )
        {
            $output = openssl_decrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
        };
    };

    return $output;
};
