<?php

/**
 * Helper para el carrusel
 */
class CarouselHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
     * Inserta una imagen de carrusel
     * @return array
     */
    public function persist()
    {
        $db = $this->db;

        $arr_fields = array(
            'title',
            'title_en',
            'description',
            'description_en',
            'url',
            'status',
            'background_color'
        );
        $arr_values = array(
            $_POST['title'],
            $_POST['title_en'],
            $_POST['description'],
            $_POST['description_en'],
            $_POST['url'],
            $_POST['status'],
            $_POST['background_color']
        );

        $carousel = $db->insertAction("carousel", $arr_fields, $arr_values);

        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($carousel, 'image');
        }

        $this->addLogs(sprintf("Agregando imagen de carrusel: %s - id: %d", $carousel[0]['title'], $carousel[0]['id']));

        return $carousel[0];
    }

    /**
     * Edita una imagen de carrusel
     * @param  integer $id ID de la imagen
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;

        $arr_fields = array(
            'title',
            'title_en',
            'description',
            'description_en',
            'url',
            'status',
            'background_color'
        );
        $arr_values = array(
            $_POST['title'],
            $_POST['title_en'],
            $_POST['description'],
            $_POST['description_en'],
            $_POST['url'],
            $_POST['status'],
            $_POST['background_color']
        );

        $carousel = $db->updateAction("carousel", $arr_fields, $arr_values, "id='".$id."'");

        if (isset($_FILES['image']['tmp_name'])) {
            $this->persistImage($carousel, 'image');
        }

        $this->addLogs(sprintf("Editando imagen de carrusel: %s - id: %d", $carousel[0]['title'], $carousel[0]['id']));

        return $carousel[0];
    }

    /**
     * Guarda las imagenes
     * @param  array  $carousel   Arreglo de la imagen
     * @param  string $parameter  Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $carousel, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_PATH_CAROUSEL;

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "slide_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($carousel[0]['image']) {
                        @unlink($path.$carousel[0]['image']);
                    }

                    $this->db->updateAction("carousel", array('image'), array($file_name), "id='".$carousel[0]['id']."'");
                }
            }
        }
    }
}


?>
