<?php

/**
* Clase que se encarga de eliminar entradas en la base de datos
*/
class RemoveHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * Elimina un código
     * @param  integer $id ID del código
     * @return void
     */
    public function removeProductCategory($id)
    {
        $s = "SELECT * FROM products_category WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("products_category", "id='".$id."'");

        if ($remove) {
            @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE.$arr_sql[0]['image']);
            @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE."min_".$arr_sql[0]['image']);

            @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE.$arr_sql[0]['banner']);
            @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE."min_".$arr_sql[0]['banner']);
        }

        $this->addLogs(sprintf("Eliminando categoría de producto: %s - ID: %d", $arr_sql[0]['name'], $id));
    }

    /**
     * Elimina un producto
     * @param  integer $id ID del producto
     * @return void
     */
    public function removeProduct($id)
    {
        $s = "SELECT * FROM products WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("products", "id='".$id."'");

        if ($remove) {
            clearDirectory(APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/");
            clearDirectory(APP_PATH_PRODUCTS_IMAGE."product_".$id."/gallery/");
            clearDirectory(APP_PATH_PRODUCTS_IMAGE."product_".$id."/");
            rmdir(APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/");
            rmdir(APP_PATH_PRODUCTS_IMAGE."product_".$id."/gallery/");
            rmdir(APP_PATH_PRODUCTS_IMAGE."product_".$id."/");
        }

        $this->addLogs(sprintf("Eliminando producto: %s - ID: %d", $arr_sql[0]['name'], $id));
    }

    /**
     * Elimina una categoría de noticia
     * @param  integer $id ID de la categoría
     * @return void
     */
    public function removeNewsCategory($id)
    {
        $s = "SELECT * FROM news_category WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("news_category", "id='".$id."'");

        $this->addLogs(sprintf("Eliminando categoría de noticia: %s - ID: %d", $arr_sql[0]['name'], $id));
    }

    /**
     * Elimina una imagen de carrusel
     * @param  integer $id ID de la imagen
     * @return void
     */
    public function removeCarousel($id)
    {
        $s = "SELECT * FROM carousel WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("carousel", "id='".$id."'");

        if ($remove) {
            @unlink(APP_PATH_CAROUSEL.$arr_sql[0]['image']);
        }

        $this->addLogs(sprintf("Eliminando imagen de carrusel: %s - ID: %d", $arr_sql[0]['title'], $id));
    }

    /**
     * Elimina una publicación
     * @return void
     */
    public function removeNews($id)
    {
        $s = "SELECT * FROM news WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("news", "id='".$id."'");

        if ($remove) {
            @unlink(APP_PATH_NEWS."banner/".$arr_sql[0]['image']);
            @unlink(APP_PATH_NEWS."cover/".$arr_sql[0]['cover_image']);
            @unlink(APP_PATH_NEWS."cover/min_".$arr_sql[0]['cover_image']);
        }

        $this->addLogs(sprintf("Eliminando publicación: %s - ID: %d", $arr_sql[0]['title'], $id));
    }

    /**
     * Elimina la configuración de un email
     * @param  integer $id ID del email
     * @return void
     */
    public function removeConfigEmail($id)
    {
        $s = "SELECT * FROM config_email WHERE id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $remove = $this->db->deleteAction("config_email", "id='".$id."'");

        $this->addLogs(sprintf("Eliminando configuración (Email): %s - ID: %d", $arr_sql[0]['email'], $id));
    }
}

?>
