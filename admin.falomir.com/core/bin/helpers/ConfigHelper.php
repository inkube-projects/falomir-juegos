<?php

/**
 * Helper para la configuración
 */
class ConfigHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
    }

    /**
     * Agrega la configuración del email
     * @return array
     */
    public function persistEmail()
    {
        $db = $this->db;

        $arr_fields = array(
            'email',
            'host',
            'password',
            'create_at',
            'update_at'
        );
        $arr_values = array(
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            $_POST['host'],
            openCypher($_POST['pass'], 'encrypt'),
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );
        $c_email = $db->insertAction("config_email", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Agregando configuración de email: %s - id: %d", $c_email[0]['email'], $c_email[0]['id']));
        return $c_email[0];
    }


    public function updateEmail($id)
    {
        $db = $this->db;

        $arr_fields = array(
            'email',
            'host',
            'update_at'
        );
        $arr_values = array(
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            $_POST['host'],
            date('Y-m-d H:i:s'),
        );

        if ($_POST['pass'] != "") {
            array_push($arr_fields, "password");
            array_push($arr_values, openCypher($_POST['pass'], 'encrypt'));
        }

        $c_email = $db->updateAction("config_email", $arr_fields, $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando configuración de email: %s - id: %d", $c_email[0]['email'], $c_email[0]['id']));
        return $c_email[0];
    }
}


?>
