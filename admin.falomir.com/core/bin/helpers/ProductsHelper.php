<?php

/**
 * Helper para los productos
 */
class ProductsHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
     * Crea un producto
     * @return array
     */
    public function persist()
    {
        $db = $this->db;
        $arr_fields = array(
            'reference',
            'name',
            'age_range',
            'measurement',
            'content',
            'content_en',
            'description_1',
            'description_2',
            'description_1_en',
            'description_2_en',
            'video',
            'novelty',
            'best_seller',
            'relevance',
            'status',
            'create_at',
            'update_at',
        );
        $arr_values = array(
            $_POST['ref'],
            $_POST['name'],
            $_POST['age'],
            $_POST['measurement'],
            secure_mysql(sanitize($_POST['content'])),
            secure_mysql(sanitize($_POST['content_en'])),
            secure_mysql(sanitize($_POST['description_1'])),
            secure_mysql(sanitize($_POST['description_2'])),
            secure_mysql(sanitize($_POST['description_1_en'])),
            secure_mysql(sanitize($_POST['description_2_en'])),
            addslashes($_POST['video']),
            $_POST['novelty'],
            $_POST['best_seller'],
            $_POST['relevance'],
            $_POST['status'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        // Se guarda el producto
        $product = $db->insertAction("products", $arr_fields, $arr_values);

        // Se guarda la categoría
        foreach ($_POST['cat'] as $val) {
            $db->insertAction("products_categories", array('product_id', 'category_id'), array($product[0]['id'], $val));
        }

        // Se guardan las subcategorías
        if (isset($_POST['subcategory'])) {
            foreach ($_POST['subcategory'] as $key => $val) {
                $db->insertAction("products_subcategories", array('product_id', 'sub_category_id'), array($product[0]['id'], $val));
            }
        }

        // Se guarda el lenguaje
        foreach ($_POST['lang'] as $val) {
            $db->insertAction("product_language", array('product_id', 'language_id'), array($product[0]['id'], $val));
        }

        // Se crea el directorio del producto
        mkdir(APP_PATH_PRODUCTS_IMAGE."product_".$product[0]['id']."/");
        mkdir(APP_PATH_PRODUCTS_IMAGE."product_".$product[0]['id']."/gallery/");
        mkdir(APP_PATH_PRODUCTS_IMAGE."product_".$product[0]['id']."/cover/");

        if (($product) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $product[0]['id']);
        }

        if (isset($_FILES['img_1']['tmp_name'])) {
            $this->persistImage($product[0], 'img_1', 1);
        }
        if (isset($_FILES['img_2']['tmp_name'])) {
            $this->persistImage($product[0], 'img_2', 2);
        }
        if (isset($_FILES['logo']['tmp_name'])) {
            $this->persistImage($product[0], 'logo', 3);
        }
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($product[0], 'banner', 4);
        }
        if (isset($_FILES['novelty_image']['tmp_name'])) {
            $this->persistImage($product[0], 'novelty_image', 5);
        }

        $arr_fields = array(
            'keywords',
            'description',
            'title',
            'section',
            'product_id'
        );
        $arr_values = array(
            secure_mysql($_POST['seo_keywords']),
            secure_mysql($_POST['seo_description']),
            secure_mysql($_POST['seo_title']),
            1,
            $product[0]['id']
        );

        $db->insertAction("seo", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Agregando producto: %s - id: %d", $product[0]['name'], $product[0]['id']));
        return $product[0];
    }

    /**
     * Edita una categoría
     * @param  integer $id ID de la categoría
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;
        $arr_fields = array(
            'reference',
            'name',
            'age_range',
            'measurement',
            'content',
            'content_en',
            'description_1',
            'description_2',
            'description_1_en',
            'description_2_en',
            'video',
            'novelty',
            'best_seller',
            'relevance',
            'status',
            'update_at'
        );
        $arr_values = array(
            $_POST['ref'],
            $_POST['name'],
            $_POST['age'],
            $_POST['measurement'],
            secure_mysql(sanitize($_POST['content'])),
            secure_mysql(sanitize($_POST['content_en'])),
            secure_mysql(sanitize($_POST['description_1'])),
            secure_mysql(sanitize($_POST['description_2'])),
            secure_mysql(sanitize($_POST['description_1_en'])),
            secure_mysql(sanitize($_POST['description_2_en'])),
            addslashes($_POST['video']),
            $_POST['novelty'],
            $_POST['best_seller'],
            $_POST['relevance'],
            $_POST['status'],
            date('Y-m-d H:i:s')
        );

        // Se edita el producto
        $product = $db->updateAction("products", $arr_fields, $arr_values, "id='".$id."'");

        // Se guarda la categoría
        $db->deleteAction("products_categories", "product_id='".$id."'");
        foreach ($_POST['cat'] as $val) {
            $db->insertAction("products_categories", array('product_id', 'category_id'), array($product[0]['id'], $val));
        }

        // Se guardan las subcategorías
        $db->deleteAction("products_subcategories", "product_id='".$id."'");
        if (isset($_POST['subcategory'])) {
            foreach ($_POST['subcategory'] as $key => $val) {
                $db->insertAction("products_subcategories", array('product_id', 'sub_category_id'), array($product[0]['id'], $val));
            }
        }

        $db->deleteAction("product_language", "product_id='".$id."'");
        foreach ($_POST['lang'] as $val) {
            $db->insertAction("product_language", array('product_id', 'language_id'), array($id, $val));
        }

        if (($product) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $product[0]['id'], $product[0]['cover_image']);
        }

        if (isset($_FILES['img_1']['tmp_name'])) {
            $this->persistImage($product[0], 'img_1', 1);
        }
        if (isset($_FILES['img_2']['tmp_name'])) {
            $this->persistImage($product[0], 'img_2', 2);
        }
        if (isset($_FILES['logo']['tmp_name'])) {
            $this->persistImage($product[0], 'logo', 3);
        }
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($product[0], 'banner', 4);
        }
        if (isset($_FILES['novelty_image']['tmp_name'])) {
            $this->persistImage($product[0], 'novelty_image', 5);
        }

        $arr_fields = array(
            'keywords',
            'description',
            'title',
            'section',
            'product_id'
        );
        $arr_values = array(
            secure_mysql($_POST['seo_keywords']),
            secure_mysql($_POST['seo_description']),
            secure_mysql($_POST['seo_title']),
            1,
            $id
        );

        $cnt_val = $db->getCount("seo", "product_id='".$id."'");

        if ($cnt_val == 0) {
            $db->insertAction("seo", $arr_fields, $arr_values);
        } else {
            $db->updateAction("seo", $arr_fields, $arr_values, "product_id='".$id."'");
        }

        $this->addLogs(sprintf("Editando producto: %s - id: %d", $product[0]['name'], $product[0]['id']));
        return $product[0];
    }

    /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la estrada
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
    public function saveImage($name, $id, $old = "")
    {
        $filename = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/".$name;

        if (fileExist($filename)) {
            rename($filename, APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/".$name);
            rename(APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/min_".$name, APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink(APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/".$old);
                @unlink(APP_PATH_PRODUCTS_IMAGE."product_".$id."/cover/min_".$old);
            }

            $update = $this->db->updateAction("products", array('cover_image'), array($name), "id = '".$id."'");

            @unlink($filename);
            @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/products/cover/min_".$name);
        }
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            $directory_temp = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";
            $directory_profile = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";
            return $this->createIMG($directory_temp, $directory_profile);
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "cover_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";
        $directory_post = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 600) {
            $altura = 600;
        } else {
            $altura = $height;
        }
        if($width > 600) {
            $ancho = 600;
        } else {
            $ancho = $width;
        }

        $form = '
        <link href="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
        <script src="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.js"></script>
        <script type="text/javascript">
        $(function(){
            $(\'#img-recortar\').Jcrop({
                setSelect: [ 0, 0, 0, 0],
                maxSize: ['.$width.', '.$height.'],
                minSize: ['.$ancho.', '.$altura.'],
                onSelect: updateCoords
            });
        });
        function updateCoords(c){
            $(\'#x\').val(c.x);
            $(\'#y\').val(c.y);
            if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
            if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
        }
        function recortarImagen(){
            var i = $("#frm-img").serialize();
            $.ajax({
                type: \'POST\',
                url: base_admin + \'ajax.php?m=products&acc=4\',
                dataType: \'json\',
                data: i,
                success: function(r){
                    $(\'#mod-img-cover\').modal(\'hide\');
                    if (r.status === "OK") {
                        $("#hid-name").val("'.$this->admin_code."_".$img_name.'");
                        $("#img-cover").attr("src", "'.BASE_URL_ADMIN.$directory_post.'"+r.data);
                    }
                }
            });
            return false;
        }
        </script>
        <img src="'.BASE_URL_ADMIN.$directory.$img_name.'" id="img-recortar" class="img-responsive">
        <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
            <input type="hidden" id="x" name="x" value="0"/>
            <input type="hidden" id="y" name="y" value="0"/>
            <input type="hidden" id="w" name="w" value="0"/>
            <input type="hidden" id="h" name="h" value="0"/>
            <div class="clearfix css-espacio10"></div>
            <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
        </form>
        ';

        return $form;
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG($directory_temp, $directory_profile)
    {
        $db = new Connection();
        // $directory_temp = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";
        // $directory_profile = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/cover/";

        $dir = $directory_temp;  $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->admin_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->admin_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->admin_code."_".$image;
    }

    /**
     * Guarda las imagenes en galeria
     * @param  array  $product   Arreglo del producto
     * @param  string $parameter Nombre del parametro del formulario
     * @param  integer $acc      Accion para el campo en la bd
     * @return void|exception
     */
    public function persistImage(array $product, $parameter, $acc)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_PATH_PRODUCTS_IMAGE."product_".$product['id']."/gallery/";

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                    list($width, $height) = getimagesize($_FILES[$parameter]['tmp_name']);

                    if (($width < 250) || ($height < 250)) {
                        throw new Exception("La imagen debe tener por lo menos 250 x 250px", 1);
                    }
                } else {
                    throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
                }
                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "gallery_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($acc == 1) {
                        $column = "image_1";
                    } elseif ($acc == 2) {
                        $column = "image_2";
                    } elseif ($acc == 3) {
                        $column = "product_logo";
                    } elseif ($acc == 4) {
                        $column = "banner";
                    } elseif ($acc == 5) {
                        $column = "novelty_image";
                    }

                    if ($product[$column]) {
                        @unlink($path.$product[$column]);
                    }

                    $this->db->updateAction("products", array($column), array($file_name), "id='".$product['id']."'");
                }
            }
        }
    }

    /**
     * Edita la relevancia
     * @param integer $product_id ID del producto
     * @return void
     */
    public function setRelevance($product_id)
    {
        $db = $this->db;
        $product_name = $db->getValue("name", "products", "id='".$product_id."'");

        $arr_fields = array(
            'relevance',
            'update_at'
        );
        $arr_values = array(
            $_POST['relevance'],
            date('Y-m-d H:i:s')
        );

        $db->updateAction("products", $arr_fields, $arr_values, "id='".$product_id."'");
        $this->addLogs(sprintf("Editando producto (relevancia): %s - id: %d", $product_name, $product_id));
    }

    /**
     * Edita las categorías de un producto
     * @param integer $product_id     ID del producto
     * @param array $arr_categories   Arreglo con las categrías
     * @return array
     */
    public function setCategories($product_id, $arr_categories)
    {
        $db = $this->db;

        // Se guarda la categoría
        $db->deleteAction("products_categories", "product_id='".$product_id."'");
        $db->deleteAction("products_subcategories", "product_id='".$product_id."'");
        foreach ($arr_categories as $val) {
            $db->insertAction("products_categories", array('product_id', 'category_id'), array($product_id, $val));
        }

        // Se imprimen las categorías como etiquestas
        $q = "SELECT * FROM products_categories WHERE product_id='".$product_id."'";
        $arr_categories = $db->fetchSQL($q);
        $c_name = "";
        foreach ($arr_categories as $cat) {
            $c_name .= '<span class="label label-info css-marginB10">'.$db->getValue('name', "products_category", "id='".$cat['category_id']."'").'</span> ';
        }

        return $c_name;
    }
}


?>
