<?php

/**
 * Helper para la categoría de los productos
 */
class ProductsCategoryHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
     * Crea una categoría
     * @return array
     */
    public function persist()
    {
        $db = $this->db;
        $arr_fields = array(
            'name',
            'name_en',
            'description',
            'description_en',
            'principal',
            'background_color',
            'father_id'
        );
        $arr_values = array(
            $_POST['name'],
            $_POST['name_en'],
            nl2br($_POST['description'], false),
            nl2br($_POST['description_en'], false),
            $_POST['principal'],
            $_POST['background_color'],
            $_POST['father']
        );

        $category = $db->insertAction("products_category", $arr_fields, $arr_values);

        if (($category) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $category[0]['id']);
        }

        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($category, 'banner');
        }

        $this->addLogs(sprintf("Agregando categoría de producto: %s - id: %d", $category[0]['name'], $category[0]['id']));

        return $category[0];
    }

    /**
     * Edita una categoría
     * @param  integer $id ID de la categoría
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;
        $arr_fields = array(
            'name',
            'name_en',
            'description',
            'description_en',
            'principal',
            'background_color',
            'father_id'
        );
        $arr_values = array(
            $_POST['name'],
            $_POST['name_en'],
            nl2br($_POST['description'], false),
            nl2br($_POST['description_en'], false),
            $_POST['principal'],
            $_POST['background_color'],
            $_POST['father']
        );

        $category = $db->updateAction("products_category", $arr_fields, $arr_values, "id='".$id."'");

        if (($category) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $category[0]['id'], $category[0]['cover_image']);
        }

        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($category, 'banner');
        }

        $this->processProducts($id);

        $this->addLogs(sprintf("Editando categoría de producto: %s - id: %d", $category[0]['name'], $category[0]['id']));
        return $category[0];
    }

    /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la estrada
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
    public function saveImage($name, $id, $old = "")
    {
        $filename = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/".$name;

        if (fileExist($filename)) {
            rename($filename, APP_PATH_PRODUCTS_CATEGORY_IMAGE.$name);
            rename(APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/min_".$name, APP_PATH_PRODUCTS_CATEGORY_IMAGE."min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE.$old);
                @unlink(APP_PATH_PRODUCTS_CATEGORY_IMAGE."min_".$old);
            }

            $update = $this->db->updateAction("products_category", array('image'), array($name), "id = '".$id."'");

            @unlink($filename);
            @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/products/category/min_".$name);
        }
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            return $this->createIMG();
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "cat_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/";
        $directory_post = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 600) {
            $altura = 600;
        } else {
            $altura = $height;
        }
        if($width > 600) {
            $ancho = 600;
        } else {
            $ancho = $width;
        }

        $form = '
        <link href="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
        <script src="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.js"></script>
        <script type="text/javascript">
        $(function(){
            $(\'#img-recortar\').Jcrop({
                setSelect: [ 0, 0, 0, 0],
                maxSize: ['.$width.', '.$height.'],
                minSize: ['.$ancho.', '.$altura.'],
                onSelect: updateCoords
            });
        });
        function updateCoords(c){
            $(\'#x\').val(c.x);
            $(\'#y\').val(c.y);
            if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
            if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
        }
        function recortarImagen(){
            var i = $("#frm-img").serialize();
            $.ajax({
                type: \'POST\',
                url: base_admin + \'ajax.php?m=products-category&acc=4\',
                dataType: \'json\',
                data: i,
                success: function(r){
                    $(\'#mod-img-cover\').modal(\'hide\');
                    if (r.status === "OK") {
                        $("#hid-name").val("'.$this->admin_code."_".$img_name.'");
                        $("#img-cover").attr("src", "'.BASE_URL_ADMIN.$directory_post.'"+r.data);
                    }
                }
            });
            return false;
        }
        </script>
        <img src="'.BASE_URL_ADMIN.$directory.$img_name.'" id="img-recortar" class="img-responsive">
        <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
            <input type="hidden" id="x" name="x" value="0"/>
            <input type="hidden" id="y" name="y" value="0"/>
            <input type="hidden" id="w" name="w" value="0"/>
            <input type="hidden" id="h" name="h" value="0"/>
            <div class="clearfix css-espacio10"></div>
            <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
        </form>
        ';

        return $form;
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG()
    {
        $db = new Connection();
        $directory_temp = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/";
        $directory_profile = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/products/category/";

        $dir = $directory_temp;  $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->admin_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->admin_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->admin_code."_".$image;
    }

    /**
     * Guarda las imagenes
     * @param  array  $category   Arreglo de la imagen
     * @param  string $parameter  Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $category, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_PATH_PRODUCTS_CATEGORY_IMAGE;

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "banner_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($category[0]['banner']) {
                        @unlink($path.$category[0]['banner']);
                    }

                    $this->db->updateAction("products_category", array('banner'), array($file_name), "id='".$category[0]['id']."'");
                }
            }
        }
    }

    /**
     * Obtiene las sub categorías
     * @param  array   $arr_category Arreglo de las categorías
     * @return string
     */
    public function showSubCategory($arr_category, $product_id = NULL)
    {
        $db = $this->db;
        $arr_subcategory = array();
        $arr_s_subcat = array();

        if (is_array($arr_category)) {
            $s = "SELECT * FROM products_category WHERE father_id IN (".implode(", ", $arr_category).")";
            $arr_subcategory = $db->fetchSQL($s);
        }

        if ($product_id) {
            $s = "SELECT * FROM products_subcategories WHERE product_id='".$product_id."'";
            $arr_p_subcategories = $db->fetchSQL($s);
            $arr_s_subcat = array();

            foreach ($arr_p_subcategories as $val) {
                $arr_s_subcat[] = $val['sub_category_id'];
            }
        }

        ob_start();
        include('html/ajx_show/select-subcategory.php');

        return ob_get_clean();
    }

    public function processProducts($cat_id)
    {
        $db = $this->db;
        if (isset($_POST['prod'])) {
            $s = "SELECT * FROM products WHERE category_id='".$cat_id."'";
            $arr_products = $db->fetchSQL($s);
            $arr_current = array();
            $arr_check = $_POST['prod'];

            // Se scaan los proeuctos de las categorias
            foreach ($arr_products as $key => $val) {
                $foo = array_search($val['id'], $arr_check, false);
                if ($foo == "" || $foo == null) {
                    $db->updateAction("products", array('category_id'), array(''), "id='".$val['id']."'");
                }
            }

            // Se agrega a la categoría los productos
            foreach ($arr_check as $key => $val) {
                $db->updateAction("products", array('category_id'), array($cat_id), "id='".$val."'");
            }
        }
    }
}


?>
