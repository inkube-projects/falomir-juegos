<?php

/**
 * Helper para las cateogrias de las noticias
 */
class NewsCategoryHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
     * Agrega una categoría
     * @return array
     */
    public function persist()
    {
        $db = $this->db;
        $category = $db->insertAction("news_category", array('name', 'name_en'), array($_POST['name'], $_POST['name_en']));

        $this->addLogs(sprintf("Agregando categoría de noticia: %s - id: %d", $category[0]['name'], $category[0]['id']));

        return $category[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID de la entrada
     * @return void
     */
    public function update($id)
    {
        $db = $this->db;
        $category = $db->updateAction("news_category", array('name', 'name_en'), array($_POST['name'], $_POST['name_en']), "id='".$id."'");

        $this->addLogs(sprintf("Editando categoría de noticia: %s - id: %d", $category[0]['name'], $category[0]['id']));

        return $category[0];
    }
}


?>
