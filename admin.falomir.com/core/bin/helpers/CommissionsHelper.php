<?php

/**
 * Helper para las comisiones
 */
class CommissionsHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
    }

    /**
     * Sincroniza las ventas de prestashop cn las de la base de datos de afiliados
     * @return boolean
     */
    public function synchronize()
    {
        $db = $this->db;
        $arr_result = $this->getComissions();

        foreach ($arr_result as $val) {
            $cnt_val = $db->getCount("commissions", "order_code='".$val['order']."'");

            if ($cnt_val == 0) {
                $user_id = $db->getValue("user_id", "view_code_user_full", "code='".$val['affiliate_code']."'");

                if ($user_id) {
                    $arr_fields = array(
                        'user_id',
                        'user_code',
                        'order_code',
                        'order_date',
                        'percentage_commission',
                        'price',
                        'commision',
                        'client',
                        'status_id',
                        'create_at',
                        'update_at'
                    );
                    $arr_values = array(
                        $user_id,
                        $val['affiliate_code'],
                        $val['order'],
                        to_date($val['date']),
                        $val['p_commission'],
                        $val['price'],
                        $val['commission'],
                        $val['client'],
                        1,
                        date('Y-m-d H:i:s'),
                        date('Y-m-d H:i:s'),
                    );

                    $db->insertAction("commissions", $arr_fields, $arr_values);
                }
            }
        }

        $arr_fields = array(
            'description',
            'create_at'
        );
        $arr_values = array(
            'Sincronización de ventas ('.date('Y-m-d H:i:s').')',
            date('Y-m-d H:i:s')
        );
        $db->insertAction("synchronization_logs", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Sincronización de datos"));

        return true;
    }

    /**
     * Retorna las ventas con códigos afiliados
     * @return array
     */
    public function getComissions()
    {
        $dbPresta = new Connection(2);

        $s = "SELECT * FROM view_presta_code_info WHERE NOT code IS NULL";
        $arr_sql = $dbPresta->fetchSQL($s);
        $arr_commissions = array();

        foreach ($arr_sql as $val) {
            $create_at = datetime_format($val['order_date']);
            $code_id = $this->db->getValue("id", "code", "code='".$val['code']."'");
            $s_aff = "SELECT * FROM view_code_user_full WHERE code_id='".$code_id."'";
            $arr_code_info = $this->db->fetchSQL($s_aff);

            $price = @number_format($val['original_product_price'],2,".","");
            $commission = @number_format(($price * ($arr_code_info[0]['commission'] / 100)),2,".","");

            $arr_commissions[] = array(
                'code_user' => $arr_code_info[0]['user_code'],
                'affiliate' => $arr_code_info[0]['user_name']." ".$arr_code_info[0]['last_name'],
                'affiliate_code' => $val['code'],
                'order' => $val['order_code'],
                'date' => $create_at['date'],
                'p_commission' => $arr_code_info[0]['commission'],
                'price' => $price,
                'commission' => $commission,
                'client' => $val['Client']
            );
        }

        return $arr_commissions;
    }

    /**
     * Edita una comision
     * @param  integer $id ID de la comision
     * @return array
     */
    public function update($id)
    {
        $db = $this->db;
        $arr_fields = array(
            'status_id',
            'update_at'
        );
        $arr_values = array(
            $_POST['status'],
            date('Y-m-d H:i:s')
        );
        $commission = $db->updateAction("commissions", $arr_fields, $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Cambiando estado de la comision: %d", $commission[0]['id']));

        $s = "SELECT * FROM view_user WHERE id='".$commission[0]['user_id']."'";
        $arr_user = $db->fetchSQL($s);
        $message = "La comisión de ID ".$commission[0]['id']." ha sido pagada";

        if ($_POST['status'] == 2) {
            // $this->sendEmail($arr_user[0]['email'], $arr_user[0]['name']." ".$arr_user[0]['last_name'], "Cambio de estado en comisión", $message);
        }

        return $commission[0];
    }

    /**
     * Realiza el envío de email
     * @param  string $targetEmail Email destino
     * @param  string $subject     Asunto del email
     * @param  string $message     Mensaje
     * @return void
     */
    public function sendEmail($targetEmail, $name, $subject, $message)
    {
        $mail = new PHPMailer(true);
        //Server settings
        // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.healthresurgence.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@healthresurgence.com';                 // SMTP username
        $mail->Password = 'stVb18%7';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->CharSet = 'UTF-8';

        //Recipients
        $mail->setFrom('noreply@healthresurgence.com', 'Health Resurgence');
        $mail->addAddress($targetEmail, $name);     // Add a recipient
        // $mail->addAddress($targetEmail, $name);     // Email del administrador

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = '<b>'.$message.'</b>';
        $mail->AltBody = $message;

        if (!$mail->send()) {
            throw new \Exception("El proceso se ha completado pero no se ha enviado el e-mail", 1);
        }

        return true;
    }
}


?>
