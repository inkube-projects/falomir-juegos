<?php

/**
 * Helper de contacto
 */
class ContactHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
    }

    public function update($id)
    {
        $db = $this->db;

        $arr_fields = array(
            'name',
            'last_name',
            'email',
            'message',
            'note',
            'update_at'
        );
        $arr_values = array(
            secure_mysql($_POST['name']),
            secure_mysql($_POST['last_name']),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            secure_mysql($_POST['message']),
            secure_mysql($_POST['note']),
            date('Y-m-d H:i:s'),
        );

        $contact = $db->updateAction("contact_messages", $arr_fields, $arr_values, "id='".$id."'");

        $this->addLogs(sprintf("Editando mensaje de contacto email: %s - id: %d", $contact[0]['email'], $contact[0]['id']));
        return $contact[0];
    }
}


?>
