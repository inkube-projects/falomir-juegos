<?php

/**
 * Helper para el SEP
 */
class SeoHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
     * Se edita el SEO
     * @return void
     */
    public function persist()
    {
        $db = $this->db;

        $arr_fields = array(
            'keywords',
            'description'
        );
        $arr_values = array(
            $_POST['keywords'],
            $_POST['description']
        );

        $db->deleteAction("seo");
        $db->insertAction("seo", $arr_fields, $arr_values);
    }
}


?>
