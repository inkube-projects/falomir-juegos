<?php

/**
* Clase que se encarga de gestionar las marcas
*/
class UserHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    /**
    * Guarda una promoción nueva
    * @return array
    */
    public function persist()
    {
        $arr_fields = array(
            "username",
            "email",
            "password",
            "role",
            "status_id",
            "username_canonical",
            "email_canonical",
            "online",
            "create_at",
            "update_at"
        );
        $arr_values = array(
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            hashPass($_POST['pass_1'], 12),
            $_POST['role'],
            $_POST['status'],
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            "2",
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $user = $this->db->insertAction("user", $arr_fields, $arr_values);

        // Se genera el codigo de usuario
        $code = genPass($user[0]['id'], 5);
        $user = $this->db->updateAction("user", array('code'), array($code), "id='".$user[0]['id']."'");

        $arr_fields = array(
            "name",
            "last_name",
            "user_id",
            "phone_1",
            "phone_2",
            "note"
        );
        $arr_values = array(
            mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['last_name'], MB_CASE_UPPER, "UTF-8"),
            $user[0]['id'],
            $_POST['phone_1'],
            $_POST['phone_2'],
            sanitize($_POST['note'])
        );

        $user_information = $this->db->insertAction("user_information", $arr_fields, $arr_values);

        mkdir(APP_PATH_USERS."user_".$user[0]['code']."/");
        mkdir(APP_PATH_USERS."user_".$user[0]['code']."/profile/");

        // se crean las carpetas de administrados/ moderador si lo amerita
        $role_name = $this->db->getValue("role", "role", "id='".$_POST['role']."'");

        if ($role_name == 'ROLE_ADMIN' || $role_name == 'ROLE_MODERATOR') {
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/category/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/cover/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/gallery/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/products/banner/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/news/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/news/cover/");
        }

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $user[0]['code']);
        }

        $this->addLogs(sprintf("Agregando un nuevo usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }

    /**
    * Edita la promoción
    * @param  integer $id ID de la promoción
    * @return void
    */
    public function edit($id)
    {
        $arr_fields = array(
            "username",
            "email",
            "role",
            "status_id",
            "username_canonical",
            "email_canonical",
            "online",
            "create_at",
            "update_at",
        );
        $arr_values = array(
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            $_POST['role'],
            $_POST['status'],
            mb_convert_case($_POST['username'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['email'], MB_CASE_LOWER, "UTF-8"),
            "2",
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );

        $user = $this->db->updateAction("user", $arr_fields, $arr_values, "id='".$id."'");

        $arr_fields = array(
            "name",
            "last_name",
            "user_id",
            "phone_1",
            "phone_2",
            "note"
        );
        $arr_values = array(
            mb_convert_case($_POST['name'], MB_CASE_UPPER, "UTF-8"),
            mb_convert_case($_POST['last_name'], MB_CASE_UPPER, "UTF-8"),
            $user[0]['id'],
            $_POST['phone_1'],
            $_POST['phone_2'],
            sanitize($_POST['note'])
        );

        $role_name = $this->db->getValue("role", "role", "id='".$_POST['role']."'");

        if ($role_name == 'ROLE_ADMIN' || $role_name == 'ROLE_MODERATOR') {
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/");
            @mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/profile/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/products/category/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/products/cover/");
            mkdir(APP_IMG_ADMIN."user_".$user[0]['code']."/temp_files/users/products/gallery/");
        }

        $user_information = $this->db->updateAction("user_information", $arr_fields, $arr_values, "user_id='".$id."'");

        if (trim($_POST['pass_1']) != "") {
            $user = $this->db->updateAction("user", array('password'), array(hashPass($_POST['pass_1'], 12)), "id='".$id."'");
        }

        if (($user) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $user[0]['code'], $user[0]['profile_image']);
        }

        $this->addLogs(sprintf("Editando usuario: %s - id: %d", $user[0]['username'], $user[0]['id']));

        return $user[0];
    }

    /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la promoción
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
    public function saveImage($name, $id, $old = "")
    {
        $filename = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/".$name;

        if (fileExist($filename)) {
            rename($filename, APP_PATH_USERS."user_".$id."/profile/".$name);
            rename(APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/min_".$name, APP_PATH_USERS."user_".$id."/profile/min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink(APP_PATH_USERS."user_".$id."/profile/".$old);
                @unlink(APP_PATH_USERS."user_".$id."/profile/"."min_".$old);
            }

            $update = $this->db->updateAction("user", array('profile_image'), array($name), "code = '".$id."'");

            @unlink($filename);
            @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/users/profile/min_".$name);
        }
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            return $this->createIMG();
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "profile_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/";
        $directory_post = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 600) {
            $altura = 600;
        } else {
            $altura = $height;
        }
        if($width > 600) {
            $ancho = 600;
        } else {
            $ancho = $width;
        }

        $form = '
        <link href="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
        <script src="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.js"></script>
        <script type="text/javascript">
        $(function(){
            $(\'#img-recortar\').Jcrop({
                setSelect: [ 0, 0, 0, 0],
                maxSize: ['.$width.', '.$height.'],
                minSize: ['.$ancho.', '.$altura.'],
                onSelect: updateCoords
            });
        });
        function updateCoords(c){
            $(\'#x\').val(c.x);
            $(\'#y\').val(c.y);
            if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
            if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
        }
        function recortarImagen(){
            var i = $("#frm-img").serialize();
            $.ajax({
                type: \'POST\',
                url: base_admin + \'ajax.php?m=user&acc=4\',
                dataType: \'json\',
                data: i,
                success: function(r){
                    $(\'#mod-img-cover\').modal(\'hide\');
                    if (r.status === "OK") {
                        $("#hid-name").val("'.$this->admin_code."_".$img_name.'");
                        $("#img-cover").attr("src", "'.BASE_URL_ADMIN.$directory_post.'"+r.data);
                    }
                }
            });
            return false;
        }
        </script>
        <img src="'.BASE_URL_ADMIN.$directory.$img_name.'" id="img-recortar" class="img-responsive">
        <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
            <input type="hidden" id="x" name="x" value="0"/>
            <input type="hidden" id="y" name="y" value="0"/>
            <input type="hidden" id="w" name="w" value="0"/>
            <input type="hidden" id="h" name="h" value="0"/>
            <div class="clearfix css-espacio10"></div>
            <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
        </form>
        ';

        return $form;
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG()
    {
        $db = new Connection();
        $directory_temp = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/";
        $directory_profile = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/users/profile/";

        $dir = $directory_temp;  $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->admin_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->admin_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->admin_code."_".$image;
    }

}

?>
