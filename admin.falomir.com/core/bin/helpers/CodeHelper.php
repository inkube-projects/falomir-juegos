<?php

/**
 * Helper para los códigos
 */
class CodeHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
    }

    /**
     * Agrega un código
     * @return array
     */
    public function persist()
    {
        $arr_fields = array(
            'status',
            'code',
            'name',
            'start_date',
            'end_date',
            'notes',
            'commission',
            'create_at',
            'update_at'
        );
        $arr_values = array(
            $_POST['status'],
            $_POST['id_code'],
            $_POST['name'],
            to_date($_POST['start_date']),
            to_date($_POST['end_date']),
            $_POST['note'],
            $_POST['commission'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s')
        );

        $code = $this->db->insertAction("code", $arr_fields, $arr_values);
        $this->addLogs(sprintf("Agregando código: %s - id: %d", $code[0]['name'], $code[0]['id']));

        return $code[0];
    }

    /**
     * Edita una entrada
     * @param  integer $id ID del codigo
     * @return array
     */
    public function edit($id)
    {
        $arr_fields = array(
            'status',
            'code',
            'name',
            'start_date',
            'end_date',
            'notes',
            'commission',
            'update_at'
        );
        $arr_values = array(
            $_POST['status'],
            $_POST['id_code'],
            $_POST['name'],
            to_date($_POST['start_date']),
            to_date($_POST['end_date']),
            $_POST['note'],
            $_POST['commission'],
            date('Y-m-d H:i:s')
        );

        $code = $this->db->updateAction("code", $arr_fields, $arr_values, "id='".$id."'");
        $this->addLogs(sprintf("Editando código: %s - id: %d", $code[0]['name'], $code[0]['id']));

        return $code[0];
    }

    /**
     * Agrega y muestra el listado de usuarios
     * @param integer $user_id ID del usuario
     * @param integer $code_id ID del código
     */
    public function addUser($code_id, $user_id)
    {
        $this->db->deleteAction("code_users", "code_id='".$code_id."'");
        $this->db->insertAction("code_users", array('code_id, user_id'), array($code_id, $user_id));
        $code = $this->db->fetchSQL("SELECT * FROM code WHERE id='".$code_id."'");
        $this->addLogs(sprintf("Agregando usuario al código: %s - id: %d", $code[0]['name'], $code[0]['id']));
        return $this->getUsers($code_id);
    }

    /**
     * Elimina un usuario del listado del código
     * @param  integer $code_id ID del Código
     * @param  integer $user_id ID del usuario
     * @return string
     */
    public function removeUser($code_id, $user_id)
    {
        $this->db->deleteAction("code_users", "user_id='".$user_id."' AND code_id='".$code_id."'");
        $code = $this->db->fetchSQL("SELECT * FROM code WHERE id='".$code_id."'");
        $this->addLogs(sprintf("Eliminando usuario del código: %s - id: %d", $code[0]['name'], $code[0]['id']));
        return $this->getUsers($code_id);
    }

    /**
     * Retorna html del listado de usuarios
     * @param  integer $id ID de los códigos
     * @return string
     */
    public function getUsers($id)
    {
        $s = "SELECT * FROM code_users WHERE code_id='".$id."'";
        $arr_sql = $this->db->fetchSQL($s);

        $html = '
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Avatar</th>
                    <th>ID usuario</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>';
                foreach ($arr_sql as $val) {
                    $s_user = "SELECT * FROM view_code_user WHERE code_id='".$id."'";
                    $arr_user = $this->db->fetchSQL($s_user);

                    $image = APP_NO_IMAGES."profile.jpg";
                    if ($arr_user[0]['profile_image']) {
                        $image = APP_USERS."user_".$arr_user[0]['user_id']."/profile/min_".$arr_user[0]['profile_image'];
                    }

                    $html .= '
                    <tr>
                        <td>
                            <div class="dashboard-comments__avatar">
                                <img src="'.$image.'" alt="">
                            </div>
                        </td>
                        <td><a href="admin-editar-usuario/'.$arr_user[0]['user_id'].'" target="_blank">'.$arr_user[0]['code'].'</a></td>
                        <td>'.$arr_user[0]['name'].'</td>
                        <td>'.$arr_user[0]['last_name'].'</td>
                        <td>'.$this->db->getValue("description", "user_status", "id='".$arr_user[0]['status_id']."'").'</td>
                        <td><a href="Javascript: removeUser('.$arr_user[0]['user_id'].')" class="btn btn-danger btn-flat">Eliminar</a></td>
                    </tr>
                    ';
                }
            $html .= '</tbody>
        </table>
        ';

        // $html .= $this->getDataTables();

        return $html;
    }

    /**
     * Retorna la libreria dataTables
     * @return string
     */
    public function getDataTables()
    {
        $script = '
            <script src="'.APP_ASSETS.'plugins/datatables/jquery.dataTables.js"></script>
            <script src="'.APP_ASSETS.'plugins/datatables/dataTables.bootstrap.min.js"></script>
            <script type="text/javascript">
                $(function(e){
                    // Configuración de la paginación con DataTable
                    $(".table").DataTable({
                        "language": {
                            "lengthMenu": "Mostrar _MENU_ entradas por página",
                            "zeroRecords": "No se han encontrado resultados",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "No se han encontrado resultados",
                            "infoFiltered": "(Se ha filtrado un total de _MAX_ entradas)",
                            "search": "Buscar: ",
                            "paginate": {
                                "previous": "Anterior",
                                "next": "Siguiente"
                            }
                        }
                    });
                });
            </script>
        ';

        return $script;
    }
}


?>
