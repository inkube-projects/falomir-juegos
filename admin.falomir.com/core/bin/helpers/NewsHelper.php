<?php

/**
 * Helper para las noticias
 */
class NewsHelper extends GeneralMethods
{
    public $db;

    function __construct($db)
    {
        parent::__construct($db);
        $this->db = $db;
        $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_code = $_SESSION['ADMIN_SESSION_FALOMIR']['code'];
    }

    public function persist()
    {
        $db = $this->db;

        $arr_fields = array(
            'title',
            'title_en',
            'description',
            'description_en',
            'category_id',
            'status',
            'relevance',
            'create_at',
            'update_at',
        );
        $arr_values = array(
            $_POST['title'],
            $_POST['title_en'],
            addslashes($_POST['description']),
            addslashes($_POST['description_en']),
            $_POST['category'],
            $_POST['status'],
            $_POST['relevance'],
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'),
        );
        $news = $db->insertAction("news", $arr_fields, $arr_values);

        if (($news) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $news[0]['id']);
        }
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($news, 'banner');
        }

        $arr_fields = array(
            'keywords',
            'description',
            'title',
            'section',
            'post_id'
        );
        $arr_values = array(
            secure_mysql($_POST['seo_keywords']),
            secure_mysql($_POST['seo_description']),
            secure_mysql($_POST['seo_title']),
            2,
            $news[0]['id']
        );

        $db->insertAction("seo", $arr_fields, $arr_values);

        $this->addLogs(sprintf("Agregando noticia: %s - id: %d", $news[0]['title'], $news[0]['id']));
        return $news[0];
    }

    public function update($id)
    {
        $db = $this->db;

        $arr_fields = array(
            'title',
            'title_en',
            'description',
            'description_en',
            'category_id',
            'status',
            'relevance',
            'update_at',
        );
        $arr_values = array(
            $_POST['title'],
            $_POST['title_en'],
            addslashes($_POST['description']),
            addslashes($_POST['description_en']),
            $_POST['category'],
            $_POST['status'],
            $_POST['relevance'],
            date('Y-m-d H:i:s'),
        );

        $news = $db->updateAction("news", $arr_fields, $arr_values, "id='".$id."'");

        if (($news) && ($_POST['hid-name'] != "")) {
            $this->saveImage($_POST['hid-name'], $news[0]['id'], $news[0]['cover_image']);
        }
        if (isset($_FILES['banner']['tmp_name'])) {
            $this->persistImage($news, 'banner');
        }

        $arr_fields = array(
            'keywords',
            'description',
            'title',
            'section',
            'post_id'
        );
        $arr_values = array(
            secure_mysql($_POST['seo_keywords']),
            secure_mysql($_POST['seo_description']),
            secure_mysql($_POST['seo_title']),
            2,
            $id
        );

        $cnt_val = $db->getCount("seo", "post_id='".$id."'");

        if ($cnt_val == 0) {
            $db->insertAction("seo", $arr_fields, $arr_values);
        } else {
            $db->updateAction("seo", $arr_fields, $arr_values, "post_id='".$id."'");
        }

        $this->addLogs(sprintf("Editando noticia: %s - id: %d", $news[0]['title'], $news[0]['id']));
        return $news[0];
    }

    /**
    * Se encarga de guardar las imagenes
    * @param  string $name Nombre de la imagen
    * @param  integer $id   ID de la estrada
    * @param  string $old  Nombre de la imagen vieja
    * @return void
    */
    public function saveImage($name, $id, $old = "")
    {
        $filename = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/".$name;

        if (fileExist($filename)) {
            rename($filename, APP_PATH_NEWS."cover/".$name);
            rename(APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/min_".$name, APP_PATH_NEWS."cover/min_".$name);

            if ((!is_null($old)) && ($old != "")) {
                @unlink(APP_PATH_NEWS."cover/".$old);
                @unlink(APP_PATH_NEWS."cover/min_".$old);
            }

            $update = $this->db->updateAction("news", array('cover_image'), array($name), "id = '".$id."'");

            @unlink($filename);
            @unlink(APP_IMG_ADMIN."user_".$this->admin_id."/temp_files/news/cover/min_".$name);
        }
    }

    /**
    * Prepara imagen
    * @param  integer $acc Acción a ejecutar [1: Guarda la imagen base, 2: Recorta la imagen]
    * @return object
    */
    public function prepareCoverImage($acc)
    {
        if ($acc == 1) {
            $tempIMG = $this->prepareCoverTemp();
            $form = $this->showFormCoverIMG($tempIMG);
            return $form;
        } elseif ($acc == 2) {
            $directory_temp = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/";
            $directory_profile = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/";
            return $this->createIMG($directory_temp, $directory_profile);
        }
    }

    /**
    * Guarda la imagen base
    * @return string
    */
    public function prepareCoverTemp()
    {
        // se elimina las imagenes temporales que esten en la carpeta
        $dir = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/";
        $handle = opendir($dir);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                @unlink($dir.$file);
            }
        }

        // se sube la nueva imagen temporal
        $extens = str_replace("image/",".",$_FILES['cover_image']['type']);
        $nb_img = "cover_".rand(00000, 99999).uniqid().$extens;
        list($width, $height) = getimagesize($_FILES["cover_image"]['tmp_name']);

        if($width > 1000){
            ImagenTemp('cover_image', 1000, $nb_img, $dir);
        }else{
            ImagenTemp('cover_image', $width, $nb_img, $dir);
        }

        return $nb_img;
    }

    /**
    * Retorna el fomulario de la imagen de perfil a recortar
    * @param  string $img_name Nombre asignado a la imagen
    * @return string
    */
    public function showFormCoverIMG($img_name)
    {
        $directory = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/";
        $directory_post = APP_IMG_ADMIN."user_".$this->admin_code."/temp_files/news/cover/";

        list($width, $height) = getimagesize($directory.$img_name);

        if($height > 600) {
            $altura = 600;
        } else {
            $altura = $height;
        }
        if($width > 600) {
            $ancho = 600;
        } else {
            $ancho = $width;
        }

        $form = '
        <link href="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.css" type="text/css" rel="stylesheet" />
        <script src="'.APP_ASSETS.'plugins/jcrop/jquery.Jcrop.js"></script>
        <script type="text/javascript">
        $(function(){
            $(\'#img-recortar\').Jcrop({
                setSelect: [ 0, 0, 0, 0],
                maxSize: ['.$width.', '.$height.'],
                minSize: ['.$ancho.', '.$altura.'],
                onSelect: updateCoords
            });
        });
        function updateCoords(c){
            $(\'#x\').val(c.x);
            $(\'#y\').val(c.y);
            if($(\'#w\').val()==0){ $(\'#w\').val('.$ancho.');}else{ $(\'#w\').val(c.w);}
            if($(\'#h\').val()==0){ $(\'#h\').val('.$altura.');}else{ $(\'#h\').val(c.h);}
        }
        function recortarImagen(){
            var i = $("#frm-img").serialize();
            $.ajax({
                type: \'POST\',
                url: base_admin + \'ajax.php?m=news&acc=4\',
                dataType: \'json\',
                data: i,
                success: function(r){
                    $(\'#mod-img-cover\').modal(\'hide\');
                    if (r.status === "OK") {
                        $("#hid-name").val("'.$this->admin_code."_".$img_name.'");
                        $("#img-cover").attr("src", "'.BASE_URL_ADMIN.$directory_post.'"+r.data);
                    }
                }
            });
            return false;
        }
        </script>
        <img src="'.BASE_URL_ADMIN.$directory.$img_name.'" id="img-recortar" class="img-responsive">
        <form method="post" name="frm-img" id="frm-img" onSubmit="return recortarImagen();">
            <input type="hidden" id="x" name="x" value="0"/>
            <input type="hidden" id="y" name="y" value="0"/>
            <input type="hidden" id="w" name="w" value="0"/>
            <input type="hidden" id="h" name="h" value="0"/>
            <div class="clearfix css-espacio10"></div>
            <button type="submit" class="btn btn-default pull-right">Cortar Imagen <i class="fa fa-scissors"></i></button>
        </form>
        ';

        return $form;
    }

    /**
    * Crea la imagen de Portada
    * @return string
    */
    public function createIMG($directory_temp, $directory_profile)
    {
        $db = new Connection();

        $dir = $directory_temp;  $handle = opendir($dir);
        $file = readdir($handle);
        while ($file = readdir($handle)){
            if (is_file($dir.$file)) {
                $image = $file;
            }
        }
        $ext = explode(".", $image)[1];

        $x = sanitize($_POST['x']);
        $y = sanitize($_POST['y']);
        $w = sanitize($_POST['w']);
        $h = sanitize($_POST['h']);
        $targ_w = $w;
        $targ_h = $h;
        $jpeg_quality = 90;
        $src = $directory_temp.$image;
        $src_min = $directory_profile."min_".$image;

        if(($ext == "jpeg")||($ext == "jpg")){
            $img_r = imagecreatefromjpeg($src);
        }if($ext == "png"){
            $img_r = imagecreatefrompng($src);
        } if($ext == "gif"){
            $img_r = imagecreatefromgif($src);
        }

        $src = $directory_temp.$this->admin_code."_".$image;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);

        if (($ext == "jpeg") || ($ext== "jpg ")){
            imagejpeg($dst_r,$src,90);
        } elseif ($ext == "png") {
            imagepng($dst_r, $src);
        } elseif ($ext=="gif") {
            imagegif($dst_r,$src);
        }

        // se crea la miniatura
        $src_min = $directory_profile."min_".$this->admin_code."_".$image;
        list($width, $height) = getimagesize($src);
        $new_width = 200;
        $new_height = floor($height * ($new_width / $width));
        $targ_w = $new_width;
        $targ_h = $new_height;
        $dst_r = imagecreatetruecolor($targ_w, $targ_h);
        imagecopyresampled($dst_r,$img_r,0,0,$x,$y, $targ_w,$targ_h,$w,$h);

        if(($ext=="jpeg")||($ext=="jpg")){
            imagejpeg($dst_r,$src_min,90);
        } if($ext=="png"){
            imagepng($dst_r,$src_min);
        } if($ext=="gif"){
            imagegif($dst_r,$src_min);
        }

        imagedestroy($dst_r);

        // rename($src, $directory_profile.$image);
        return $this->admin_code."_".$image;
    }

    /**
     * Guarda las imagenes en galeria
     * @param  array  $news   Arreglo del producto
     * @param  string $parameter Nombre del parametro del formulario
     * @return void|exception
     */
    public function persistImage(array $news, $parameter)
    {
        if ($_FILES[$parameter]['tmp_name'] != "") {
            $path = APP_PATH_NEWS."banner/";

            if (!empty($_FILES[$parameter]['tmp_name'])) {
                $img_type = $_FILES[$parameter]['type'];

                if ((strpos($img_type, "gif"))||(strpos($img_type, "jpeg"))||(strpos($img_type,"jpg"))||(strpos($img_type,"png"))){
                    list($width, $height) = getimagesize($_FILES[$parameter]['tmp_name']);

                    if (($width < 250) || ($height < 250)) {
                        throw new Exception("La imagen debe tener por lo menos 250 x 250px", 1);
                    }
                } else {
                    throw new Exception("Debes ingresar una imagen válida (jpg, jpeg, png, gif)", 1);
                }
                $extens = str_replace("image/",".",$_FILES[$parameter]['type']);
                $file_name = "banner_".rand(00000, 99999).uniqid().$extens;

                if(move_uploaded_file($_FILES[$parameter]['tmp_name'], $path.$file_name)){
                    if ($news[0]['image']) {
                        @unlink($path.$news[0]['image']);
                    }

                    $this->db->updateAction("news", array('image'), array($file_name), "id='".$news[0]['id']."'");
                }
            }
        }
    }

    /**
     * Edita la relevancia
     * @param integer $post_id ID del producto
     * @return void
     */
    public function setRelevance($post_id)
    {
        $db = $this->db;

        $arr_fields = array(
            'relevance',
            'update_at'
        );
        $arr_values = array(
            $_POST['relevance'],
            date('Y-m-d H:i:s')
        );

        $db->updateAction("news", $arr_fields, $arr_values, "id='".$post_id."'");
        $this->addLogs(sprintf("Editando post (relevancia) id: %d", $post_id));
    }
}


?>
