<?php
include('core/handler/session-handler.php');

/**
* Controlador para el index y el Home
*/
class IndexController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
    }

    /**
    * Pantalla de Inicio de sesión
    * @return void
    */
    public function indexAction()
    {
        require_once('html/index/login.php');
    }

    /**
    * Pantalla principal del gestor
    * @return [type] [description]
    */
    public function homeAction()
    {
        $db = new Connection();

        if (isset($_SESSION['ADMIN_SESSION_FALOMIR']) && $_SESSION['ADMIN_SESSION_FALOMIR'] != "") {
            $cnt_products = $db->getCount("products", "");
            $cnt_users = $db->getCount("user", "");
            $cnt_messages = $db->getCount("contact_messages", "status='0'");
            $cnt_news = $db->getCount("news", "");

            require_once('html/index/home.php');
        } else {
            header('location: '.BASE_URL.'admin-login');
        }
    }

    /**
    * Cerrar sesión
    * @return void
    */
    public function logOutAction()
    {
        $_SESSION['ADMIN_SESSION_FALOMIR'] = "";
        session_unset(); //para eliminar las variables de sesion
        session_destroy(); //con esto destruyes la sesion
        header('location: '.BASE_URL.'admin-login');
    }
}

?>
