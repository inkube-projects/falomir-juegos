<?php

include('core/handler/session-handler.php');

/**
 * Controlador del carrusel
 */
class CarouselController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }


    /**
     * Lista las imagenes de carrusel
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;

        $s = "SELECT * FROM carousel";
        $arr_carousel = $db->fetchSQL($s);

        foreach ($arr_carousel as $key => $val) {
            $image = APP_NO_IMAGES."no_image.jpg";

            if ($val['image']) { $image = APP_CAROUSEL.$val['image']; }

            $arr_carousel[$key]['status_name'] = ($val['status'] == 0) ? '<span class="text-danger">Inhabilitado</span>' : '<span class="text-success">Habilitado</span>';
            $arr_carousel[$key]['slider_image'] = $image;
        }

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 La categoría ha sido <b>Eliminada</b> correctamente
              </div>';
           }
        }

        require_once('html/carousel/carousel-list.php');
    }

    /**
     * Acción para agregar imagen de carrusel
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $flash_message = ""; $acc = 1;
        $title = "";
        $title_en = "";
        $description = "";
        $description_en = "";
        $image = APP_NO_IMAGES."no_image.jpg";
        $url = "";
        $status = 0;
        $background_color = "";

        require_once('html/carousel/carousel-form.php');
    }

    /**
     * Accion para editar la imagen
     * @return [type] [description]
     */
    public function editAction()
    {
        $db = new Connection;
        $frm = "frm-edit"; $acc = 2;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("carousel", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-carrusel"); exit;
        }

        $s = "SELECT * FROM carousel WHERE id='".$id."'";
        $arr_carousel = $db->fetchSQL($s);

        $title = $arr_carousel[0]['title'];
        $title_en = $arr_carousel[0]['title_en'];
        $description = $arr_carousel[0]['description'];
        $description_en = $arr_carousel[0]['description_en'];
        $image = APP_NO_IMAGES."no_image.jpg";
        if ($arr_carousel[0]['image']) { $image = APP_CAROUSEL.$arr_carousel[0]['image']; }
        $url = $arr_carousel[0]['url'];
        $status = $arr_carousel[0]['status'];
        $background_color = $arr_carousel[0]['background_color'];

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La imagen ha sido <b>creada</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La imagen ha sido <b>editada</b> correctamente
                </div>';
            }
        }

        require_once('html/carousel/carousel-form.php');
    }
}

?>
