<?php

include('core/handler/session-handler.php');

/**
 * Controlador para las noticias
 */
class NewsController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    /**
     * Listado de categorias de productos
     * @return void
     */
    public function categoryListAction()
    {
        $db = new Connection;
        $s = "SELECT * FROM news_category";
        $arr_category = $db->fetchSQL($s);

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 La categoría ha sido <b>Eliminada</b> correctamente
              </div>';
           }
        }

        require_once('html/news/category-list.php');
    }

    /**
     * Formulario para agregar categoría de noticias
     * @return void
     */
    public function categoryAddAction()
    {
        $db = new Connection;
        $name = ""; $flash_message = ""; $frm = "frm-add"; $acc = 1; $id = "";

        require_once('html/news/category-form.php');
    }

    /**
     * Formulario para editar categoría de noticias
     * @return void
     */
    public function categoryEditAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("news_category", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-categoria-noticias"); exit;
        }

        $s = "SELECT * FROM news_category WHERE id='".$id."'";
        $arr_category = $db->fetchSQL($s);
        $name = $arr_category[0]['name'];
        $name_en = $arr_category[0]['name_en'];

        $frm = "frm-edit"; $acc = 2; $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>creada</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>editada</b> correctamente
                </div>';
            }
        }

        require_once('html/news/category-form.php');
    }

    /**
     * Listado de noticias
     * @return void
     */
    public function listAction()
    {
        $db = new Connection;
        $src_category = (isset($_POST['src_category']) && (!empty($_POST['src_category']))) ? number_format($_POST['src_category'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? sanitize($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? sanitize($_POST['src_end_date']) : "" ;

        $s = "SELECT * FROM news_category";
        $arr_category = $db->fetchSQL($s);

        $s = "SELECT * FROM view_news WHERE NOT id=''";

        if (($src_category != "") && ($src_category != 0)) {
            $s .= " AND category_id = '".$src_category."'";
        }

        if ($src_start_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }

        if ($src_end_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }

        $arr_sql = $db->fetchSQL($s);
        $arr_news = array();

        foreach ($arr_sql as $val) {
            $create = datetime_format($val['create_at']);
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['cover_image']) {
                $image = APP_NEWS."cover/min_".$val['cover_image'];
            }

            $arr_news[] = array(
                'id' => $val['id'],
                'title' => $val['title'],
                'image' => $image,
                'category' => $val['category_name'],
                'category_id' => $val['category_id'],
                'relevance' => $val['relevance'],
                'create' => $create['date']
            );
        }

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 La noticia ha sido <b>Eliminada</b> correctamente
              </div>';
           }
        }

        require_once('html/news/news-list.php');
    }

    /**
     * Formulario para agregar noticias
     * @return void
     */
    public function addAction()
    {
        $db = new Connection;
        $id = ""; $frm = "frm-add"; $acc = 1; $title = ""; $title_en = ""; $description = ""; $description_en = ""; $category = ""; $cover_image = APP_NO_IMAGES."no_image.jpg";
        $image = APP_NO_IMAGES."no_image.jpg"; $status = 1; $relevance = $db->getCount("news", "") + 1; $seo_title = ""; $seo_keywords = ""; $seo_description = "";

        $s = "SELECT * FROM news_category";
        $arr_category = $db->fetchSQL($s);

        $flash_message = "";
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/news/cover/");
        require_once('html/news/news-form.php');
    }

    /**
     * Formulario para editar noticias
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $frm = "frm-edit"; $acc = 2;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("news", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-noticias"); exit;
        }

        $s = "SELECT * FROM news WHERE id='".$id."'";
        $arr_news = $db->fetchSQL($s);

        $title = $arr_news[0]['title'];
        $title_en = $arr_news[0]['title_en'];
        $description = $arr_news[0]['description'];
        $description_en = $arr_news[0]['description_en'];
        $category = $arr_news[0]['category_id'];
        $status = $arr_news[0]['status'];
        $relevance = $arr_news[0]['relevance'];
        $cover_image = APP_NO_IMAGES."no_image.jpg";
        $image = APP_NO_IMAGES."no_image.jpg";

        if ($arr_news[0]['cover_image']) {
            $cover_image = APP_NEWS."cover/".$arr_news[0]['cover_image'];
        }

        if ($arr_news[0]['image']) {
            $image = APP_NEWS."banner/".$arr_news[0]['image'];
        }

        $s = "SELECT * FROM news_category";
        $arr_category = $db->fetchSQL($s);

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La noticia ha sido <b>creada</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La noticia ha sido <b>editada</b> correctamente
                </div>';
            }
        }

        $s = "SELECT * FROM seo WHERE post_id='".$id."'";
        $arr_seo = $db->fetchSQL($s);

        $seo_title = $arr_seo[0]['title'];
        $seo_keywords = $arr_seo[0]['keywords'];
        $seo_description = $arr_seo[0]['description'];

        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/news/cover/");
        require_once('html/news/news-form.php');
    }
}


?>
