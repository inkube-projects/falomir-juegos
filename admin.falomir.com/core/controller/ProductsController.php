<?php

include('core/handler/session-handler.php');

/**
 * Controlador para los productos
 */
class ProductsController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    /**
     * Lista las categorías de los productos
     * @return void
     */
    public function categoryListAction()
    {
        $db = new Connection;
        $role_name = $this->user_role_name;

        $s = "SELECT * FROM products_category";
        $arr_sql = $db->fetchSQL($s);
        $arr_category = array();

        foreach ($arr_sql as $val) {
            $image = APP_NO_IMAGES."no_image.jpg";
            if ($val['image']) {
                $image = APP_PRODUCTS_CATEGORY_IMAGE."min_".$val['image'];
            }

            $arr_category[] = array(
                'id' => $val['id'],
                'name' => $val['name'],
                'image' => $image,
                'type' => ($val['principal'] == 0) ? "Sub-categoría" : "Categoría"
            );
        }

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 La categoría ha sido <b>Eliminada</b> correctamente
              </div>';
           }
        }

        require_once('html/products/category-list.php');
    }

    /**
     * Acción para agregar una categoria
     * @return void
     */
    public function categoryAddAction()
    {
        $db = new Connection;
        $image = APP_NO_IMAGES."no_image.jpg";
        $banner = APP_NO_IMAGES."no_image.jpg";
        $name = ""; $name_en = ""; $flash_message = ""; $frm = "frm-add"; $acc = 1; $id = ""; $description = ""; $description_en = ""; $principal = ""; $background_color = ""; $father = "";

        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_p_categories = $db->fetchSQL($s);

        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/category/");
        require_once('html/products/category-form.php');
    }

    /**
     * Acción para editar una categoría
     * @return void
     */
    public function categoryEditAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("products_category","id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-categoria-productos"); exit;
        }

        // Padres
        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_p_categories = $db->fetchSQL($s);

        // Info de la categoria
        $s = "SELECT * FROM products_category WHERE id='".$id."'";
        $arr_category = $db->fetchSQL($s);

        $image = APP_NO_IMAGES."no_image.jpg";
        if ($arr_category[0]['image']) {
            $image = APP_PRODUCTS_CATEGORY_IMAGE.$arr_category[0]['image'];
        }

        $banner = APP_NO_IMAGES."no_image.jpg";
        if ($arr_category[0]['banner']) {
            $banner = APP_PRODUCTS_CATEGORY_IMAGE.$arr_category[0]['banner'];
        }

        $name = $arr_category[0]['name'];
        $name_en = $arr_category[0]['name_en'];
        $description = sanitize($arr_category[0]['description']);
        $description_en = sanitize($arr_category[0]['description_en']);
        $principal = $arr_category[0]['principal'];
        $background_color = $arr_category[0]['background_color'];
        $father = $arr_category[0]['father_id'];
        $frm = "frm-edit";
        $acc = 2;
        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>creada</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>editada</b> correctamente
                </div>';
            }
        }

        // listado de productos
        $s = "SELECT * FROM view_products";
        $arr_products = $db->fetchSQL($s);

        foreach ($arr_products as $key => $val) {
            $arr_products[$key]['formed_category'] = ($val['category_name']) ? $val['category_name'] : "Sin categoría";
        }

        // Productos de la categoria
        $s = "SELECT * FROM products WHERE category_id='".$id."'";
        $arr_p_cheked = $db->fetchSQL($s);
        $arr_check = array();

        foreach ($arr_p_cheked as $key => $val) {
            $arr_check[] = $val['id'];
        }

        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/category/");
        require_once('html/products/category-form.php');
    }

    /**
     * Lista los productos
     * @return void
     */
    public function listAction()
    {
        $db = new Connection();
        $role_name = $this->user_role_name;
        $src_category = (isset($_POST['src_category']) && (!empty($_POST['src_category']))) ? number_format($_POST['src_category'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? sanitize($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? sanitize($_POST['src_end_date']) : "" ;
        $src_novelty = (isset($_POST['src_novelty']) && (!empty($_POST['src_novelty']))) ? number_format($_POST['src_novelty'],0,'','') : "" ;
        $src_relevance = (isset($_POST['src_relevance']) && (!empty($_POST['src_relevance']))) ? number_format($_POST['src_relevance'],0,'','') : "" ;
        $o = "";

        if ($src_relevance == 1) {
            $o = " ORDER BY relevance ASC";
        }
        if ($src_relevance == 2) {
            $o = " ORDER BY relevance DESC";
        }

        // Productos
        $s = "SELECT * FROM view_products WHERE NOT id=''".$o;

        if (($src_category != "") && ($src_category != 0)) {
            // $s .= " AND category_id = '".$src_category."'";
            $s .= " AND id IN (SELECT product_id FROM products_categories WHERE category_id='".$src_category."')";
        }
        if ($src_start_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }
        if ($src_end_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }
        if (($src_novelty != "") && ($src_novelty != 0)) {
            $s .= " AND novelty = '".$src_novelty."'";
        }

        $arr_sql = $db->fetchSQL($s);
        $arr_porducts = array();

        // Categoría de productos
        $s = "SELECT * FROM products_category";
        $arr_category = $db->fetchSQL($s);

        foreach ($arr_sql as $val) {
            $create = datetime_format($val['create_at']);

            $q = "SELECT * FROM products_categories WHERE product_id='".$val['id']."'";
            $arr_categories = $db->fetchSQL($q);
            $c_name = "";
            foreach ($arr_categories as $cat) {
                $c_name .= '<span class="label label-info css-marginB10">'.$db->getValue('name', "products_category", "id='".$cat['category_id']."'").'</span> ';
            }

            $arr_porducts[] = array(
                'id' => $val['id'],
                'ref' => $val['reference'],
                'product_name' => $val['product_name'],
                'age' => $val['age_range'],
                'relevance' => $val['relevance'],
                'category_name' => $c_name,
                'create' => $create['date']
            );
        }

        // Categoría de productos
        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_categories = $db->fetchSQL($s);

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 El producto ha sido <b>Eliminado</b> correctamente
              </div>';
           }
        }
        require_once('html/products/product-list.php');
    }

    /**
     * Accion para agregar un producto
     */
    public function addAction()
    {
        $db = new Connection;
        $flash_message = ""; $frm = "frm-add"; $acc = 1; $id = "";

        // Categoría de productos
        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_category = $db->fetchSQL($s);

        // Idiomas
        $s = "SELECT * FROM language ORDER BY name ASC";
        $arr_lang = $db->fetchSQL($s);

        $ref = ""; $name = ""; $age = ""; $measurement = ""; $language = array(); $arr_product_lang = array(); $content = ""; $description_1 = "";
        $description_2 = ""; $video = ""; $category_id = ""; $novelty = ""; $best_seller = ""; $arr_subcategory = array(); $subcategory_id = ""; $description_1_en = ""; $description_2_en = "";
        $content_en = ""; $status = 1; $seo_title = ""; $seo_keywords = ""; $seo_description = ""; $arr_s_cat = array(); $arr_s_subcat = array();
        $relevance = $db->getCount("products", "") + 1;
        $cover_image = APP_NO_IMAGES."no_image.jpg";
        $image_1 = APP_NO_IMAGES."no_image.jpg";
        $image_2 = APP_NO_IMAGES."no_image.jpg";
        $banner = APP_NO_IMAGES."no_image.jpg";
        $logo = APP_NO_IMAGES."no_image_logo.jpg";
        $novelty_image = APP_NO_IMAGES."no_image.jpg";

        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/");
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/");
        require_once('html/products/product-form.php');
    }

    /**
     * Editar producto
     * @return void
     */
    public function editAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $frm = "frm-edit"; $acc = 2;
        $cnt_val = $db->getCount("products","id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-productos"); exit;
        }

        // Categoría de productos
        $s = "SELECT * FROM products_category WHERE principal='1'";
        $arr_category = $db->fetchSQL($s);

        // Idiomas
        $s = "SELECT * FROM language ORDER BY name ASC";
        $arr_lang = $db->fetchSQL($s);

        $s = "SELECT * FROM products WHERE id='".$id."'";
        $arr_product = $db->fetchSQL($s);

        $s = "SELECT * FROM product_language WHERE product_id='".$id."'";
        $arr_sql = $db->fetchSQL($s);
        $arr_product_lang = array();

        foreach ($arr_sql as $val) {
            $arr_product_lang[] = $val['language_id'];
        }

        // Categrias del producto
        $s = "SELECT * FROM products_categories WHERE product_id='".$id."'";
        $arr_p_categories = $db->fetchSQL($s);
        $arr_s_cat = array();

        foreach ($arr_p_categories as $val) {
            $arr_s_cat[] = $val['category_id'];
        }

        $s = "SELECT * FROM products_subcategories WHERE product_id='".$id."'";
        $arr_p_subcategories = $db->fetchSQL($s);
        $arr_s_subcat = array();

        foreach ($arr_p_subcategories as $val) {
            $arr_s_subcat[] = $val['sub_category_id'];
        }

        // sub-Categoría de productos
        if (count($arr_s_cat) > 0) {
            $s = "SELECT * FROM products_category WHERE father_id IN (".implode(", ", $arr_s_cat).")";
            $arr_subcategory = $db->fetchSQL($s);
        } else {
            $arr_subcategory = array();
        }
        

        $ref = $arr_product[0]['reference'];
        $name = $arr_product[0]['name'];
        $age = $arr_product[0]['age_range'];
        $measurement = $arr_product[0]['measurement'];
        $content = $arr_product[0]['content'];
        $content_en = $arr_product[0]['content_en'];
        $description_1 = $arr_product[0]['description_1'];
        $description_2 = $arr_product[0]['description_2'];
        $description_1_en = $arr_product[0]['description_1_en'];
        $description_2_en = $arr_product[0]['description_2_en'];
        $video = $arr_product[0]['video'];
        $category_id = $arr_product[0]['category_id'];
        $subcategory_id = $arr_product[0]['sub_category_id'];
        $novelty = $arr_product[0]['novelty'];
        $relevance = $arr_product[0]['relevance'];
        $status = $arr_product[0]['status'];
        $best_seller = $arr_product[0]['best_seller'];
        $cover_image = APP_NO_IMAGES."no_image.jpg";
        $image_1 = APP_NO_IMAGES."no_image.jpg";
        $image_2 = APP_NO_IMAGES."no_image.jpg";
        $banner = APP_NO_IMAGES."no_image.jpg";
        $logo = APP_NO_IMAGES."no_image_logo.jpg";
        $novelty_image = APP_NO_IMAGES."no_image.jpg";

        if ($arr_product[0]['cover_image']) {
            $cover_image = APP_PRODUCTS_IMAGE."product_".$id."/cover/".$arr_product[0]['cover_image'];
        }
        if ($arr_product[0]['image_1']) {
            $image_1 = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$arr_product[0]['image_1'];
        }
        if ($arr_product[0]['image_2']) {
            $image_2 = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$arr_product[0]['image_2'];
        }
        if ($arr_product[0]['banner']) {
            $banner = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$arr_product[0]['banner'];
        }
        if ($arr_product[0]['product_logo']) {
            $logo = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$arr_product[0]['product_logo'];
        }
        if ($arr_product[0]['novelty_image']) {
            $novelty_image = APP_PRODUCTS_IMAGE."product_".$id."/gallery/".$arr_product[0]['novelty_image'];
        }

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                El producto ha sido <b>creado</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                El producto ha sido <b>editado</b> correctamente
                </div>';
            }
        }

        $s = "SELECT * FROM seo WHERE product_id='".$id."'";
        $arr_seo = $db->fetchSQL($s);

        $seo_title = @$arr_seo[0]['title'];
        $seo_keywords = @$arr_seo[0]['keywords'];
        $seo_description = @$arr_seo[0]['description'];

        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/gallery/");
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/products/cover/");
        require_once('html/products/product-form.php');
    }

    /**
     * retorna en formato los arreglos
     * @param  array  $arr Arreglo
     * @return array
     */
    public function formatedArray($arr, $m = 4)
    {
        $arr_result = array();
        $position = 0;
        for ($i = 0; $i < count($arr); $i++) {
            if ((($i + 0) % $m) == 0) {
                $position++;
        	}

            $arr_result[($position - 1)][] = $arr[$i];
        }

        return $arr_result;
    }
}


?>
