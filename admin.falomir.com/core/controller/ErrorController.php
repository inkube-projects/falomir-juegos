<?php

/**
 * Controlador para los errores
 */
class ErrorController
{
   public function notFoundAction()
   {
      require_once('html/error/404.php');
   }

   public function serverErrorAction()
   {
      require_once('html/error/500.php');
   }
}
?>
