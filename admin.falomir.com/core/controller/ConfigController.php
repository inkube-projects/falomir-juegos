<?php

include('core/handler/session-handler.php');

/**
 * Controlador para las configuraciones
 */
class ConfigController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    /**
     * Pantalla de configuraciones
     * @return void
     */
    public function configAction()
    {
        $db = new Connection;

        // Emails
        $s = "SELECT * FROM config_email";
        $arr_emails = $db->fetchSQL($s);

        foreach ($arr_emails as $key => $val) {
            $create = datetime_format($val['create_at']);
            $update = datetime_format($val['update_at']);

            $arr_emails[$key]['formed_create'] = $create['date'];
            $arr_emails[$key]['formed_update'] = $update['date'];
        }

        $flash_message = "";
        require_once('html/config/config.php');
    }

    /**
     * Agregar email
     * @return void
     */
    public function addEmailAction()
    {
        $db = new Connection;
        $frm = "frm-add"; $acc = 1; $id = "";

        $email = "";
        $password = "";
        $host = "";

        $flash_message = "";
        require_once('html/config/email-form.php');
    }

    /**
     * Editar email
     * @return void
     */
    public function editEmailAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $frm = "frm-edit"; $acc = 2;
        $cnt_val = $db->getCount("config_email", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-configuraciones"); exit;
        }

        $s = "SELECT * FROM config_email WHERE id='".$id."'";
        $arr_email = $db->fetchSQL($s);

        $email = $arr_email[0]['email'];
        $password = openCypher($arr_email[0]['password'], 'decrypt');
        $host = $arr_email[0]['host'];

        $frm = "frm-edit"; $acc = 2; $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>creada</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                La categoría ha sido <b>editada</b> correctamente
                </div>';
            }
        }
        require_once('html/config/email-form.php');
    }
}


?>
