<?php

/**
 * Controlador de los mensjaes de contacto
 */
class ContactController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    /**
     * Lista todos los mensajes
     * @return void
     */
    public function listACtion()
    {
        $db = new Connection;
        $src_status = (isset($_POST['src_status']) && (!empty($_POST['src_status']))) ? number_format($_POST['src_status'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? sanitize($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? sanitize($_POST['src_end_date']) : "" ;

        $s = "SELECT * FROM contact_messages WHERE NOT id = ''";

        if (($src_status != "") && ($src_status != 0)) {
            $s .= " AND status = '".$src_status."'";
        }
        if ($src_start_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }
        if ($src_end_date != "") {
           $s .= " AND DATE_FORMAT(create_at, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }

        $arr_sql = $db->fetchSQL($s);
        $arr_messages = array();

        foreach ($arr_sql as $val) {
            $status = ($val['status'] == 0) ? "No leído" : "Leído";
            $class = ($val['status'] == 0) ? "css-bold text-black" : "";
            $create = datetime_format($val['create_at']);

            $arr_messages[] = array(
                'id' => $val['id'],
                'email' => $val['email'],
                'names' => $val['name']." ".$val['last_name'],
                'status' => $val['id'],
                'status' => $status,
                'class' => $class,
                'create' => $create['date']
            );
        }

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 El mensaje ha sido <b>Eliminado</b> correctamente
              </div>';
           }
        }

        require_once('html/contact/messages-list.php');
    }

    public function showAction()
    {
        $db = new Connection;
        $id = @number_format($_GET['id'],0,"","");
        $frm = "frm-edit"; $acc = 2;
        $cnt_val = $db->getCount("contact_messages", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-contacto"); exit;
        }

        $s = "SELECT * FROM contact_messages WHERE id='".$id."'";
        $arr_message = $db->fetchSQL($s);

        $db->updateAction("contact_messages", array('status'), array(1), "id='".$id."'");

        require_once('html/contact/message-show.php');
    }
}


?>
