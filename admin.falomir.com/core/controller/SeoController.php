<?php

include('core/handler/session-handler.php');


/**
 * Controlador para el SEO
 */
class SeoController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    public function editAction()
    {
        $db = new Connection;
        $frm = "frm-seo"; $acc = 1;

        $s = "SELECT * FROM seo LIMIT 0, 1";
        $arr_seo = $db->fetchSQL($s);

        $keywords = @$arr_seo[0]['keywords'];
        $description = @$arr_seo[0]['description'];

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Se ha agregado el SEO correctamente.
                </div>';
            }
        }

        require_once('html/seo/seo-form.php');
    }
}


?>
