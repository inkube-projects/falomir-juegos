<?php

include('core/handler/session-handler.php');

/**
 * Controlador para los usuarios
 */
class UsersController
{
    function __construct()
    {
        $this->user_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->user_code = @$_SESSION['ADMIN_SESSION_FALOMIR']['code'];
        $this->admin_pp_id = @$_SESSION['ADMIN_SESSION_FALOMIR']['id'];
        $this->admin_username = @$_SESSION['ADMIN_SESSION_FALOMIR']['username'];
        $this->admin_role = @$_SESSION['ADMIN_SESSION_FALOMIR']['role'];
        $this->user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index";
        $this->user_method = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "index";
        $this->user_role_name = @$_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'];
    }

    /**
     * Lista los usuarios
     * @return void
     */
    public function listAction() {
        $db = new Connection();
        $role_name = $db->getValue("role", "role", "id='".$this->admin_role."'");
        $src_status = (isset($_POST['src_status']) && (!empty($_POST['src_status']))) ? number_format($_POST['src_status'],0,'','') : "" ;
        $src_role = (isset($_POST['src_role']) && (!empty($_POST['src_role']))) ? number_format($_POST['src_role'],0,'','') : "" ;
        $src_category = (isset($_POST['src_category']) && (!empty($_POST['src_category']))) ? number_format($_POST['src_category'],0,'','') : "" ;
        $src_start_date = (isset($_POST['src_start_date']) && (!empty($_POST['src_start_date']))) ? sanitize($_POST['src_start_date']) : "" ;
        $src_end_date = (isset($_POST['src_end_date']) && (!empty($_POST['src_end_date']))) ? sanitize($_POST['src_end_date']) : "" ;

        if ($role_name == "ROLE_ADMIN") {
            $s = "SELECT * FROM view_user WHERE NOT id=''";
        } else {
            $s = "SELECT * FROM view_user WHERE NOT role = '1'";
        }

        if (($src_status != "") && ($src_status != 0)) {
            $s .= " AND status_id = '".$src_status."'";
        }

        if (($src_role != "") && ($src_role != 0)) {
            $s .= " AND role = '".$src_role."'";
        }

        if ($src_start_date != "") {
           $s .= " AND DATE_FORMAT(discharge_date, '%Y-%m-%d') >= '".to_date($src_start_date)."'";
        }

        if ($src_end_date != "") {
           $s .= " AND DATE_FORMAT(discharge_date, '%Y-%m-%d') <= '".to_date($src_end_date)."'";
        }

        $arr_sql = $db->fetchSQL($s);
        $arr_users = array();

        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        if ($role_name == "ROLE_ADMIN") {
            $s = "SELECT * FROM role";
        } else {
            $s = "SELECT * FROM role WHERE NOT id IN (1)";
        }
        $arr_role = $db->fetchSQL($s);


        foreach ($arr_sql as $val) {
            $create = datetime_format($val['create_at']);
            $image = APP_NO_IMAGES."profile.jpg";
            if ($val['profile_image']) {
                $image = APP_USERS."user_".$val['code']."/profile/min_".$val['profile_image'];
            }

            $arr_users[] = array(
                'id' => $val['id'],
                'profile_image' => $image,
                'client_code' => $val['code'],
                'name' => $val['name'],
                'last_name' => $val['last_name'],
                'role' => $db->getValue("role_name", "role", "id='".$val['role']."'"),
                'status' => $db->getValue("description", "user_status", "id='".$val['status_id']."'"),
                'create' => $create['date']
            );
        }

        // Se carga el mensaje flash
        $flash_message = "";
        if (isset($_GET['m'])) {
           if ($_GET['m'] == "OK1") {
              $flash_message = '<div class="alert alert-warning alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 El usuario ha sido <b>Eliminada</b> correctamente
              </div>';
           }
        }

        require_once('html/users/user-list.php');
    }

    /**
     * Agrega un nuevo usuario
     * @return void
     */
    public function addAction()
    {
        $db = new Connection();
        $role_name = $db->getValue("role", "role", "id='".$this->admin_role."'");
        $image = APP_NO_IMAGES."profile.jpg";
        $frm = "frm-add"; $id = ""; $acc = 1;
        $client_code = ""; $name = ""; $last_name = ""; $phone_1 = ""; $email = ""; $phone_2 = ""; $username = ""; $note = "";
        $role = ""; $status = ""; $flash_message = "";

        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        if ($role_name == "ROLE_ADMIN") {
            $s = "SELECT * FROM role";
        } else {
            $s = "SELECT * FROM role WHERE NOT id IN (1)";
        }
        $arr_role = $db->fetchSQL($s);

        // Limpiar el directorio temporal
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/");
        require_once('html/users/user-form.php');
    }

    /**
     * Editar usuario
     * @return void
     */
    public function editAction()
    {
        $db = new Connection(1);
        $role_name = $db->getValue("role", "role", "id='".$this->admin_role."'");
        $id = @number_format($_GET['id'],0,"","");
        $cnt_val = $db->getCount("view_user", "id='".$id."'");

        if ($cnt_val == 0) {
            header("location: ".BASE_URL."admin-usuarios"); exit;
        }

        $s = "SELECT * FROM view_user WHERE id='".$id."'";
        $arr_user = $db->fetchSQL($s);

        $image = APP_NO_IMAGES."profile.jpg";
        if ($arr_user[0]['profile_image']) {
            $image = APP_USERS."user_".$arr_user[0]['code']."/profile/".$arr_user[0]['profile_image'];
        }

        $frm = "frm-edit";
        $acc = 2;
        $client_code = $arr_user[0]['code'];
        $create_at = datetime_format($arr_user[0]['create_at']);

        $name = $arr_user[0]['name'];
        $last_name = $arr_user[0]['last_name'];
        $phone_1 = $arr_user[0]['phone_1'];
        $phone_2 = $arr_user[0]['phone_2'];
        $email = $arr_user[0]['email'];
        $username = $arr_user[0]['username'];
        $note = $arr_user[0]['note'];
        $role = $arr_user[0]['role'];
        $status = $arr_user[0]['status_id'];

        $s = "SELECT * FROM user_status";
        $arr_status = $db->fetchSQL($s);

        if ($role_name == "ROLE_ADMIN") {
            $s = "SELECT * FROM role";
        } else {
            $s = "SELECT * FROM role WHERE NOT id IN (1)";
        }
        $arr_role = $db->fetchSQL($s);

        $flash_message = "";
        if (isset($_GET['m'])) {
            if ($_GET['m'] == "OK1") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                El usuario ha sido <b>creado</b> correctamente
                </div>';
            } elseif ($_GET['m'] == "OK2") {
                $flash_message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                El usuario ha sido <b>editado</b> correctamente
                </div>';
            }
        }

        // Limpiar el directorio temporal
        clearDirectory(APP_IMG_ADMIN."user_".$this->user_code."/temp_files/users/profile/");
        require_once('html/users/user-form.php');
    }
}


?>
