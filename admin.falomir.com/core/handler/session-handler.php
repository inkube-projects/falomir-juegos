<?php
/**
 * Clase de conexion
 * @var Connection
 */
$db = new Connection();
$user_view = (isset($_GET['view']) && (!empty($_GET['view']))) ? $_GET['view'] : "index" ;
$user_meth = (isset($_GET['meth']) && (!empty($_GET['meth']))) ? $_GET['meth'] : "home" ;
$redirect = (isset($_GET['r']) && (!empty($_GET['r']))) ? $_GET['r'] : "" ;
$role = "ROLE_ANONYMOUS";

$arr_pages = array(
   'Index' => array('ROLE_ANONYMOUS', 'ROLE_ADMIN', 'ROLE_MODERATOR'),
   'Users' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'Products' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'News' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'Carousel' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'Seo' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
   'Config' => array('ROLE_ADMIN', 'ROLE_MODERATOR'),
);

/**
 * Se verifica si existe una sesión, sino se le asigna un Rol anónimo
*/
if (isset($_SESSION['ADMIN_SESSION_FALOMIR'])) {
   if ($_SESSION['ADMIN_SESSION_FALOMIR'] != "") {
      $role = $db->getValue("role", "role", "id='".$_SESSION['ADMIN_SESSION_FALOMIR']['role']."'");
      $_SESSION['ADMIN_SESSION_FALOMIR']['ROLE_NAME'] = $role;
      $admin_pp_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
      $admin_username = $_SESSION['ADMIN_SESSION_FALOMIR']['username'];
   }
}

foreach ($arr_pages as $key => $val) {
   if ($key == $user_view) {
      if (!in_array($role, $val)) {
         header("location: ".BASE_URL."admin-login-r"); exit;
      }
   }
}

if (($role != "ROLE_ANONYMOUS") && $user_view == "Index" && $user_meth == "index") {
   header("location: ".BASE_URL."admin-dashboard"); exit;
}
?>
