<?php

/**
 * Metodos generales
 */
class GeneralMethods
{
   public $db;

   function __construct($db)
   {
      $this->db = $db;
      $this->admin_id = $_SESSION['ADMIN_SESSION_FALOMIR']['id'];
      $this->admin_username = $_SESSION['ADMIN_SESSION_FALOMIR']['username'];
   }

   /**
    * Crea los logs que relacionados con las acciones a los módulos de los usuarios
    * @param string $desc Descripcion del log
    */
   protected function addLogs($desc)
   {
      $ip_acces = @$_SERVER['REMOTE_ADDR'];
      $ip_proxy = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $ip_compa = @$_SERVER['HTTP_CLIENT_IP'];

      $arr_fields = array(
         'user_id',
         'username',
         'action',
         'ip_access',
         'ip_proxy',
         'ip_company',
         'create_at',
      );

      $arr_values = array(
         $this->admin_id,
         $this->admin_username,
         $desc,
         $ip_acces,
         $ip_proxy,
         $ip_compa,
         date('Y-m-d H:i:s'),
      );

      $result = $this->db->insertAction("user_logs", $arr_fields, $arr_values);

      return true;
   }

   /**
    * Obtiene el objeto del usuario
    * @return array
    */
   protected function getUser($id)
   {
      $s = "SELECT * FROM user WHERE id='".$id."'";
      $arr_user = $this->db->fetchSQL($s);

      return $arr_user[0];
   }
}


?>
