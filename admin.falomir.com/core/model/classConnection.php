<?php
/**
*  Clase para crear la conexión con la BD MySQL
*/
class Connection extends PDO
{

    function __construct()
    {
        try {
            parent::__construct(DB_DSN, DB_USER, DB_PASS);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
     * Encuentra el valor máximo en una tabla
     * @param  string $campo_max Campo de la tabla que se quiere obtener
     * @param  string $q_from    Nombre de la tabla a buscar
     * @param  string $q_where   Condición
     * @return string
     */
    public function getMaxValue($campo_max, $q_from, $q_where)
    {
        $maximo = 0;
        $q = "SELECT MAX(".$campo_max.") AS ".$campo_max." FROM ".$q_from;
        if ($q_where!="") {
            $q.= " WHERE ".$q_where;
        }
        $sql_obj = $this->query($q);
        if ($rs = $sql_obj->fetch(PDO::FETCH_ASSOC)) {
            $maximo = $rs[$campo_max];
        } $sql_obj->closeCursor();
        return $maximo;
    }

    /**
     * Obtiene la sumatoria del campo ingreado
     * @param  string $parameter Campo con valor a sumar
     * @param  string $q_from    Nombre de la tabla a buscar
     * @param  string $q_where   Condición
     * @return integer
     */
    public function getSum($parameter, $q_from, $q_where)
    {
        $sum = 0;
        $q = "SELECT SUM(".$parameter.") AS ".$parameter." FROM ".$q_from;
        if ($q_where!="") {
            $q.= " WHERE ".$q_where;
        }

        $sql_obj = $this->query($q);
        if ($rs = $sql_obj->fetch(PDO::FETCH_ASSOC)) {
            $sum = $rs[$parameter];
        } $sql_obj->closeCursor();

        return $sum;
    }

    /**
     * Cuenta los resultados de una busqueda determinada en una tabla
     * @param  string $q_from  Nombre de la tabla
     * @param  string $q_where Condición
     * @return integer
     */
    public function getCount($q_from, $q_where)
    {
        $count = 0;
        $q = "SELECT COUNT(*) AS cuantos FROM ".$q_from;
        if ($q_where!="") {
            $q.= " WHERE ".$q_where;
        }

        $sql_obj = $this->query($q);
        if($rs = $sql_obj->fetch(PDO::FETCH_ASSOC)) {
            $count = $rs["cuantos"];
        }$sql_obj->closeCursor();
        return $count;
    }

    /**
     * Obtiene un solo valor de una tabla
     * @param  String $req   Campo que se requiere
     * @param  String $from  Nombre de la tabla
     * @param  String $where Condición de la busqueda
     * @return String | Integer | Boolean
     */
    public function getValue($req, $from, $where)
    {
        $valor = "";
        $q = "SELECT ".$req." AS campo FROM ".$from;
        if ($where!="") { $q.= " WHERE ".$where; }
        $q.= " LIMIT 0,1";
        $sql_obj = $this->query($q);
        if ($rs = $sql_obj->fetch(PDO::FETCH_ASSOC)) {
            $valor = $rs['campo'];
        } $sql_obj->closeCursor();
        return $valor;
    }

    /**
     * Metodo para ejecutar un query y retornar como arreglo el resultado de la sentencia
     * @param  String $query Query a ejecutar
     * @return Array         resultado del query
     */
    public function fetchSQL($query)
    {
        $arr_response = array();
        $sql = $this->query($query);

        while ($rs = $sql->fetch(PDO::FETCH_ASSOC)) {
            $arr_response[] = $rs;
        }

        $sql->closeCursor();
        return $arr_response;
    }

    /**
     * Metodo que se encarga de realizar los insert
     * @param  String $table     Tabla donde se va a insertar
     * @param  Array $arr_fields Nombre de los campos de la tabla que se van a llenar
     * @param  Array $arr_values Valores de los campos
     * @return Array             Arreglo con toda la informacion recien insertada
     */
    function insertAction($table, $arr_fields, $arr_values)
    {
        $cnt = 0;
        $i = "INSERT INTO ".$table." (";
        foreach ($arr_fields as $field) { $cnt++; if ($cnt == 1) { $i.= $field; } else { $i.= ", ".$field; } }
        $i.= ") values (";
        $cnt = 0;
        foreach ($arr_values as $value) {
            $cnt++; if ($cnt == 1) {
                if ($value == "") {
                    $i.= "NULL";
                } else {
                    $i.= "'".$value."'";
                }
            } else {
                if ($value == "") {
                    $i.= ", NULL";
                } else {
                    $i.= ", '".$value."'";
                }
            }
        }
        $i.= ")";

        if (!$this->query($i)) {
            // throw new Exception($i, 1);
            throw new Exception("Ha ocurrido un error al insertar", 1);
        }

        $s = "SELECT * FROM ".$table." WHERE id='".$this->getMaxValue("id", $table, "")."'";
        $result = $this->fetchSQL($s);

        return $result;
    }

    /**
     * Metodo para realizar actualizaciones
     * @param  String $table     Nombre de la tabla donde se va a Editar
     * @param  Array $arr_fields Nombre de los campos que se van a editar
     * @param  Array $arr_values Valores de los campos
     * @param  String $where     Condiciones de los valores a editar
     * @return Array             Arreglo con toda la informacion del recien editada
     */
    function updateAction($table, $arr_fields, $arr_values, $where = "")
    {
        $size_arr = sizeof($arr_fields);
        $cnt = 0;
        $w = " LIMIT 0,1";
        $u = "UPDATE ".$table." SET ";
        for ($i=0; $i < $size_arr; $i++) {
            if ($i == 0) {
                if ($arr_values[$i] == "") {
                    $u.= $arr_fields[$i]." = NULL";
                } else {
                    $u.= $arr_fields[$i]." = '".$arr_values[$i]."'";
                }
            } else {
                if ($arr_values[$i] == "") {
                    $u.= ", ".$arr_fields[$i]." = NULL";
                } else {
                    $u.= ", ".$arr_fields[$i]." = '".$arr_values[$i]."'";
                }

            }
        }
        if ($where != "") {
            $u.= " WHERE ".$where;
            $w = " WHERE ".$where." LIMIT 0, 1";
        }

        if (!$this->query($u)) {
            // throw new Exception("Se ha producido un error al actualizar", 1);
            throw new Exception($u, 1);
        }

        $s = "SELECT * FROM ".$table.$w;
        $result = $this->fetchSQL($s);

        return $result;
    }

    /**
     * Método para eliminar entradas en una tabla
     * @param  String $table Nombre de la tabla
     * @param  String $where Condicion para el eliminado (Opcional)
     * @return Boolean
     */
    function deleteAction($table, $where = "")
    {
        $d = "DELETE FROM ".$table;
        if ($where != "") { $d.= " WHERE ".$where; }
        if ($this->query($d)) {
            $response = true;
        } else {
            // throw new Exception("Se ha producido un error al eliminar", 1);
            throw new Exception($d, 1);
        }

        return $response;
    }

    /**
     * Verifica si existe un registro en la bd
     * @param  string $where   Condición
     * @param  string $table   Tabla del registro
     * @param  string $message Mensaje de la excepción
     * @return void
     */
    public function existRecord($where, $table, $message)
    {
        $cnt_val = $this->getCount($table, $where);
        if ($cnt_val == 0) {
            throw new Exception($message, 1);
        }

        return true;
    }

    /**
     * Verifica si un registro es duplicado
     * @param string $where   Condición
     * @param string $table   Tabla del registro
     * @param string $message Mensaje de la excepción
     * @return boolean
     */
    public function duplicatedRecord($where, $table, $message)
    {
        $cnt_val = $this->getCount($table, $where);

        if ($cnt_val == 1) {
            throw new Exception($message, 1);
        }

        return true;
    }
}

?>
