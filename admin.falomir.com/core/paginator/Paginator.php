<?php

/**
 * Clase para paginador
 */
class Paginator
{
    public $db;

    public $arr;

    function __construct($db, array $arr)
    {
        $this->db = $db;
        $this->columns = $arr['columns'];
        $this->table = $arr['table'];
        $this->where = $arr['where'];
        $this->results_page = $arr['results_pages'];
        $this->page = $arr['actually_page'];
    }

    /**
     * Retorna un arreglo con los resultados por página
     * @param  string $sort Forma de ordenar los resultados
     * @return array
     */
    public function getResults($sort = "")
    {
        // Se obtiene el número total de resultados
        $total = $this->getTotal();

        // Se verifica la página actual
        $act_page = $this->page;
        if ($act_page > ceil($total / $this->results_page)) {
            $act_page = 1;
        } elseif ($act_page == 0){
            $act_page = 1;
        }

        // Se obtiene el total de páginas
        $page_total = ceil($total / $this->results_page);

        // Resultados por pagina
        $limit  = "LIMIT ".($act_page - 1) * $this->results_page.", ".$this->results_page;

        // Se arma y ejecuta el query final
        $query = "SELECT ".$this->columns." FROM ".$this->table." WHERE ".$this->where." ".$sort." ".$limit;
        $arr_result = $this->db->fetchSQL($query);

        return $arr_result;
    }

    public function getPaginator()
    {
        $total = $this->getTotal();
        $act_page = $this->page;
        if ($act_page > ceil($total / $this->results_page)) { $act_page = 1; } elseif ($act_page == 0){ $act_page = 1;}
        $page_total = ceil($total / $this->results_page);

        if ($act_page > 2){
            $pager_it_lo = $act_page - 2;

            if (($act_page + 2) >$pager_tot) {
                $pager_it_hi = $pager_tot;
            } else {
                $pager_it_hi = $act_page + 2;
            }
        } else {
            $pager_it_lo = 1;

            if($pager_tot > 5){
                $pager_it_hi = 5;
            } else {
                $pager_it_hi = $pager_tot;
            }
        }
    }

    public function getHtmlPaginator()
    {

    }

    /**
     * Total de entradas que tiene la tabla
     * @return integer
     */
    public function getTotal()
    {
        return $this->db->getCount($this->table, $this->where);
    }
}


?>
