<?php

$controllers = array(
	'Index' => array('index', 'home', 'logOut'),
	'Error' => array('notFoundAction', 'serverErrorAction'),
	'Users' => array('list', 'add', 'edit'),
	'Products' => array('categoryList', 'categoryAdd', 'categoryEdit', 'list', 'add', 'edit'),
	'News' => array('categoryList', 'categoryAdd', 'categoryEdit', 'list', 'add', 'edit'),
	'Contact' => array('list', 'show'),
	'Carousel' => array('list', 'add', 'edit'),
	'Seo' => array('edit'),
	'Config' => array('config', 'addEmail', 'editEmail'),
);

?>
